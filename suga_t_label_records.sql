USE [SugaExtDB]
GO

/****** Object:  Table [dbo].[suga_t_label_records]    Script Date: 2020/5/7 15:07:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[suga_t_label_records](
	[po_no] [varchar](40) NOT NULL,
	[line_no] [varchar](4) NOT NULL,
	[material_no] [varchar](40) NULL,
	[material_desc] [nvarchar](255) NULL,
	[po_date] [date] NULL,
	[po_qty] [decimal](12, 2) NULL,
	[printed_qty] [decimal](12, 2) NULL,
	[po_unit] [varchar](10) NULL,
	[mfg_no] [varchar](40) NULL,
	[mfg_name] [nvarchar](255) NULL,
	[material_type] [varchar](4) NULL,
	[normt] [varchar](255) NULL,
	[maabc] [varchar](255) NULL,
	[prod_no] [varchar](40) NULL,
	[prod_date] [date] NULL,
	[spq_qty] [decimal](12, 2) NULL,
	[box_qty] [decimal](4, 2) NULL,
	[create_time] [datetime2](0) NULL,
	[expire_date] [date] NULL,
	[store_position] [varchar](10) NULL,
	[status] [varchar](2) NULL,
	[create_by] [varchar](50) NULL,
	[update_time] [datetime2](0) NULL,
	[vendor_code] [varchar](12) NOT NULL,
	[werks] [varchar](4) NULL,
	[werks_name] [nvarchar](255) NULL,
	[mfg] [varchar](40) NULL,
 CONSTRAINT [PK__suga_t_l__3213E83FA280D6AC] PRIMARY KEY CLUSTERED 
(
	[po_no] ASC,
	[line_no] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'采购订单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'po_no'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'行项目号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'line_no'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物料编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'material_no'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物料描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'material_desc'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'采购订单日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'po_date'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'采购订单数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'po_qty'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'已打印数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'printed_qty'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'计量单位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'po_unit'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'制造商编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'mfg_no'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'制造商名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'mfg_name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物料类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'material_type'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'关键物料，行业标准描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'normt'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ABC标识' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'maabc'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生产批次' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'prod_no'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生产日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'prod_date'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最小包装数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'spq_qty'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'每箱可装spq' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'box_qty'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'create_time'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'过期日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'expire_date'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'仓储位置' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'store_position'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'status'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'create_by'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'update_time'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商代码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'vendor_code'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'工厂' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'werks'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'工厂名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'werks_name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'供应商' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_records', @level2type=N'COLUMN',@level2name=N'mfg'
GO

