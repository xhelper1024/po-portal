<%@ CodePage=65001 %>
<% Option Explicit %>

<!--#include virtual="/b_util/user_common_include.asp"-->
<%
	On Error Resume Next
	If getDefaultRegion <> "0" Then
		Session("loginStaffId") = empty
	'20110113 fix the cookies name missing issue by Joe
		response.cookies(Application("gDefaultCookiesName")) =""
		'Application("gDefaultCookiesName") = ""
		response.cookies(Application("gDefaultCookiesName")).expires = DATE - 1
		Response.Redirect "/index.asp"

	End If
	
	dim loginStaffId
	loginStaffId=request.cookies(Application("gDefaultCookiesName"))
	Session("loginStaffId") = empty

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'Set RecordSet
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	Dim objConn 'Used for the Connection object
	Dim objRst 'Used for the Recordset object
	Dim sqlStr
	set objConn = getDbConn(0)
	Set objRst = Server.CreateObject("ADODB.Recordset")

	sqlStr = "SELECT * FROM Staff WHERE Status = 'Y' AND StaffId = '" & loginStaffId & "'"


	objRst.Open sqlStr, objConn, 3, 3
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'End Set RecordSet	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	
	If not objRst.EOF then
		Response.Write "OK"
		Dim StaffId
		StaffId = trim(objRst("StaffId"))
		Session("loginFail") = empty
		Session("loginStaffId") = StaffId
		Session("Username")=trim(objRst("StaffName"))
		Session("StaffLevel")=trim(objRst("StaffLevel"))
		Session("Password")=trim(objRst("Password"))
		Session("NewsPermiss") = trim(objRst("NewsPermiss"))
		Session("LoginName")=trim(objRst("Email"))
		Session("ForumPermiss") = trim(objRst("ForumPermiss"))
		expiredate=dateadd("d",Application("gDefaultCookiesExpireDay"),date)
		Response.Cookies(Application("gDefaultCookiesName"))=StaffId 
		Response.Cookies(Application("gDefaultCookiesName")).expires=DATE + Application("gDefaultCookiesExpireDay")
		objRst.Close
		'Do update login time
		sqlStr = " UPDATE Staff SET LastLogin = getdate(), Online='Y' WHERE StaffId = " & StaffId
		objConn.Execute(sqlStr)		
		sqlStr = " INSERT INTO StaffLog (StaffId, LoginTime, IPAddress ) VALUES ( " & StaffId & ", getdate(), '" & Request.ServerVariables("REMOTE_ADDR") & "')"
		objConn.Execute(sqlStr)
		' KH add
		objRst.close
		set objRst = Nothing
		Set objConn = Nothing

		Response.Redirect "/homepage.asp"
	Else
		Response.Write "NOT OK"
		Session("loginStaffId") = empty
		Session("loginFail") = 1
		response.cookies(Application("gDefaultCookiesName")).expires = DATE - 1
		' KH add 
                objRst.close 
		'Response.Redirect "/index.asp"
	End If

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'Footer to reset all resources and error handling
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	objRst.close
	set objRst = Nothing
	Set objConn = Nothing

'Err Handling here
if Err.Number <> 0 then
	Response.write "<br>[" & Err.number & "]-["&Err.Description&"]<br>"
 	Err.Clear
end if
	
%>