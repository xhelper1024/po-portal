﻿<%
Dim exitRecTotal, exitDBQ, showSQL,vendorcode,staLevel
vendorcode=Session("Username")
staLevel=Session("StaffLevel")
' role = 0 is admin, role =1 is viewer
'连接数据库
Function getDbConn(role)
	Dim sHost
	Dim sDBName
	Dim sUsrType
	Dim sPwd

	sHost = Application("gsHost")
	sDBName = Application("gsDBName")
	
	If role = 0 Then
		sUsrType = Application("gsUsrTypeAdmin")
		sPwd = Application("gsAdminPwd")
	Else
		sUsrType = Application("gsUrsTypeViewer")
		sPwd = Application("gsViewerPwd")
	End If
	
	Dim sConnStr	
	sConnStr = "Provider=sqloledb;Data Source=" & sHost & _
		";Initial Catalog=" & sDBName &";User Id=" & sUsrType & ";Password=" & sPwd
		
	Dim dbConn
	Set dbConn = Server.CreateObject("ADODB.Connection")
	
	dbConn.Open sConnStr
	
	Set getDbConn = dbConn
End Function
'得到查询记录行总数
Function dbGetRecTotal(conn,tableName, aColumn, whereClause) 
	Dim rs, sqlStr
	sqlStr = "select count(" & aColumn & ") as total from suga_t_maintenance_po" '& tableName
	If trim(whereClause) <> "" Then
		sqlStr = sqlStr & " where " & whereClause
		if vendorcode<>"" and session("staffLevel")=3 then
		sqlStr=sqlStr & " and vendor='" & vendorcode & "'"
		end if
	else
	    if vendorcode<>"" and session("staffLevel")=3 then
		sqlStr=sqlStr & " where vendor='" & vendorcode & "' "	
	    end if
	end if
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.Open sqlStr, conn, 3, 3
	rs.MoveFirst
	dbGetRecTotal = Int(rs.Fields("total").value)
	rs.close
end Function
'进行查询
Function dbQuery(conn, tableName, columnName, whereClause ,orderColumnName, orderType, top)

	Dim colNameLen, sqlStr, i, tableGroup,l_line_id
	
	colNameLen = Ubound(columnName)
	If top > 0 Then
		sqlStr = "select top " & top & " "
	else
		sqlStr = "select "
	end if
	
	For i=0 To colNameLen
		sqlStr = sqlStr & columnName(i)
		If i <> colNameLen then
			sqlStr = sqlStr &", "
		end if
	Next
	tableGroup = ""
	If isarray(tableName) then
		for i = 0 to arraylength(tableName)-1
			tableGroup = tableGroup & tableName(i)
			if i < arraylength(tableName)-1 then
				tableGroup = tableGroup & ", "	
			end if
		next
	else
		tableGroup = tableName
	end if
	tableGroup="suga_t_maintenance_po"
	If trim(whereClause) <> "" Then   
		sqlStr = sqlStr & " from " & tableGroup & " where " & whereClause
		if vendorcode<>"" and session("staffLevel")=3 then
		sqlStr=sqlStr & " and vendor='" & vendorcode & "'" '根据供应商显示自己的PO
		end if
	else
	  sqlStr = sqlStr & " from " & tableGroup
	    if vendorcode<>"" and session("staffLevel")=3 then
		sqlStr=sqlStr & " where vendor='" & vendorcode & "'"	
	    end if
	end if
   sqlStr=sqlStr & " order by PO,ORIG_LINE,SPLIT_LINE,SHIP_NUM,DELIVERY_DATE"
	Dim rs
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.Open sqlStr, conn, 1, 1
	If not rs.eof Then 
	rs.MoveFirst
		Dim record(), rsArray(), j
	  
		ReDim record(Ubound(columnName))

		ReDim rsArray(rs.RecordCount-1)
		j = 0
		do while Not rs.eof
			For i=0 To colNameLen
				If isarray(tableName) then
						record(i) = rs.Fields(Mid(columnName(i), InStr(columnName(i), ".")+1, len(columnName(i)))).value
				else
				 if columnName(i)="SELECTED" then
				 l_line_id=rs.Fields("LINE_ID").value
				 end if
				 if Session("StaffLevel")=2 and columnName(i)="SELECTED" then
				  Dim rs1
				  sqlStr="select selected from  WhoSelected where line_id=" & rs.Fields("LINE_ID").value& " and whoselected="& Session("loginStaffId")
				  Set rs1 = Server.CreateObject("ADODB.Recordset")
	              rs1.Open sqlStr, conn, 3,3
				
	              If not rs1.eof Then
				  record(i)=rs1.Fields("SELECTED").value
				  else
				  record(i)=""
				  end if
				   rs1.close
                  set rs1 = nothing
				  else
				  record(i) = rs.Fields(columnName(i)).value
				  end if
				end if
			Next
			rsArray(j) = record
			j = j +1			
			rs.MoveNext
		loop	
	      rs.close
           set rs = nothing	
		dbQuery = rsArray
		Exit Function
	end if
	rs.close
   set rs = nothing
	dbQuery = null
	
End Function

'采购订单行分Lines
Function insertLine(l_line_id,delivery_date,qty,l_last,l_split_line,l_orig_line)
Dim rs,sqlStr,l_success,l_delidate,l_new_req_date,l_v_new_req_date
Dim l_issue_date,l_need_by_date,l_pro_date,l_frm_date,l_to_date,l_os_qty
    l_delidate=trim(delivery_date)
    if  l_delidate="" or isNull( l_delidate) then
	 l_delidate="NULL"
	 l_pro_date="NULL"
	else 
	 l_delidate="cast('" &l_delidate & "' as datetime)"
	end if
    sqlStr="select * from suga_t_maintenance_po where" & _
	" line_id="& l_line_id & ""
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.Open sqlStr, conn, 3, 3
	If not rs.eof Then
	l_os_qty=rs.Fields("UNIT_PRICE").VALUE*qty
	if  l_delidate<>"NULL" then
	 l_pro_date=l_delidate & " + " & rs.Fields("ADD_DAYS").VALUE
	end if
	if l_last="Y" then
	sqlStr="update suga_t_maintenance_po set ship_os_qty=" &qty& ",delivery_date="&l_delidate&","&_
	"last_update_by="&session("loginStaffId")&",promised_date="&l_pro_date&",last_update_date=getdate() where line_id=" & l_line_id
	else
	l_to_date=trim(rs.Fields("TO_DATE").VALUE)
	if  l_to_date="" or isNull(l_to_date) then
	 l_to_date="NULL"
	else 
	l_to_date="cast('" &l_to_date & "' as datetime)"
	end if
	l_frm_date=trim(rs.Fields("FROM_DATE").VALUE)
	if  l_frm_date="" or isNull(l_frm_date) then
	 l_frm_date="NULL"
	else 
	l_frm_date="cast('" &l_frm_date & "' as datetime)"
	end if
	
	
	
	l_need_by_date=trim(rs.Fields("NEED_BY_DATE").VALUE)
	if  l_need_by_date="" or isNull(l_need_by_date) then
	 l_need_by_date="NULL"
	else 
	 l_need_by_date="cast('" &l_need_by_date & "' as datetime)"
	end if
	
	l_issue_date=trim(rs.Fields("PO_ISSU_DATE").VALUE)
	if  l_issue_date="" or isNull(l_issue_date) then
	 l_issue_date="NULL"
	else 
	 l_issue_date="cast('" &l_issue_date & "' as datetime)"
	end if
	l_new_req_date=trim(rs.Fields("NEW_REQ_DATE").VALUE)
	if  l_new_req_date="" or isNull(l_new_req_date) then
	 l_new_req_date="NULL"
	else 
	 l_new_req_date="cast('" &l_new_req_date & "' as datetime)"
	end if
	l_v_new_req_date=trim(rs.Fields("VENDOR_NEW_REQ_DATE").VALUE)
    if  l_v_new_req_date="" or isNull(l_v_new_req_date) then
	 l_v_new_req_date="NULL"
	else 
	 l_v_new_req_date="cast('" &l_v_new_req_date & "' as datetime)"
	end if
	sqlStr="insert into suga_t_maintenance_po values('"& rs.Fields("BUYER").VALUE &"',"&l_issue_date&",'"&rs.Fields("PO")&"'," & _
	""&rs.Fields("ORG_ID").VALUE&",null,null,'"&rs.Fields("ITEM").VALUE&"','"&rs.Fields("MFG_PART_NAME").VALUE&"',"&_
	"'"&rs.Fields("MFG_PART_NUM").VALUE&"',"&qty&","&qty&",'"&rs.Fields("UOM").VALUE&"',"&rs.Fields("LEAD_TIME").VALUE&"," &_
	""&l_need_by_date&","&l_delidate&","&l_pro_date&","&l_frm_date&","&l_to_date&","&_
	"N'"&rs.Fields("REMARK").VALUE&"','"&rs.Fields("CURRENCY").VALUE&"',"&rs.Fields("UNIT_PRICE").VALUE&","&l_os_qty&",'"&rs.Fields("MOQ").VALUE&"'," & _
	"'"&rs.Fields("SPQ").VALUE&"',N'"&rs.Fields("VENDOR").VALUE&"',N'"&rs.Fields("VENDOR_NAME").VALUE&"','"&rs.Fields("ITEM_DESC").VALUE&"',"& _
	"'"&rs.Fields("PAYMENT_TERMS").VALUE&"','"&rs.Fields("DELIVERY_TERMS").VALUE&"',"&rs.Fields("ADD_DAYS").VALUE&","&rs.Fields("VENDOR_STOCK").VALUE&","&_
	""& l_new_req_date&","& l_v_new_req_date&",N'"&rs.Fields("PURCHASE_COMM").VALUE&"','N',"&_
	"'N','N',000,'N',"&session("loginStaffId")&",getdate(),'"&l_split_line&"',"&l_orig_line&")"
	end if
	else
	insertLine=0
    exit function
	end if
conn.execute sqlStr,l_success
if l_success=1 then
	insertLine=1 'true
	else
	insertLine=0 'true
	end if
End Function

'更新采购订单行
Function updatePOBuyer(l_line_id,prom_date,new_req_date,v_comment)
Dim rs,sqlStr,l_success,l_prom_date,l_new_req_date,l_comment
    l_prom_date=trim(prom_date)
    l_new_req_date=trim(new_req_date)
	l_comment=trim(v_comment)
	if l_prom_date="" or isNull(l_prom_date) then
	l_prom_date="NULL"
	else
	l_prom_date="cast('" & l_prom_date & "' as datetime)"
	end if
	if l_new_req_date= "" or isNull(l_new_req_date) then
	l_new_req_date="NULL"
	else
	l_new_req_date="cast('" & l_new_req_date & "' as datetime)"
	end if
	if l_comment=""  or isNull(l_comment) then
	l_comment=""
	else
	l_comment=l_comment
	end if
    sqlStr="select line_id  from suga_t_maintenance_po where line_id="& l_line_id & ""
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.Open sqlStr, conn, 3, 3
	If not rs.eof Then 
	sqlStr="update suga_t_maintenance_po set PROMISED_DATE="& l_prom_date &",PURCHASE_COMM=N'" &l_comment& "'" &_
	",last_update_by="&session("loginStaffId")&",last_update_date=getdate() where  line_id="& l_line_id & ""
   else
   updatePO="0" 'true
   exit function
   end if
conn.execute sqlStr,l_success
    if l_success=1 then
	updatePO="1" 'true
	else
	updatePO="0" 'true
	end if
End Function

'同步新需求时间给供应商
Function syncNewDate(l_line_id)
Dim rs,sqlStr,l_success,l_new_req_date
    sqlStr="select new_req_date  from suga_t_maintenance_po where line_id="& l_line_id & ""
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.Open sqlStr, conn, 3, 3
	If not rs.eof Then 
	l_new_req_date=trim(rs.Fields("NEW_REQ_DATE").VALUE)
	if l_new_req_date= "" or isNull(l_new_req_date) then
	l_new_req_date="NULL"
	else
	l_new_req_date="cast('" & l_new_req_date & "' as datetime)"
	end if
	sqlStr="update suga_t_maintenance_po set vendor_new_req_date="& l_new_req_date &"" &_
	",last_update_by="&session("loginStaffId")&",last_update_date=getdate() where  line_id="& l_line_id & ""
   else
   syncNewDate="0" 'true
   exit function
   end if
conn.execute sqlStr,l_success
    if l_success=1 then
	syncNewDate="1" 'true
	else
	syncNewDate="0" 'true
	end if
End Function

'供应商更新相关的数据
Function updateVNQD(l_line_id,deli_date,remark)
Dim rs,sqlStr,l_success,l_deli_date,l_remark,l_pro_date,l_reply
    l_deli_date=trim(deli_date)
	l_remark=trim(remark)
	if (l_remark=""  or isNull(l_remark)) and (l_deli_date="" or isNull(l_deli_date)) then
	l_reply="N"
	else
	l_reply="Y"
	end if
    sqlStr="select add_days  from suga_t_maintenance_po where line_id="& l_line_id & ""
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.Open sqlStr, conn, 3, 3
	If not rs.eof Then 
	if l_deli_date="" or isNull(l_deli_date) then
	l_deli_date="NULL"
	l_pro_date="NULL"
	else
	l_pro_date="cast('" & l_deli_date & "' as datetime) + " & rs.fields("ADD_DAYS").value
	l_deli_date="cast('" & l_deli_date & "' as datetime)"
	end if
	sqlStr="update suga_t_maintenance_po set DELIVERY_DATE="& l_deli_date &",PROMISED_DATE=" &l_pro_date& ",REMARK=N'" &l_remark& "'" &_
	",last_update_by="&session("loginStaffId")&",last_update_date=getdate(),reply_flag='"&l_reply&"' where  line_id="& l_line_id & " and post_flag<>'Y'"
   else
   updateVNQD="0" 'true
   exit function
   end if
conn.execute sqlStr,l_success
    if l_success=1 then
	updateVNQD="1" 'true
	else
	updateVNQD="0" 'true
	end if
End Function

Function updatePO(l_line_id,deli_date,add_day,prom_date,new_req_date,l_stock,sync,v_comment)

Dim rs,sqlStr,l_success,l_deli_date,l_add_day,l_prom_date
Dim l_new_req_date,l_inv_qty,l_sync,l_sync_temp,l_comment,l_reply
    l_deli_date=trim(deli_date)
    l_add_day=trim(add_day)
    l_prom_date=trim(prom_date)
    l_new_req_date=trim(new_req_date)
    l_inv_qty=trim(l_stock)
	l_sync=trim(sync)
	l_comment=trim(v_comment)
	l_reply="N"
	if l_deli_date=""  or isNull(l_deli_date)then
	l_deli_date="NULL"
	else
	l_reply="Y"
	l_deli_date="cast('" &l_deli_date & "' as datetime)"
	end if
	if l_add_day="" or isNull(l_add_day) then
	l_add_day="NULL"
	else
	l_add_day=Cint(l_add_day)
	end if
	if l_prom_date="" or isNull(l_prom_date) then
	l_prom_date="NULL"
	else
	l_prom_date="cast('" & l_prom_date & "' as datetime)"
	end if
	if l_new_req_date= "" or isNull(l_new_req_date) then
	l_new_req_date="NULL"
	else
	l_new_req_date="cast('" & l_new_req_date & "' as datetime)"
	end if
	if l_inv_qty=""  or isNull(l_inv_qty) then
	l_inv_qty="NULL"
	else
	l_inv_qty=l_inv_qty
	end if
	if l_comment=""  or isNull(l_comment) then
	l_comment=""
	else
	l_comment=l_comment
	end if
	
    sqlStr="select promised_date, new_req_date, add_days, delivery_date, vendor_stock  from suga_t_maintenance_po where" & _
	" line_id="& l_line_id & ""
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.Open sqlStr, conn, 3, 3
	If not rs.eof Then 
	if l_sync="Y" then
	sqlStr="update suga_t_maintenance_po set DELIVERY_DATE=" & l_deli_date &",ADD_DAYS="& l_add_day &",VENDOR_NEW_REQ_DATE="&l_new_req_date&"," &_
	"PROMISED_DATE="& l_prom_date &",NEW_REQ_DATE=" &l_new_req_date& ",VENDOR_STOCK=" &l_inv_qty& ",PURCHASE_COMM=N'" &l_comment& "',REPLY_FLAG='" &l_reply& "'" &_
	",last_update_by="&session("loginStaffId")&",last_update_date=getdate() where  line_id="& line_id & ""
	else
	 if staLevel=2 then
	sqlStr="update suga_t_maintenance_po set DELIVERY_DATE=" & l_deli_date &",ADD_DAYS="& l_add_day &"," &_
	"PROMISED_DATE="& l_prom_date &",NEW_REQ_DATE=" &l_new_req_date& ",VENDOR_STOCK=" &l_inv_qty& ",PURCHASE_COMM=N'" &l_comment& "' ,REPLY_FLAG='" &l_reply& "'" &_
	",last_update_by="&session("loginStaffId")&",last_update_date=getdate() where  line_id="& line_id & ""
	else
	sqlStr="update suga_t_maintenance_po set DELIVERY_DATE=" & l_deli_date &",ADD_DAYS="& l_add_day &"," &_
	"PROMISED_DATE="& l_prom_date &",NEW_REQ_DATE=" &l_new_req_date& ",VENDOR_STOCK=" &l_inv_qty& " ,REPLY_FLAG='" &l_reply& "'"&_ 
	",last_update_by="&session("loginStaffId")&",last_update_date=getdate() where line_id="& line_id & ""
	end if
	end if
  else
  updatePO=0 'true
   exit function
End If

conn.execute sqlStr,l_success
if l_success=1 then
	updatePO=1 'true
	else
	updatePO=0 'true
	end if
End Function
'对采购订单行进行选中
Function updateSelected(line_no,selected,who)
Dim rs,sqlStr,whoselect,whoArry,buyer,l_success,MFGNo,PONo,MFGName,replysql
Dim frmItem,needByDate, whereClause,sqlStrFirst,vendor,promisedDate,rtv,reply
    buyer=Session("buyer")
	PONo=Session("PONo")
	vendor=Session("vendor")
    frmItem=Session("frmItem")
    MFGNo=Session("MFGNo")
	MFGName=Session("MFGName")
	needByDate=Session("needByDate")
	promisedDate=Session("promisedDate")
	rtv=Session("rtv")
	reply=Session("reply")
	 whereClause=""
	 sqlStrFirst=""
	if trim(buyer) <> "" then
	    If whereClause <> "" Then
		whereClause = " BUYER='" & buyer& "'"
	    else
			whereClause = " where BUYER='" & buyer& "'"
	 end if
	end if
	if trim(vendor) <>"" then
		If whereClause <> "" Then
			whereClause = whereClause & " and  vendor='" & vendor & "'"
		else
			whereClause = " where vendor='" & vendor & "'"
		end if
	end if
	if trim(PONo) <>"" then
		If whereClause <> "" Then
			whereClause = whereClause & " and  PO='" & PONo & "'"
		else
			whereClause = " where PO='" & PONo & "'"
		end if
	end if
	
	if trim(frmItem) <>"" then
		If whereClause <> "" Then
			whereClause = whereClause & " and  ITEM like '" & frmItem & "'"
		else
			whereClause = " where ITEM like '" & frmItem & "'"
		end if
	end if
		
	if trim(MFGNo) <>"" then
		If whereClause <> "" Then
			whereClause = whereClause & " and MFG_PART_NUM ='" & MFGNo & "'" 
		else
			whereClause = " where MFG_PART_NUM='" & MFGNo & "'" 
		end if
	end if
	if trim(MFGName) <>"" then
		If whereClause <> "" Then
			whereClause = whereClause & " and MFG_PART_NAME ='" & MFGName & "'" 
		else
			whereClause = " where MFG_PART_NAME='" & MFGName & "'" 
		end if
	end if
	
    if trim(needByDate) <>"" then
		If whereClause <> "" Then '" and convert(varchar(100), " & columnName(10) & ",101)='" & formatDate( promisedDate,1) & "'"
			whereClause = whereClause & " and convert(varchar(100), NEED_BY_DATE,101)='" & formatDate(needByDate,1)& "'"
		else
			whereClause =  " where convert(varchar(100), NEED_BY_DATE,101)='" & formatDate(needByDate,1) & "'" 
		end if
	end if
	 
     if trim(promisedDate) <>"" then
		If whereClause <> "" Then '" and convert(varchar(100), " & columnName(10) & ",101)='" & formatDate( promisedDate,1) & "'"
			whereClause = whereClause & " and convert(varchar(100), PROMISED_DATE,101)='" & formatDate(promisedDate,1)& "'"
		else
			whereClause =  " where convert(varchar(100),PROMISED_DATE,101)='" & formatDate(promisedDate,1) & "'" 
		end if
	end if'20140519
	
	
		If whereClause <> "" Then
			whereClause = whereClause & " and post_flag ='N'" 
		else
			whereClause = " where post_flag='N'" 
		end if
		
	if trim(rtv) <>"" then
		If whereClause <> "" Then
			whereClause = whereClause & " and rtv_flag ='" & rtv & "'" 
		else
			whereClause = " where rtv_flag='" & rtv & "'" 
		end if
	end if
	if trim(reply) <>"" then
		If whereClause <> "" Then
			whereClause = whereClause & " and reply_flag ='" & reply & "'" 
		else
			whereClause = " where reply_flag='" & reply & "'" 
		end if
	end if
	
    if line_no="a" then
	if whereClause<>"" then
	replysql="select top 1 line_id from suga_t_maintenance_po " & whereClause &" and reply_flag='N'"
	else
	replysql="select top 1 line_id from suga_t_maintenance_po where reply_flag='N'"
	end if
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.Open replysql, conn, 1, 1
	If not rs.eof Then 
	updateSelected=2
	exit Function
	end if
	sqlStrFirst="delete from WhoSelected where whoselected=" & who
    sqlStr="insert into WhoSelected select 'Y'," & who &",line_id from suga_t_maintenance_po " & whereClause
	Set rs = Server.CreateObject("ADODB.Recordset")
	conn.execute sqlStrFirst,l_success
	conn.execute sqlStr,l_success
	elseif line_no="n" then
	if trim(whereClause)<>"" then
	sqlStr="SELECT line_id FROM SUGA_V_PO_SUMMARY " & whereClause & " and  whoselected=" & who
	else
	sqlStr="delete from whoselected where whoselected=" & who  	
	conn.execute sqlStr,l_success
	updateSelected=l_succes
	exit Function
	end if
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.Open sqlStr, conn, 1, 1
	If not rs.eof Then 
	    rs.MoveFirst
		Dim  rsArray(),i
		ReDim rsArray(rs.RecordCount-1)
		i = 0
		do while Not rs.eof
				 rsArray(i) = rs.Fields("LINE_ID").value
		    i=i+1
			rs.MoveNext
		loop	
	      rs.close
          set rs = nothing
	end if
	Dim where_line_id
	 where_line_id=""
     for i=0 to ubound(rsArray)
	 where_line_id=rsArray(i)
    sqlStr="delete from whoselected where whoselected=" & who &"  and line_id=" & where_line_id & "" 
	conn.execute sqlStr,l_success
	next
   else
	 if selected="Y" then
	  sqlStr="insert into WhoSelected values('Y', " & who &"," & line_no & ")"
	  conn.execute sqlStr,l_success
	  if l_success=0 then
	  sqlStr="delete from WhoSelected where whoselected=" & who & " and line_id=" & line_no & "" 
	  conn.execute sqlStr,l_success
	  end if
	 else
	   sqlStr="delete from WhoSelected where whoselected=" & who & " and line_id=" & line_no & " " 
	  conn.execute sqlStr,l_success
	 end if
  end if
    if l_succes=1  then 'true
	 updateSelected=1
    else
	 updateSelected=0 'false
	end if
	
End Function
Function sendtToERP(who)
    Dim buyer,rs,SqlStr,whereClause,MFGNo,PONo,MFGName,where_line_id,rtv,reply
	Dim frmItem,needByDate,vendor,promisedDate,whereClause1,i,rsArray(),header_id
	buyer=Session("buyer")
	PONo=Session("PONo")
	vendor=Session("vendor")
    frmItem=Session("frmItem")
    MFGNo=Session("MFGNo")
	MFGName=Session("MFGName")
	needByDate=Session("needByDate")
	promisedDate=Session("promisedDate")
	rtv=Session("rtv")
	reply=Session("reply")
	 whereClause="WHERE post_flag='N' AND WHOSELECTED=" & who
	 whereClause1="WHERE post_flag='N' AND WHOSELECTED=" & who
	if trim(buyer) <> "" then
			whereClause = whereClause & " and BUYER='" & buyer& "'"
	end if
	if trim(vendor) <>"" then
			whereClause = whereClause & " and vendor='" & vendor & "'"
	end if
	if trim(PONo) <>"" then
			whereClause = whereClause & " and PO='" & PONo & "'"
	end if
	if trim(frmItem) <>"" then
			whereClause = whereClause & " and  ITEM like '" & frmItem & "'"
	end if
		
	if trim(MFGNo) <>"" then
			whereClause = whereClause & " and MFG_PART_NUM ='" & MFGNo & "'" 
	end if
    if trim(MFGName) <>"" then
			whereClause = whereClause & " and MFG_PART_NAME ='" & MFGName & "'" 
	end if

    if trim(needByDate) <>"" then
			whereClause = whereClause & " and convert(varchar(100), NEED_BY_DATE,101)='" & formatDate(needByDate,1)& "'"
	end if
     if trim(promisedDate) <>"" then
			whereClause = whereClause & " and convert(varchar(100), PROMISED_DATE,101)='" & formatDate(promisedDate,1)& "'"
	end if
	if trim(rtv) <>"" then
			whereClause = whereClause & " and rtv_flag='" & rtv & "'"
	end if
	if trim(reply) <>"" then
			whereClause = whereClause & " and reply_flag='" & reply & "'"
	end if
	
	SqlStr="SELECT LINE_ID FROM SUGA_V_PO_SUMMARY " & whereClause
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.Open SqlStr, conn, 1, 1
	If rs.eof Then
	rs.close
    set rs = nothing
	sendtToERP="NR"
	EXIT FUNCTION
	else
	    rs.MoveFirst
		i=0
		ReDim rsArray(rs.RecordCount-1)
		do while Not rs.eof
				 rsArray(i) = rs.Fields("LINE_ID").value
			i=i+1
			rs.MoveNext
		loop	
	      rs.close
          set rs = nothing
	 end if
	 '20160804 ADD
	  Randomize
	 header_id=Int((9000 * Rnd) + 1)
	 'END
    SqlStr="INSERT OPENQUERY(PROD_ERPUAT,'SELECT * FROM apps.SUGA_T_PO_PROMISED_DATE_TEMP')SELECT PO, " & _
	  "ORG_ID,LINE,SHIP_NUM,ITEM,SHIP_OS_QTY,UNIT_PRICE,UOM,PROMISED_DATE,getdate(),WHOSELECTED,LINE_ID,NEED_BY_DATE," & _
	  " RESCHEDULED_DATE,DELIVERY_DATE," & header_id & " " & _
	 " FROM SUGA_V_PO_SUMMARY  " & whereClause
	conn.execute sqlStr,l_success
    if l_success>=1 then
	if trim(whereClause)=trim(whereClause1) then
	sqlStr="delete from whoselected where whoselected=" & who  	
	conn.execute sqlStr,l_success
	if l_success>=1 then
	for i=0 to ubound(rsArray)
	where_line_id=rsArray(i)
	sqlStr="update suga_t_maintenance_po set post_flag='Y',last_update_by="&session("loginStaffId")&",last_update_date=getdate()where  line_id=" & where_line_id & ""
	conn.execute sqlStr,l_success
	next
	sendtToERP="success"
	EXIT FUNCTION
	'conn.CommitTrans '如果没有conn错误，则执行事务提交   
	else
	'conn.RollbackTrans '否则回滚  
	sendtToERP="failure"
	EXIT FUNCTION
	end if
	else
	sqlStr="SELECT line_id FROM SUGA_V_PO_SUMMARY " & whereClause 
	end if
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.Open sqlStr, conn, 1, 1
	If not rs.eof Then 
	    rs.MoveFirst
		i=0
		ReDim rsArray(rs.RecordCount-1)
		do while Not rs.eof
				 rsArray(i) = rs.Fields("LINE_ID").value
				 i=i+1
			rs.MoveNext
		loop	
	      rs.close
          set rs = nothing
	else
	rs.close
    set rs = nothing
	sendtToERP=""
	exit function
	end if
     where_line_id=0
     for i=0 to ubound(rsArray)
	 where_line_id=rsArray(i)
    sqlStr="delete from whoselected where whoselected=" & who &" and  line_id=" & where_line_id & ""
	conn.execute sqlStr,l_success 
	sqlStr="update suga_t_maintenance_po set post_flag='Y',last_update_by="&session("loginStaffId")&",last_update_date=getdate() where  line_id=" & where_line_id & ""
	conn.execute sqlStr,l_success
	'conn.execute sqlStr2,l_success
	next
	if l_success>=1 then
	sendtToERP="success"
	'conn.CommitTrans '如果没有conn错误，则执行事务提交   
	else
	'conn.RollbackTrans '否则回滚  
	sendtToERP="failure"
	end if
	else
	'conn.RollbackTrans '否则回滚  
	sendtToERP="failure"
	end if
End Function
'check Password
Function checkPwd(uid,pwd)
    Dim sqlStr,rs,oldPwd,newPwd
   sqlStr = "SELECT top 1 Email as newpwd FROM Staff WHERE password=HashBytes('MD5','"&pwd&"') and StaffId =" & Session("loginStaffId")
    Set rs = Server.CreateObject("ADODB.Recordset")
    rs.Open sqlStr, conn, 3,3
	If not rs.eof Then
	checkPwd="success"
	Else
	'conn.close
	checkPwd="failure"
	End If
	rs.close
	Set rs=nothing
End Function
'update Password
'sql server 2008
Function updatePwd(upwd)
    Dim sqlStr,rs,l_success
   sqlStr = "SELECT StaffId,Password,Email FROM Staff WHERE StaffId =" & Session("loginStaffId")
    Set rs = Server.CreateObject("ADODB.Recordset")
    rs.Open sqlStr, conn, 3,3
	If not rs.eof Then
	sqlStr="UPDATE Staff SET PASSWORD=HashBytes('MD5','"&upwd& "') WHERE StaffId =" & Session("loginStaffId")
	conn.execute sqlStr,l_success
	if l_success=1 then
    updatePwd=1
    else
    updatePwd=0
    end if
	rs.close
	Set rs=nothing
	'conn.close
	Else
	rs.close
	Set rs=nothing
	'conn.close
	updatePwd=0
	End If
End Function
'add user 
Function addUser(name_cn,name_en,pwd,tel,mobi,email,fax,level)
Dim sqlStr,l_success
  sqlStr="insert into staff(StaffNameChi,StaffName,Password,Tel, Mobile, Email,Fax, Status,StaffLevel,CrtDt,CompanyId,"  & _
  "DeptId,DistrictId,ModBy,ModDt,ForumPermiss) values (N'" &name_cn& "',N'"&name_en&"',HashBytes('MD5','"&pwd& "')," & _
  "'"&tel& "','"&mobi&"','" &email& "','" &fax& "','Y'," &level&",getDate(),0,7,1,84,getDate(),'Y')" 
  conn.execute sqlStr,l_success
  if l_success=1 then
  addUser=1
  else
  addUser=0
  end if
End Function
'delete user
Function deleteUser(userid)
Dim rs,sqlStr
sqlStr="select Staffid from staff where StaffId=" & userid 
Set rs = Server.CreateObject("ADODB.Recordset")
    rs.Open sqlStr, conn, 3,3
	If not rs.eof Then
	rs.delete
	rs.close
	Set rs=nothing
	'conn.close
	deleteUser="success"
	Else
	rs.close
	Set rs=nothing
	'conn.close
	deleteUser="failure"
	End If
End Function
'update user
Function updateUser(userid,name_cn,name_en,pwd,tel,mobi,l_email,fax,level) 
   Dim rs,sqlStr,l_success
   sqlStr="select Staffid from staff where StaffId=" & userid 
   Set rs = Server.CreateObject("ADODB.Recordset")
    rs.Open sqlStr, conn, 3,3
	If not rs.eof Then
	sqlStr="update staff set StaffNameChi=N'"&name_cn&"',StaffName=N'"&name_en&"',Password = HashBytes('MD5','"&pwd&"'),"& _
	"Tel='"&tel&"', Mobile='"&mobi&"', Email='"&email&"',Fax='"&fax&"',StaffLevel="&level&"  where StaffId=" & userid 
	conn.execute sqlStr,l_success
	rs.close
	Set rs=nothing
	if l_success=1 then
	updateUser=1
	else
	updateUser=0
	end if
	'conn.close
	Else
	rs.close
	Set rs=nothing
	'conn.close
	updateUser=0
	End If
End Function
Function checkUserExist(l_email)
Dim rs,sqlStr,l_success
   sqlStr="select Staffid from staff where email='"&l_email&"'"
   Set rs = Server.CreateObject("ADODB.Recordset")
    rs.Open sqlStr, conn, 3,3
	If not rs.eof Then
	checkUserExist="success"
	rs.close
	Set rs=nothing
	else
	checkUserExist="failure"
	rs.close
	Set rs=nothing
    end if
End Function
Function searchUser(userid)
  Dim rs,sqlStr
  sqlStr="select Staffid,StaffName, StaffNameChi,Password,Tel,Mobile,Email,Fax,StaffLevel from staff where StaffId=" & userid 
  Set rs = Server.CreateObject("ADODB.Recordset")
    rs.Open sqlStr, conn, 3,3
	 Dim columnName(8)
	 Dim record(), rsArray(),i, j
	If not rs.eof Then
        rs.MoveFirst
		columnName(0) = "Staffid"
	    columnName(1) = "StaffName"
	    columnName(2) = "StaffNameChi"
		columnName(3) = "Password"
		columnName(4) = "Tel"
	    columnName(5) = "Mobile"
	    columnName(6) = "Email"
		columnName(7) = "Fax"
		columnName(8) = "StaffLevel"
		ReDim record(Ubound(columnName))
		ReDim rsArray(rs.RecordCount-1)
		j = 0
		do while Not rs.eof
				For i=0 To Ubound(columnName)
				If isarray(tableName) then
						record(i) = rs.Fields(Mid(columnName(i), InStr(columnName(i), ".")+1, len(columnName(i)))).value
				else
						record(i) = rs.Fields(columnName(i)).value
				end if
			Next
			rsArray(j) = record
			j = j +1			
			rs.MoveNext
		loop	
	      rs.close
          set rs = nothing
	Else
	rs.close
	Set rs=nothing
	End If
	searchUser=rsArray
End Function

'转换时间 时间格式化 
Function formatDate(Byval t,Byval ftype) 
dim y, m, d, h, mi, s 
formatDate="" 
If IsDate(t)=False Then Exit Function 
y=cstr(year(t)) 
m=cstr(month(t)) 
If len(m)=1 Then m="0" & m 
d=cstr(day(t)) 
If len(d)=1 Then d="0" & d 
h = cstr(hour(t)) 
If len(h)=1 Then h="0" & h 
mi = cstr(minute(t)) 
If len(mi)=1 Then mi="0" & mi 
s = cstr(second(t)) 
If len(s)=1 Then s="0" & s 
select case cint(ftype) 
case 1 
' yyyy-mm-dd 
formatDate= m & "/" & d & "/" & y 
case 2 
' yy-mm-dd 
formatDate=right(y,2) & "-" & m & "-" & d 
case 3 
' mm-dd 
formatDate=m & "-" & d 
case 4 
' yyyy-mm-dd hh:mm:ss 
formatDate=y & "-" & m & "-" & d & " " & h & ":" & mi & ":" & s 
case 5 
' hh:mm:ss 
formatDate=h & ":" & mi & ":" & s 
case 6 
' yyyy年mm月dd日 
formatDate=y & "年" & m & "月" & d & "日" 
case 7 
' yyyymmdd 
formatDate=y & m & d 
case 8 
'yyyymmddhhmmss 
formatDate=y & m & d & h & mi & s 
end select 
End Function 
''''''''''''''''''''''''''''''''''''''''''''''''
'	sv : search value
'	svTrim :
'		0 : right trim
'		1 : left trim
'		2 : both trim
'	svOption :
'		0 :	add % in the front, ie. %sv
'		1 : add % behind , ie. sv%
'		2 : add % both side, ie %sv%
'	other number : do noting
'''''''''''''''''''''''''''''''''''''''''''''''
Function createBasicSearchCriteria(column, byval sv, operator, svNeedAddQuote, svTrim, svOption)
	If trim(sv) = "" or trim(column) = "" Then
		createBasicSearchCriteria = ""
		Exit Function
	end if
	
	Select Case svTrim
		Case 0
			sv = rtrim(sv)
		case 1
			sv = ltrim(sv)
		case 2
			sv = trim(sv)
	end select
	
	Select Case svOption
		Case 0
			sv = "%" & sv
		case 1
			sv = sv  & "%"
		case 2
			sv = "%" & sv  & "%"	
	end select
		
	If svNeedAddQuote Then			sv = "'" & sv & "'"		end if
	
	createBasicSearchCriteria = column & operator & sv

end function

''''''''''''''''''''''''''''''''''''''''''
' 	*sv can be string or array
'
'	createOption :
'		0 : compare case sensitive using like 	' not code yet
'		1 : compare case insensitive using like		
'		2 : compare case sensitive using =
'		3 : compare case insensitive using =
'		4 : compare every word in string individually (English only) case insensitive
'		5 : date between sv[0] sv[1],
''''''''''''''''''''''''''''''''''''''''''
Function createComplexSearchCriteria(columnArray, sv, createOption)
	If arrayLength(columnArray) = 0 Then
		createComplexSearchCriteria = ""
		Exit Function
	end if
	
	If	IsEmpty(columnArray) Then
		createComplexSearchCriteria = ""
		Exit Function
	end if
	
	If Not IsArray(sv) Then
		If Not IsEnglish(sv) Then
			select case createOption
				case 1 :
					createOption = 0
				case 3
					createOption = 2
			end select	
		End If
	end if
	
	Dim operator, returnSC, tempSC, column
	
	Select Case createOption
		case 0 :
			operator = " like "
			For each column in columnArray
				tempSC = createBasicSearchCriteria(column, sv, operator, 1, 2, 2)
				returnSC = concatSearchCriteria(returnSC, tempSC, "or") 	
			Next
		case 1 :
			operator = " like "
			For each column in columnArray
				tempSC = createBasicSearchCriteria(column, lcase(sv), operator, 1, 2, 2)
				returnSC = concatSearchCriteria(returnSC, tempSC, "or") 	
				tempSC = createBasicSearchCriteria(column, ucase(sv), operator, 1, 2, 2)
				returnSC = concatSearchCriteria(returnSC, tempSC, "or") 	
			Next
		case 5 :
			If arrayLength(sv) = 0 Then
				createComplexSearchCriteria = ""
				exit function
			end if
			For each column in columnArray
				tempSC = createBasicSearchCriteria(column, sv(0), ">=", 1, 2, -1)
				returnSC = concatSearchCriteria(returnSC, tempSC, "or") 	
				tempSC = createBasicSearchCriteria(column, sv(1), "<=", 1, 2, -1)
				returnSC = concatSearchCriteria(returnSC, tempSC, "and") 	
			Next
	End select
	
	If returnSC = "" Then
		createComplexSearchCriteria  = ""
	else
		createComplexSearchCriteria = "(" & returnSC & ")"
	end if

end function

Function concatSearchCriteria(scStr, sc, logic) 
	Dim newSC

	If scStr <> ""  and sc <> "" Then 
		newSC = scStr & " " & logic & " " & sc 
	else
		If scStr <> "" then
			newSC = scStr
		elseif sc <> "" then
			newSC = sc
		end if
	End if
	
	concatSearchCriteria = newSC
end function

Function getSafeSql(sqlStr)
    If isEmpty(sqlStr) then
		getSafeSql = ""
    else
		if trim(sqlStr) <> "" then
			getSafeSql = replace(sqlStr, "'", "''")
		else
			getSafeSql = sqlStr
		end if
	end if
End Function

Function getDateSql(dateStr)
    If isEmpty(dateStr) then
		getDateSql = ""
    else
		if trim(dateStr) <> "" then
			getDateSql = convertDatePattern(dateStr, "/", "-", 0, 2) 
		else
			getDateSql = dateStr
		end if
	end if
End Function
Private Function EncryptText(ByVal strEncryptionKey, ByVal strTextToEncrypt)
    ' Declare variables
    Dim outer, inner, Key, strTemp
    ' For each character in strEncryptionKey
    For outer = 1 To Len(strEncryptionKey)
        ' Get a character to use as our encryption 
        ' key in this iteration of the OUTER loop
        key = Asc(Mid(strEncryptionKey, outer, 1))
	
        ' For each character in strTextToEncrypt
        For inner = 1 To Len(strTextToEncrypt)
            ' Update our encrypted text
            strTemp = strTemp & Chr(Asc(Mid(strTextToEncrypt, inner, 1)) Xor key)
            ' Change our encryption key to mix things up in the INNER loop.
            key = (key + Len(strEncryptionKey)) Mod 256
        Next
        ' Update the strTextToEncrypt variable before 
        ' the next iteration of the OUTER loop
        strTextToEncrypt = strTemp
        ' Reset strTemp for the next iteration of the OUTER loop.
        strTemp = ""
    Next
    ' Assign the value of the encrypted text to the function name 
    ' so we can return the value to the caller
    EncryptText = strTextToEncrypt
End Function
%>
