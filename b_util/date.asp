﻿<%

' argument type is Date, return type
Function getFirstDateOfMonth(theDate) 
	if varType(theDate) <> 7 then
		getFirstDateOfMonth = theDate
		exit function
	end if
	
	getFirstDateOfMonth = DateAdd("D", -Day(theDate), theDate) + 1
end function

Function getLastDateOfMonth(theDate)
	getLastDateOfMonth = DateAdd("D", getMonthLastDay(theDate) - 1, getFirstDateOfMonth(theDate))
end function

Function getMonthLastDay(theDate)
	
	Dim m
	m = Month(theDate)
	
	If m = 2 Then
		If IsLeapYear(Year(theDate)) Then
			getMonthLastDay = 29
		else
			getMonthLastDay = 28
		end if
		
		exit function
	end if

	If IsBigMonth(m) Then
		getMonthLastDay = 31
	else
		getMonthLastDay = 30
	end if	
end function

Function IsLeapYear(y) 
	IsLeapYear = (y mod 4 = 0 and (y mod 100 <> 0 or y mod 400 = 0))

end Function

Function IsMonth(m) 
	If m > 0 and m < 13 Then
		IsMonth = true
	else
		IsMonth = false
	end if
end function 

Function IsBigMonth(m)
	IsBigMonth = ( m = 1 or m = 3 or m = 5 or m = 7 or m = 8 or m = 10 or m = 12)

end function

' Date in VBDate Subtype
' Date1 > Date2 return true
' equalTrue , if equal, return true
function compareDate(date1, date2, equalTrue)
	If vartype(date1) <> 7 or vartype(date2) <> 7 Then
		compareDate = false
		exit function
	end if
	
	If Year(date1) > Year(date2) Then
		compareDate = true
		exit function
	elseif Year(date1) < Year(date2) Then
		compareDate = false
		exit function
	end if
	
	If Month(date1) > Month(date2) Then
		compareDate = true
		exit function
	elseif Month(date1) < Month(date2) Then
		compareDate = false
		exit function
	end if
	
	If Day(date1) > Day(date2) Then
		compareDate = true
		exit function
	elseif Day(date1) < Day(date2) Then
		compareDate = false
		exit function
	end if
	
	If equalTrue Then
		compareDate = true
	else
		compareDate = false
	end if
	
end function

function formatChineseDate(theDate)
	if not isDate(theDate) Then
		formatChineseDate = ""
		exit function
	end if
	
	formatChineseDate = Year(theDate) & "年" & month(theDate) & "月" & Day(theDate) & "日"
end function

function formatChineseDate2(theDate)
	if not isDate(theDate) Then
		formatChineseDate2 = ""
		exit function
	end if
	
	formatChineseDate2 = Year(theDate) & "年" & month(theDate) & "月" & Day(theDate) & "日"
end function

function formatChineseDate3(theDate)
	if not isDate(theDate) Then
		formatChineseDate3 = ""
		exit function
	end if
	
	formatChineseDate3 = month(theDate) & "月" & Day(theDate) & "日"
end function

' inputPattern, outputPattern
'	0 : DD/MM/YYYY 		, where inputSep or outputSep = /
'	1 : MM/DD/YYYY		, where inputSep or outputSep = /
'	2 : DD/MMM/YYYY		, where inputSep or outputSep = / and MMM is Month name such Jan, Feb
' 	3 : DD/MMMM/YYYY	, where inputSep or outputSep = / and MMMM is Month name such January, February
function convertDatePattern(theDateStr, inputSep, outputSep, inputPattern, outputPattern) 
	if not isDate(theDateStr) then
		convertDatePattern = ""
		exit function
	end if
	
	dim m, d, y, splitDate
	
	splitDate = split(theDateStr, inputSep)
	
	If arrayLength(splitDate) <> 3 Then
		convertDatePattern = ""
		exit function
	end if
	
	select case inputPattern
		case 0, 2, 3:
			d = trim(splitDate(0))
			m = trim(splitDate(1))
			y = trim(splitDate(2))
		case 1:
			m = trim(splitDate(0))
			d = trim(splitDate(1))
			y = trim(splitDate(2))
	end select
	
	select case outputPattern
		case 0 : 
			convertDatePattern = d & outputSep & m & outputSep & y
		case 1 : 
			convertDatePattern = m & outputSep & d & outputSep & y
		case 2 :
			convertDatePattern = d & outputSep & monthname(int(m), true) & outputSep & y
		case 3 :
			convertDatePattern = d & outputSep & monthname(int(m), false) & outputSep & y
	end select
end function

' outputPattern
'	0 : DD/MM/YYYY 		, where sep = /
'	1 : MM/DD/YYYY		, where sep = /
'	2 : DD/MMM/YYYY		, where sep = / and MMM is Month name such Jan, Feb
' 	3 : DD/MMMM/YYYY	, where sep = / and MMMM is Month name such January, February
' theDate has to been date, String will not be processed
' formatDate will return String
function formatDate(theDate, sep, outputPattern)
	if varType(theDate) <> 7 then
		formatDate = ""
		exit function
	end if
	
	select case outputPattern
		case 0 : 
			formatDate = Day(theDate) & sep & month(theDate) & sep & Year(theDate)
		case 1 : 
			formatDate = month(theDate) & sep & Day(theDate) & sep & Year(theDate)
		case 2 :
			formatDate = Day(theDate) & sep & monthname(month(theDate), true) & sep & Year(theDate)
		case 3 :
			formatDate = Day(theDate) & sep & monthname(month(theDate), false) & sep & Year(theDate)
	end select
	
end function

function IsValidDate(theDateStr) 
	Dim regEx
  
	set regEx = New RegExp
	regEx.IgnoreCase = False

	regEx.Pattern = "([0-3][0-9]|[0-9])/([0-1][0-9]|[0-9])/[1-2][0-9]{3}"

	If Not regEx.Test(theDateStr) Then
		IsValidDate = false
		exit function
	end if
	
	Dim splitDate, d, m , y
		
	splitDate = split(theDateStr, "/")
	
	d = int(splitDate(0))
	m = int(splitDate(1))
	y = int(splitDate(2))
	
	If  not IsMonth(m) or _
		d > 31 or _
		Not IsLeapYear(y) and m = 2 and d > 28 or _ 
		IsLeapYear(y) and m = 2 and d > 29 or _
		Not IsBigMonth(m) and d > 30 Then
		IsValidDate = false
		exit function
	end if
	
	IsValidDate = true
end function



%>