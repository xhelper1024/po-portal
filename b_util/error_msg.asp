﻿<%
	ReDim commonErrMsg(75)
	
	commonErrMsg(0) = "输入的页数不正确。"
	commonErrMsg(2) = "输入的最近多少天不正确。"
	commonErrMsg(4) = "输入的日期不正确。"
	commonErrMsg(6) = "请输入作者。"
	commonErrMsg(8) = "请输入标题。"
	commonErrMsg(9) = "必须选择准许看到新闻的地区"
	commonErrMsg(10) = "请选择公司。"
	commonErrMsg(11) = "必须选择公司。"
	commonErrMsg(12) = "请选择地区。"
	commonErrMsg(13) = "必须选择地区。"
	commonErrMsg(14) = "请选择部门。"
	commonErrMsg(15) = "必须选择部门。"
	commonErrMsg(16) = "请输入投票选择。"
	commonErrMsg(17) = "所选择的地区、公司或部门组合不正確。"
	commonErrMsg(18) = "请正确地输入投票选择。"
	commonErrMsg(19) = "所选择的地区、公司或部门组合相同。"
	commonErrMsg(20) = "请输入标题。"
	commonErrMsg(21) = "公司不可以没有指定。"
	commonErrMsg(22) = "没有此留言。"
	commonErrMsg(24) = "没有此投票项目。"
	commonErrMsg(26) = "投票被拒绝，因你之前已经投过了。"
	commonErrMsg(28) = "系统发生错误，导致投票失败，请联络系统管理员。"
	commonErrMsg(30) = "相片转换失败：找不到事件文件夹："
	commonErrMsg(32) = "相片转换失败：没有上载相片。"
	commonErrMsg(34) = "没有此事件。"
	commonErrMsg(36) = "抱歉！相片还未准备好。"
	commonErrMsg(38) = "没有此新闻。"
	commonErrMsg(40) = "找不到原文。"
	commonErrMsg(42) = "输入的电邮不正确。"
	commonErrMsg(44) = "找不到要回覆的留言。"
	
	commonErrMsg(45) = "必须输入主题分类名称"
	
	commonErrMsg(46) = "Do you confirm to delete ?"
	commonErrMsg(47) = "你是否確定刪除 ?"
	commonErrMsg(48) = "你是否确定删除 ?"
	
	commonErrMsg(49) = "必须输入电邮"
	commonErrMsg(50) = "输入的管理员名称或密码不正确。"
	commonErrMsg(51) = "必须输入管理员登入名称。"
	commonErrMsg(52) = "输入的管理员登入名称己被选用。"
	commonErrMsg(53) = "必须输入发表日期"
	commonErrMsg(54) = "发表日期不正确"
	commonErrMsg(55) = "必须输入标题"
	commonErrMsg(56) = "必须上载档案"
	commonErrMsg(57) = "必须输入内文"
	commonErrMsg(58) = "必须输入表格说明"
	commonErrMsg(59) = "必须输入英文名"
	commonErrMsg(60) = "电邮不正确"
	commonErrMsg(61) = "必须输入事件名称"
	commonErrMsg(62) = "必须输入新密码及重入密码"
	commonErrMsg(63) = "新密码与重入密码不符"
	commonErrMsg(64) = "必须输入开始及结束日期"
	commonErrMsg(65) = "开始日期不正确"
	commonErrMsg(66) = "结束日期不正确"
	commonErrMsg(67) = "结束日期必须在开始日期以后"
	commonErrMsg(68) = "必须输入事件名称"
	commonErrMsg(69) = "必须输入详情"
	commonErrMsg(70) = "必须输入发表时间"
	commonErrMsg(71) = "发表时间不正确"
	commonErrMsg(72) = "电邮已存在, 请另选其他电邮"
'	commonErrMsg(73) = "上传文件大小不得小于1K"
' The display error is not correct, it always show '小于1K', amend it to fix temporarily.
	commonErrMsg(73) = "上传文件大小不得大于200K"
	commonErrMsg(74) = "上传文件大小不得大于2M"
	commonErrMsg(75) = "请上传正确的文件"
	
function getDeleteAlertMsg()
	if instr(request.ServerVariables("HTTP_ACCEPT_LANGUAGE"), "zh") then
		if request.ServerVariables("HTTP_ACCEPT_LANGUAGE") = "zh-cn" then
			getDeleteAlertMsg = commonErrMsg(48)
		else
			getDeleteAlertMsg = commonErrMsg(47)
		end if
	else
		getDeleteAlertMsg = commonErrMsg(46)
	end if
end function

Sub showErrMsg
	if not isEmpty(Session("actionErrIdArr")) then
		if IsArray(Session("actionErrIdArr")) then
			if ubound(Session("actionErrIdArr")) > 0 and Not isEmpty(Session("actionErrIdArr")(0)) and Not isEmpty(Session("actionErrIdArr")(1)) then
				Dim i
				Response.Write "<font size=""2"" color=""#FF0000"">错误：<ul>"
				for i = 0 to ubound(Session("actionErrIdArr")) - 1
					if errIdArr(i) <> "" then
						Response.Write "<li>" & commonErrMsg(Session("actionErrIdArr")(i))
					end if
				Next
				Response.Write "</ul></font>"
				Session("actionErrIdArr") = empty
			elseif Not isEmpty(Session("actionErrIdArr")(0)) then
				Response.Write "<font size=""2"" color=""#FF0000"">错误：" & commonErrMsg(Session("actionErrIdArr")(0)) & "</font>"
				Session("actionErrIdArr") = empty
			end if
		else
			if Session("actionErrIdArr") <> "" then
				Response.Write "<font size=""2"" color=""#FF0000"">错误：" & commonErrMsg(Session("actionErrIdArr")) & "</font>"
				Session("actionErrIdArr") = empty
			end if
		end if
	end if
End Sub

Sub showLocalErrMsg(errIdArr)
	if not isEmpty(errIdArr) then
		if IsArray(errIdArr) then
			if ubound(errIdArr) > 0 and Not isEmpty(errIdArr(0)) and Not isEmpty(errIdArr(1)) then
				Dim i
				Response.Write "<font size=""2"" color=""#FF0000"">错误：<ul>"
				for i = 0 to ubound(errIdArr) - 1
					if errIdArr(i) <> "" then 
						'Response.Write "<li>" & errIdArr(i) & " - "  & commonErrMsg(errIdArr(i))
						Response.Write "<li>" & commonErrMsg(errIdArr(i))
					end if
				Next
				Response.Write "</ul></font>"
			elseif Not isEmpty(errIdArr(0)) then
				Response.Write "<font size=""2"" color=""#FF0000"">错误：" & commonErrMsg(errIdArr(0)) & "</font>"
			end if
		else 
			if IsInteger(errIdArr) then
				Response.Write "<font size=""2"" color=""#FF0000"">错误：" & commonErrMsg(errIdArr)
			elseif errIdArr <> "" then
				Response.Write "<font size=""2"" color=""#FF0000"">错误：" & commonErrMsg(errIdArr) & "</font>"
			end if
		end if
		errIdArr = empty
	end if
End Sub
%>