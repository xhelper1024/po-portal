<html>
<head>
<meta charset="utf-8"/>
</head>
<div id="qrcode"></div>
<script src="/script/qrcodejs-master/qrcode.js"></script>
<script type="text/javascript">
var jsonTxt = {
	"姓名": "xiehui",
	"age": 28,
	"timezone": new Date().getTime()
}
var text = <%=request.queryString("text")%>;
var qrcode = new QRCode(document.getElementById("qrcode"), {
	// text: JSON.stringify(jsonTxt),
	text: text,
	width: 128,
	height: 128,
	colorDark : "#000000",
	colorLight : "#ffffff",
	correctLevel : QRCode.CorrectLevel.H
});
</script>
</html>