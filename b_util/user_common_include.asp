<!DOCTYPE html>
<!--#include virtual="/b_util/meta_utf8.asp"-->
<!--#include virtual="/b_util/validation.asp"-->
<!--#include virtual="/b_util/error_msg.asp"-->
<!--#include virtual="/b_util/data.asp"-->
<!--#include virtual="/b_ui/display.asp"-->
<!--#include virtual="/b_util/date.asp"-->
<!--#include virtual="/b_util/dbutil.asp"-->
<!--#include virtual="/b_util/debug.asp"-->
<!--#include virtual="/b_util/com_reg_dept.asp"-->
<!--#include virtual="/b_util/network.asp"-->
<link href="/static/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="/static/bootstrap-table/bootstrap-table.css" rel="stylesheet" />
<link href="/static/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" />
<link href="/static/layui/css/layui.css" rel="stylesheet" />
<script src="/static/jquery/jquery.min.js"></script>
<script src="/static/layui/layui.all.js"></script>
<script src="/static/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
<script src="/static/bootstrap-table/bootstrap-table.js"></script>
<script src="/static/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/static/bootstrap-table/locale/bootstrap-table-zh-CN.js"></script>
<script src="/static/bootstrap-datepicker/locales/bootstrap-datepicker.zh-CN.min.js"></script>
<script src="/label/common/utils/bluebird.js"></script>
<script src="/label/common/utils/html2canvas.js"></script>
<script src="/label/common/utils/jsPDF.js"></script>
<script src="/label/common/base64.js"></script>
<style rel="stylesheet">
	.headingbar {
		background-image: linear-gradient(to top, #ffffff, #d1d8e8, #565e61);
		border: 0px;
	}

	.sidebar {
		position: fixed;
		top: 51px;
		bottom: 61px;
		z-index: 1000;
		display: block;
		padding: 0 0 0 20px;
		overflow-x: hidden;
		overflow-y: auto;
		background-color: #f5f5f5;
		border-right: 1px solid #eee;
	}

	.footbar {
		position: fixed;
		bottom: 0;
		right: 0;
		left: 0;
		z-index: 1001;
		display: block;
		padding: 20px;
		color: white;
		min-height: 50px;
		background-image: linear-gradient(to bottom, #ffffff, #d1d8e8, #565e61);
		border-top: 1px solid #fff;
	}

	.myiframe {
		width: 100%;
		height: 98%;
		border: 0px;
		padding: 0px;
	}

	.data-table {
		white-space: nowrap;
		overflow: hidden;
	}

	#print-page {
		width: 0rem;
		border: 0rem;
		height: 0rem;
	}

	.table tbody tr td {
		vertical-align: middle;
	}
</style>

<style media="print">
	#print-page {
		display: block;
		margin: 0rem;
		padding: 0rem;
	}
</style>
<script>
	(function() {
		// Private array of chars to use
		var CHARS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');

		Math.uuid = function(len, radix) {
			var chars = CHARS,
				uuid = [],
				i;
			radix = radix || chars.length;

			if (len) {
				// Compact form
				for (i = 0; i < len; i++)
					uuid[i] = chars[0 | Math.random() * radix];
			} else {
				// rfc4122, version 4 form
				var r;

				// rfc4122 requires these characters
				uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
				uuid[14] = '4';

				// Fill in random data.  At i==19 set the high bits of clock sequence as
				// per rfc4122, sec. 4.1.5
				for (i = 0; i < 36; i++) {
					if (!uuid[i]) {
						r = 0 | Math.random() * 16;
						uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
					}
				}
			}
			return uuid.join('');
		};
	})();

	function sayWord($obj, interval) {
		var words = $obj.text();
		$obj.html("");
		var index = 0;
		var time = setInterval(function() {
			if (index == words.length) {
				clearInterval(time);
				setTimeout(function() {
					sayWord($obj, interval);
				}, 2400);
			}
			$obj.append(words.charAt(index++));
		}, interval);
	}

	// 需要Base64见：http://www.webtoolkit.info/javascript-base64.html
	function make_base_auth(user, password) {
		var tok = user + ':' + password;
		var hash = Base64.encode(tok);
		// console.log(hash)
		return "Basic " + hash;
	}
	var baseAuth = make_base_auth("<%=Application("API_UNAME")%>", "<%=Application("API_PWORD")%>");

	/* var ua = navigator.userAgent.toLocaleLowerCase();
	if(ua.indexOf("chrome") == -1 || ua.indexOf("edge") != -1) {
		parent.layer.alert("请使用Chrome浏览器浏览本站点，右下角有下载链接");
	} */
	$(window).resize(function() {
		$(".data-table").bootstrapTable('resetView');
	});
	
	// 日期，在原有日期基础上，增加days天数，默认增加1天
	function addDate(date, days) {
		if (days == undefined || days == '') {
			days = 0;
		} else {
			days = parseInt(days);
		}
		var date = new Date(date);
		date.setDate(date.getDate() + days);
		var month = date.getMonth() + 1;
		var day = date.getDate();
		return date.getFullYear() + '-' + getDoubleDate(month) + '-' + getDoubleDate(day);
	}
	
	// 日期月份/天的显示，如果是1位数，则在前面加上'0'
	function getDoubleDate(arg) {
		if (arg == undefined || arg == '') {
			return '';
		}
		var re = arg + '';
		if (re.length < 2) {
			re = '0' + re;
		}
		return re;
	}
</script>

<%
	response.Charset = "utf-8"
%>
<%
function nl2br(str)
	nl2br=replace(str,VbCrLf, "<br >")
end function

function substr(body,start,ends)
	'start=Instr(body,start)
	content=mid(body,start+len(start))
	'ends=Instr(content,ends)
	substr=left(content,ends-1)
end function
Function IsBlank(ByRef Var) 
IsBlank = False 
Select Case True 
Case IsObject(Var) 
If Var Is Nothing Then IsBlank = True 
Case IsEmpty(Var), IsNull(Var) 
IsBlank = True 
Case IsArray(Var) 
If UBound(Var) = 0 Then IsBlank = True 
Case IsNumeric(Var) 
If (Var = 0) Then IsBlank = True 
Case Else 
If Trim(Var) = "" Then IsBlank = True 
End Select 
End Function 
'转换时间 时间格式化 
Function formatDate(Byval t,Byval ftype) 
dim y, m, d, h, mi, s 
formatDate="" 
If IsDate(t)=False Then Exit Function 
y=cstr(year(t)) 
m=cstr(month(t)) 
If len(m)=1 Then m="0" & m 
d=cstr(day(t)) 
If len(d)=1 Then d="0" & d 
h = cstr(hour(t)) 
If len(h)=1 Then h="0" & h 
mi = cstr(minute(t)) 
If len(mi)=1 Then mi="0" & mi 
s = cstr(second(t)) 
If len(s)=1 Then s="0" & s 
select case cint(ftype) 
case 1 
' yyyy-mm-dd 
formatDate= m & "/" & d & "/" & y 
case 2 
' yy-mm-dd 
formatDate=right(y,2) & "-" & m & "-" & d 
case 3 
' mm-dd 
formatDate=m & "-" & d 
case 4 
' yyyy-mm-dd hh:mm:ss 
formatDate=y & "-" & m & "-" & d & " " & h & ":" & mi & ":" & s 
case 5 
' hh:mm:ss 
formatDate=h & ":" & mi & ":" & s 
case 6 
' yyyy年mm月dd日 
formatDate=y & "年" & m & "月" & d & "日" 
case 7 
' yyyymmdd 
formatDate=y & m & d 
case 8 
'yyyymmddhhmmss 
formatDate=y & m & d & h & mi & s 
end select 
End Function 
%>
