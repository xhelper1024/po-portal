﻿<%

Function IsInteger(num)
	If not isNumeric(num) Then
		IsInteger = false
		Exit function
	end if
	
	If VarType(num) = VBString Then
		If Len(cstr(int(num))) = Len(num) then
			IsInteger = true
		else
			IsInteger = false
		end if 
		Exit function
	end if
	
	If int(num) = num Then
		IsInteger = true
	else
		IsInteger = false
	end if

End Function

Function IsEnglish(aStr)
	Dim ucaseStr, lcaseStr
	
	ucaseStr = ucase(aStr)
	lcaseStr = lcase(aStr)
	
	If ucaseStr = lcaseStr Then
		IsEnglish = false
	else
		dim i, aC
		For i = 1 to len(aStr) 
		aC = mid(cStr(aStr), i, 1)
		if ucase(aC) = lcase(aC) and not isNormalSymbol(aC) and not isnumeric(aC) then
			
			IsEnglish = false
			exit function
		end if
	next
		
	IsEnglish = true
	end if	
end function 

function isNormalSymbol(aChar)
		if isnumeric(aChar) or len(aChar) > 1 then
			isNormalSymbol = false
			exit function
		end if
		
		dim c
		
		for each c in Application("gArrNormalSymbol")
			if aChar = c then
				isNormalSymbol = true
				exit function
			end if
		next 
		
		isNormalSymbol = false
	end function

Function arrayLength(arr) 
	If Isnull(arr) or not IsArray(arr) or Isempty(arr)Then
		arrayLength = 0
		exit function
	end if
	
	arrayLength = Ubound(arr) + 1
end function

Function compareDate(date1, date2)
	compareDate = 0
end function


Function isValidEmail(myEmail)
  dim isValidE
  dim regEx
  
  isValidE = True
  set regEx = New RegExp
  
  regEx.IgnoreCase = False
  
  regEx.Pattern = "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
  isValidE = regEx.Test(myEmail)
  
  isValidEmail = isValidE
End Function

function isTheExtension(filename, extension)
	dim theFilenameExt
	
	theFilenameExt = Mid(filename, InStrRev(filename, ".")+1, Len(filename))
	
	if extension = theFilenameExt then
		isTheExtension = true
	else
		isTheExtension = false
	end if
end function

%>