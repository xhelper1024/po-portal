﻿<%	
	ReDim gArrCompanyName(6)
	ReDim gArrRegion(7)
	ReDim gArrDeptName(40)
	ReDim gArrIgnoreSearchCriteria(2)
	ReDim gArrEventType(13)
	ReDim gArrGalleryEventType(4)

	'''''''''''''''''''''''''''''''''''''''''''''''
	'
	'	Initialize the global variable once when application start
	'
	'''''''''''''''''''''''''''''''''''''''''''''''
	gArrCompanyName(0) = "信佳集团"
	gArrCompanyName(1) = "信佳电子"
	gArrCompanyName(2) = "信佳网络"
	gArrCompanyName(3) = "柏信实业"
	gArrCompanyName(4) = "雅科产品"
	gArrCompanyName(5) = "信佳数码"
	gArrCompanyName(6) = "精工/脑力"
		'gArrCompanyName(7) = "信美佳"
        'gArrCompanyName(8) = "创佳照明"

	
	gArrRegion(0) = "香港"
	gArrRegion(1) = "布吉"
	gArrRegion(2) = "西乡"
	gArrRegion(3) = "澳门"
	gArrRegion(4) = "深圳"
	gArrRegion(5) = "北京"
	gArrRegion(6) = "新加坡"
	gArrRegion(7) = "惠州"

	gArrDeptName(0) = "董事局"
	gArrDeptName(1) = "人力资源及行政部"
	gArrDeptName(2) = "主席办公室"
	gArrDeptName(3) = "财务部"
	gArrDeptName(4) = ""
	gArrDeptName(5) = ""
	gArrDeptName(6) = "集团事务部"
	gArrDeptName(7) = "市场部"
	gArrDeptName(8) = "资讯部"
	gArrDeptName(9) = "工程部"
	gArrDeptName(10) = "物流部 - 船务部"
	gArrDeptName(11) = "物流部 - 货仓部"
	gArrDeptName(12) = "物控部"
	gArrDeptName(13) = "报关部"
	gArrDeptName(14) = "生产部"
	gArrDeptName(15) = "物流部"
	gArrDeptName(16) = "采购部"
	gArrDeptName(17) = "总经理"
	gArrDeptName(18) = "开发部"
	gArrDeptName(19) = "生产及工业工程"
	gArrDeptName(20) = "品质控制部"
	gArrDeptName(21) = ""
	gArrDeptName(22) = "机械工程部"
	gArrDeptName(23) = "行政部"
	gArrDeptName(24) = "法律咨询部"
	gArrDeptName(25) = "市场推广支援部"
	gArrDeptName(26) = "供应商开发部"
	gArrDeptName(27) = ""
	gArrDeptName(28) = "工模部"
	gArrDeptName(29) = "系統檢討"
	'新增项目部于20110426
	gArrdeptName(30) = "项目部"
        '新增项目部于20110818
	gArrdeptName(31) = "企业社会责任委员会"	

	gArrIgnoreSearchCriteria(0) = "所有地区"
	gArrIgnoreSearchCriteria(1) = "所有公司"
	gArrIgnoreSearchCriteria(2) = "所有部门"
	
	gArrEventType(0) = "假期"
	gArrEventType(1) = "活动"
	gArrEventType(2) = "年假"
	gArrEventType(3) = "事假"
	gArrEventType(4) = "病假"
	gArrEventType(5) = "无薪假"
	gArrEventType(6) = "布吉厂"
	gArrEventType(7) = "西乡厂"
	gArrEventType(8) = "澳门办公室"
	gArrEventType(9) = "外出工作"
	gArrEventType(10) = "海外工干"
	gArrEventType(11) = "惠州厂"
        gArrEventType(12) = "香港办公室"
	gArrEventType(13) = "其他"
	
	gArrGalleryEventType(0) = "商务事件"
	gArrGalleryEventType(1) = "员工活动"
	gArrGalleryEventType(2) = "年度聚餐"
	gArrGalleryEventType(3) = "圣诞晚会"
	gArrGalleryEventType(4) = "公共事件"
	
	function getCompanyName(i) 
		i = cint(i)
		if i < 0  or i > ubound(gArrCompanyName) then
			getCompanyName = "没有指定"
			exit function
		end if
		getCompanyName = gArrCompanyName(i)
	end function
	
	function getDeptName(i) 
		i = cint(i)
		if i < 0  or i > ubound(gArrDeptName) then
			getDeptName = "没有指定"
			exit function
		end if
		getDeptName = gArrDeptName(i)
	end function
	
	function getRegionName(i) 
		i = cint(i)
		if i < 0  or i > ubound(gArrRegion) then
			getRegionName = "没有指定"
			exit function
		end if
		getRegionName = gArrRegion(i)
	end function
	
	function getDestination(Destination)
	
	if Destination = 1 then 
		getDestination = "回香港"
	elseif Destination = 2 then
		getDestination = "去布吉"
	elseif Destination = 3 then
		getDestination = "全部"
	else
		getDestination = "出错"
	end if
	
	end function

	function getBookTime(BookTime)
	
	if BookTime = 1 then 
		getBookTime = "08:30"
	elseif BookTime = 2 then
		getBookTime = "18:00"
	else
		getBookTime = "出错"
	end if
	
	end function
%>