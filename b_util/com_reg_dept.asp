<%


function isRegionSpecified(comId)
	isRegionSpecified = instr(gArrComLocDept(comId), ":") <> 0
end function


function getRegion(comId)
	dim regionArr
	
	if not isRegionSpecified(comId) then
		redim regionArr(0)
		regionArr(0) = Application("gRegionNotAssignedIndex") 
		getRegion = regionArr
		exit function
	end if
	
	dim i
	
	regionArr = split(gArrComLocDept(comId), "; ")

	for i=0 to arrayLength(regionArr) - 1
		regionArr(i) = cint(split(regionArr(i), ": ")(0))
	next
	
	getRegion = regionArr
end function

function getDept(comId, regionId)
	dim deptArr
	
	if not isRegionSpecified(comId) then
		redim deptArr(0)
		deptArr(0) = Application("gRegionNotAssignedIndex")  
		getDept = deptArr
		exit function
	end if
	
end function

function getRegionTotal(comId)
	getRegionTotal = arrayLength(split(gArrComLocDept(comId), "; ")) 
end function



%>