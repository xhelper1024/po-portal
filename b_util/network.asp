﻿<%

redim regionSubnet(7)

' Init the IP accordingly
regionSubnet(0) = "10.10.10;10.10.11;219.76.185.233;219.76.214.198;218.102.200.195;202.64.168.247;210.245.223.78;192.168.0.102"	'Hong Kong
regionSubnet(1) = "10.10.20"					'Buji
regionSubnet(2) = "218.17;218.18"				'XX
regionSubnet(3) = "192.168.0"					'Macau
regionSubnet(4) = "10.10.40"					'Shenzhen
regionSubnet(5) = "0.0.0.0"					'Beijing
regionSubnet(6) = "0.0.0.0"					'Singapore
regionSubnet(7) = "0.0.0.0"					'HiuZhou

function getDefaultRegion()
	Dim remoteAddr, subnet, i, j, addr, classbsubnet
	

	'20110114 not checking the subnet region, return 0 for all subnet
	getDefaultRegion = 0

	if false then
		remoteAddr = Request.ServerVariables("REMOTE_ADDR")
	
		subnet = left(remoteAddr, InStrRev(remoteAddr, ".") - 1 )
		
		classbsubnet = left(subnet, InStrRev(subnet, ".") - 1 )

		for i=0 to arraylength(regionSubnet)-1
		addr = split(regionSubnet(i), ";")
		for j=0 to arraylength(addr)-1
			if remoteAddr = addr(j) or subnet = addr(j) or classbsubnet = addr(j) then
				getDefaultRegion = i		
				exit function
			end if
		next
		next
	getDefaultRegion = -1
	end if
	
end function

Function getRegion() 
	if Session.Contents("remoteRegion") <> "" then
		getRegion = Session.Contents("remoteRegion")
		exit function
	end if

	getRegion = getDefaultRegion()
	
end function

function isLocal()
	if Session.Contents("remoteRegion") = "" then
		isLocal = true
	else
		isLocal = false
	end if
		
end function

%>