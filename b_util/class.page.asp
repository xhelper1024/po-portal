<%
' USAGE:
'	limit=20
'	if (page="") THEN page=0
'	startrow = page * limit

' page(current page, total number of rows, number of item per page, url)  ps: url, business_cat.php
' return array(previous, page list, next)
' eg. list($prev, $pagelist, $next)=$classpage->page($page, $num, $limit, "comp_cataloglist.php?");
	dim prev
	dim pnext
	dim pageno
	
	FUNCTION pagef(curr, inAllRow, limit, url)		
		dim prevPage, pageNum, pg, cnow, cu, fend, nextPage
		prevPage = curr - 1
		IF (prevPage < 0) THEN 
			prev = ""
		ELSE
			prev = "<a class=link3ZC href="&chr(34)&url&"page="&prevPage&chr(34)&">上一页</a>"
		END IF
			
		pg = 0
		cnow = 0
		fend = 0
		cu = curr + 0

		Do While fend=0
			cnow = cnow + 1
			IF (pg=cu) THEN				
				pageNum = "<span style='color:#FF0000; font-weight:bold; font-size:16px;text-decoration:none'>" & cnow & "</span>"				
			else
				pageNum = "<a class=link3ZC href="&chr(34)&url&"page="&pg&chr(34)&">"&cnow&"</a>"
			END IF
			pageno = pageno & pageNum&" "
			pg = pg + 1
			if (inAllRow/limit<=pg) THEN fend = 1
		Loop 

		if (pg = 1) then
			pageno = ""
		end if
		nextPage = curr+1
		IF (curr * limit >= inAllRow - limit) THEN
			pnext=""
		else
			pnext = "<a class=link3ZC href="&chr(34)&url&"page="&nextPage&chr(34)&">下一页</a>"
		END IF
		pagef = prev & chr(38) & "nbsp" & pageno & chr(38) & "nbsp" & pnext
		
	END FUNCTION
%>