﻿<%

Dim exitRecTotal, exitDBQ, showSQL

' role = 0 is admin, role =1 is viewer
Function getDbConn(role)
	Dim sHost
	Dim sDBName
	Dim sUsrType
	Dim sPwd
	
	sHost = Application("gsHost")
	sDBName = Application("gsDBName")
	
	If role = 0 Then
		sUsrType = Application("gsUsrTypeAdmin")
		sPwd = Application("gsAdminPwd")
	Else
		sUsrType = Application("gsUrsTypeViewer")
		sPwd = Application("gsViewerPwd")
	End If
	
	Dim sConnStr	
	sConnStr = "Provider=sqloledb;Data Source=" & sHost & _
		";Initial Catalog=" & sDBName &";User Id=" & sUsrType & ";Password=" & sPwd
		
	Dim dbConn
	Set dbConn = Server.CreateObject("ADODB.Connection")
	
	dbConn.Open sConnStr
	
	Set getDbConn = dbConn
End Function

Function dbGetRecTotal(conn,tableName, aColumn, whereClause) 
	Dim rs, sqlStr
	sqlStr = "select count(" & aColumn & ") as total from " & tableName
	
	If trim(whereClause) <> "" Then
		sqlStr = sqlStr & " where " & whereClause
	end if	

	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.Open sqlStr, conn, 3, 3
	
	rs.MoveFirst
	
	dbGetRecTotal = Int(rs.Fields("total").value)
	rs.close
end Function

Function dbQuery(conn, tableName, columnName, whereClause ,orderColumnName, orderType, top)
	Dim colNameLen, sqlStr, i, tableGroup
	colNameLen = Ubound(columnName)
	
	If top > 0 Then
		sqlStr = "select top " & top & " "
	else
		sqlStr = "select "
	end if
	
	For i=0 To colNameLen
		sqlStr = sqlStr & columnName(i)
		If i <> colNameLen then
			sqlStr = sqlStr &", "
		end if
	Next
	
	tableGroup = ""
	If isarray(tableName) then
		for i = 0 to arraylength(tableName)-1
			tableGroup = tableGroup & tableName(i)
			if i < arraylength(tableName)-1 then
				tableGroup = tableGroup & ", "	
			end if
		next
	else
		tableGroup = tableName
	end if
	
	If trim(whereClause) <> "" Then
		sqlStr = sqlStr & " from " & tableGroup & " where " & whereClause
	else
		sqlStr = sqlStr & " from " & tableGroup
	end if
	
	if isarray(orderColumnName) then
		sqlStr = sqlStr &  " order by "
		for i = 0 to arraylength(orderColumnName)-1
			If orderColumnName(i) <> "" Then
				sqlStr = sqlStr & orderColumnName(i) & " "
			End if
			
			If orderType = "desc" Then
				sqlStr = sqlStr & " desc"
			end if	
			
			if i < arraylength(orderColumnName)-1 then
				sqlStr  = sqlStr & ", "
			end if
		next
	else
		If orderColumnName <> "" Then
			sqlStr = sqlStr & " order by " & orderColumnName
		End if
		
		If orderType = "desc" Then
			sqlStr = sqlStr & " desc"
		end if	
	end if

	Dim rs
	Set rs = Server.CreateObject("ADODB.Recordset")
	rs.Open sqlStr, conn, 3, 3
	
	If not rs.eof Then 
	
		rs.MoveFirst
		
		Dim record(), rsArray(), j
		
		ReDim record(Ubound(columnName))
		ReDim rsArray(rs.RecordCount-1)
		j = 0
		

		do while Not rs.eof
			For i=0 To colNameLen
				If isarray(tableName) then
						record(i) = rs.Fields(Mid(columnName(i), InStr(columnName(i), ".")+1, len(columnName(i)))).value
				else
						record(i) = rs.Fields(columnName(i)).value
				end if
			Next
			rsArray(j) = record
			j = j +1
			
			rs.MoveNext
		loop

                'Added by Ken, 02/05/04, to close rs before exit funtion	
	        rs.close
                set rs = nothing
                'End added by Ken, 02/05/04, to close rs before exit funtion	

		dbQuery = rsArray
		Exit Function
	end if
	rs.close
        'Added by Ken, 02/05/04, to close rs before exit funtion	
        set rs = nothing
        'End added by Ken, 02/05/04, to close rs before exit funtion	
	
	dbQuery = null
	
End Function

''''''''''''''''''''''''''''''''''''''''''''''''
'	sv : search value
'	svTrim :
'		0 : right trim
'		1 : left trim
'		2 : both trim
'	svOption :
'		0 :	add % in the front, ie. %sv
'		1 : add % behind , ie. sv%
'		2 : add % both side, ie %sv%
'	other number : do noting
'''''''''''''''''''''''''''''''''''''''''''''''
Function createBasicSearchCriteria(column, byval sv, operator, svNeedAddQuote, svTrim, svOption)
	If trim(sv) = "" or trim(column) = "" Then
		createBasicSearchCriteria = ""
		Exit Function
	end if
	
	Select Case svTrim
		Case 0
			sv = rtrim(sv)
		case 1
			sv = ltrim(sv)
		case 2
			sv = trim(sv)
	end select
	
	Select Case svOption
		Case 0
			sv = "%" & sv
		case 1
			sv = sv  & "%"
		case 2
			sv = "%" & sv  & "%"	
	end select
		
	If svNeedAddQuote Then			sv = "'" & sv & "'"		end if
	
	createBasicSearchCriteria = column & operator & sv

end function

''''''''''''''''''''''''''''''''''''''''''
' 	*sv can be string or array
'
'	createOption :
'		0 : compare case sensitive using like 	' not code yet
'		1 : compare case insensitive using like		
'		2 : compare case sensitive using =
'		3 : compare case insensitive using =
'		4 : compare every word in string individually (English only) case insensitive
'		5 : date between sv[0] sv[1],
''''''''''''''''''''''''''''''''''''''''''
Function createComplexSearchCriteria(columnArray, sv, createOption)
	If arrayLength(columnArray) = 0 Then
		createComplexSearchCriteria = ""
		Exit Function
	end if
	
	If	IsEmpty(columnArray) Then
		createComplexSearchCriteria = ""
		Exit Function
	end if
	
	If Not IsArray(sv) Then
		If Not IsEnglish(sv) Then
			select case createOption
				case 1 :
					createOption = 0
				case 3
					createOption = 2
			end select	
		End If
	end if
	
	Dim operator, returnSC, tempSC, column
	
	Select Case createOption
		case 0 :
			operator = " like "
			For each column in columnArray
				tempSC = createBasicSearchCriteria(column, sv, operator, 1, 2, 2)
				returnSC = concatSearchCriteria(returnSC, tempSC, "or") 	
			Next
		case 1 :
			operator = " like "
			For each column in columnArray
				tempSC = createBasicSearchCriteria(column, lcase(sv), operator, 1, 2, 2)
				returnSC = concatSearchCriteria(returnSC, tempSC, "or") 	
				tempSC = createBasicSearchCriteria(column, ucase(sv), operator, 1, 2, 2)
				returnSC = concatSearchCriteria(returnSC, tempSC, "or") 	
			Next
		case 5 :
			If arrayLength(sv) = 0 Then
				createComplexSearchCriteria = ""
				exit function
			end if
			For each column in columnArray
				tempSC = createBasicSearchCriteria(column, sv(0), ">=", 1, 2, -1)
				returnSC = concatSearchCriteria(returnSC, tempSC, "or") 	
				tempSC = createBasicSearchCriteria(column, sv(1), "<=", 1, 2, -1)
				returnSC = concatSearchCriteria(returnSC, tempSC, "and") 	
			Next
	End select
	
	If returnSC = "" Then
		createComplexSearchCriteria  = ""
	else
		createComplexSearchCriteria = "(" & returnSC & ")"
	end if

end function

Function concatSearchCriteria(scStr, sc, logic) 
	Dim newSC

	If scStr <> ""  and sc <> "" Then 
		newSC = scStr & " " & logic & " " & sc 
	else
		If scStr <> "" then
			newSC = scStr
		elseif sc <> "" then
			newSC = sc
		end if
	End if
	
	concatSearchCriteria = newSC
end function

Function getSafeSql(sqlStr)
    If isEmpty(sqlStr) then
		getSafeSql = ""
    else
		if trim(sqlStr) <> "" then
			getSafeSql = replace(sqlStr, "'", "''")
		else
			getSafeSql = sqlStr
		end if
	end if
End Function

Function getDateSql(dateStr)
    If isEmpty(dateStr) then
		getDateSql = ""
    else
		if trim(dateStr) <> "" then
			
			getDateSql = convertDatePattern(dateStr, "/", "-", 0, 2) 
			
			
		else
			getDateSql = dateStr
		end if
	end if
End Function

%>
