<%@ CodePage=65001 %>
<% Option Explicit %>

<!--#include virtual="/b_util/dbutil.asp"-->
<!--#include virtual="/b_util/validation.asp"-->
<!--#include virtual="/b_util/data.asp"-->
<!--#include virtual="/admin/admin_util.asp"-->
<!--#INCLUDE file="clsUpload.asp"-->

<%
Dim objUpload
Dim strFileName
Dim fileSize
Dim strPath
Dim filesys
Dim docPath
Dim i

docPath = "\doc\staff"

' Instantiate Upload Class
Set objUpload = New clsUpload

'  Instantiate Upload Class File System
Set filesys = CreateObject("Scripting.FileSystemObject")

' Grab the file name
strFileName = objUpload.Fields("File1").FileName
fileSize = objUpload.Fields("File1").Length
if CLng(fileSize) < 1 then
	response.Redirect "/forum/replace_photo.asp?e=1"
end if

' check file existenance, if existed, give a new name with counter like xxxxx(1).pdf, xxxxx(2).pdf
i = 0

Dim tempStrFileName
tempStrFileName = strFileName
do 
	i = i + 1
	if filesys.fileExists(Server.MapPath(docPath) & "\" & tempStrFileName) then
		tempStrFileName = strFileName
		tempStrFileName = Left(tempStrFileName, InstrRev(tempStrFileName, ".") - 1) & _ 
							"("  & i & ")" & _ 
							"." & _
							Mid(tempStrFileName, InstrRev(tempStrFileName, ".") + 1, len(tempStrFileName))

	end if
loop until not filesys.fileExists(Server.MapPath(docPath) & "\" & tempStrFileName)

strFileName = tempStrFileName

' Compile path to save file to
strPath = Server.MapPath(docPath) & "\" & strFileName

' Save the binary data to the file system
objUpload("File1").SaveAs strPath

' Release upload object from memory
Set objUpload = Nothing
Set filesys = Nothing

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'Set RecordSet
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	Dim objConn 'Used for the Connection object
	Dim sqlStr

	set objConn = getDbConn(0)
	sqlStr = "UPDATE Staff SET Photo='/doc/staff/" & strFileName & "' WHERE StaffId=" & Session("loginStaffId")
	objConn.Execute(sqlStr)
	Set objConn = Nothing
%>

<html>
<head>
<script language="JavaScript" src="/script/browser.js"></script>

</head>
<body onLoad="javascript:window.location.replace('/forum/replace_photo.asp')">
</body>
</html>

