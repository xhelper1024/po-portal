<!--#INCLUDE file="clsUpload.asp"-->
<%
Response.CacheControl = "no-cache"
Dim objUpload
Dim strFileName
Dim fileSize
Dim maxFileSize
Dim minFileSize
Dim strPath
Dim filesys
Dim docPath
Dim i

docPath = "\doc\forum"

' Instantiate Upload Class
Set objUpload = New clsUpload

'  Instantiate Upload Class File System
Set filesys = CreateObject("Scripting.FileSystemObject")

' Grab the file name
strFileName = objUpload.Fields("File1").FileName
fileSize = objUpload.Fields("File1").Length
maxFileSize = 1024*1024*8
minFileSize = 1024*1
if CLng(fileSize) > maxFileSize OR CLng(fileSize) < minFileSize then
	if CLng(fileSize) < minFileSize then
		response.Redirect "/forum/upload.asp?e=1"
	elseif CLng(fileSize) > maxFileSize then
		response.Redirect "/forum/upload.asp?e=2"
	end if
end if
' check file existenance, if existed, give a new name with counter like xxxxx(1).pdf, xxxxx(2).pdf
i = 0

Dim tempStrFileName
tempStrFileName = strFileName
do 
	i = i + 1
	if filesys.fileExists(Server.MapPath(docPath) & "\" & tempStrFileName) then
		tempStrFileName = strFileName
		tempStrFileName = Left(tempStrFileName, InstrRev(tempStrFileName, ".") - 1) & _ 
							"("  & i & ")" & _ 
							"." & _
							Mid(tempStrFileName, InstrRev(tempStrFileName, ".") + 1, len(tempStrFileName))

	end if
loop until not filesys.fileExists(Server.MapPath(docPath) & "\" & tempStrFileName)

strFileName = tempStrFileName

' Compile path to save file to
strPath = Server.MapPath(docPath) & "\" & strFileName

' Save the binary data to the file system
objUpload("File1").SaveAs strPath

' Release upload object from memory
Set objUpload = Nothing
Set filesys = Nothing
%>

<html>
<head>
<script language="JavaScript" src="/script/browser.js"></script>
<script language="Javascript">
function addAttachFileList(){
	d = getLayerInstanceWithDocument(window.opener.document, "divAttachment");
	d.innerHTML+="<br /><A href=\"/doc/forum/<%=strFileName%>\" target=\"_blank\"><%=strFileName%></A>";
	//window.opener.document.frmAction.hidDocUrl.value = "<%=strPath%>" ;
	window.opener.document.frmAction.hidDocUrl.value += "@#@/doc/forum/<%=strFileName%>" ;
    window.close();
}
</script>
</head>
<body onLoad="javascript:addAttachFileList();">
</body>
</html>

