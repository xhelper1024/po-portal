<!--#INCLUDE file="clsUpload.asp"-->
<!--#include virtual="/b_util/debug.asp"-->
<%
Dim objUpload
Dim strFileName
Dim strPath
Dim filesys
Dim docPath
Dim i

docPath = "\doc\policy"

' Instantiate Upload Class
Set objUpload = New clsUpload

'  Instantiate Upload Class File System
Set filesys = CreateObject("Scripting.FileSystemObject")

' Grab the file name
strFileName = objUpload.Fields("File1").FileName

' check file existenance, if existed, give a new name with counter like xxxxx(1).pdf, xxxxx(2).pdf
i = 0

Dim tempStrFileName
tempStrFileName = strFileName
do 
	i = i + 1
	if filesys.fileExists(Server.MapPath(docPath) & "\" & tempStrFileName) then
		tempStrFileName = strFileName
		tempStrFileName = Left(tempStrFileName, InstrRev(tempStrFileName, ".") - 1) & _ 
							"("  & i & ")" & _ 
							"." & _
							Mid(tempStrFileName, InstrRev(tempStrFileName, ".") + 1, len(tempStrFileName))
		ww tempStrFileName, 1

	end if
loop until not filesys.fileExists(Server.MapPath(docPath) & "\" & tempStrFileName)

strFileName = tempStrFileName

' Compile path to save file to
strPath = Server.MapPath(docPath) & "\" & strFileName

' Save the binary data to the file system
objUpload("File1").SaveAs strPath

' Release upload object from memory
Set objUpload = Nothing
Set filesys = Nothing
%>

<html>
<head>
<script language="JavaScript" src="/script/browser.js"></script>
<script language="Javascript">
function addAttachFileList(){
	d = getLayerInstanceWithDocument(window.opener.document, "divAttachment");
	d.innerHTML="<A href=\"/doc/policy/<%=strFileName%>\" target=\"_blank\"><%=strFileName%></A>";
	//window.opener.document.frmAction.hidDocUrl.value = "<%=strPath%>" ;
	window.opener.document.frmAction.hidDocUrl.value = "/doc/policy/<%=strFileName%>" ;
    window.close();
}
</script>
</head>
<body onload="javascript:addAttachFileList();">
</body>
</html>

