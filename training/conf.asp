<%
	dim TrainE(15)
	'column names in database
	TrainE(0) = "Id"						
	TrainE(1) = "EventDate"
	TrainE(2) = "CourseName" 
	TrainE(3) = "Organizer"
	TrainE(4) = "Address"
	TrainE(5) = "Website"
	TrainE(6) = "Fee"
	TrainE(7) = "Contact"
	TrainE(8) = "Registration"
	TrainE(9) = "EnrollStartDate"
	TrainE(10) = "EnrollEndDate"
	TrainE(11) = "Remarks"
	TrainE(12) = "RefDocUrl"
	TrainE(13) = "Location"
	TrainE(14) = "DateAdded"
	TrainE(15) = "EnrollStatus"

	dim TrainA(15)
	'chinese ver of column names in database
	TrainA(0) = "Id"						'Id
	TrainA(1) = "课程日期"					'EventDate
	TrainA(2) = "课程名称及编号" 			'CourseName 
	TrainA(3) = "主办单位" 					'Organizer
	TrainA(4) = "上课地址"					'Address
	TrainA(5) = "相关网站"					'Website
	TrainA(6) = "费用"						'Fee
	TrainA(7) = "查询或联络人 , 联络方法"	'Contact
	TrainA(8) = "报名方法"					'Registration
	TrainA(9) = "报名开始日期"				'EnrollStartDate
	TrainA(10) = "截止报名日期"				'EnrollEndDate
	TrainA(11) = "备注"						'Remarks
	TrainA(12) = "附件"						'RefDocUrl
	TrainA(13) = "地区"						'Location
	TrainA(14) = "发表日期"					'DateAdded
	TrainA(15) = "报名狀態"					'EnrollStatus
	
	'other names
	dim enrollDate
	
	enrollDate = "报名日期 (dd/mm/yyyy)"					'


	dim yearFormat
	yearFormat = " (dd/mm/yyyy)"
	
	dim yearFormatCh
	yearFormatCh = " (dd/mm/yyyy)"			
	
	dim star
	star = "<span class=""H1ZC"">*</span>"
	
	dim http
	http = " (http://...)"
	
	dim newNumDay
	newNumDay = 7
	

%>