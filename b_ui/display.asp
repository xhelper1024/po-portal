﻿<%

Function calculatePageTotal(recordTotal, recordPerPage)
	If recordTotal Mod recordPerPage = 0 Then
		calculatePageTotal = recordTotal / recordPerPage
	else
		calculatePageTotal = Int(recordTotal / recordPerPage) + 1
	end if
end function

function changeDisplayAllRecord()
	If Request("dar") <> "y"  then
		changeDisplayAllRecord = false
		exit function
	end if

	If 	displayAllRecord <> "y" then
		displayAllRecord = "y"
		Session("displayAllRecord") = displayAllRecord
		setNeedToAccessDB
		setSessionVarDefault
		changeDisplayAllRecord = true		
	else
		changeDisplayAllRecord = false
	end if
end function 

function changeSearchMode()
	if 	Request("sm") = "v" or Request("sm") = "va" or Request("sm") = "h" or Request("sm") = "vh" or Request("sm") = "vah" then
		searchMode = Request("sm")
		Response.Cookies(cookieName)("sm") = searchMode
		changeSearchMode = true
	else
		changeSearchMode = false
	end if
end function

function changeOrderType()
	if 	Request("ot") = "desc" or Request("ot") = "asc" then 
		orderType = Request("ot")
		Response.Cookies(cookieName)("ot") = orderType
		Session("currentPage") = 1
		currentPage = 1
		setNeedToAccessDB
		changeOrderType = true
	else
		changeOrderType = false
	end if
	

end function
	
function changeOrderColumn()
	if 	Request("oc") <> "" and isnumeric(Request("oc")) then
		dim intOC 
		intOC = int(Request("oc"))
		if  intOC >= 0 and intOC < arrayLength(ocOptions) Then
			If intOC <> orderColumn Then
				orderColumn = intOC
				Response.Cookies(cookieName)("oc") = orderColumn
				Session("currentPage") = 1
				currentPage =1 
				orderType = defaultOrderType
				Response.Cookies(cookieName)("ot") = orderType
				setNeedToAccessDB
				changeOrderColumn = true
				exit function
			end if
		end if
	end if
	
	changeOrderColumn = false
end function

function changeRecordPerPage()	'Deprecate On Maintenance of Sep-2004
	if 	Request("rpp") <> "" then
		Dim intRPP
		intRPP = int(Request("rpp"))
		if	intRPP <> recordPerPage Then
			recordPerPage = intRPP
			Response.Cookies(cookieName)("rpp") = intRPP
			Session("currentPage") = 1
			currentPage =1 
			changeRecordPerPage = true
			
			if arrayLength(recordArray) < recordTotal and currentPage * recordPerPage > arrayLength(recordArray) then

				setNeedToAccessDB
			end if
		end if
	else
		changeRecordPerPage = false
	end if
end function

sub changePeriodSearchOption()
	if 	Request("pso") = "m" or Request("pso") = "rd"  or Request("pso") = "p" or Request("pso") = "td" or _
		Request("pso") = "" and periodSearchOption <> ""	Then
		Session("fromDate") = ""
		Session("toDate") = ""
		Session("recentNDays") = ""
		setNeedToAccessDB
	end if
	
	periodSearchOption = Request("pso")
	Session("periodSearchOption") = periodSearchOption
end sub

sub changeRecentNDays()
	Session("recentNDays") = Request("rnd")

	If periodSearchOption <> "rd" then
		exit sub
	end if
	
	dim tempRND 
	
	tempRND = trim(Request("rnd"))
	
	If tempRND = "" Then
		errorMsgCode = 2
	else
		If isnumeric(tempRND) then
			if int(tempRND) < 1 then
				errorMsgCode = 2
			elseif tempRND <> recentNDays then
				setNeedToAccessDB
				recentNDays = Request("rnd")
			end if 		
		else
			errorMsgCode = 2
		end if
	end if
	
	Session("fromDate") = ""
	Session("toDate") = ""
end sub

sub changeFromDateAndToDate()	
	if periodSearchOption <> "p" then
		exit sub
	end if
	
	If Request("fd") = "" or Request("td") = "" then
		errorMsgCode = 4
	else
		if IsValidDate(Request("fd")) and IsValidDate(Request("td")) then
			Dim d, m, y, splitDate, tempFromDate, tempToDate
			
			splitDate = split(Request("fd"), "/")
			d = splitDate(0)
			m = splitDate(1)
			y = splitDate(2)											
			tempFromDate = DateSerial(y, m, d) 
			
			splitDate = split(Request("td"), "/")
			d = splitDate(0)
			m = splitDate(1)
			y = splitDate(2)											
			tempToDate = DateSerial(y, m, d) 
			
			If not compareDate(tempToDate, tempFromDate, true) Then
				errorMsgCode = 4				
			else
				setNeedToAccessDB
				fromDate = Request("fd")
				toDate = Request("td")
			end if
		else
			errorMsgCode = 4
		end if
	end if
	
	Session("fromDate") = Request("fd")
	Session("toDate") = Request("td")
	Session("recentNDays") = ""
end sub

sub changeBuyer()
   Dim v_buyer
  v_buyer =trim(Request("BUYER"))

	if v_buyer<>"" then
		if v_buyer <>buyer  then
			setNeedToAccessDB
			buyer = v_buyer
			Session("buyer") = buyer
		end if 
    else
	Session.Contents.Remove("buyer")
	end if	
end sub
sub changePost()
   Dim v_post
  v_post=trim(Request("POST"))

	if v_post<>"" then
		if v_post <>post  then
			setNeedToAccessDB
			post = v_post
			Session("post") = post
		end if 
    else
	Session.Contents.Remove("post")
	end if	
end sub
sub changeRtv()
   Dim v_rtv
  v_rtv =trim(Request("RTV"))

	if v_rtv<>"" then
		if v_rtv <>rtv  then
			setNeedToAccessDB
			rtv = v_rtv
			Session("rtv") = rtv
		end if 
    else
	Session.Contents.Remove("rtv")
	end if	
end sub

sub changeReply()
   Dim v_reply
  v_reply =trim(Request("REPLY"))

	if v_reply<>"" then
		if v_reply <>reply  then
			setNeedToAccessDB
			reply = v_reply
			Session("reply") = reply
		end if 
    else
	Session.Contents.Remove("reply")
	end if	
end sub

sub changePONo()
   Dim v_po
  v_po =trim(Request("PO_NO"))

	if v_po<>"" then
		if v_po <>PONo  then
			setNeedToAccessDB
			PONo = v_po
			Session("PONo") = PONo
		end if 
    else
	Session.Contents.Remove("PONo")
	end if	
end sub

sub changeVendor()
   Dim v_vendor
  v_vendor =trim(Request("VENDOR"))
	if v_vendor<>"" then
		if v_vendor <>vendor  then
			setNeedToAccessDB
			vendor= v_vendor
			Session("vendor") = vendor
		end if 
    else
	Session.Contents.Remove("vendor")
	end if	
end sub

sub changeItemFrm()
  Dim frmI
  frmI=trim(request("FRMITEM"))
   if frmI<>"" then
    if frmI<>frmItem then
	setNeedToAccessDB
	frmItem=frmI
	Session("frmItem")=frmItem
	end if
   else
   Session.Contents.Remove("frmItem")
   end if
end sub

sub changeneedByDate()
   Dim v_needByDate
  v_needByDate=trim(Request("NEED_BY_DATE"))
	if v_needByDate<>"" then
		if v_needByDate <>needByDate  then
			setNeedToAccessDB
			needByDate = v_needByDate
			Session("needByDate") = needByDate
		end if 
    else
	Session.Contents.Remove("needByDate")
	end if	
end sub

sub changepromisedDate()
   Dim v_promisedDate
  v_promisedDate=trim(Request("PROMISED_DATE"))
	if v_promisedDate<>"" then
		if v_promisedDate <>promisedDate  then
			setNeedToAccessDB
			promisedDate = v_promisedDate
			Session("promisedDate") = promisedDate
		end if 
    else
	Session.Contents.Remove("promisedDate")
	end if	
end sub

sub changeMFGNo()
   Dim v_mfgno
  v_mfgno=trim(Request("MFG_PART"))
	if v_mfgno<>"" then
		if v_mfgno <>MFGNo  then
			setNeedToAccessDB
			MFGNo = v_mfgno
			Session("MFGNo") = MFGNo
		end if 
    else
	Session.Contents.Remove("MFGNo")
	end if	
end sub

sub changeMFGName()
   Dim v_mfgname
  v_mfgname=trim(Request("MFG_NAME"))
	if v_mfgname<>"" then
		if v_mfgname <>MFGName  then
			setNeedToAccessDB
			MFGName = v_mfgname
			Session("MFGName") = MFGName
		end if 
    else
	Session.Contents.Remove("MFGName")
	end if	
end sub

sub changeWhereAboutEventType()
	if isnumeric(Request("wa")) then
		if int(Request("wa")) <> eventType and int(Request("wa")) < arrayLength(gArrEventType) then
			setNeedToAccessDB
			eventType = int(Request("wa"))
			Session("eventType") = eventType
		end if 
	end if	
end sub

sub changeGalleryEventType()
	if isnumeric(Request("get")) then
		if int(Request("get")) <> eventType and int(Request("get")) < arrayLength(gArrGalleryEventType) then
			setNeedToAccessDB
			eventType = int(Request("get"))
			Session("eventType") = eventType
		end if 
	end if	
end sub

function changeCurrentPage()
	if pageTotal < 1 Then
		changeCurrentPage = false
		exit function
	end if

	Dim tempCP
	if trim(Request("jumpPage")) <> ""  then

		If isNumeric(Request("jumpPage"))	then

		 	If int(Request("jumpPage")) > 0  Then
				tempCP = int(Request("jumpPage"))
			else
				errorMsgCode = 0
				changeCurrentPage = false
				exit function
			end if
		else
			errorMsgCode = 0
			changeCurrentPage = false
			exit function
		end if
	else
		tempCP = Request("cp")
	end if
	
	if isNumeric(tempCP) then
		tempCP = int(tempCP)
		if tempCP > 0 and tempCP <= pageTotal then 
			currentPage = tempCP
			Session("currentPage") = currentPage
			changeCurrentPage = true
			if arrayLength(recordArray) < recordTotal and tempCP * recordPerPage > arrayLength(recordArray) then
				setNeedToAccessDB
			end if
			exit function
		end if
	end if
	
	changeCurrentPage = false
end function

sub removeSessionValue()
	Session.Contents.Remove("buyer")
	Session.Contents.Remove("vendor")
	Session.Contents.Remove("frmItem")
	Session.Contents.Remove("MFGNo")
	Session.Contents.Remove("MFGName")
	Session.Contents.Remove("PONo")
	Session.Contents.Remoce("needByDate")
	Session.Contents.Remove("promisedDate")
	Session.Contents.Remove("post")
	Session.Contents.Remove("rtv")
	Session.Contents.Remove("reply")
	Session.Contents.Remove("eventType")
	Session.Contents.Remove("displayAllRecord")
	Session.Contents.Remove("currentCategory")
	Session.Contents.Remove("currentPage")
	Session.Contents.Remove("recordTotal")
	Session.Contents.Remove("recordArray")
	'Session.Contents.Remove("parentMsgHeading")
	'Session.Contents.Remove("pid")
	'Session.Contents.Remove("tid")
end sub

function cutTooLongText(theText, theLen, endOfText)
	if len(theText) > theLen then
		theText = left(theText, theLen) & endOfText
	end if
	
	cutTooLongText = theText
end function

'转换时间 时间格式化 
Function formatDate(Byval t,Byval ftype) 
dim y, m, d, h, mi, s 
formatDate="" 
If IsDate(t)=False Then Exit Function 
y=cstr(year(t)) 
m=cstr(month(t)) 
If len(m)=1 Then m="0" & m 
d=cstr(day(t)) 
If len(d)=1 Then d="0" & d 
h = cstr(hour(t)) 
If len(h)=1 Then h="0" & h 
mi = cstr(minute(t)) 
If len(mi)=1 Then mi="0" & mi 
s = cstr(second(t)) 
If len(s)=1 Then s="0" & s 
select case cint(ftype) 
case 1 
' yyyy-mm-dd 
formatDate= m & "/" & d & "/" & y 
case 2 
' yy-mm-dd 
formatDate=right(y,2) & "-" & m & "-" & d 
case 3 
' mm-dd 
formatDate=m & "-" & d 
case 4 
' yyyy-mm-dd hh:mm:ss 
formatDate=y & "-" & m & "-" & d & " " & h & ":" & mi & ":" & s 
case 5 
' hh:mm:ss 
formatDate=h & ":" & mi & ":" & s 
case 6 
' yyyy年mm月dd日 
formatDate=y & "年" & m & "月" & d & "日" 
case 7 
' yyyymmdd 
formatDate=y & m & d 
case 8 
'yyyymmddhhmmss 
formatDate=y & m & d & h & mi & s 
end select 
End Function 
%>