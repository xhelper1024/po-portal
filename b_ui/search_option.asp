﻿<%	
	Dim maxSearchElemCombi
	
	maxSearchElemCombi = 12
	Function getBit(bitPatternStr, posi)
		dim theBit
		
		theBit = mid(bitPatternStr, posi, 1)
		
		getBit = int(theBit)
	End function
	
	
	Sub displaySearch(allowAdvanced, bgColor)
	
	If len(searchElemCombi) <> maxSearchElemCombi  Then
		exit sub
	end if
	
	Dim i
	
	If searchMode <> "h" Then
%> 
<script language="JavaScript">
<!--
	function clearSearchOpt() {
		theForm = document.searchForm
		
		if (theForm.pso) 
			for (i=0 ; i < 4 ; i++) 
				if (theForm.pso[i]) 
					theForm.pso[i].checked = false

		if (theForm.c)		theForm.c.selectedIndex = 0
		if (theForm.d)		theForm.d.selectedIndex = 0
		if (theForm.r)		theForm.r.selectedIndex = 0
		if (theForm.wa)		theForm.wa.selectedIndex = 0
		if (theForm.get)	theForm.get.selectedIndex = 0
		if (theForm.rnd)	theForm.rnd.value = ""
		if (theForm.fd)		theForm.fd.value = "DD/MM/YYYY"
		if (theForm.td)		theForm.td.value= "DD/MM/YYYY"
		if (theForm.kw)		theForm.kw.value= ""
	}

//-->
</script>
<html>
<header>
<body topmargin="0" bottommargin="0">
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="<%=bgColor%>" >
  <tr bgcolor="#EAEAEA"> 
    <td class="contentID"><img src="/images/spacer.gif" width="10" height="10"></td>
    <td  valign="baseline" id="contentID"><font color="#FF0000"><font color="#000096">搜寻选择</font></font></td>
    <td nowrap id="contentID"> <div align="right">
        <% 
		if searchMode <> "va" and allowAdvanced Then %>
        <a href="search_control.asp?sm=va"><img src="/images/icon_more_option.gif" width="20" height="20" border="0" align="absmiddle"></a> 
        <% end if %>
        <% if searchMode = "vah" or searchMode = "vh" Then 
	  		if searchMode = "vah" then
				url = "va"
			else
				url = "v"
			end if
	  %>
        <a href="search_control.asp?sm=<%=url%>"><img src="/images/icon_open.gif" width="20" height="20" border="0" align="absmiddle"></a> 
        <% else 
	  	if searchMode = "va" then
			url = "vah"
		else
			url = "vh"
		end if
	  %>
        <a href="search_control.asp?sm=<%=url%>"><img src="/images/icon_half_close.gif" width="20" height="20" border="0" align="absmiddle"></a> 
        <% end if%>
        
        <a href="search_control.asp?sm=h"><img src="/images/icon_close.gif" width="20" height="20" border="0" align="absmiddle"></a>&nbsp;&nbsp; 
      </div></td>
  </tr>
  <% if searchMode = "va" or searchMode = "v" Then %>
  <tr> 
    <td height="25" id="contentID">&nbsp;</td>
    <td height="25" colspan="2" nowrap id="contentID"><% 	for i = 1 to Len(searchElemCombi) 
		if getBit(searchElemCombi, i) = 1 Then
			displayUI(i) %>
		  <%	end if 
		  next %> <% if searchMode = "v" then %>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="/images/btn_small_search.gif" width="32" height="25" align="absmiddle" onClick="document.searchForm.submit()" style="cursor: hand;"> 
    </td>
    <% end if %>
  </tr>
  <% end if %>
  <% if searchMode = "va" and allowAdvanced Then %>
  <tr> 
    <td class="contentNZC">&nbsp;</td>
    <td height="80" nowrap class="contentNZC"><% 	for i = 1 to Len(searchElemCombi) 
		if getBit(searchElemCombi, i) = 2 Then
			displayUI(i) %>
	<%	end if 
	 next 
	%> 
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="/images/btn_small_search.gif" width="32" height="25" align="absmiddle" onClick="document.searchForm.submit()" style="cursor: hand;"> 
    </td>
    <td align="right" valign="bottom" class="contentNZC"><a href="search_control.asp?sm=v"><img src="/images/icon_less_option.gif" width="20" height="20" border="0" align="absmiddle"></a>&nbsp;&nbsp;</td>
  </tr>
  <% end if %>
  
  <tr> 
    <td height="10" colspan="3"><img src="/images/color_20k.gif" width="100%" height="1"></td>
  </tr>
</table>
<% 		end if 
	end sub

sub displayUI(uiID)
	Dim i  ,StaffLevel
	StaffLevel=Session("StaffLevel")
	select case uiID
		case 1 :
%>
<label><font color="#000096"><strong>&nbsp;&nbsp;Vendor Code:&nbsp;</strong> <% if session("StaffLevel")<>3 then
	%> <input  type="text" name="VENDOR" value="<%=session("vendor")%>" size="9"/>
	<%else
	%>
	<input  type="text" name="VENDOR" value="<%=session("Username")%>" size="9" readonly="readonly"/>
	<%end if%></label>&nbsp;
<label><font color="#000096"><strong>&nbsp;&nbsp;MFG Name:&nbsp; </strong>
<input  type="text" name="MFG_NAME" value="<%=session("MFGName")%>"/>
</label>&nbsp;
<label><font color="#000096"><strong>&nbsp;&nbsp;MFG Part#:&nbsp; </strong>
<input  type="text" name="MFG_PART" value="<%session("MFGNo")%>"/></label>&nbsp;
<label><font color="#000096"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Post:&nbsp;</strong>
<% if trim(session("post"))="Y" then%>
<select name="POST" style="width:88px"><option value="Y">Y</option><option value="N">N</option><option value=""></option></select>
<%elseif trim(session("post"))="N" then%>
<select name="POST" style="width:88px"><option value="N">N</option><option value="Y">Y</option><option value=""></option></select>
<%else%>
<select name="POST" style="width:88px"><option value=""></option><option value="Y">Y</option><option value="N">N</option></select>
<%end if%>
</label>&nbsp;
<label><font color="#000096"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Reply:&nbsp;</strong>
<% if trim(session("reply"))="Y" then%>
<select name="REPLY" style="width:88px" ><option value="Y">Y</option><option value="N">N</option><option value=""></option></select>
<%elseif trim(session("reply"))="N" then%>
<select name="REPLY" style="width:88px"><option value="N">N</option><option value="Y">Y</option><option value=""></option></select>
<%else%>
<select name="REPLY" style="width:88px"><option value=""></option><option value="Y">Y</option><option value="N">N</option></select>
<%end if%>
</label>&nbsp;
<label><font color="#000096"></label>
<br>
<label><font color="#000096"><strong>&nbsp;&nbsp;&nbsp;&nbsp;PO Number:&nbsp;</strong>
<input  type="text" name="PO_NO" value="<%=session("PONo")%>" size="9"/></label>&nbsp;
<label><font color="#000096"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Buyer:&nbsp;</strong> <input  type="text" name="BUYER" value="<%=session("buyer")%>" size="20"/></label>&nbsp;
<label><font color="#000096"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Item:&nbsp; </strong><input  type="text" name="FRMITEM" value="<%=session("frmItem")%>" size="20"/></label>&nbsp;
<label><font color="#000096"><strong>&nbsp;&nbsp;Need By Date:&nbsp;</strong> <input  size="11" maxlength="10" type="text" name="NEED_BY_DATE"  value="<%=session("needByDate")%>" class="cn_date" onClick="new Calendar().show(this);" id="select_dates" /></label>&nbsp;
<label><font color="#000096"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Delivery Date:&nbsp;</strong> 
<input  size="11"  maxlength="10" type="text" name="PROMISED_DATE"  value="<%=session("promisedDate")%>" class="cn_date" onClick="new Calendar().show(this);" id="select_date"/></label>
<%end select 
end sub %>
</body>
</header>
</html>




