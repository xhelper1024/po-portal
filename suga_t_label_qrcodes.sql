USE [SugaExtDB]
GO

/****** Object:  Table [dbo].[suga_t_label_qrcodes]    Script Date: 2020/5/7 15:06:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[suga_t_label_qrcodes](
	[qrcode_id] [varchar](20) NOT NULL,
	[parent_qrcode_id] [varchar](20) NULL,
	[package_qty] [decimal](12, 2) NULL,
	[package_type] [nvarchar](2) NULL,
	[po_no] [varchar](40) NULL,
	[create_time] [datetime2](0) NULL,
	[line_no] [varchar](4) NULL,
	[is_delete] [varchar](1) NOT NULL,
	[material_no] [varchar](40) NULL,
 CONSTRAINT [PK__suga_t_l] PRIMARY KEY CLUSTERED 
(
	[qrcode_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[suga_t_label_qrcodes] ADD  DEFAULT ('N') FOR [is_delete]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'条码ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_qrcodes', @level2type=N'COLUMN',@level2name=N'qrcode_id'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'父条码ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_qrcodes', @level2type=N'COLUMN',@level2name=N'parent_qrcode_id'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'包装数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_qrcodes', @level2type=N'COLUMN',@level2name=N'package_qty'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'包装类型：01独立包装，02外箱包装' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_qrcodes', @level2type=N'COLUMN',@level2name=N'package_type'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PO记录ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_qrcodes', @level2type=N'COLUMN',@level2name=N'po_no'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_qrcodes', @level2type=N'COLUMN',@level2name=N'create_time'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PO记录ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_qrcodes', @level2type=N'COLUMN',@level2name=N'line_no'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_qrcodes', @level2type=N'COLUMN',@level2name=N'is_delete'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'物料编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'suga_t_label_qrcodes', @level2type=N'COLUMN',@level2name=N'material_no'
GO

