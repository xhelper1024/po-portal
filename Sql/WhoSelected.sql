CREATE TABLE [WhoSelected] (
	[selected] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[whoselected] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[line_no] [numeric](18, 0) NOT NULL ,
	CONSTRAINT [PK_WhoSelected] PRIMARY KEY  CLUSTERED 
	(
		[whoselected],
		[line_no]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO


