if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[StaffLog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[StaffLog]
GO

CREATE TABLE [dbo].[StaffLog] (
	[StaffLogId] [numeric](18, 0) IDENTITY (1, 1) NOT NULL ,
	[StaffId] [numeric](18, 0) NULL ,
	[LoginTime] [datetime] NOT NULL ,
	[IPAddress] [varchar] (20) COLLATE Chinese_PRC_Stroke_CS_AI_WS NOT NULL 
) ON [PRIMARY]
GO

