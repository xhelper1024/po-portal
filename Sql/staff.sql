if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Staff]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Staff]
GO

CREATE TABLE [dbo].[Staff] (
	[StaffId] [numeric](18, 0) IDENTITY (1, 1) NOT NULL ,
	[StaffName] [varchar] (255) COLLATE Chinese_Taiwan_Stroke_CI_AS NULL ,
	[StaffNameChi] [nvarchar] (255) COLLATE Chinese_Taiwan_Stroke_CI_AS NULL ,
	[Ext] [nvarchar] (50) COLLATE Chinese_Taiwan_Stroke_CI_AS NULL ,
	[Tel] [nvarchar] (50) COLLATE Chinese_Taiwan_Stroke_CI_AS NULL ,
	[Mobile] [nvarchar] (50) COLLATE Chinese_Taiwan_Stroke_CI_AS NULL ,
	[Email] [nvarchar] (255) COLLATE Chinese_Taiwan_Stroke_CI_AS NULL ,
	[CompanyId] [numeric](18, 0) NOT NULL ,
	[DeptId] [numeric](18, 0) NULL ,
	[DistrictId] [numeric](18, 0) NOT NULL ,
	[ModBy] [numeric](18, 0) NOT NULL ,
	[ModDt] [datetime] NOT NULL ,
	[Fax] [nvarchar] (50) COLLATE Chinese_PRC_Stroke_CS_AI_WS NULL ,
	[Password] [varchar] (50) COLLATE Chinese_PRC_Stroke_CS_AI_WS NULL ,
	[LastLogin] [datetime] NULL ,
	[ForumPermiss] [char] (1) COLLATE Chinese_PRC_Stroke_CS_AI_WS NOT NULL ,
	[NewsPermiss] [varchar] (50) COLLATE Chinese_PRC_Stroke_CS_AI_WS NULL ,
	[MsgPost] [numeric](18, 0) NULL ,
	[Online] [char] (1) COLLATE Chinese_PRC_Stroke_CS_AI_WS NULL ,
	[Photo] [varchar] (50) COLLATE Chinese_PRC_Stroke_CS_AI_WS NULL ,
	[Status] [char] (1) COLLATE Chinese_PRC_Stroke_CS_AI_WS NULL ,
	[ForumAlert] [char] (1) COLLATE Chinese_PRC_Stroke_CS_AI_WS NULL ,
	[CrtDt] [datetime] NULL 
) ON [PRIMARY]
GO

