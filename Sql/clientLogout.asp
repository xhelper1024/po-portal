<%@ CodePage=65001 %>
<% Option Explicit %>

<!--#include virtual="/b_util/dbutil.asp"-->
<%
On Error Resume Next
response.buffer=true
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'Set RecordSet
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	Dim objConn 'Used for the Connection object
	Dim sqlStr

if Session("loginStaffId") = empty or Session("loginStaffId") = "" then 
	response.Redirect("/index.asp") 
else
	set objConn = getDbConn(1)
	sqlStr = "UPDATE Staff SET Online='N' WHERE StaffId=" & Session("loginStaffId")
	response.Write(sqlStr)
	'response.End()
	objConn.Execute(sqlStr)
	Set objConn = Nothing
	response.cookies(Application("gDefaultCookiesName")).expires = DATE - 1
	
	'added
	session.Abandon()

	'Err Handling here
	if Err.Number <> 0 then
		Response.write "<br>[" & Err.number & "]-["&Err.Description&"]<br>"
	 	Err.Clear
	end if

	Response.Redirect("/index.asp")
end if
%>