SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

create view suga_v_po_summary  as select  BUYER,
 PO_ISSU_DATE, PO, LINE, ITEM, 
SHIP_QTY, SHIP_OS_QTY, UOM, NEED_BY_DATE, 
DELIVERY_DATE, PROMISED_DATE, NEW_REQ_DATE,
 stm.LINE_NO, ws.SELECTED,ws.whoselected
from suga_t_maintenance_po 
stm left outer join WhoSelected
 ws on stm.line_no=ws.line_no 


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

