<%@ CodePage=65001 %>
<% Option Explicit %>

<!--#include virtual="/b_util/dbutil.asp"-->
<!--#include virtual="/b_util/validation.asp"-->
<!--#include virtual="/b_util/data.asp"-->
<!--#include virtual="/admin/admin_util.asp"-->
<%
On Error Resume Next
'response.buffer=true
Session("loginStaffId") = empty

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'Set RecordSet
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	Dim objConn 'Used for the Connection object
	Dim objRst 'Used for the Recordset object
	Dim sqlStr
	set objConn = getDbConn(0)
	Set objRst = Server.CreateObject("ADODB.Recordset")
	
	'Variable
	Dim strUserName
	Dim strPwd
	strUserName = trim(Request.Form("txtUserName"))
	strPwd = Request.Form("txtUserPass")
	
	sqlStr = "SELECT * FROM Staff WHERE Status = 'Y' AND Email = '" & getSafeSql(strUserName) & "' AND Password ='" & EncryptText(strUserName, strPwd) & "'"
	'sqlStr = "SELECT * FROM Staff WHERE Email = '" & getSafeSql(strUserName) & "' and Password = '" & getSafeSql(strPwd) & "'"

	objRst.Open sqlStr, objConn, 3, 3
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'End Set RecordSet	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	
	If not objRst.EOF then
		Dim StaffId
		StaffId = trim(objRst("StaffId"))
		Session("loginFail") = empty
		Session("loginStaffId") = StaffId
		Session("Username")=trim(objRst("StaffName"))
		Session("StaffLevel")=trim(objRst("StaffLevel"))
		Session("Password")=trim(objRst("Password"))
		Session("LoginName")=trim(objRst("Email"))
		Session("NewsPermiss") = trim(objRst("NewsPermiss"))
		Session("ForumPermiss") = trim(objRst("ForumPermiss"))
		'expiredate=dateadd("d",Application("gDefaultCookiesExpireDay"),date)
		'Response.Write(Application("gDefaultCookiesName"))
		Response.Cookies(Application("gDefaultCookiesName"))=StaffId 
		'20150817
		'Response.Cookies(Application("gDefaultCookiesName")).expires=DATE + Application("gDefaultCookiesExpireDay")
		Response.Cookies(Application("gDefaultCookiesName")).expires=DATE +DateAdd("n",15,now()) 
		objRst.Close
		'Do update login time
		sqlStr = " UPDATE Staff SET LastLogin = getdate(), Online='Y' WHERE StaffId = " & StaffId
		objConn.Execute(sqlStr)
		sqlStr = " INSERT INTO StaffLog (StaffId, LoginTime, IPAddress ) VALUES ( " & StaffId & ", getdate(), '" & Request.ServerVariables("REMOTE_ADDR") & "')"
		objConn.Execute(sqlStr)
		Response.Redirect "/homepage.asp"
	Else
	'response.Redirect("https://www.baidu.com/")
		Session("loginStaffId") = empty
		Session("loginFail") = 1
		Response.Redirect "/index.asp"
	End If

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'Footer to reset all resources and error handling
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	set objRst = Nothing
	Set objConn = Nothing

'Err Handling here
if Err.Number <> 0 then
	Response.write "<br>[" & Err.number & "]-["&Err.Description&"]<br>"
 	Err.Clear
end if
	
%>