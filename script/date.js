function formatDDMMYYYY(date, sep) {

	return date.getDate() + sep + (date.getMonth()+1) + sep + date.getFullYear()
}

function getLastDateOfMonth(date) {
	
	m = date.getMonth()+1
	
	if ( m == 2 ) {
		if  (isLeapYear(date.getFullYear()) )
			return 29
		else
			return 28
	}
	
	if (isBigMonth(m))
		return 31
	else	
		return 30
}

function getCurrDateDDMMYYYY(){
	var now = new Date();
	var day = now.getDate();
	if (day < 10){
		day = "0" + day;
	}
	var month = now.getMonth() + 1;
	if(month < 10){
	  month = "0" + month;
	}
	var year = now.getYear();
	
	return day + "/" + month + "/" + year;
}

function getCurrTimeHHMMSS(){
	var now = new Date();
	
	var hr = now.getHours();
	if (hr < 10)
		hr = "0" + hr;
	
	var minutes = now.getMinutes();
	if(minutes < 10)
	  minutes = "0" + minutes;
	  
  	var sec = now.getSeconds();
	if(sec < 10)
	  sec = "0" + sec;
	
	return hr + ":" + minutes + ":" + sec;
}