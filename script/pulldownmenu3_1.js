// For fast response, it is recommend to use version 2 or early 
//
// version 3.1
// getLayerInstance() is moved to broswer.js. So the calling function of 
// getLayerInstance// in this file need to be changed
//
// version 3
// nearly reprogram many things. add the layer object so each layer has its own property
//
// version 2, solved for netscape 6 or later
// add var originPosition
// add fly in function for horizontal pulldown menu (see Kooll)
// amend many programing defect
// add fly out idea. But not implement coz of the complication.
//
// version 1.3, solved for netscape 6 or later
// isIE(), isV6Later from browser.js
// solve the problem of movable menu

var NO_FLY_IN = "nfi";
var FROM_LEFT = "fl";
var NO_FLY_OUT = "nfo";
var TO_LEFT = "tl";

var maxMenuTotal = 5;
var allMenuName = new Array("pdm0", "pdm2", "pdm3");
var originCoord = new Array(true, true, true);
var posiShift = new Array(-40,-40,-40);
var movable = new Array(false, false, false); 
/* var movable: menu move with mouse pointer horizontally 
after shown while the mouse pointer is still over the item of Navigation Bar */
var flyIn = new Array(NO_FLY_IN, NO_FLY_IN, NO_FLY_IN);
var flyInSpeed = new Array(15,15,15); // smaller the value, higher the speed.

/*	Very difficult to code
var flyOut = new Array(TO_LEFT, TO_LEFT, NO_FLY_OUT, NO_FLY_OUT);
var flyOutSpeed = new Array(15,15,15,15); // smaller the value, higher the speed.*/

var blurSpeed = 300;	// smaller the value, faster the menu will hide after the mouse out of the menu

menuList = new MenuList();

var x;
var y;
var currentMenu;
var menuBuffer = null;
var timerID = null;
var timerRunning = false;
var mouseoverOnNaviBar = false;

function createMenuList() {
	flyOutMenuTotal = 0;
	
	for(i=0 ; i < allMenuName.length ; i++) {
		newMenu = new Menu(allMenuName[i]);
		newMenu.originCoord = originCoord[i];
		if(! originCoord[i]);
			newMenu.posiShift = posiShift[i];
		newMenu.movable = movable[i];
		newMenu.blurSpeed = blurSpeed[i]
		
		if(flyIn[i] != NO_FLY_IN) {
			newMenu.flyIn = flyIn[i];
			newMenu.flyInSpeed = flyInSpeed[i];
		}
		
		/* Very difficult to code
		/*if(flyOut[i] != NO_FLY_OUT) {
			newMenu.flyOut = flyOut[i];
			newMenu.flyOutSpeed = flyOutSpeed[i];
			flyOutMenuTotal++;
		}*/
		
		menuList.add(newMenu);
	}
	menuBuffer = new Array(flyOutMenuTotal);
	
}

//============================== Menu ======================================
function Menu(name) {
	this.name = name;
	this.theLayer = null;
	this.originCoord = false;
	this.posiShift = 0;
	this.movable = false;
	this.flyIn = "";
	this.flyInSpeed = 0;
	this.flyOut = "";
	this.flyOutSpeed = 0;
	this.flyInIntvl = null;
	this.flyOutIntvl = null;
	
	this.show = show;
	this.hide = hide;
	this.isMouseOver = isMouseOver;
	this.flyInFromLeft = flyInFromLeft;
	this.flyOutToLeft = flyOutToLeft;
}

function show(x) {
	if (this.theLayer == null)
		this.theLayer = getLayerInstance(this.name);
		
	if (this.theLayer) {
		
		if (isIE() || (! isIE() && isV6Later()) ) {
			if ( this.flyIn == FROM_LEFT) {
				this.flyInFromLeft();
			}
			else
				if (! this.originCoord )	
					this.theLayer.style.left = x + this.posiShift;
			
			this.theLayer.style.visibility = "visible";
		}
		else {
			if (! this.originCoord )	this.theLayer.left = x + this.posiShift;
			this.theLayer.visibility = "visible";
		}
		
		if ( this.movable == false )
			document.onmousemove = ""; 
	}
}

function hide() {
	if (this.theLayer == null)
		this.theLayer = getLayerInstance(this.name);
		
	if (this.theLayer) {
	
		if (isIE() || (! isIE() && isV6Later()) ) {
			this.theLayer.style.visibility = 'hidden';
		}
		else {
			this.theLayer.visibility = 'hidden';
			document.releaseEvents(Event.MOUSEMOVE);
		}
		
		if ( timerRunning ) {
			clearInterval(timerID);
			timerRunning = false;
		}
		clearInterval(this.flyInIntvl);
	}
}

function isMouseOver() {
	if (! this.theLayer) 
		return false;
	
	if ( isIE() || (!isIE() && isV6Later()) ){			
		layerWidth = parseInt(this.theLayer.style.width);
		layerHeight = parseInt(this.theLayer.style.height);
		layerTop = parseInt(this.theLayer.style.top);
		layerLeft = parseInt(this.theLayer.style.left);
	}
	else {
		layerWidth = this.theLayer.clip.width;
		layerHeight = this.theLayer.clip.height;
		layerTop = this.theLayer.top;
		layerLeft = this.theLayer.left;			
	}
	
	if ( x < layerLeft || x > layerWidth + layerLeft || y < layerTop || y > layerHeight + layerTop )
		return false;
	
	return true;	
}

function flyInFromLeft() {
	if (isIE() || isV6Later()) {
		this.theLayer.style.left = 0 - parseInt(this.theLayer.style.width);
		this.flyInIntvl = setInterval("if (parseInt(currentMenu.theLayer.style.left) < 0) currentMenu.theLayer.style.left = parseInt(currentMenu.theLayer.style.left) + 20;", this.flyInSpeed);
	}
}

// Not well. very difficult to code
function flyOutToLeft() {
	code = "";
	for(i=0 ; i < menuBuffer.length; i++)
		if(menuBuffer[i] == null && (isIE() || isV6Later())){
			menuBuffer[i] = this;
			menuString = "menuBuffer[" + i + "]";
			code = "if (parseInt(" + menuString + ".theLayer.style.width) + parseInt(" + 
				menuString + ".theLayer.style.left) > 0)" +
				menuString + ".theLayer.style.left = parseInt(" + 
				menuString + ".theLayer.style.left) - 20; else {" + 
				"clearInterval(" + menuString+ ".flyOutIntvl);" +
				menuString + " = null;}";
			break;
		}

	clearInterval(this.flyInIntvl);
	this.flyOutIntvl = setInterval(code, this.flyOutSpeed);

}

//=========================== end of Menu =============================

//====================== MenuList =========================
function MenuList() {
	this.theList = new Array(maxMenuTotal);
	this.length = 0;
	this.add = add;
	this.getMenu = getMenu;
	this.hideAll = hideAll;
}

function add(newMenu) {
	this.theList[this.length] = newMenu;
	this.length++;
}

function getMenu(name) {
	for(i=0; i < this.length ;i++)
		if (this.theList[i].name == name) {
			return this.theList[i];
		}
	
	return null;
}

function hideAll() {
	for(i=0; i < this.length ;i++)
		this.theList[i].hide();
}

//===================== end of MenuList ========================

function showMenu(menuName) {
	menuList.hideAll();
	mouseoverOnNaviBar = true;
	
	currentMenu = menuList.getMenu(menuName);
	
	if ( ! timerRunning) {
		timerID = setInterval("if (! mouseoverOnNaviBar && currentMenu) if (! currentMenu.isMouseOver()) {" + 
									"currentMenu.hide(); showHideRadio(currentMenu.name, true);} " , blurSpeed);
							
		// showHideRadio(currentMenu.name, true  is added for suga intranet		
							
		timerRunning = true;
	}
	
	if ( !isIE() ) 
		document.captureEvents(Event.MOUSEMOVE);
	
	document.onmousemove = showLayerHandler; // use event Handler aim to capture x-coord
}

function showLayerHandler(e) {
	if ( isIE() ) 
		x = event.x;
	else 
		x = e.pageX;
		
	currentMenu.show(x);
	
}

function hideMenu(menuName) {
	document.onmousemove = hideLayerHandler;
	mouseoverOnNaviBar = false;
}

function hideLayerHandler(e) {
	if ( isIE() ) {
		x = event.clientX + document.body.scrollLeft;
		y = event.clientY + document.body.scrollTop;
	}
	else {
		x = e.pageX;
		y = e.pageY;
	}
}

