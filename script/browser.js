// version 1

function isIE() {
	return navigator.appName == "Netscape" ? false : true;
}

function isV6Later() {
	return parseInt(navigator.appVersion)>=5;	
}

function getLayerInstance(layerName) {
	
	if ( isIE())
		return document.all[layerName];
	else
		if (isV6Later())
			return document.getElementById(layerName);
		else
			return document[layerName];
}

function getObject(objName) {
	return 	getLayerInstance(objName)
}

//Added by Ken, 5-5-2004
function getLayerInstanceWithDocument(docObj, layerName) {

    if ( isIE())
        return docObj.all[layerName];
    else
        if (isV6Later())
            return docObj.getElementById(layerName);
        else
            return docObj[layerName];
}
