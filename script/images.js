var addPreloadImg = null;

function newImage(arg) {
	if (document.images) {
		rslt = new Image();
		rslt.src = arg;
		return rslt;
	}
}

function changeImages() {
	if (document.images && (preloadFlag == true)) {
		for (var i=0; i<changeImages.arguments.length; i+=2) {
			document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
		}
	}
}

function changeImagesInLayer() {
	if (document.images && (preloadFlag == true)) {
		if (isIE() ) {
			for (var i=0; i<changeImagesInLayer.arguments.length-1; i+=2) {
				document[changeImagesInLayer.arguments[i]].src = changeImagesInLayer.arguments[i+1];
			}
		}
		else {
			var j = changeImagesInLayer.arguments[changeImagesInLayer.arguments.length-1];
			var	theLayer = eval("document.layers['layer" + j + "']");
			for (var i=0; i<changeImagesInLayer.arguments.length-1; i+=2) {
				theLayer.document.images[changeImagesInLayer.arguments[i]].src = changeImagesInLayer.arguments[i+1]
			}
		}
	}
}

var preloadFlag = false;
function preloadImages() {
	if (document.images) {
		nav_01_nav_02_over = newImage("/images/nav_01-nav_02_over.gif");
		nav_02_over = newImage("/images/nav_02-over.gif");
		nav_02_nav_03_over = newImage("/images/nav_02-nav_03_over.gif");
		nav_03_over = newImage("/images/nav_03-over.gif");
		nav_03_nav_04_over = newImage("/images/nav_03-nav_04_over.gif");
		nav_04_over = newImage("/images/nav_04-over.gif");
		nav_04_nav_05_over = newImage("/images/nav_04-nav_05_over.gif");
		nav_05_over = newImage("/images/nav_05-over.gif");
		nav_05_nav_06_over = newImage("/images/nav_05-nav_06_over.gif");
		nav_06_over = newImage("/images/nav_06-over.gif");
		nav_06_nav_07_over = newImage("/images/nav_06-nav_07_over.gif");
		nav_07_over = newImage("/images/nav_07-over.gif");
		nav_07_nav_08_over = newImage("/images/nav_07-nav_08_over.gif");
		nav_08_over = newImage("/images/nav_08-over.gif");
		nav_08_nav_09_over = newImage("/images/nav_08-nav_09_over.gif");
		nav_09_over = newImage("/images/nav_09-over.gif");
		nav_09_nav_10_over = newImage("/images/nav_09-nav_10_over.gif");
		nav_10_over = newImage("/images/nav_10-over.gif");
		nav_10_nav_11_over = newImage("/images/nav_10-nav_11_over.gif");
		nav_11_over = newImage("/images/nav_11-over.gif");
		nav_11_nav_12_over = newImage("/images/nav_11-nav_12_over.gif");
		nav_12_over = newImage("/images/nav_12-over.gif");
		nav_12_nav_13_over = newImage("/images/nav_12-nav_13_over.gif");
		nav_13_over = newImage("/images/nav_13-over.gif");
		nav_13_nav_14_over = newImage("/images/nav_13-nav_14_over.gif");
		nav_14_nav_15_over = newImage("/images/nav_14-nav_15_over.gif");
		nav_14_over = newImage("/images/nav_14-over.gif");
		nav_15_over = newImage("/images/nav_15-over.gif");
 
		if (addPreloadImg != null)  {
			tempImg = new Array(addPreloadImg.length);
			for(i = 0 ; i < addPreloadImg.length; i++)
				tempImg[i] = newImage(addPreloadImg[i]);
		}

			
		preloadFlag = true;
	}
}
