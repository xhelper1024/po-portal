function isLeapYear(year) {
	return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
}

function isBigMonth(month) {
	return month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12;
}