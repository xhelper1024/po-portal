currentAlign = '';
			
function openFullPicWin(name, location, align) {			
		t = 0;
		l = 0;
		w = 500;
		h = 375;
		url = '/gallery/full_pic_h.htm';
		
		if (location == "right") {
			t = 100;
			l = 500;
		}
		else {
			t = 100;
			l = 100;
		}
		
		if (currentAlign == "") {
			currentAlign = align
		}
		
		if (align == "vertical") {
			temp = w;
			w = h;
			h = temp;
			url = '/gallery/full_pic_v.htm';
		}
		
		alignChanged = false;
		if (align != currentAlign) {
			currentAlign = align;
			alignChanged = true;
		}
		
		feature = 'width=' + w +', height=' + h + ', top=' + t + ', left=' + l +'';
		
		if (alignChanged) {
			if (fullPicWin)
				fullPicWin.close();
		}
		
		fullPicWin = window.open(url, 'windowName', feature); 
		fullPicWin.focus()
		picName = name;
}

function openFullPicWin2(imageFileName, imageFilePath, imageFileExt, title, location) {
	
	dotI = imageFileName.indexOf(".")
	
	
	if (dotI > -1) {
		
		imageFileName = imageFileName.substring(0, dotI) + "." + imageFileExt
		
		if (location == "right") {
			t = 100;
			l = 500;
		}
		else {
			t = 100;
			l = 100;
		}
		
		feature = 'width=' + 500 +', height=' + 500 + ', top=' + t + ', left=' + l +'';
		
		fullPicFile = imageFilePath + "/" + imageFileName
		fullPicWin = window.open('/gallery/full_pic.htm', 'windowName', feature); 
		

		fullPicWinTitle = title
		fullPicWin.focus()
	}
}