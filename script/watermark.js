// version 1 , 
/* different from lifepower's waterMark. 
   contentWidth is uselees in this Suga Intranet
   rightOffset and determineX() is added
*/

wmName = "waterMark"
wmX = 0
wmY = 0
contentWidth = 819
rightOffset = 280
restoreIntrv = 100

// code this function if neccessary
function determineX() {
	return innerW - rightOffset

}

// ============================================================================
wmLayer = null
wmStyle = null

function startWaterMark() {
	if(wmLayer == null)
		wmLayer = getLayerInstance(wmName)

	wmStyle = wmLayer.style ? wmLayer.style : wmLayer;
	
	setInterval("restoreXY()", restoreIntrv);
}

function restoreXY() {
	// get layer widht and height
	if (wmStyle.clip) {
		layerW = parseInt(wmStyle.clip.width)
		layerH = parseInt(wmStyle.clip.height)
	}
	else {
		layerW = parseInt(wmStyle.width)
		layerH = parseInt(wmStyle.height)
	}
	
	// get inner width and height
	if (document.body) {
		innerW = document.body.clientWidth
		innerH = document.body.clientHeight
		scrollW = document.body.scrollLeft
		scrollH = document.body.scrollTop
	}
	else {
		innerW = window.innerWidth
		innerH = window.innerHeight
		scrollW = window.pageXOffset
		scrollH = window.pageYOffset
	}
	
	wmX = determineX();
	wmY = innerH/2;
	
	wmStyle.left = wmX + scrollW
	wmStyle.top = wmY + scrollH
	
	if (scrollH > 250) 
		wmStyle.visibility = 'visible';
	else
		wmStyle.visibility = 'hidden';

}


