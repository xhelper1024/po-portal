var xmlHttp;
function createXMLHttp(){
    try{ // Firefox, Opera 8.0+, Safari
         xmlHttp=new XMLHttpRequest();

    }catch (e){ // Internet Explorer

         try{

              xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");

         }catch (e){

              try{

                   xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");

              }catch (e){

                   alert("您的浏览器不支持AJAX！");

              }

         }

    }

}
//建立主过程
 var content=document.getElementById("content");
 var tip=document.getElementById("tip");
function getPage(page){
     tip.style.display="inline";
     var search=document.searchForm.search.value;
      createXMLHttp(); //建立xmlHttp 对象
      if( xmlHttp != undefined ){  //简单检测xmlHttp是否获取为XMLHttpRequest对象,或者使用 typeof(xmlHttp) == "object" 来检测
	  var url="user_search.asp?search="+escape(search)+"&page="+page+"&t="+new Date().getTime();
  	  xmlHttp.open("get",url,true);
	  xmlHttp.send(null);	
      xmlHttp.onreadystatechange = doaction; //xmlHttp下的onreadystatechange 属性存有处理服务器响应的函数，注意，此处没有括号
      }

}
function doaction(){
     if(xmlHttp.readyState==4){ // xmlHttp下的readyState方法 4表示传送完毕
          if(xmlHttp.status==200){ // xmlHttp的status方法读取状态（服务器HTTP状态码） 200对应OK 404对应Not Found（未找到）等
               document.getElementById("tip").innerHTML=xmlHttp.responseText; //xmlHttp的responseText方法 得到读取页数据
          }else{
		  alert(xmlHttp.status);
		  }
     }
}


function deleteUser(userid){
      createXMLHttp(); //建立xmlHttp 对象
      if( xmlHttp != undefined ){  //简单检测xmlHttp是否获取为XMLHttpRequest对象,或者使用 typeof(xmlHttp) == "object" 来检测
	  var url="user_control.asp?flag_aud="+escape("1")+"&userid="+userid+"&t="+new Date().getTime();
  	  xmlHttp.open("get",url,true);
	  xmlHttp.send(null);	
      xmlHttp.onreadystatechange = doactionDelete; //xmlHttp下的onreadystatechange 属性存有处理服务器响应的函数，注意，此处没有括号
      }

}
function doactionDelete(){
     if(xmlHttp.readyState==4){ // xmlHttp下的readyState方法 4表示传送完毕
          if(xmlHttp.status==200){ // xmlHttp的status方法读取状态（服务器HTTP状态码） 200对应OK 404对应Not Found（未找到）等
               if (xmlHttp.responseText.indexOf("success")>=0){
				 alert("Delete the success!")
			   }else{
				alert("Delete failed !")   
			   }; //xmlHttp的responseText方法 得到读取页数据
          }else{
		  alert(xmlHttp.status);
		  }
     }
}

function openWindow(url){
 window.open (url, 'newwindow', 'height=400, width=600, top=0,left=0, toolbar=no,menubar=no, scrollbars=no, resizable=no,location=no, status=no target=_self');
 //window.open (""+url+"", 'newwindow', 'height=400, width=900, top=0,left=0, toolbar=no,menubar=no, scrollbars=no, resizable=no,location=no, status=no target=_self');
 }
 
function checkSelected(url,flag){
 var cbs = document.getElementsByName("user_select");
 var b = false;
 var j=0
 var rowid=0
 for(var i=0;i<cbs.length;i++){
 if(cbs[i].checked){
  rowid=cbs[i].value;
  b = true;
  j=j+1
  }
 }
 if(!b){
  if (flag==1){
  alert("Please select a user to modify!");
  }else{
   alert("Please select a user to delete!");
  }
 return false;
 }
 if(j>1){
 if (flag==1){
 alert("Can only select a user to modify!");
 }else{
 alert("Can only select a user to delete!");
 }
 return false;
 }
 if (flag==2){
 delcfm(rowid);
 }else if(flag==1){
  var url="user_control.asp?flag_aud="+escape("2")+"&userid="+rowid+"&t="+new Date().getTime();
  openWindow(url);
  }
}
function delcfm(id) {
   if (!confirm("Confirm to delete?")) {
       window.event.returnValue = false;
     }else{
	 deleteUser(id)
	 deleteRow(id);
	 }
	 
 }
function deleteRow(id){
    var table = document.getElementById("tab");
	var rowid=document.getElementById(id).rowIndex;
    table.deleteRow(rowid);
}

