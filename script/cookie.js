function alertCookie() {
	alert(document.cookie);
}

function getCookie(name) {
	result = null;
	theCookie = " " + document.cookie + ";"
	searchName = " " + name + "=";
	startOfVar = theCookie.indexOf(searchName)
	endOfVar = 0;
	
	if (startOfVar != -1) {
		startOfVar += searchName.length;
		endOfVar = theCookie.indexOf(";", startOfVar);
		result = unescape(theCookie.substring(startOfVar, endOfVar));
	}
	
	return result;
}

function setCookie(name, value, expires, path, domain, secure) {
	expStr = expires == null ? "" : ("; expires=" + expires.toGMTString())
	pathStr = path == null ? "" : ("; path=" + path)
	domainStr = domain == null ? "" : ("; domain=" + domain)
	secureStr = secure == true ? "; secure" : ""
	document.cookie = name + "=" + escape(value) + expStr + pathStr + domainStr + secureStr
}

function clearCookie() {
	threeDays = 3*24*60*60*1000;
	expDate = new Date();
	expDate.setTime(expDate.getTime() - threeDays);
	document.cookie += "name=killCookie; expires=" + expDate.toGMTString();
}