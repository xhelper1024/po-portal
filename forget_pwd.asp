﻿<%@ CodePage=65001 %>
<!--#include virtual="/b_util/user_common_include.asp"-->
<html>
<!--#include virtual="/admin/admin_util.asp"-->
<%
Dim errMsg
errMsg = ""

Function generatePassword( allowNumbers )
 NUMLOWER    = 48  ' 48 = 0
 NUMUPPER    = 57  ' 57 = 9
 LOWERBOUND  = 65  ' 65 = A
 UPPERBOUND  = 90  ' 90 = Z
 LOWERBOUND1 = 97  ' 97 = a
 UPPERBOUND1 = 122 ' 122 = z
 PASSWORD_LENGTH = 8

 ' initialize the random number generator
 Randomize()

 newPassword = ""
 count = 0
 DO UNTIL count = PASSWORD_LENGTH
  If allowNumbers Then
   pwd = Int( ( NUMUPPER - NUMLOWER ) * Rnd + NUMLOWER )
  Else
   ' generate a num between 2 and 10 ;
   ' if num > 4 create an uppercase else create lowercase
   If Int( ( 10 - 2 + 1 ) * Rnd + 2 ) > 4 Then
    pwd = Int( ( UPPERBOUND - LOWERBOUND + 1 ) * Rnd + LOWERBOUND )
   Else
    pwd = Int( ( UPPERBOUND1 - LOWERBOUND1 + 1 ) * Rnd + LOWERBOUND1 )
   End If
  End If

  newPassword = newPassword + Chr( pwd )

  count = count + 1
 Loop

 generatePassword = newPassword
End Function

if NOT isEmpty(Request.Form("frmPost")) AND trim(Request.Form("frmPost")) = "1" then

	Dim sFinalMailBody	
	Dim objMail
	Dim objConn 'Used for the Connection object
	Dim objRst 'Used for the Recordset object
	Dim sqlStr, LoginName, newPass
	set objConn = getDbConn(0)
	Set objRst = Server.CreateObject("ADODB.Recordset")

	LoginName = trim(Request.Form("txtEmail"))
	
	if isValidEmail(LoginName) then	
		sqlStr = "SELECT StaffId FROM Staff WHERE Email = '" & LoginName & "'"
		objRst.Open sqlStr, objConn, 3, 3
	
		If not objRst.EOF then
			newPass = generatePassword(false)
			sqlStr = " UPDATE Staff SET Password = HashBytes('MD5','" & newPass & "'), ModDt = getdate() WHERE StaffId = " & objRst("StaffId")
			objConn.Execute(sqlStr)
	
			sFinalMailBody = "用戶登入名稱: "&getSafeSql(LoginName)&"<br>新密碼: "&newPass             
                        On Error Resume Next
			Set objMail = Server.CreateObject("JMail.Message")
                        if err.number<> 0 then
                          response.Write(err.description)
                         response.End()
                        end if
			objMail.AddRecipient getSafeSql(LoginName)
			objMail.From = Application("gsMailFromAccount")
			objMail.Charset = "UTF-8"'邮件的文字编码为中文
          objMail.EnableCharsetTranslation = true
          objMail.ISOEncodeHeaders = false
          objMail.ContentType="text/html"
			objMail.fromName = "suga-electronics.com"
			
			'objMail.charset = "auto"
			objMail.Subject = "The user password reset"				
			objMail.HTMLBody = "<html><body><font size=""+1"" face=""Arial, Helvetica, sans-serif"">"
			objMail.appendHTML "" & sFinalMailBody & "</body></html>"
							
			if Application("gsMailUserName") <> ""	OR Application("gsMailUserPwd") <> "" then
			   objMail.MailServerUserName = Application("gsMailUserName")
			   objMail.MailServerPassword = Application("gsMailUserPwd")
			end if
			'objMail.Priority = 1 '最快发送
			objMail.Send Application("gsMailHost")				
			set objMail = Nothing
			Response.Redirect "/fpwd_success.asp"
		else
			errMsg = "输入的电邮不存在, 请重新输入"
		end if
	else
		errMsg = "输入的电邮不正确"
	end if
end if

%>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="6" bgcolor="#000096">&nbsp;</td>
    <td width="178"><img src="/images/suga_logo.gif" width="178" height="65"></td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td bgcolor="#000096" height="1"></td>
    <td colspan="2" background="/images/line_h_long_dash_20k.gif"></td>
  </tr>
</table>
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
   
   <tr>
     <td width="184" background="/images/bg_nav.gif">&nbsp;</td>
     <td><table width="100%" height="300" border="0" cellpadding="0" cellspacing="0">
         <tr>
           <td height="23" background="/images/bg_heading.gif">&nbsp;&nbsp;<span class="H1ZC"><font color="#FF7216">忘记密码</font></span></td>
         </tr>
		 <form name="frmForgotPwd" method="post" action="forget_pwd.asp">
         <tr>
           <td valign="top" class="contentNEN"><br>
		   <table width="500" border="0" cellpadding="2" cellspacing="0">
		   		<% if errMsg <> "" then %>
               <tr><td height="30" style="color:#FF0000" class="contentNZC" colspan="2"><%=errMsg%></td></tr>
			  	<% end if %>
               <tr>
                 <td height="40" colspan="2" valign="bottom" class="contentNZC"><p>请输入您的电邮，系统管理员核实资料后来尽快重设你的密码。</p></td>
               </tr>
               <tr>
                 <td height="5" colspan="2"><img src="/images/color_20k.gif" width="100%" height="1"></td>
               </tr>
               <tr>
                  <td width="80" height="20" valign="top" nowrap bgcolor="#F0F0F0" ><strong><font color="#000096">电邮</font></strong></td>
                 <td width="420" bgcolor="#F0F0F0"><p><font color="#000096">
                     <input name="txtEmail" type="text" class="textField1ZC" id="txtEmail" value="" style="width:250px">
                 </font></p></td>
               </tr>
               <tr>
                 <td height="5" colspan="2"><img src="/images/color_20k.gif" width="100%" height="1"></td>
               </tr>
               <tr>
                 <td height="44" valign="top" nowrap bgcolor="#F0F0F0" >&nbsp;</td>
                 <td bgcolor="#F0F0F0"><p><font color="#000096">
				 	<input type="hidden" name="frmPost" value="1">
                     <input name="btnSubmit" type="submit" class="button3ZC" id="btnSubmit" value="递交">
                 </font></p></td>
               </tr>
               <tr>
                 <td height="40" colspan="2" valign="bottom">&nbsp;</td>
               </tr>
           </table>
		   </td>
		 </tr>
		 </form>
         <tr>
           <td height="23">&nbsp;</td>
         </tr>
     </table></td>
   </tr>
 </table></body>
</html>
