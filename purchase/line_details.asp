﻿<%@ CodePage=65001%>
<% Option Explicit %>
<!--#include virtual="/b_util/user_common_include.asp"-->
<!--#include virtual="/b_ui/display.asp"-->
<!--#include virtual="/purchase/conf.asp"-->
<link   href="/style/form.css" type="text/css"></link >
<link   href="/style/text.css" type="text/css"></link >
<script type="text/javascript" src="/script/form.js"></script>
<html>
<%
dim ship_num,stafflevel,records
staffLevel=Session("StaffLevel")
records=session.Contents("records")
%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>View Purchase Order Line Detail</title>
<body >
<style type="text/css">
table{border-collapse:collapse;border-spacing:0;border-left:1px solid #888;border-top:1px solid #888;background:#efefef;}
th,td{border-right:1px solid #888;border-bottom:1px solid #888; font:"宋体"; font-size:14px; color:#0033FF;cborder-spacing:0px;  }
</style> 

<table width="797" border="1" cellpadding="0" cellspacing="0" align="center" >
<tr height="23"> 
                <td height="26" background="/images/bg_heading.gif" colspan="6">&nbsp;&nbsp;<span class="H1ZC">Purchase Orders&gt;&gt;Line Details</span></td>
 </tr>
  <tr>
    <td width="132" align="right" class=""><div align="right">Buyer:</div></td>
    <td width="105" >&nbsp;<%=records(0)(0)%></td>
    <td align="right" >L/T:</td>
    <td width="84" colspan="-2" >&nbsp;<%=records(0)(9)%></td>
    <td width="111" align="right">SPQ:</td>
    <td width="182">&nbsp;<%=records(0)(17)%></td>
  </tr>
  <tr>
    <td height="17" align="right"><div align="right">PO Issue Date:</div></td>
    <td>&nbsp;<%=formatDate(records(0)(1),1)%></td>
    <td align="right">Need By Date: </td>
    <td colspan="-2">&nbsp;<%=formatDate(records(0)(10),1)%></td>
    <td align="right">Vendor No.:</td>
    <td>&nbsp;<%=records(0)(18)%></td>
  </tr>
  <tr>
    <td align="right"><div align="right">PO:</div></td>
    <td>&nbsp;<%=records(0)(2)%></td>
    <td align="right">Delivery Date: </td>
    <td colspan="-2" align="left">&nbsp;<%=formatDate(records(0)(11),1)%></td>
    <td align="right">Vendor Name:</td>
    <td>&nbsp;<%=records(0)(19)%></td>
  </tr>
  <tr>
    <td align="right"><div align="right">Line:</div></td>
    <td>&nbsp;<%=records(0)(3)%></td>
    <td align="right">Currency:</td>
    <td colspan="-2">&nbsp;<%=records(0)(14)%></td>
    <td align="right">Item Description: </td>
    <td>&nbsp;<%=records(0)(20)%></td>
  </tr>
  <tr>
    <td align="right"><div align="right">Item:</div></td>
    <td>&nbsp;<%=records(0)(4)%></td>
    <td height="17"><div align="right">Supply Qty: </div></td>
    <td>&nbsp;<%=records(0)(7)%></td>
    <td align="right">Payment Terms:</td>
    <td>&nbsp;<%=records(0)(21)%></td>
  </tr>
  <tr>
    <td><div align="right">MFG: </div></td>
    <td>&nbsp;<%=records(0)(5)%></td>
    <td><div align="right">Uom:</div></td>
    <td>&nbsp;<%=records(0)(8)%></td>
    <td align="right">Delivery Terms: </td>
    <td>&nbsp;<%=records(0)(22)%></td>
  </tr>
  <tr>
    <td><div align="right">MFG P/N: </div></td>
    <td>&nbsp;<%=records(0)(6)%></td>
    <td align="right">Price:</td>
    <td>&nbsp;<%=records(0)(15)%></td>
    <td align="right">Rescheduled Date:</td>
    <td>&nbsp;<%=formatDate(records(0)(12),1)%></td>
  </tr>
  <tr>
    <td height="17" colspan="6">Remark:
      <textarea name="BUYER_COMMENT" cols="30" rows="2" readonly="readonly"><%=records(0)(13)%></textarea></td>
  </tr>
  <tr>
    <td height="73" colspan="6" align="center"> 
	  <input  type="button" name="Return"  id="Return" value="关闭"  class="button3ZC" onClick="custom_close()"> </td>
</table>

</body>
</html>