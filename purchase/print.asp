﻿<%@ CodePage=65001 %>
<% Option Explicit %>
<!--#include virtual="/b_util/user_common_include.asp"-->
<!--#include virtual="/purchase/conf.asp"-->

<% 	On Error Resume Next
	Dim i, url
	Dim errorMsgCode
	Dim recordTotal, recordArray
	errorMsgCode = Request.QueryString("err")
	If errorMsgCode <> "" and isnumeric(errorMsgCode)then
		errorMsgCode = int(errorMsgCode)
	else
		errorMsgCode = -1
	end if
	recordTotal = Session.Contents("recordTotal")
	recordArray = Session.Contents("recordArray")
	
%>

<html>
<head>
<title>Purchase Orders</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="/style/print.css" rel="stylesheet" type="text/css"></head>

<body onLoad="window.print()">
<% If recordTotal > 0 and (errorMsgCode = -1 or errorMsgCode = 0) Then %>
<table width="750" border="0" align="center" cellpadding="2" cellspacing="0">
  <tr> 
    <td  width="80" nowrap class="printH1NoPageBreak">Buyer </td>
    <td nowrap class="printH1NoPageBreak">Issue Date</td>
    <td nowrap class="printH1NoPageBreak">PO</td>
    <td nowrap class="printH1NoPageBreak">Line</td>
    <td nowrap class="printH1NoPageBreak">Item</td>
    <td nowrap class="printH1NoPageBreak">Ship# Line Qty</td>
    <td nowrap class="printH1NoPageBreak">Ship# O/S Qty</td>
    <td nowrap class="printH1NoPageBreak">Uom</td>
    <td nowrap class="printH1NoPageBreak">Need By Date </td>
    <td nowrap class="printH1NoPageBreak">Delivery Date </td>
    <td nowrap class="printH1NoPageBreak">Promised Date </td>
    <td nowrap class="printH1NoPageBreak">New Req. Date</td>
  </tr>
  <tr> 
    <td height="1" colspan="12"><img src="/images/color_20k.gif" width="100%" height="1"></td>
  </tr>
  <% 	
								For i=0 To ubound(recordArray)  %>
  <tr> 
    <td valign="top" class="printC1"> <%=recordArray(i)(0)%> </td>
    <td valign="top" nowrap class="printC1"> <%=recordArray(i)(1)%> </td>
    <td valign="top" nowrap class="printC1">  <%=recordArray(i)(2)%> </td>
    <td valign="top" nowrap class="printC1">  <%=recordArray(i)(3)%> </td>
    <td valign="top" nowrap class="printC1">  <%=recordArray(i)(4)%></td>
    <td valign="top" nowrap class="printC1"> <%=recordArray(i)(5)%> </td>
    <td valign="top" nowrap class="printC1"><%=recordArray(i)(6)%></td>
    <td valign="top" nowrap class="printC1"><%=recordArray(i)(7)%></td>
    <td valign="top" nowrap class="printC1"><%=recordArray(i)(8)%></td>
    <td valign="top" nowrap class="printC1"><%=recordArray(i)(9)%></td>
    <td valign="top" nowrap class="printC1"><%=recordArray(i)(10)%></td>
    <td valign="top" nowrap class="printC1"><%=recordArray(i)(11)%></td>
  </tr>
  <tr> 
    <td height="1" colspan="12"><img src="/images/color_20k.gif" width="100%" height="1"></td>
  </tr>
  <% Next ' Next record %>
</table>
<% end if %>
</body>
</html>
