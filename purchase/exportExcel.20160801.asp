﻿<%@ CodePage=65001 %>
<% Option Explicit %>
<!--#include virtual="/b_util/user_common_include.asp"-->
<!--#include virtual="/purchase/conf.asp"-->

<% 	on error resume next
	Dim i, url
	Dim errorMsgCode
	Dim recordTotal, recordArray
	errorMsgCode = Request.QueryString("err")
	If errorMsgCode <> "" and isnumeric(errorMsgCode)then
		errorMsgCode = int(errorMsgCode)
	else
		errorMsgCode = -1
	end if
	recordTotal = Session.Contents("recordTotal")
	recordArray = Session.Contents("recordArray")
if session("StaffLevel")=3 then
  dim fs,fo,filePath,copyFP,fileExi,fileP
  dim Connb,Driver,DBPath,sqlSt,Rses,recaffected
  set fs=Server.CreateObject("Scripting.FileSystemObject")
  filePath=Server.MapPath("/doc/SUGA.xls")
  set fo=fs.GetFile(filePath)
  fileP="/doc/SUGA" & year(date)&month(date)&day(date)& hour(now())& minute(now())&second(now())&".xls"
  copyFP = Server.MapPath(fileP)
  fo.Copy copyFP ,false
  fileExi=fs.FileExists(copyFP)
   if fileExi =false then
 end if
Set Connb = Server.CreateObject("ADODB.Connection")
Dim connString
Dim RS1
connString= "Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties=Excel 8.0;Data Source=" & copyFP

Set RS1=Server.CreateObject("ADODB.Recordset")

' [MFG],[MFG P/N] 20160517 add
RS1.ActiveConnection = connString
   For i=0 To ubound(recordArray)
   sqlSt="INSERT INTO [SUGA$]([ID], [Issue Date],[Buyer], [PO],[Line],[Item],[MFG],[MFG P/N],[Supply Qty],[Uom],[Need By Date],[Last Reply Date],[Rescheduled Date]," & _ 
  "[Delivery Date],[Remark],[Post],[Reply],[RTV]) VALUES("&recordArray(i)(17)&",'"&recordArray(i)(1)&"','"&recordArray(i)(0)&"', '"&recordArray(i)(2)&"'," & _
   ""&recordArray(i)(3)&",'"&recordArray(i)(4)&"','"&recordArray(i)(18)&"','"&recordArray(i)(16)&"'," & _
   ""&recordArray(i)(6)&",'"&recordArray(i)(7)&"','"&recordArray(i)(8)&"','"&recordArray(i)(11)&"','"&recordArray(i)(19)&"'," & _
   "'"&recordArray(i)(9)&"','"&recordArray(i)(24)&"','"&recordArray(i)(21)&"','"&recordArray(i)(23)&"','"&recordArray(i)(22)&"')"
on error resume next
RS1.Source = sqlSt 
RS1.Open()
Next
RS1.Close 
Set RS1=Nothing 
set fo=nothing
set fs=nothing
response.Redirect(fileP)
response.End()
end if
%>
<html>
<head>
<title>Purchase Orders</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<meta http-equiv="pragma" content="no-cache">  
<meta http-equiv="cache-control" content="no-cache">  
<meta http-equiv="expires" content="0"> 
</head>
<style type="text/css">
table{border-collapse:collapse;border-spacing:0;border-left:1px solid #888;border-top:1px solid #888;background:#efefef;}
th,td{border-right:1px solid #888;border-bottom:1px solid #888; font-size:14px; color:#0033FF;cborder-spacing:0px;  }
</style> 
<body >
<% If recordTotal > 0 and (errorMsgCode = -1 or errorMsgCode = 0) Then %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<table width="750" border="1" align="center" cellpadding="0" cellspacing="0">
  <tr> 
   <td>ID</td>
    <td>Buyer</td>
	<td >Issue Date</td>
    <td>PO</td>
	<td>Status</td>
    <td>Line</td>
    <td >Item</td>
	<td >MFG</td>
    <td>MFG P/N</td>
	<td>Supply Qty</td>
    <td >UOM</td>
    <td >Need By Date</td>
	<td >Promised Date</td>
    <td >Last Reply Date</td>
	<td>Rescheduled Date</td>
    <td >Delivery Date</td>
    <td >Remark</td>
    <td >Post</td>
	<td >Reply</td>
    <td >RTV</td>
	
	
  </tr>
  <% 	
 For i=0 To ubound(recordArray)  %>
  <tr>
    <td valign="top" > <%=recordArray(i)(17)%> </td>
    <td valign="top" > <%=recordArray(i)(0)%> </td>
    <td valign="top"> <%=recordArray(i)(1)%> </td>
    <td valign="top" >  <%=recordArray(i)(2)%> </td>
	<td valign="top"> <%=recordArray(i)(25)%> </td>
    <td valign="top" >  <%=recordArray(i)(3)%> </td>
    <td valign="top" >  <%=recordArray(i)(4)%></td>
   <td valign="top"> <%=recordArray(i)(18)%> </td>
    <td valign="top"><%=recordArray(i)(16)%></td>
    <td valign="top"><%=recordArray(i)(6)%></td>
    <td valign="top" ><%=recordArray(i)(7)%></td>
    <td valign="top" ><%=recordArray(i)(8)%></td>
    <td valign="top" ><%=recordArray(i)(10)%></td>
	<td valign="top"> <%=recordArray(i)(11)%></td>
	<td valign="top"><%=recordArray(i)(19)%></td>
    <td valign="top"><%=recordArray(i)(9)%></td>
	<td valign="top" ><%=recordArray(i)(24)%></td>
    <td valign="top" ><%=recordArray(i)(21)%></td>
    <td valign="top" ><%=recordArray(i)(23)%></td>
    <td valign="top"><%=recordArray(i)(22)%></td>
  </tr>
    <% Next ' Next record %>
</table>
<% end if %>
<%Response.CharSet  = "UTF-8"%>
<%response.ContentType="application/vnd.ms-excel"%>
<%response.Addheader"Content-Disposition", "attachment;Filename=SUGA" & year(date)&month(date)&day(date)& hour(now())& minute(now())&second(now())&".xls"%> 
</body>
</html>
