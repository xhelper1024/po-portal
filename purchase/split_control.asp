﻿<%@ CodePage=65001 %>
<% Option Explicit %>

<!--#include virtual="/b_util/user_common_include.asp"-->
<!--#include virtual="/purchase/conf.asp"-->
<%
On error resume next
Dim l_line_id,records ,whereClause,counter,l_flag,l_success,l_temp,conn,l_split,l_orig_line
l_line_id=request.QueryString("line_id")
l_flag=request.QueryString("l_split")
l_temp=0
l_split=request.QueryString("split_line")
l_orig_line=request.QueryString("orig_line")
if l_line_id <>"" and l_flag<>1 then
whereClause="line_id=" & l_line_id 
accessDB(whereClause)
response.Redirect("split_lines.asp?line_id="& l_line_id)
end if
if l_flag=1 then
set conn = getDbConn(1)
For counter=1 To  Request.Form("DELIVERY_DATE").Count
 if counter=1 then
 l_success=insertLine(l_line_id,Request.Form("DELIVERY_DATE")(counter),Request.Form("QTY")(counter),"Y",l_split,l_orig_line)
 else
 l_success=insertLine(l_line_id,Request.Form("DELIVERY_DATE")(counter),Request.Form("QTY")(counter),"N",l_split&"."&counter-1,l_orig_line)
 end if
 if l_success=0 then
 l_temp=l_temp+1
 end if
 Next 
 if l_temp>0 then
 response.Write("很抱歉,分Line失败" )
 else
 response.Redirect("split_success.asp")
 end if
else
response.Write("对不起，系统发生异常，请稍候再试？")
end if
sub accessDB(whereSql)
	Dim columnName(33), dbTableName,line_no
	dbTableName = "suga_v_po_summary"
	' Column To be display, corresponding to Database
	columnName(0) = "BUYER"		   'Buyer
	columnName(1) = "PO_ISSU_DATE" 'Issu Date
	columnName(2) = "PO"	    'PO
	columnName(3) = "LINE"		   'Line	
	columnName(4) = "ITEM"		   'Item
	columnName(5) = "MFG_PART_NAME"'MFG Part Name 
	columnName(6) = "MFG_PART_NUM" 'MFG Part Num
	columnName(7) = "SHIP_QTY"	   'Ship Line Qty
	columnName(8) = "SHIP_OS_QTY"  'Shipment O/S Qty
	columnName(9) = "UOM"		   'Uom
	columnName(10) ="LEAD_TIME"      'Lead Time
	columnName(11) = "NEED_BY_DATE"'Need By Date
	columnName(12)="DELIVERY_DATE" 'Delivery Date
	columnName(13)="ADD_DAYS"       'Add Days
	columnName(14)="PROMISED_DATE" 'Promised Date
	columnName(15) ="FROM_DATE"   'From Date
	columnName(16) ="TO_DATE"	   'To Date
    columnName(17)="NEW_REQ_DATE" 'New Request Date
	columnName(18) ="REMARK"       'Remak 
	columnName(19) = "CURRENCY"    'Currency
	columnName(20)="UNIT_PRICE"    'Unit Price
	columnName(21)="OS_QTY"        'O/S Qty
	columnName(22)="MOQ"           'MOQ
	columnName(23) = "SPQ"         'SPQ
	columnName(24)="VENDOR"        'Ventor Code
	columnName(25)="VENDOR_NAME"   'Ventor Name
	columnName(26)="ITEM_DESC"     'Item Description
	columnName(27)="PAYMENT_TERMS" 'Payment Terms
	columnName(28)="DELIVERY_TERMS"'Deliver terms
	columnName(29)="VENDOR_STOCK"    'Ventor Stork
	columnName(30)="VENDOR_NEW_REQ_DATE"    'Ventor Stork
	columnName(31)="PURCHASE_COMM" 'Buyer comment,
	columnName(32)="SPLIT_LINE"
	columnName(33)="ORIG_LINE"
	
	session("records")=""
	set conn = getDbConn(1) 
	records = dbQuery(conn, dbTableName, columnName, whereSql, null, null,1)
	session("records")=records
	
	conn.close()
end sub


%>

