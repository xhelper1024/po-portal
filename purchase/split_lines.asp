﻿<%@ CodePage=65001%>
<% Option Explicit %>
<!--#include virtual="/b_util/user_common_include.asp"-->
<!--#include virtual="/b_ui/display.asp"-->
<!--#include virtual="/purchase/conf.asp"-->
<!--#include virtual="/calendar/select_date.asp"-->
<link   href="/style/form.css" type="text/css"></link >
<link   href="/style/text.css" type="text/css"></link >
<script type="text/javascript" src="/script/form.js"></script>
<html>
<%
dim l_line_id,stafflevel,records
l_line_id=request.QueryString("line_id")
staffLevel=Session("StaffLevel")
records=session.Contents("records")
%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purchase Order Split Lines</title>
<body>
<script type="text/javascript"
   src="/script/jquery-1.5.2.js"></script>
  <script type="text/javascript">
  ////////添加一行、删除一行封装方法///////
  /**
   * 为table指定行添加一行
   *
   * tab 表id
   * row 行数，如：0->第一行 1->第二行 -2->倒数第二行 -1->最后一行
   * trHtml 添加行的html代码
   *
   */
   var j=0
  function addTr(tab, row, trHtml){
     //获取table最后一行 $("#tab tr:last")
     //获取table第一行 $("#tab tr").eq(0)
     //获取table倒数第二行 $("#tab tr").eq(-2)
     var $tr=$("#"+tab+" tr").eq(row+j);
     if($tr.size()==0){
        alert("指定的table id或行数不存在！");
        return;
     }
     $tr.after(trHtml);
  }
   
  function delTr(ckb){
     //获取选中的复选框，然后循环遍历删除
     var ckbs=$("input[name="+ckb+"]:checked");
     if(ckbs.size()==0){
        alert("要删除指定行，需选中要删除的行！");
        return;
     }
           ckbs.each(function(){
              $(this).parent().parent().remove();
			  if (j>1){
			   j=j-1;
			  }
           });
  }
   
  /**
   * 全选
   * 
   * allCkb 全选复选框的id
   * items 复选框的name
   */
  function allCheck(allCkb, items){
   $("#"+allCkb).click(function(){
      $('[name='+items+']:checkbox').attr("checked", this.checked );
   });
  }
   
  ////////添加一行、删除一行测试方法///////
  $(function(){
   //全选
   allCheck("allCkb", "ckb");
  });
   
  function addTr2(tab, row){
    j=j+1;
	
    var trHtml="<tr><td align='center'><input type='checkbox' name='ckb'></td>"
	trHtml=trHtml+"<td  align=center><%=records(0)(2)%></td>"
    trHtml=trHtml+"<td width='64' align='center' ><%=records(0)(3)%></td>"
    trHtml=trHtml+"<td align='center' width='215' ><%=records(0)(4)%></td>"
    trHtml=trHtml+"<td width='169' align='center' ><input type='text'  name='QTY'></td>"
    trHtml=trHtml+"<td  align='center' width='135' ><%=records(0)(20)%></td>"
    trHtml=trHtml+"<td width='94' align='center'><%=records(0)(9)%></td>"
	trHtml=trHtml+" <td align='center'><%=records(0)(11)%></td>"
    trHtml=trHtml+"<td  align='center'><input type='text' value='<%=records(0)(12)%>' name='DELIVERY_DATE' onClick='javascript:ShowCalendar(this.id)'  "
	trHtml=trHtml+"id='DELIVERY_DATE"+j+"'"
	trHtml=trHtml+" ></td></tr>";
    addTr(tab, row, trHtml);
  }
  function delTr2(){
     delTr('ckb');
  }
  
function sumQty(v_sum){
    var sum=0;
   $("input[name='QTY']").each(  
   function(){    
    sum += parseFloat($(this).val());  
   }  
 )
 if (v_sum!=sum){
  alert("对不起，订单行拆行后的总数只能等于订单行的数量！")
  return false;
 }
return true;

  };
  
  </script>
<style type="text/css">
table{border-collapse:collapse;border-spacing:0;border-left:1px solid #888;border-top:1px solid #888;background:#efefef;}
th,td{border-right:1px solid #888;border-bottom:1px solid #888; font:"宋体"; font-size:14px; color:#0033FF;cborder-spacing:0px;  }
</style> 
<form  action="split_control.asp?l_split=1&line_id=<%=l_line_id%>&split_line=<%=records(0)(32)%>&orig_line=<%=records(0)(33)%>" 
method="post" onSubmit="return sumQty(<%=records(0)(8)%>);">
<table width="100%" border="1" cellpadding="0" cellspacing="0" align="center"  id="tab">
<tr height="23"> 
                <td height="26" background="/images/bg_heading.gif" colspan="9">&nbsp;&nbsp;<span class="H1ZC">Purchase Orders&gt;&gt;Split Lines </span></td>
 </tr>
  <tr>
    <td colspan="9"  align="left"><input name="button2" type="button" onClick="addTr2('tab',2)" value="添加">
     &nbsp; <input name="button" type="button" onClick="delTr2()" value="删除"></td>
    </tr>
  <tr>
    <td  width="47"  align="center">Select</td>
    <td  width="176"  align="center">PO</td>
    <td width="64" align="center" >Line</td>
    <td align="center" width="215" >Item</td>
    <td width="169" align="center" >Quantity</td>
    <td  align="center" width="158" >Price</td>
    <td width="94" align="center">Uom</td>
    <td  width="203" align="center">Need By Date </td>
    <td  width="171" align="center">Delivery Date </td>
  </tr>
   <tr>
    <td  align="center"><input type="checkbox" name='ckb'  disabled="disabled"></td>
    <td  align="center"><%=records(0)(2)%></td>
    <td width="64" align="center" ><%=records(0)(3)%></td>
    <td align="center" width="215" ><%=records(0)(4)%></td>
    <td width="169" align="center" ><input type="text" value="<%=records(0)(8)%>" name="QTY"></td>
    <td  align="center" width="158" ><%=records(0)(20)%></td>
    <td width="94" align="center"><%=records(0)(9)%></td>
    <td align="center"><%=records(0)(11)%></td>
    <td align="center"><input type="text" value="<%=records(0)(12)%>" onClick="javascript:ShowCalendar(this.id)"  id='DELIVERY_DATE0' name="DELIVERY_DATE"></td>
   </tr>
   <tr>
     <td  colspan="9" align="right">Total Quantity:<%=records(0)(8)%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
   <tr>
    <td height="73" colspan="9" align="center">
	 
      <input type="submit" name="Submit"  id="Submit" value="提交" class="button3ZC"> &nbsp;&nbsp;&nbsp; 
	  <input  type="button" name="Return"  id="Return" value="关闭"  class="button3ZC" onClick="custom_close()">	  </td></tr>
</table>
</form>
</body>
</html>