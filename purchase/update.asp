﻿<%@ CodePage=65001%>
<% Option Explicit %>
<!--#include virtual="/b_util/user_common_include.asp"-->
<!--#include virtual="/b_ui/display.asp"-->
<!--#include virtual="/purchase/conf.asp"-->
<!--#include virtual="/calendar/select_date.asp"-->
<link   href="/style/form.css" type="text/css"></link >
<link   href="/style/text.css" type="text/css"></link >
<script type="text/javascript" src="/script/form.js"></script>
<html>
<%
dim ship_num,stafflevel,records
staffLevel=Session("StaffLevel")
records=session.Contents("records")
%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Update Purchase Order</title>
<body onLoad="initLevel(<%=staffLevel%>,'<%=records(0)(2)%>')">
<style type="text/css">
table{border-collapse:collapse;border-spacing:0;border-left:1px solid #888;border-top:1px solid #888;background:#efefef;}
th,td{border-right:1px solid #888;border-bottom:1px solid #888; font:"宋体"; font-size:14px; color:#0033FF;cborder-spacing:0px;  }
</style> 
<form  action="update_control.asp?line_id=<%=records(0)(32)%>&flag=Y" 
method="post" onSubmit="return checkUpdate();">
<table width="797" border="1" cellpadding="0" cellspacing="0" align="center" >
<tr height="23"> 
                <td height="26" background="/images/bg_heading.gif" colspan="9">&nbsp;&nbsp;<span class="H1ZC">Purchase Orders&gt;&gt;Update</span></td>
 </tr>
  <tr>
    <td width="132" align="right" class=""><div align="right">Buyer:</div></td>
    <td width="105" ><%=records(0)(0)%></td>
    <td colspan="4" align="right" >L/T:</td>
    <td width="84" ><%=records(0)(10)%></td>
    <td width="111" align="right">Price:</td>
    <td width="182"><%=records(0)(20)%></td>
  </tr>
  <tr>
    <td height="17" align="right"><div align="right">PO Issu Date:</div></td>
    <td><%=records(0)(1)%></td>
    <td colspan="4" align="right">Need By Date: </td>
    <td><%=records(0)(11)%></td>
    <td align="right">O/S Total: </td>
    <td><%=records(0)(21)%></td>
  </tr>
  <tr>
    <td align="right"><div align="right">PO:</div></td>
    <td><%=records(0)(2)%></td>
    <td colspan="4" align="right">Delivery Date: </td>
    <td align="left">
      <input type="text" name="DELIVERY_DATE" size="12"value="<%=records(0)(12)%>"  onFocus="showContent(this.id)" onBlur="checkData(this.id)" onClick="javascript:ShowCalendar(this.id)" id="DELIVERY_DATE" tabindex="1" ></td>
    <td align="right">MOQ:</td>
    <td><%=records(0)(22)%></td>
  </tr>
  <tr>
    <td align="right"><div align="right">Line:</div></td>
    <td><%=records(0)(3)%></td>
    <td colspan="4" align="right" width="200">SUGA HandingTime:</td>
    <td align="left"><input type="text" id="ADD_DAY" name="ADD_DAYS" onFocus="showContent(this.id)" onBlur="checkData(this.id)" size="12" value="<%=records(0)(13)%>" tabindex="2"></td>
    <td align="right">SPQ:</td>
    <td><%=records(0)(23)%></td>
  </tr>
  <tr>
    <td align="right"><div align="right">Item:</div></td>
    <td><%=records(0)(4)%></td>
    <td colspan="4" align="right">Promised Date:</td>
    <td align="left"><input type="text" name="PROMISED_DATE" size="12"value="<%=records(0)(14)%>"  onFocus="showContent(this.id)" onBlur="checkData(this.id)" onClick="javascript:ShowCalendar(this.id)" id="PROMISED_DATE" tabindex="3" ></td>
    <td align="right">Vendor No.:</td>
    <td><%=records(0)(24)%></td>
  </tr>
  <tr>
    <td><div align="right">MFG Part Name: </div></td>
    <td><%=records(0)(5)%></td>
    <td colspan="4" align="right">From Date:</td>
    <td><%=records(0)(15)%></td>
    <td align="right">Vendor Name:</td>
    <td><%=records(0)(25)%></td>
  </tr>
  <tr>
    <td><div align="right">MFG Part#: </div></td>
    <td><%=records(0)(6)%></td>
    <td colspan="4" align="right">To Date:</td>
    <td><%=records(0)(16)%><input type="hidden" value="<%=records(0)(16)%>" id="TO_DATE"></td>
    <td align="right">Item Description: </td>
    <td><%=records(0)(26)%></td>
  </tr>
  <tr>
    <td><div align="right">Ship Line Qty: </div></td>
    <td><%=records(0)(7)%></td>
    <td colspan="4" align="right">New Req. Date:</td>
    <td align="left"><input type="text" name="NEW_REQ_DATE" size="12" value="<%if stafflevel=1 or stafflevel=2 then%><%=records(0)(17)%>
	<%else%><%=records(0)(30)%><%end if%>" onFocus="showContent(this.id)" onBlur="checkData(this.id)"  onClick="javascript:ShowCalendar(this.id)" id="NEW_REQ_DATE" tabindex="4"></td>
    <td align="right">Payment Terms:</td>
    <td><%=records(0)(27)%></td>
  </tr>
  <tr>
    <td height="17"><div align="right">Shipment O/S Qty: </div></td>
    <td><%=records(0)(8)%></td>
    <td colspan="4" align="right">Remark:</td>
    <td><%=records(0)(18)%></td>
    <td align="right">Delivery Terms: </td>
    <td><%=records(0)(28)%></td>
  </tr>
  <tr>
    <td><div align="right">Uom:</div></td>
    <td><%=records(0)(9)%></td>
    <td colspan="4" align="right">Currency:</td>
    <td><%=records(0)(19)%></td>
    <td align="right">Vendor's Stock:</td>
    <td><label>
      <input type="text" name="STOCK" value="<%=records(0)(29)%>" size="12" id="STOCK" tabindex="5">
    </label></td>
  </tr>
   <tr id="vendor_date">   
    <td height="55" colspan="6">New Req. Date Synchronous Vendor:  
      <input name="SYNC" type="checkbox"  id="SYNC" value="Y" tabindex="6"></td>
    <td colspan="3"><label>
      Comment:<textarea name="BUYER_COMMENT" cols="30" rows="2"><%=records(0)(31)%></textarea>
    </label></td>
    </tr>
  <tr>
    <td height="73" colspan="9" align="center">
	  <input type="text" id="REMARK" value="<%=records(0)(18)%>" style="display:none" size="20">
	  <input type="text" id="TODATE" value="<%=records(0)(16)%>" style="display:none" size="12">
      <input type="submit" name="Submit"  id="Submit" value="提交" class="button3ZC"> &nbsp;&nbsp;&nbsp; 
	  <input  type="button" name="Return"  id="Return" value="关闭"  class="button3ZC" onClick="custom_close()"> </td>
</table>
</form>
</body>
</html>