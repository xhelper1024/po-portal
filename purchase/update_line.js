﻿
var xmlHttp;
//根据不同浏览器创建不同的XMLHttpRequest对象

function createXMLHttpLine(){
    try{ // Firefox, Opera 8.0+, Safari

         xmlHttp=new XMLHttpRequest();

    }catch (e){ // Internet Explorer

         try{

              xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");

         }catch (e){

              try{

                   xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");

              }catch (e){

                   alert("您的浏览器不支持AJAX！");

              }

         }

    }

}
var v_success=1
//建立主过程
function updateData(){
var line_ids=document.getElementsByName("LINEID");
var promised_dates=document.getElementsByName("PROMISEDDATE");
var new_req_dates=document.getElementsByName("NEW_REQ_DATE");
var notes=document.getElementsByName("NOTE");

		if(line_ids.length>0){
		 for(var i=0;i<line_ids.length;i++){
		 updateLines(line_ids[i].value,promised_dates[i].value,new_req_dates[i].value,notes[i].value);
		 }
		 if (i=line_ids.length){
			 alert("The page data updated, please check!")
		 }
		}	
}
function updateLines(l_line_id,l_pro_date,l_newreq_date,l_notes){
      createXMLHttpLine(); //建立xmlHttp 对象
      if( xmlHttp != undefined ){  //简单检测xmlHttp是否获取为XMLHttpRequest对象,或者使用 typeof(xmlHttp) == "object" 来检测
  xmlHttp.open("get","update_control.asp?flag=1&line_id="+l_line_id+"&PROMISED_DATE="+l_pro_date+"&NEW_REQ_DATE="+l_newreq_date+"&BUYER_COMMENT="+encodeURI(l_notes)+"&t="+new Date().getTime()+"",true);    //open() 方法需要三个参数。第一个参数定义发送请求所使用的方法（GET 还是 POST）。第二个参数规定服务器端脚本的 URL。第三个方法规定应当对请求进行异步地处理。
    xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded"); 
           xmlHttp.send(null); //send() 方法可将请求送往服务器。
           xmlHttp.onreadystatechange = doactionLines; //xmlHttp下的onreadystatechange 属性存有处理服务器响应的函数，注意，此处没有括号
	}
}
function doactionLines(){
     if(xmlHttp.readyState==4){ // xmlHttp下的readyState方法 4表示传送完毕
          if(xmlHttp.status==200){ // xmlHttp的status方法读取状态（服务器HTTP状态码） 200对应OK 404对应Not Found（未找到）等
        if (xmlHttp.responseText.indexOf("successupdate")>=0){
			v_success =1;
		}else{
		   v_success=0;
		 }
		}else{
			alert("操作失败，请稍候再试")
			}
     }
}
//同步new request date 给供应商
function syscDate(l_line_id){
		if(document.getElementById("SYC"+l_line_id+"").checked){
		 createXMLHttpLine(); //建立xmlHttp 对象
      if( xmlHttp != undefined ){  //简单检测xmlHttp是否获取为XMLHttpRequest对象,或者使用 typeof(xmlHttp) == "object" 来检测
  xmlHttp.open("get","update_control.asp?flag=2&line_id="+l_line_id+"&t="+new Date().getTime()+"",true);    //open() 方法需要三个参数。第一个参数定义发送请求所使用的方法（GET 还是 POST）。第二个参数规定服务器端脚本的 URL。第三个方法规定应当对请求进行异步地处理。
           xmlHttp.send(null); //send() 方法可将请求送往服务器。
           xmlHttp.onreadystatechange =function(){doactionSysc(l_line_id)} ; //xmlHttp下的onreadystatechange 属性存有处理服务器响应的函数，注意，此处没有括号
	     }
		}
      
}
function doactionSysc(line_id){
	var lineStr="SYC"+line_id
     if(xmlHttp.readyState==4){ // xmlHttp下的readyState方法 4表示传送完毕
          if(xmlHttp.status==200){ // xmlHttp的status方法读取状态（服务器HTTP状态码） 200对应OK 404对应Not Found（未找到）等
        if (xmlHttp.responseText.indexOf("successupdate")>=0){
			document.getElementById(lineStr).checked=false;
			alert("同步给供应商新需求成功!");
		}else{
		  alert("同步给供应商新需求失败!");
		 }
		}else{
			alert("操作失败，请稍候再试")
			}
     }
}
function updateVendorData(){
var line_ids=document.getElementsByName("LINEID");
var deli_dates=document.getElementsByName("DELIVERYDATE");
var remarks=document.getElementsByName("REMARK");
		if(line_ids.length>0){
		 for(var i=0;i<line_ids.length;i++){
	     updateVNQD(line_ids[i].value,deli_dates[i].value,remarks[i].value);
		 }
		 if (i=line_ids.length-1){
			 alert("保存成功，仅保存当前页!")
		 }
		}	
}

function updateVNQD(l_line_id,l_deli_date,l_remark){
      createXMLHttpLine(); //建立xmlHttp 对象
      if( xmlHttp != undefined ){  //简单检测xmlHttp是否获取为XMLHttpRequest对象,或者使用 typeof(xmlHttp) == "object" 来检测
  xmlHttp.open("get","update_control.asp?flag=3&line_id="+l_line_id+"&DELIVERY_DATE="+l_deli_date+"&REMARK="+encodeURI(l_remark)+"&t="+new Date().getTime()+"",true);    //open() 方法需要三个参数。第一个参数定义发送请求所使用的方法（GET 还是 POST）。第二个参数规定服务器端脚本的 URL。第三个方法规定应当对请求进行异步地处理。
    xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded"); 
           xmlHttp.send(null); //send() 方法可将请求送往服务器。
           xmlHttp.onreadystatechange = doactionLinesV; //xmlHttp下的onreadystatechange 属性存有处理服务器响应的函数，注意，此处没有括号
	}
}
function flash(l_url){
document.URL=l_url
}
function doactionLinesV(){
     if(xmlHttp.readyState==4){ // xmlHttp下的readyState方法 4表示传送完毕
          if(xmlHttp.status==200){ // xmlHttp的status方法读取状态（服务器HTTP状态码） 200对应OK 404对应Not Found（未找到）等
        if (xmlHttp.responseText.indexOf("successupdate")>=0){
			v_success =1;
		}else{
		   v_success=0;
		 }
		}else{
			alert("操作失败，请稍候再试")
			}
     }
}