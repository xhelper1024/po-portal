﻿<%

	'''''''''''''''''''''''''''''''''''''''''''''''''
	'Config variable for this page
	'''''''''''''''''''''''''''''''''''''''''''''''''
	Dim pageAllowSelect, defaultOrderColumn, defaultOrderType, defaultSearchMode, defaultRecordPerPage
	Dim cookieNameExpires, cookieName, searchElemCombi, keywordLabel, dbRecEachTime
	
	pageAllowSelect = 1			'ie. <<首頁 <上一頁 1 2 3 3 <<<pageAllowSelect>>> 下一頁> 最後一頁>> MUST BE ODD NUMBER
	defaultOrderType = "desc"
	defaultSearchMode = "v" ' v : visible, va : display with advance search, h: hidden, vah : half hidden from advance search, vh : half hidden from simple search
	cookieName = "connactCookie"
	cookieNameExpires = 365
	defaultRecordPerPage = 10
	searchElemCombi = "111000000010"
	keywordLabel = "姓名"
	dbRecEachTime = 5
	
	Dim rppOptions(3)
	rppOptions(0) = 5		' Record Per Page Options
	rppOptions(1) = 10
	rppOptions(2) = 20	
	rppOptions(3) = 50
	
	Dim ocOptions(4)
	ocOptions(0) = "StaffName"		'英文名
	ocOptions(1) = "StaffNameChi"	'中文名
	ocOptions(2) = "CompanyId"		'公司	
	ocOptions(3) = "DeptId"		'部門
	ocOptions(4) = "DistrictId"	'地區
	defaultOrderColumn = 0
	
%>