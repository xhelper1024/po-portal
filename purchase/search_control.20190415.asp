﻿<%@ CodePage=65001 %>
<% Option Explicit %>
<!--#include virtual="/b_util/user_common_include.asp"-->
<!--#include virtual="/purchase/conf.asp"-->
<!--#include virtual="/b_ui/display.asp"-->
<%
On error resume next
Dim needToAccessDB, errorMsgCode
errorMsgCode = -1
If Request.QueryString("new") = "y" then
	removeSessionValue
	getCookieValue
	setSessionVarDefault
	Response.Cookies(cookieName).expires = Date + cookieNameExpires 
	needToAccessDB = true
	if Request.QueryString("kw") <> "" then
		changeKeyword()
		accessDB(0)
	else
		accessDB(1)
	end if
	Session.Contents.Remove("promisedDate")
	Session.Contents.Remove("post")
	Session.Contents.Remove("reply")
	Session.Contents.Remove("rtv")
	if session("StaffLevel")=1 then
	Response.Redirect("/purchase/index.asp?display=y")
	elseif session("StaffLevel")=2 then
	Response.Redirect("/purchase/purchase_index.asp?display=y")
	elseif session("StaffLevel")=3 then
	Response.Redirect("/purchase/vendor_index.asp?display=y")
	end if
end if

if (Request("dar")="y") or (Request("dis")="all") then
    Session.Contents.Remove("vendor")
    Session.Contents.Remove("buyer")
    Session.Contents.Remove("PONo")
	Session.Contents.Remove("frmItem")
	Session.Contents.Remove("needByDate")
	Session.Contents.Remove("promisedDate")
	Session.Contents.Remove("MFGNo")
	Session.Contents.Remove("MFGName")
	Session.Contents.Remove("post")
	Session.Contents.Remove("reply")
	Session.Contents.Remove("rtv")
end if
Dim jumpPage
Dim orderColumn
Dim orderType
Dim searchMode
Dim recordPerPage
Dim displayAllRecord
Dim currentPage
Dim recordTotal
Dim recordArray
Dim buyer
Dim vendor
Dim frmItem
Dim PONo
Dim MFGNo
Dim MFGName
Dim post
Dim reply
Dim rtv
Dim needByDate
Dim promisedDate
Dim sessionEmpty, cookieEmpty, 	pageTotal
Dim formSubmit, qsSubmit
pageTotal = 0
getCookieValue
if cookieEmpty Then
	setCookieVarDefault
end if
checkSession
if sessionEmpty then
	setSessionVarDefault
else
	getSessionValue
	pageTotal = calculatePageTotal(recordTotal, recordPerPage)
end if
If Request.QueryString("print") = "y" then
	if arrayLength(recordArray) < recordTotal  then
		setNeedToAccessDB
		recordPerPage = recordTotal
	end if
	accessDB(2)
	response.Redirect("print.asp")
end if
If Request.QueryString("export") = "y" then
	if arrayLength(recordArray) < recordTotal  then
		setNeedToAccessDB
		recordPerPage = recordTotal
	end if
	accessDB(2)
	response.Redirect("exportExcel.asp")
end if
if changeDisplayAllRecord() then
	accessDB(1)
else
	if changeCurrentPage() then
		accessDB(2)
	else
		 if not changeSearchMode() then
			if changeRecordPerPage() then
				accessDB(2)
			else 
				changeBuyer()
				changeVendor()
				changePONo()
				changeItemFrm()
				changeneedByDate()
				changepromisedDate()
				changeMFGNo()
				changeMFGName()
				changePost()
				changeRtv()
				changeReply()
				currentPage = 1
				Session("currentPage") = 1
				accessDB(0)
			end if
		else
			accessDB(2)
		end if
	 end if	
end if
Response.Cookies(cookieName).expires = Date + cookieNameExpires
If errorMsgCode <> -1 Then
    if session("StaffLevel")=1 then
	Response.Redirect("/purchase/index.asp?display=y&err=" & errorMsgCode)
	elseif session("StaffLevel")=2 then
	Response.Redirect("/purchase/purchase_index.asp?display=y&err=" & errorMsgCode)
	elseif session("StaffLevel")=3 then
	Response.Redirect("/purchase/vendor_index.asp?display=y&err=" & errorMsgCode)
	end if
	
else
   	if session("StaffLevel")=1 then
	Response.Redirect("/purchase/index.asp?display=y")
	elseif session("StaffLevel")=2 then
	Response.Redirect("/purchase/purchase_index.asp?display=y")
	elseif session("StaffLevel")=3 then
	Response.Redirect("/purchase/vendor_index.asp?display=y")
	end if
end if
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Function and Sub implementation
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

sub setNeedToAccessDB()
	if needToAccessDB = true then
		exit sub
	end if

	needToAccessDB = true
end sub


sub setSessionVarDefault()
	displayAllRecord = "y"
	buyer=""
	vendor=""
	PONo=""
    frmItem=""
    MFGNo=""
	MFGName=""
	needByDate=""
	promisedDate=""
	post=""
	reply=""
	rtv=""
	currentPage = 1
	recordTotal = 0
	Session("displayAllRecord") = displayAllRecord
	Session("currentPage") = currentPage
	Session("recordTotal") = recordTotal
end sub

' Get value from session
sub checkSession()
	If  IsEmpty(Session.Contents("currentPage")) or _
		IsEmpty(Session.Contents("recordTotal")) _
	Then
		sessionEmpty = true
	end if
end sub


sub getSessionValue()
	post=Session.Contents("post")
	reply=Session.Contents("reply")
	rtv=Session.Contents("rtv")
	buyer=Session.Contents("buyer")
	PONo=Session.Contents("PONo")
	vendor=Session.Contents("vendor")
    frmItem=Session.Contents("frmItem")
    MFGNo=Session.Contents("MFGNo")
	MFGName=Session.Contents("MFGName")
	needByDate=Session.Contents("needByDate")
	promisedDate=Session.Contents("promisedDate")
	displayAllRecord = Session.Contents("displayAllRecord")
	currentPage = Session.Contents("currentPage")
	recordTotal = Session.Contents("recordTotal")
	recordArray = Session.Contents("recordArray")
end sub

sub getCookieValue()
	searchMode = 			Request.Cookies(cookieName)("sm")			' "v", "h", "hh", "va"
	orderType = 			Request.Cookies(cookieName)("ot")
	orderColumn =		 	Request.Cookies(cookieName)("oc")
	recordPerPage = 		Request.Cookies(cookieName)("rpp")
	
	If searchMode = "" then 	
		searchMode = defaultSearchMode 
		Response.Cookies(cookieName)("sm") = searchMode
	end if
	
	If orderType = "" then 	
		orderType = defaultOrderType 
		Response.Cookies(cookieName)("ot") = orderType
	end if
	
	If orderColumn = "" then 	
		orderColumn = defaultOrderColumn 
		Response.Cookies(cookieName)("oc") = orderColumn
	end if
	
	If recordPerPage = "" then 	
		recordPerPage = defaultRecordPerPage 
		Response.Cookies(cookieName)("rpp") = recordPerPage
	end if
	
	If isnumeric(orderColumn) then
		orderColumn = int(orderColumn)
	end if
	
	If isnumeric(recordPerPage) then
		recordPerPage = int(recordPerPage)
	end if
	
	If searchMode = "" and orderType = "" and orderColumn = "" and recordPerPage = "" Then
		cookieEmpty = true
	end if
end sub
sub accessDB(accessMode) ' accessMode-  0 : has where clause, 1 : all record,  3, changePage
	Dim columnName(28), dbTableName
	dbTableName = "suga_v_po_summary"
	' Column To be display, corresponding to Database
	columnName(0) = "BUYER"		   'Buyer
	columnName(1) = "PO_ISSU_DATE" 'Issu Date
	columnName(2) = "PO"	       'PO
	columnName(3) = "LINE"		   'Line	
	columnName(4) = "ITEM"		   'Item
	columnName(5) = "SHIP_QTY"	   'Ship Line Qty
	columnName(6) = "SHIP_OS_QTY"  'Shipment O/S Qty
	columnName(7) = "UOM"		   'Uom
	columnName(8) = "NEED_BY_DATE"'Need By Date
	columnName(9)="DELIVERY_DATE" 'Delivery Date
	columnName(10)="PROMISED_DATE" 'Promised Date
    columnName(11)="LAST_REPLY_DATE" 'New Request Date
	columnName(12)="SHIP_NUM"      'LINE No.
	columnName(13)="SELECTED"     'Selected
	columnName(14)="WHOSELECTED"     'Who elected
	columnName(15)="VENDOR"     'Venodr
	columnName(16)="MFG_PART_NUM"
	columnName(17)="LINE_ID"
	columnName(18)="MFG_PART_NAME"
	columnName(19)="RESCHEDULED_DATE"
	columnName(20)="LEAD_TIME"
	columnName(21)="POST_FLAG" 'POST TO ERP
	columnName(22)="RTV_FLAG"  'PO RETURN
	columnName(23)="REPLY_FLAG" 'REPLY FLAY 
	columnName(24)="REMARK" 'REPLY FLAY 
	columnName(25)="PO_STATUS" 
	columnName(26)="MRP_NAME" '20160517 add
	columnName(27)="VENDOR_NAME"     'Venodr Name 20160801 add
	columnName(28)="ITEM_DESC"     'Item Desc 20180611 add
	Dim conn, whereClause, top
	set conn = getDbConn(1) 
	select case accessMode
		case 0 :
			whereClause = constructWhereClause(columnName)
			If dbRecEachTime > recordPerPage then
				top = dbRecEachTime
			else
				top = recordPerPage
			end if
		case 1 : 
			whereClause = ""
			If dbRecEachTime > recordPerPage then
				top = dbRecEachTime
			else
				top = recordPerPage
			end if
		case 2 :
			
			whereClause = constructWhereClause(columnName)
			dim retrRecTotalNeeded
			retrRecTotalNeeded = currentPage * recordPerPage
			top = arraylength(recordArray)
			Do While retrRecTotalNeeded > top 
				top = top + dbRecEachTime 
			Loop
	end select 
	dim oc
	recordTotal = dbGetRecTotal(conn, dbTableName, columnName(0), whereClause) 
	recordArray = dbQuery(conn, dbTableName, columnName, whereClause, oc, orderType, top)
	
	Session("recordTotal") = recordTotal
	Session("recordArray") = recordArray
	If whereClause <> "" Then
		displayAllRecord = "n"
		Session("displayAllRecord") = displayAllRecord
	end if
	conn.close()
end sub

function constructWhereClause(columnName)
	
	Dim whereClause
	
	if trim(buyer) <> "" then
		If whereClause <> "" Then
			whereClause = whereClause & " and " & columnName(0) & "= '" & buyer & "' "
		else
			whereClause = columnName(0) & "= '" & buyer & "' "
		end if
	end if
	if trim(PONo) <> "" then
		If whereClause <> "" Then
			whereClause = whereClause & " and " & columnName(2) & "= '" & PONo & "' "
		else
			whereClause = columnName(2) & "= '" & PONo & "' "
		end if
	end if
	
	if trim(vendor) <> "" then
		If whereClause <> "" Then
			whereClause = whereClause & " and " & columnName(15) & "='" & vendor& "' "
		else
			whereClause = columnName(15) &"='" & vendor & "' "
		end if
	end if	
	if trim(frmItem) <>"" then

		If whereClause <> "" Then
			whereClause = whereClause & " and " & columnName(4) & " like '" & frmItem & "'"
		else
			whereClause = columnName(4) & " like '" & frmItem & "'"
		end if
	end if 	
	if trim(MFGNo) <>"" then
		If whereClause <> "" Then
			whereClause = whereClause & " and " & columnName(16) & "='" & MFGNo & "'" 
		else
			whereClause = columnName(16) & "='" & MFGNo & "'" 
		end if
	end if
	
	if trim(MFGName) <>"" then
		If whereClause <> "" Then
			whereClause = whereClause & " and " & columnName(18) & "='" & MFGName & "'" 
		else
			whereClause = columnName(18) & "='" & MFGName & "'" 
		end if
	end if
	
    if trim(needByDate) <>"" then
		If whereClause <> "" Then
			whereClause = whereClause & " and convert(varchar(100), " & columnName(8) & ",101)='" & formatDate( needByDate,1) & "'"
		else
			whereClause =  "convert(varchar(100), " & columnName(8) & ",101)='" & formatDate( needByDate,1) & "'"
		end if
	end if
	if trim(promisedDate) <>"" then
		If whereClause <> "" Then
			whereClause = whereClause & " and convert(varchar(100), " & columnName(10) & ",101)='" & formatDate( promisedDate,1) & "'"
		else
			whereClause =  "convert(varchar(100), " & columnName(10) & ",101)='" & formatDate( promisedDate,1) & "'"
		end if
	end if
	if trim(post) <> "" then
		If whereClause <> "" Then
			whereClause = whereClause & " and " & columnName(21) & "= '" & post & "' "
		else
			whereClause = columnName(21) & "= '" & post & "' "
		end if
	end if
	if trim(rtv) <> "" then
		If whereClause <> "" Then
			whereClause = whereClause & " and " & columnName(22) & "= '" & rtv & "' "
		else
			whereClause = columnName(22) & "= '" & rtv & "' "
		end if
	end if
	if trim(reply) <> "" then
		If whereClause <> "" Then
			whereClause = whereClause & " and " & columnName(23) & "= '" & reply & "' "
		else
			whereClause = columnName(23) & "= '" & reply & "' "
		end if
	end if
	constructWhereClause =whereClause 
end function
%>

