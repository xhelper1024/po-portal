﻿<%@ CodePage=65001 %>
<% Option Explicit %>
<!--#include virtual="/b_util/user_common_include.asp"-->
<!--#include virtual="/b_ui/display.asp"-->
<!--#include virtual="/purchase/conf.asp"-->
<link   href="/style/form.css" type="text/css"></link >
<% 	On Error Resume Next
	If Request("display") <> "y" or Session.Contents("currentPage") = "" Then
		Response.Redirect("search_control.asp?new=y")
	end if
	Dim conn, i, url
	Dim addPreloadImg
%>
<html>
<!--#include virtual="/framework/head.asp"--><body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<script type="text/javascript" src="/script/jquery-1.5.2.js"></script>
<script type="text/javascript" src="/script/update_line.js"></script>
<script type="text/javascript" src="/script/calendar.js"></script>
 <link href="/style/form.css" rel="stylesheet" type="text/css"/>
<script language="JavaScript">
radioNameNeedToHide = new Array("c", "r", "oc")
function openWindow(url,l_line_id){
 window.open (""+url+"&line_id="+escape(l_line_id)+"", 'newwindow', 'height=400, width=900, top=0,left=0, toolbar=no,menubar=no, scrollbars=no, resizable=no,location=no, status=no target=_self');
 }
var xmlHttp;
//根据不同浏览器创建不同的XMLHttpRequest对象
function createXMLHttp(){
    try{ // Firefox, Opera 8.0+, Safari
         xmlHttp=new XMLHttpRequest();

    }catch (e){ // Internet Explorer
         try{
              xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
         }catch (e){
              try{
                   xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
              }catch (e){
                   alert("您的浏览器不支持AJAX！");
              }
         }
    }
}
//建立主过程
function startXMLHttp(line_no,who,l_send){
      var selected;
	  if (line_no=="a" || line_no=="n" && line_no!="s"){
	  var checkboxs=document.getElementsByName("SELECTED");
		if(checkboxs.length>0){
		 for(var i=0;i<checkboxs.length;i++){
		   if (line_no=="a"){
		   checkboxs[i].checked=true;
		   }else{
		   checkboxs[i].checked=false;
		   }
		 }
		}
	  }else{
	   if (line_no!="s"){
	    if ((document.getElementById(""+line_no+"").checked)){
	    selected="Y";
	    }else{
	    selected="N";
	    }
	   }
	  }
	  var oldhref
      createXMLHttp(); //建立xmlHttp 对象
	  if( xmlHttp != undefined ){  //简单检测xmlHttp是否获取为XMLHttpRequest对象,或者使用 typeof(xmlHttp) == "object" 来检测
		   xmlHttp.open("get","updateSelected.asp?selected=" + escape(selected) + "&who=" +escape(who)+ "&line_no="+ escape(line_no)+"&t="+new Date().getTime()+"",true); 
		      //open() 方法需要三个参数。第一个参数定义发送请求所使用的方法（GET 还是 POST）。第二个参数规定服务器端脚本的 URL。第三个方法规定应当对请求进行异步地处理。
		  xmlHttp.send(null); //send() 方法可将请求送往服务器。
		  if (line_no=="s"){
		      if (l_send==1){
			  oldhref=document.getElementById("Send").href
			  document.getElementById("Send").href="JavaScript:void(0)";
			  }else if(l_send==2){
			  oldhref=document.getElementById("Send1").href
			  document.getElementById("Send1").href="JavaScript:void(0)";
			  }
		      xmlHttp.onreadystatechange = doaction; //xmlHttp下的onreadystatechange 属性存有处理服务器响应的函数，注意，此处没有括号
			   if (l_send==1){
			  document.getElementById("Send").href=oldhref;
			  }else if(l_send==2){
			  document.getElementById("Send1").href=oldhref;
			  }
		  }
      }
}
function doaction(){
     if(xmlHttp.readyState==4){ // xmlHttp下的readyState方法 4表示传送完毕
		  if(xmlHttp.status==200){ // xmlHttp的status方法读取状态（服务器HTTP状态码） 200对应OK 404对应Not Found（未找到）等
			 var checkboxs=document.getElementsByName("SELECTED");
			  if (xmlHttp.responseText.indexOf("success")>=0){
			   if(checkboxs.length>0){
		        for(var i=0;i<checkboxs.length;i++){
		        checkboxs[i].checked=false;
		        }
		       }
			  alert("已导入成功，请按刷新图标！")
			  }else if(xmlHttp.responseText.indexOf("NR")>=0){
			  alert("请选中记录！")
			  }else{
			  alert("已导入失败！")
			  } //xmlHttp的responseText方法 得到读取页数据
          }else{
		  //alert(xmlHttp.status)
		  }

     }
}

 </script>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr><td colspan="2"><!--#include virtual="/framework/top_nav.asp"--><td></tr>
<tr><td colspan="2"><!--#include virtual="/framework/banner.asp"--></td></tr>
  <tr>
    <!--#include virtual="/framework/left_nav.asp"-->
    <td valign="top"><table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" >
        <tr>  
		 <%	
					Dim orderColumn, recordPerPage,staffLevel,staffid,checkdisplay,l_color,l_color2
					Dim orderType, searchMode, errorMsgCode, displayAllRecord, currentPage, keyword
					Dim pageTotal, recordTotal, recordArray,ll_post,ll_disable
					errorMsgCode = Request.QueryString("err")
					If errorMsgCode <> "" and isnumeric(errorMsgCode)then
						errorMsgCode = int(errorMsgCode)
					else
						errorMsgCode = -1
					end if
					displayAllRecord = Session.Contents("displayAllRecord")
					currentPage = Session.Contents("currentPage")
					recordTotal = Session.Contents("recordTotal")
					recordArray = Session.Contents("recordArray")
					searchMode = 			Request.Cookies(cookieName)("sm")
					orderType = 			Request.Cookies(cookieName)("ot")
					orderColumn =		 	Request.Cookies(cookieName)("oc")
					recordPerPage = 		Request.Cookies(cookieName)("rpp")
					staffLevel=Session("StaffLevel")
					staffid=Session("loginStaffId")
					If isNumeric(orderColumn) Then
						orderColumn = int(orderColumn)
					end if
					If isNumeric(recordPerPage) Then
						recordPerPage = int(recordPerPage)
					end if
					pageTotal = calculatePageTotal(recordTotal, recordPerPage)
					if currentPage > pageTotal then
						currentPage = pageTotal 
					end if
				%>
          <td height="350" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" >
              <tr> 
                <td height="22" background="/images/bg_heading.gif">&nbsp;&nbsp;<span class="H1ZC">Purchase Orders&nbsp;&nbsp;</span></td>
              </tr>
              <tr> 
                <td valign="top"  id="contentID">
                    <% If Err.Number <> 0 Then %>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td height="200" align="center" class="contentNZC">系统发生错误！请联络系统管理员！<BR> 
                          <%=Err.Source   %>
                          <br> 
                          <%=Err.Description%></td>
                      </tr>
                    </table>
                    <% else %>
					<form action="search_control.asp" method="post" name="searchForm" id="searchForm">
					<!-- #include virtual="/b_ui/search_option.asp" -->
						<% displaySearch false, "#F4E5E5" %>
                    <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" overflow:scroll>
					<% If recordTotal > 0 and (errorMsgCode = -1 or errorMsgCode = 0) Then %>
                      <tr> 
                        <td height="40" colspan="29" valign="top"><table border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <% if searchMode = "h" Then
										%>
                              <td width="91"><a href="search_control.asp?sm=v"><img src="/images/btn_search.gif" width="81" height="24" border="0"></a></td>
                              <%	end if %>
                              <%if displayAllRecord <> "y" Then
										%>
                              <td width="91"><a href="search_control.asp?dar=y"><img src="/images/btn_show_all.gif" width="81" height="24" border="0"></a></td>
                              <% end if %>
                              <td colspan="4"><img src="/images/btn_each_pg_display.gif" width="74" height="24"></td>
                              <td align="right" background="/images/btn_bg_1.gif"> 
                                <select name="rpp" class="textField1ZC" id="rpp" onChange="this.form.submit()">
                                  <% 
											Dim tempRPP
											For each tempRPP in rppOptions
												If tempRPP = recordPerPage Then
										%>
                                  <option value="<%=tempRPP%>" selected> 
                                  <%=tempRPP %> 份表格</option>
                                  <% else %>
                                  <option value="<%=tempRPP%>"> 
                                  <%=tempRPP %> 份表格</option>
                                  <% 	End if
										 	Next%>
                                </select></td>
                              <td width="17"><img src="/images/btn_bg_2.gif" width="7" height="24"></td>
                              <td width="79"><a href="search_control.asp?print=y" target="_blank"><img src="/images/btn_print.gif" width="69" height="24" border="0"></a></td>
                              <td width="91"><a href="search_control.asp?export=y" target="_blank"><img src="/images/btn_excel.gif" width="81" height="24" border="0"></a></td>
                              <td>备注:日期格式:月/日/年</td>
                            </tr>
                          </table> </td>
                      </tr>
                      <%If pageTotal > 1 Then %>
                      <tr> 
                        <td height="20" colspan="12" >
						<!-- #include virtual="/b_ui/jump_page.asp" -->
						<% displayJumpPage false, "search_control.asp"%></td>
					     <% if staffLevel=2 then%>
						 <td height="20" colspan="7" >&nbsp;</td>
						 <%end if%>
					    <td width="100" height="20" >&nbsp;</td>
					    <td width="100" height="20" >&nbsp;</td>
					    <td width="99" height="20" >&nbsp;</td>
					    <td width="105" height="20" colspan="3" >&nbsp;</td>
					    <td width="1" >&nbsp;</td>
					    <td width="1" >&nbsp;</td>
					    <td width="1" >&nbsp;</td>
					    <td width="82" height="20" >&nbsp;</td>
                      </tr>
					  <%else%>
					   <tr> 
					      <% if staffLevel=2 then%>
                        <td height="20" colspan="17" ><img  src="/images/flash.jpg" onClick="flash('search_control.asp?cp=1')"  alt="在完成操作后，请点击刷新"/>&nbsp;&nbsp;</td>
						<%end if%>
						<td height="20" >&nbsp;</td>
					    <td width="68" height="20" >&nbsp;</td>
					    <td height="20" >&nbsp;</td>
					    <td height="20" >&nbsp;</td>
					    <td height="20" >&nbsp;</td>
					    <td height="20" colspan="3" >&nbsp;</td>
					    <td height="20" >&nbsp;</td>
					    <td height="20" >&nbsp;</td>
					    <td height="20" >&nbsp;</td>
					    <td height="20" >&nbsp;</td>
                      </tr>
                      <%end if 'If pageTotal > 1 Then %>
					  <tr><td width="6" height="2" nowrap bgcolor="#F0F0F0" class="contentNZC">&nbsp;</td>
					   <% if staffLevel=2 then%>
					    <td nowrap bgcolor="#F0F0F0" class="contentNZC" width="57"><font color="#000096"><strong>Selected</strong></font></td>
					   <%end if%>
                        <td nowrap bgcolor="#F0F0F0" class="contentNZC" width="68"><font color="#000096"><strong>Buyer</strong></font></td>
                        <td nowrap bgcolor="#F0F0F0" class="contentNZC" width="75"><font color="#000096"><strong>Issue Date</strong></font></td>
                        <td width="80" colspan="2" nowrap bgcolor="#F0F0F0" class="contentNZC"><font color="#000096"><strong>PO</strong></font></td>
                        <td width="40" colspan="4" nowrap bgcolor="#F0F0F0" class="contentNZC"><font color="#000096"><strong>Line</strong></font></td>
                        <td width="100" colspan="3" nowrap bgcolor="#F0F0F0" class="contentNZC"><font color="#000096"><strong>Item</strong></font></td>
                        <td width="40" colspan="2" nowrap bgcolor="#F0F0F0" class="contentNZC"><font color="#000096"><strong>MFG</strong></font></td>
                        <td colspan="2" nowrap bgcolor="#F0F0F0" class="contentNZC"><font color="#000096"><strong>MFG P/N</strong></font></td>
                        <td nowrap bgcolor="#F0F0F0" class="contentNZC" width="80"><font color="#000096"><strong>Supply Qty</strong></font></td>
                        <td nowrap bgcolor="#F0F0F0" class="contentNZC"><font color="#000096"><strong>UOM</strong></font></td>
                        <td nowrap bgcolor="#F0F0F0" class="contentNZC" width="100"><font color="#000096"><strong>Need By Date</strong></font></td>
						 <td nowrap bgcolor="#F0F0F0" class="contentNZC" width="100"><font color="#000096"><strong>Promised Date</strong></font></td>
                        <td nowrap bgcolor="#F0F0F0" class="contentNZC" width="99"><font color="#000096"><strong>Last Reply Date</strong></font> </td>
                        <td nowrap bgcolor="#F0F0F0" class="contentNZC" width="105"><font color="#000096"><strong>Rescheduled Date</strong></font></td>
                        <td nowrap bgcolor="#F0F0F0" class="contentNZC" width="95"><font color="#000096"><strong>Delivery Date</strong></font></td>
						<td nowrap bgcolor="#F0F0F0" class="contentNZC" width="95"><strong><font color="#000096">Remark</font></strong></td>
						<td nowrap bgcolor="#F0F0F0" class="contentNZC"><font color="#000096"><strong>Post</strong></font></td>
						<td nowrap bgcolor="#F0F0F0" class="contentNZC"><font color="#000096"><strong>Reply</strong></font></td>
						<td nowrap bgcolor="#F0F0F0" class="contentNZC"><font color="#000096"><strong>RTV</strong></font></td>
						<td nowrap bgcolor="#F0F0F0" class="contentNZC"><font color="#000096"><strong>View</strong></font></td>
					    <% 	Dim startI, endI
								startI = (currentPage - 1) * recordPerPage
								If currentPage = pageTotal Then
									endI = recordTotal-1
								else
									endI = startI + recordPerPage-1
								end if
								For i=startI To endI%>
                      <tr> 
                        <td width="6" height="2" nowrapalign="center" valign="top" class="contentNZC"><img src="/images/dot_orange_2.gif" width="5" height="10"></td>      <% if staffLevel=2 then%>
                        <td valign="top" nowrap class="contentNZC" width="57">
						<%ll_post=trim(recordArray(i)(21))
						 if ll_post="Y" then
						 ll_disable="disabled=""disabled"""
						 else
						 ll_disable=""
						 end if
						 %>
						<% if(trim(recordArray(i)(13)))="Y" then
						'checkdisplay="checked"
						%>
						  <input type="checkbox" name="SELECTED"  <%=ll_disable%> id="<%=recordArray(i)(17)%>"  checked="checked"  onClick="javascript:startXMLHttp(<%=recordArray(i)(17)%>,<%=staffid%>,0)">     
						  <%
						  else%>
						  <input type="checkbox" name="SELECTED"  <%=ll_disable%> id="<%=recordArray(i)(17)%>"  onClick="javascript:startXMLHttp(<%=recordArray(i)(17)%>,<%=staffid%>,0)">     
						 <% end if%></td>
						<%end if%>
						<% if  IsBlank(recordArray(i)(8))=false and IsBlank(recordArray(i)(9))=false then
						if recordArray(i)(8)=recordArray(i)(9) then
						l_color=""
						else
						l_color="#FF0000"
						end if
						else
						l_color=""
						end if
						if  IsBlank(recordArray(i)(20))=false and  IsBlank(recordArray(i)(9))=false then
						if DateAdd("d",recordArray(i)(20),CDate(recordArray(i)(9)))=<now() then
					    l_color2=""
						else
						l_color2="#CCCCCC"
						end if
						else
						 l_color2=""
						end if
						if  IsBlank(recordArray(i)(8))=false and IsBlank(recordArray(i)(9))=false and IsBlank(recordArray(i)(20))=false and  IsBlank(recordArray(i)(9))=false then
						if recordArray(i)(8)<>recordArray(i)(9) and DateAdd("d",recordArray(i)(20),CDate(recordArray(i)(9)))>now() then
						l_color2=""
						l_color="#0033FF"
						end if
						end if
						%>
                        <td valign="top" nowrap class="contentNZC" width="68"><% if cstr(recordArray(i)(0)) = "" then %><% else %><%=recordArray(i)(0)%><% end if %></td>
                        <td valign="top" nowrap class="contentNZC" width="75"><% if cstr(recordArray(i)(1)) = "" then %><% else %><%=formatDate(recordArray(i)(1),1)%><% end if %></td>
                        <td width="80" colspan="2" valign="top" nowrap class="contentNZC"><% if cstr(recordArray(i)(2)) = "" then %><% else %><%=recordArray(i)(2)%><% end if %></td>
                        <td width="40" colspan="4" valign="top" nowrap class="contentNZC"><% if cstr(recordArray(i)(3)) = "" then %><% else %><%=recordArray(i)(3)%><% end if %></td>
                        <td width="100" colspan="3" valign="top" nowrap class="contentNZC"><% if cstr(recordArray(i)(4)) = "" then %><% else %><%=recordArray(i)(4)%><% end if %></td>
                        <td width="40" colspan="2" valign="top" nowrap class="contentNZC"><%=recordArray(i)(18)%> </td>
                        <td colspan="2" valign="top" nowrap class="contentNZC"><%=recordArray(i)(16)%> </td>
                        <td valign="top" nowrap class="contentNZC" width="80"><% if cstr(recordArray(i)(6)) = "" then %><% else %><%=recordArray(i)(6)%><% end if %></td>
                        <td valign="top" nowrap class="contentNZC" ><% if cstr(recordArray(i)(7)) = "" then %><% else %><%=recordArray(i)(7)%><% end if %></td>
                        <td valign="top" nowrap class="contentNZC"><% if cstr(recordArray(i)(8)) = "" then %><% else %><%=formatDate(recordArray(i)(8),1)%><% end if %></td>
                        <td valign="top" nowrap class="contentNZC" style=" color:<%=l_color%> ; background:<%=l_color2%>"><%=formatDate(recordArray(i)(10),1)%></td>
                        <td valign="top" nowrap class="contentNZC"><% if cstr(recordArray(i)(11)) = "" then %><% else %><%=formatDate(recordArray(i)(11),1)%><% end if %><input type="text" name="LINEID" value="<%=recordArray(i)(17)%>" style="display:none"></td>				
						 <td valign="top" nowrap class="contentNZC" style=" color:<%=l_color%> ; background:<%=l_color2%>"><% if cstr(recordArray(i)(19)) = "" then %><% else %><%=formatDate(recordArray(i)(19),1)%><% end if %></td>
						 <td valign="top" nowrap class="contentNZC" style=" color:<%=l_color%> ; background:<%=l_color2%>"><% if cstr(recordArray(i)(9)) = "" then %>
						     <% else %>
						   <%=formatDate(recordArray(i)(9),1)%>
						   <% end if %></td>
						  <td valign="top" nowrap class="contentNZC"><% if cstr(recordArray(i)(24)) = "" then %><% else %><%=recordArray(i)(24)%><% end if %></td>
						  <td valign="top" nowrap class="contentNZC"><% if cstr(recordArray(i)(21)) = "" then %><% else %><%=recordArray(i)(21)%><% end if %></td>
						  <td valign="top" nowrap class="contentNZC"><% if cstr(recordArray(i)(22)) = "" then %><% else %><%=recordArray(i)(23)%><% end if %></td>
						  <td valign="top" nowrap class="contentNZC"><% if cstr(recordArray(i)(23)) = "" then %><% else %><%=recordArray(i)(22)%><% end if %></td>
						  <td valign="top" nowrap class="contentNZC"><img   alt="view purchase order Line detail"src="/images/view.jpg" height="22" onClick="openWindow('view_control.asp?search=Y','<%=recordArray(i)(17)%>')"></td>
                      </tr>
					   <tr> 
                        <td height="1" colspan="31"><img src="/images/color_20k.gif" width="100%" height="1"></td>
                      </tr>
					  <% Next ' Next record %>
					   <%If pageTotal > 1 Then %>
                      <tr> 
						<td height="20" colspan="12" >
						<% displayJumpPage false, "search_control.asp"%></td>
                        <td height="20" colspan="2" >&nbsp;</td>
                        <td >&nbsp;</td>
                        <td width="60" height="20" >&nbsp;</td>
                        <td width="75" height="20" >&nbsp;</td>
                        <td height="20" >&nbsp;</td>
                        <td height="20" >&nbsp;</td>
                        <td height="20" >&nbsp;</td>
                        <td height="20" >&nbsp;</td>
                        <td height="20" >&nbsp;</td>
                        <td >&nbsp;</td>
                        <td height="20" >&nbsp;</td>
                        <td height="20" >&nbsp;</td>
                        <td height="20" >&nbsp;</td>
                        <td height="20" >&nbsp;</td>
                        <td height="20" >&nbsp;</td>
                        <td height="20" >&nbsp;</td>
                        <% end if 'If pageTotal > 1 Then %>
                      </tr>
                    </table>
					 <% else ' else of If recordTotal > 0 Then %>
                          <table width="100%" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#FFF2CF">
                            <tr> 
                              <td height="80" align="center" valign="bottom" class="contentNZC"><p>&nbsp;</p>
                                <p><font color="#FF0000">找不到你要搜寻的资料</font></p>
                                <% if errorMsgCode > 0 then %>
									<p>错误：<%=commonErrMsg(errorMsgCode)%></p>
									<% end if %>
                                <p><font color="#FF0000">请重新选择，或</font></p>
                                <p><font color="#FF0000"><a href="search_control.asp?dar=y"><img src="/images/btn_show_all.gif" width="81" height="24" border="0"></a><br>
                                  <br>
                                  </font></p></td>
                            </tr>
                          </table>
					 <% end if 'End of If recordTotal > 0 Then %>								
				  </form>
							<%end if ' end of err.number <> 0%> </td>
              </tr>
            </table></td>
        </tr>
        <!--#include virtual="/framework/foot_nav.asp"-->
      </table></td>
  </tr>
  <tr><td colspan="2"><!--#include virtual="/framework/footing.asp"--></td></tr>
</table>
</body>
</html>
