﻿<%@ CodePage=65001%>
<% Option Explicit %>
<!--#include virtual="/b_util/upload_5xsoft.inc"-->
<!--#include virtual="/b_util/user_common_include.asp"-->
<!--#include virtual="/purchase/conf.asp"-->

<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bulk Update Purchase Order</title>
<head>
<script type="text/javascript" >
var xmlHttp;
//根据不同浏览器创建不同的XMLHttpRequest对象
function createXMLHttpLine(){
    try{ // Firefox, Opera 8.0+, Safari

         xmlHttp=new XMLHttpRequest();

    }catch (e){ // Internet Explorer
         try{
              xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
         }catch (e){
              try{
                   xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");

              }catch (e){

                   alert("您的浏览器不支持AJAX！");
              }
         }
    }
}

function updateLines(err,filepath){
var dp=document.getElementById("divProgress");
     if (err==1){
	 dp.style.display="none";
	 var target=document.getElementById("deadly");
     target.style.display="";
	 return false;
	 }
     createXMLHttpLine(); //建立xmlHttp 对象
      if( xmlHttp != undefined ){  //简单检测xmlHttp是否获取为XMLHttpRequest对象,或者使用 typeof(xmlHttp) == "object" 来检测
    xmlHttp.open("get","bulkupdate_lines_control.asp?err="+err+"&filepath="+filepath+"&t="+new Date().getTime()+"",true);    //open() 方法需要三个参数。第一个参数定义发送请求所使用    的方法（GET 还是 POST）。第二个参数规定服务器端脚本的 URL。第三个方法规定应当对请求进行异步地处理。
    xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded"); 
           xmlHttp.send(null); //send() 方法可将请求送往服务器。
           xmlHttp.onreadystatechange = doactionLines; //xmlHttp下的onreadystatechange 属性存有处理服务器响应的函数，注意，此处没有括号
	}
}
function doactionLines(){
     if(xmlHttp.readyState==4){ // xmlHttp下的readyState方法 4表示传送完毕
          if(xmlHttp.status==200){ // xmlHttp的status方法读取状态（服务器HTTP状态码） 200对应OK 404对应Not Found（未找到）等
          var dp1=document.getElementById("divProgress");
		  var msg
		  if (xmlHttp.responseText.indexOf("errs")>0){
		   dp1.style.display="none";
		   msg=document.getElementById("errs");
		   msg.style.display="";
		  }else if(xmlHttp.responseText.indexOf("success")>0){
		  dp1.style.display="none";
		   msg=document.getElementById("su");
		   msg.style.display="";
		  }else{
		  dp1.style.display="none";
		  msg=document.getElementById("fa");
		  msg.style.display="";
		  var str=xmlHttp.responseText;
		  document.getElementById("ids").innerText="失败记录ID:"+str.substr(str.lastIndexOf(':')+1,str.length);
		  }
		}else{
			dp.style.display="none";
	    var targ=document.getElementById("deadly");
          targ.style.display="";
			}
     }
}
</script>
</head>
<body>
<div id="divProgress" align="center"> 
<img src="/images/ajax-loader.gif" width="202"></div> 
<div align="left" id="deadly" style="display:none">
<table width="331" border="0" cellpadding="0" cellspacing="0" align="left" >
<tr height="132"><td><img src="/images/deadly.jpg" ></td>
 </tr>
</table>
</div>
<%
On Error Resume Next
dim upload,file,filepath
dim corr
set upload=new upload_5xsoft
set file=upload.file("filename")
if file.fileSize>0 then
file.saveAs Server.mappath("/doc/"&file.FileName)
filepath="\\doc\\"&file.FileName
end if
set file=nothing
set upload=nothing
if err.number<>0 then
response.Write(err.number & ":" & err.description)
response.end
'response.Write("<script type='text/javascript'>updateLines(1,'"&filepath&"')</script>")
else
response.Write("<script type='text/javascript'>updateLines(0,'"&filepath&"')</script>")
end if
%>
<div align="center" id="su" style="display:none">
<table width="331" border="0" cellpadding="0" cellspacing="0" align="center" >
<tr height="132"><td><img src="/images/success.jpg"></td>
 </tr>
</table>
</div>
<div align="left" id="fa" style="display:none">
<table width="331" border="0" cellpadding="0" cellspacing="0" align="center" >
<tr height="132"><td><img src="/images/error.jpg" height="132"></td>
<tr><td><span id="ids" style=" color:#FF0000"></span><td><tr>
 </tr>
</table>
</div>
<div align="left" id="errs" style="display:none">
<table width="331" border="0" cellpadding="0" cellspacing="0" align="left" >
<tr height="132"><td><img src="/images/exception.jpg"></td>
 </tr>
</table>
</div>
</body>
</html>