# PO Portal 项目安装说明
### 安装项目
1. 解压PO Portal.zip文件
2. 在服务器Host文件中加入以下两行，并保存：
```
	192.168.3.39 qasapps.suga.com.cn
	192.168.3.40 prodapps.suga.com.cn
```
3. 如果没有启用IIS请先启用服务器的IIS服务，如果不会，请参考下文：
	[Windows Server 2008开启IIS](https://blog.csdn.net/zhengjiafa/article/details/103929088)
4. 在解压出来的文件根目录下找到以下两个文件
```
	suga_t_label_qrcodes.sql
	suga_t_label_records.sql
```
5. 将上面两个文件运行到服务器的SQL Server数据库中，会在现有数据库中生成以下两张表：
```
	suga_t_label_qrcodes
	suga_t_label_records
```
6. 将项目部署到IIS服务上，指定端口、地址等等，项目就可以正常使用

### 注意事项
1. 根目录下`global.asa`文件中，可修改下列参数
```
	API_URL: 表示调用哪个端(201或800)的接口

	API_UNAME: 表示登录对应SAP端的账号名称

	API_PWORD: 表示登录对应SAP端的密码
```
