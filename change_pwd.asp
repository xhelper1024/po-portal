<%@ CodePage=65001 %>
<!--#include virtual="/b_util/user_common_include.asp"-->
<!--#include virtual="/admin/admin_util.asp"-->
<html>
<script type="text/javascript" src="/script/form.js"></script>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="preloadImages();">
<% 
Dim uid,upwd
uid=session("loginStaffId")
upwd=session("Password")
%>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top"><table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td height="350" valign="top"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>  
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td height="23" background="/images/bg_heading.gif">&nbsp;&nbsp;<span class="H1ZC">更改密码</span></td>
                    </tr>
                    <tr> 
                      <td height="250" valign="top" class="contentNZC"><br>
					   <form name="changePwd" method="post" action="user_control.asp" onSubmit=" return check(<%=uid%>);">
                        <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" >
						  <tr><td colspan="3" style="color:#FF0000"><%=errMsg%></td></tr>
                          <tr>
                            <td width="80" height="20" nowrap bgcolor="#F0F0F0">&nbsp;</td>
                            <td width="265" nowrap bgcolor="#F0F0F0"><font color="#000096">
                              <strong>旧&nbsp;&nbsp;&nbsp;&nbsp;密&nbsp;&nbsp;&nbsp;&nbsp;码:</strong>
                              <input name="oldPassword" type="password" class="textField1ZC" id="oldPassword"   onFocus="showContent(this.id)" 
							   onBlur="checkChangePwd(this.id,<%=uid%>)">
                            </font></td>
                            <td width="315" nowrap bgcolor="#F0F0F0" align="left"><label id="oldPwd" style=" color:#FF0000; font-size:12px"></label></td>
                          </tr>
                          <tr>
                            <td height="5" colspan="3"><img src="/images/color_20k.gif" width="100%" height="1"></td>
                          </tr>
                          <tr>
                            <td height="20" nowrap bgcolor="#F0F0F0" >&nbsp;</td>
                            <td nowrap bgcolor="#F0F0F0"><font color="#000096">
                              <strong>新&nbsp;&nbsp;&nbsp;&nbsp;密&nbsp;&nbsp;&nbsp;&nbsp;码:</strong>
                              <input name="newPassword1" type="password" class="textField1ZC" id="newPassword1"  onBlur="checkChangePwd(this.id,'')"  onFocus="showContent(this.id)">
                            </font></td>
                            <td nowrap bgcolor="#F0F0F0" align="left"><label id="newPwd1" style=" color:#FF0000; font-size:12px"></label></td>
                          </tr>
                          <tr>
                            <td height="5" colspan="3"><img src="/images/color_20k.gif" width="100%" height="1"></td>
                          </tr>
                          <tr>
                            <td height="20" nowrap bgcolor="#F0F0F0" >&nbsp;</td>
                            <td nowrap bgcolor="#F0F0F0"><font color="#000096">
                              <strong>新密码确认:</strong>
                              <input name="newPassword2" type="password" class="textField1ZC" id="newPassword2"  onBlur="checkChangePwd(this.id,'')"  onFocus="showContent(this.id)">
                            </font></td>
                            <td nowrap bgcolor="#F0F0F0" align="left"><label id="newPwd2" style=" color:#FF0000; font-size:12px;"></label></td>
                          </tr>
                          <tr>
                            <td height="5" colspan="3"><img src="/images/color_20k.gif" width="100%" height="1"></td>
                          </tr>
                          <tr>
                            <td height="40" colspan="3" valign="bottom" align="center"><font color="#000096">
                              <input name="提交" type="submit" class="button3ZC"  value="更改">
                            </font></td>
                          </tr>
                        </table>
                        </form>
                        </td>
                    </tr>
                  </table></td> 
                <td width="1" background="/images/line_v_long_dash_20k.gif"></td>
               
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
               
              <tr> 
                <td background="/images/bg_content_foot_2.gif">&nbsp;</td>
                <td width="176"><img src="/images/bg_r_nav_foot_2.gif" width="176" height="45"></td>
               </tr>
            </table>
            <br></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>