﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>添加用户</title>
<style type="text/css">
table{border-collapse:collapse;border-spacing:0;border-left:1px solid #888;border-top:1px solid #888;background:#efefef;}
th,td{border-right:1px solid #888;border-bottom:1px solid #888; font:"宋体"; font-size:14px; color:#0033FF;cborder-spacing:0px;  }
</style> 
<script type="text/javascript" src="/script/addUser.js"></script>
</head>
<body topmargin="10" leftmargin="0">
<form  name="addUser" method="post" action="/admin/user/user_control.asp?flag_aud=4" onsubmit="return submitCheckUser()">
<table width="556" border="1" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td>用户管理&gt;&gt;添加用户</td>
  </tr>
  <tr>
    <td>中文名*:
      <input type="text" name="name_cn" id="name_cn" onfocus="showDesc(this)" onblur="checkText(this)"/><label id="namecn" style=" color:#FF0000; font-size:12px"></label></td>
    </tr>
  <tr>
    <td>英文名*:
      <input type="text" name="name_en" id="name_en" onfocus="showDesc(this)" onblur="checkText(this)" /><label id="nameen" style=" color:#FF0000; font-size:12px"></label></td>
    </tr>
  <tr>
    <td>密&nbsp;&nbsp;&nbsp;&nbsp;码*:
      <input type="password" name="pwd" size="21" onfocus="showDesc(this)" onblur="checkText(this)" id="pwd"/><label id="pwdl" style=" color:#FF0000; font-size:12px"></label></td>
    </tr>
  <tr>
    <td>电&nbsp;&nbsp;&nbsp;&nbsp;话&nbsp;:
      <input type="text" name="tel" onfocus="showDesc(this)" onblur="checkText(this)" id="tel"/><label id="tell" style=" color:#FF0000; font-size:12px"></label></td>
    </tr>
  <tr>
    <td>手&nbsp;&nbsp;&nbsp;&nbsp;机&nbsp;:
      <input type="text" name="mobi" onfocus="showDesc(this)" onblur="checkText(this)" id="mobi"/><label id="mobil" style=" color:#FF0000; font-size:12px"></label></td>
    </tr>
  <tr>
    <td>电&nbsp;&nbsp;&nbsp;&nbsp;邮*:
      <input type="text" name="email" onfocus="showDesc(this)" onblur="checkText(this)" id="email"/><label id="emaill" style=" color:#FF0000; font-size:12px"></label></td>
	  <input  type="hidden" value="" name="oldemail" id="oldemail"/>
    </tr>
  <tr>
    <td>传&nbsp;&nbsp;&nbsp;&nbsp;真&nbsp;:
      <input type="text" name="fax" onfocus="showDesc(this)" onblur="checkText(this)" id="fax"/><label id="faxl" style=" color:#FF0000; font-size:12px"></label></td>
    </tr>
  <tr>
    <td>级&nbsp;&nbsp;&nbsp;&nbsp;别*:
      <select name="level">
        <option value="1" selected="selected">MIS</option>
        <option value="2">采购部</option>
        <option value="3">供应商</option>
      </select></td>
    </tr>
  <tr>
    <td height="40" align="center">
      <input type="submit" name="Submit" value="提交" />&nbsp;&nbsp;
      <input type="reset" name="Submit2" value="重置" />   </td>
</tr>
</table>
</form>
</body>
</html>
