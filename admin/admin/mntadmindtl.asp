<!--#include virtual="/admin/commadmheader.asp"-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><!-- InstanceBegin template="/Templates/admin.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" --> 
<title>Suga Intranet Administration System</title>
<script language="Javascript">
function doAction(actionType, recId){
	if(actionType == 'D'){
		document.frmAction.action = "/admin/delete.asp";	
		document.frmAction.hidRecId.value = recId;	
		document.frmAction.hidCaller.value = "/admin/admin/mntadmin.asp";
		document.frmAction.submit();
	}else if(actionType == 'R'){
		document.frmAction.action = "/admin/admin/resetpwd.asp";	
		document.frmAction.hidRecId.value = recId;	
		document.frmAction.hidCaller.value = "/admin/admin/mntadmin.asp";
		document.frmAction.submit();
	}
}
</script>
<!-- InstanceEndEditable --> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- InstanceBeginEditable name="head" --> <!-- InstanceEndEditable --> 
<link href="/style/link.css" rel="stylesheet" type="text/css">
<link href="/style/text.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_showHideLayers() { //v6.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}
//-->
</script>
<link href="/style/form.css" rel="stylesheet" type="text/css">
</head>

<body background="/images/admin/page_bg.gif" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="20" bgcolor="#000096"><table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="15">&nbsp;</td>
          <td><img src="/images/admin/logo.gif" width="47" height="13"></td>
          <td align="right"><img src="/images/admin/sys.gif" width="131" height="18"></td>
        </tr>
      </table>
      
    </td>
  </tr>
  <tr> 
    <td height="23" valign="bottom" bgcolor="#D7D7D7"><table border="0" cellspacing="0" cellpadding="0">
        <tr align="center">
		  <%writeAdminHeaderMenu%>

        </tr>
      </table></td>
  </tr>
  <tr> 
    <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="30" height="45">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td height="20">&nbsp;</td>
          <!-- InstanceBeginEditable name="heading" --> 
          <td valign="top" class="H1ZC">
		  <% if trim(Request.Form("hidRecId")) = "" then%>
		  新增管理员
		  <% else %>
		  更新管理员资料
		  <% end if %></td>
          <!-- InstanceEndEditable --></tr>
        <tr> 
          <td height="20" colspan="2" valign="top"><img src="/images/color_30k.gif" width="100%" height="1"></td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
          <!-- InstanceBeginEditable name="content" -->
          <td>
<%
'check the access right first 
if Session("AdminAdminAdd") Or Session("AdminAdminUpd") then
	Dim isSaved
	isSaved = false
	Dim errMsgId(50)

	if not isEmpty(Request.Form("doSaveAction")) and "Y" = Request.Form("doSaveAction")  then
		'''''''''''''''''''''''''''''''''''''
		'
		' Validation +
		'
		''''''''''''''''''''''''''''''''''''
		Dim currNoOfErr
		currNoOfErr = 0
	
		Dim loginName 
		loginName = Request.Form("txtAdminLoginName")
		loginName = trim(loginName)
		
		if loginName = "" then
			currNoOfErr = currNoOfErr + 1
			errMsgId(currNoOfErr - 1) = 51	
		else
			' check whether the login name is exist	
			if isEmpty(Request.Form("hidRecId")) or trim(Request.Form("hidRecId")) = "" then
				sqlStr = " Select count(AdminId) from AdminUser where AdminLoginName = '" & getSafeSql(loginName) & "'"
			else
				sqlStr = " Select count(AdminId) from AdminUser where AdminId <> " & Request.Form("hidRecId") & " and AdminLoginName = '" & getSafeSql(loginName) & "'"
			end if
			
			openRs objRst, objConn, sqlStr
			while NOT objRst.EOF
				if objRst(0) > 0 then
					currNoOfErr = currNoOfErr + 1
					errMsgId(currNoOfErr - 1) = 52	
				end if
				objRst.MoveNext
			wend
		end if
		
		
		
		'''''''''''''''''''''''''''''''''''''
		'
		' Validation -
		'
		''''''''''''''''''''''''''''''''''''
		
			
		'''''''''''''''''''''''''''''''''''''
		'
		' Save +
		'
		''''''''''''''''''''''''''''''''''''
		if currNoOfErr <= 0 then
			Dim noOfFunction 
			Dim i
			
			noOfFunction = 9
				
			Dim addArr(9)
			Dim updArr(9)
			Dim delArr(9)
	
			addArr(0) =  IIf( isEmpty(Request.Form("chkAdminAdd")) Or Request.Form("chkAdminAdd") = "", "N", "Y" )
			addArr(1) =  IIf( isEmpty(Request.Form("chkNewsAdd")) Or Request.Form("chkNewsAdd") = "", "N", "Y" )
			addArr(2) =  IIf( isEmpty(Request.Form("chkPolicyAdd")) Or Request.Form("chkPolicyAdd") = "", "N", "Y" )
			addArr(3) =  IIf( isEmpty(Request.Form("chkEventAdd")) Or Request.Form("chkEventAdd") = "", "N", "Y" )
			addArr(4) =  IIf( isEmpty(Request.Form("chkStaffAdd")) Or Request.Form("chkStaffAdd") = "", "N", "Y" )
			addArr(5) =  IIf( isEmpty(Request.Form("chkGfcAdd")) Or Request.Form("chkGfcAdd") = "", "N", "Y" )
			addArr(6) =  IIf( isEmpty(Request.Form("chkFormAdd")) Or Request.Form("chkFormAdd") = "", "N", "Y" )
			addArr(7) =  IIf( isEmpty(Request.Form("chkEventPhotoAdd")) Or Request.Form("chkEventPhotoAdd") = "", "N", "Y" )
			addArr(8) =  IIf( isEmpty(Request.Form("chkForumAdd")) Or Request.Form("chkForumAdd") = "", "N", "Y" )			

			updArr(0) =  IIf( isEmpty(Request.Form("chkAdminUpd")) Or Request.Form("chkAdminUpd") = "", "N", "Y" )
			updArr(1) =  IIf( isEmpty(Request.Form("chkNewsUpd")) Or Request.Form("chkNewsUpd") = "", "N", "Y" )
			updArr(2) =  IIf( isEmpty(Request.Form("chkPolicyUpd")) Or Request.Form("chkPolicyUpd") = "", "N", "Y" )
			updArr(3) =  IIf( isEmpty(Request.Form("chkEventUpd")) Or Request.Form("chkEventUpd") = "", "N", "Y" )
			updArr(4) =  IIf( isEmpty(Request.Form("chkStaffUpd")) Or Request.Form("chkStaffUpd") = "", "N", "Y" )
			updArr(5) =  IIf( isEmpty(Request.Form("chkGfcUpd")) Or Request.Form("chkGfcUpd") = "", "N", "Y" )
			updArr(6) =  IIf( isEmpty(Request.Form("chkFormUpd")) Or Request.Form("chkFormUpd") = "", "N", "Y" )
			updArr(7) =  IIf( isEmpty(Request.Form("chkEventPhotoUpd")) Or Request.Form("chkEventPhotoUpd") = "", "N", "Y" )	
			updArr(8) =  IIf( isEmpty(Request.Form("chkForumUpd")) Or Request.Form("chkForumUpd") = "", "N", "Y" )					
	
			delArr(0) =  IIf( isEmpty(Request.Form("chkAdminDel")) Or Request.Form("chkAdminDel") = "", "N", "Y" )
			delArr(1) =  IIf( isEmpty(Request.Form("chkNewsDel")) Or Request.Form("chkNewsDel") = "", "N", "Y" )
			delArr(2) =  IIf( isEmpty(Request.Form("chkPolicyDel")) Or Request.Form("chkPolicyDel") = "", "N", "Y" )
			delArr(3) =  IIf( isEmpty(Request.Form("chkEventDel")) Or Request.Form("chkEventDel") = "", "N", "Y" )
			delArr(4) =  IIf( isEmpty(Request.Form("chkStaffDel")) Or Request.Form("chkStaffDel") = "", "N", "Y" )
			delArr(5) =  IIf( isEmpty(Request.Form("chkGfcDel")) Or Request.Form("chkGfcDel") = "", "N", "Y" )
			delArr(6) =  IIf( isEmpty(Request.Form("chkFormDel")) Or Request.Form("chkFormDel") = "", "N", "Y" )
			delArr(7) =  IIf( isEmpty(Request.Form("chkEventPhotoDel")) Or Request.Form("chkEventPhotoDel") = "", "N", "Y" )	
			delArr(8) =  IIf( isEmpty(Request.Form("chkForumDel")) Or Request.Form("chkForumDel") = "", "N", "Y" )			
	
			objConn.BeginTrans
	
			if isEmpty(Request.Form("hidRecId")) or trim(Request.Form("hidRecId")) = "" then
				Dim beforeAddMaxId , afterAddMaxId
				
				sqlStr = " Select max(AdminId) from AdminUser"
				openRs objRst, objConn, sqlStr
				while NOT objRst.EOF
					beforeAddMaxId = objRst(0)
					objRst.MoveNext
				wend
				
				sqlStr = " insert into AdminUser (AdminLoginName, AdminPwd, AdminType, CompanyId, DistrictId, ModBy) " & vbCrLf & _
						 " values ('" & getSafeSql(loginName) & "', '" & EncryptText(loginName, application("gDefaultAdminPwd")) & "', " & vbCrLf & _
						 " '" & Request.Form("cboAdminType") & "', " & Request.Form("cboCompany") & ", " & Request.Form("cboDistrict") & " , " & Session("loginAdminId") & vbCrLf & _
						 " )"
				response.Write("<br>"&sqlStr)
				objConn.Execute(sqlStr)		
				
				sqlStr = " Select max(AdminId) from AdminUser"
				openRs objRst, objConn, sqlStr
				while NOT objRst.EOF
					afterAddMaxId = objRst(0)
					objRst.MoveNext
				wend
				
				if CInt(beforeAddMaxId) >= CInt(afterAddMaxId) then
					if objConn.state > 0 then
						objConn.RollbackTrans
					end if
				else
					' add access control data
					For i=0 To noOfFunction - 1
						sqlStr = "insert into AccessControl (AdminId, FunctionId, AllowAdd, AllowUpd, AllowDel, ModBy) values " & vbCrLf & _
								 " ( " & afterAddMaxId & ", " & i & ", '" & IIf(Request.Form("cboAdminType") = "S", "Y", addArr(i))  & "', '" & IIf(Request.Form("cboAdminType") = "S", "Y", updArr(i)) & "',  '" & IIf(Request.Form("cboAdminType") = "S", "Y", delArr(i)) & "', " & Session("loginAdminId") & ") "

						objConn.Execute(sqlStr)		
					Next
				end if
				
			else
				'Do update admin user here
				sqlStr = " update AdminUser set AdminLoginName = '" & getSafeSql(loginName) & "', AdminType = '" & Request.Form("cboAdminType") & "' " & vbCrLf & _
						 " , CompanyId = " & Request.Form("cboCompany") & ", DistrictId = " & Request.Form("cboDistrict") & " , ModBy = " & Session("loginAdminId") & ", ModDt = getdate()" & vbCrLf & _
						 " WHERE AdminId = " & Request.Form("hidRecId")
				
				objConn.Execute(sqlStr)
				
				' add access control data
				For i=0 To noOfFunction - 1
					''sqlStr = "update AccessControl set AllowAdd = '" & IIf(Request.Form("cboAdminType") = "S", "Y", addArr(i))  & "', " & vbCrLf & _
					''		 " AllowUpd = '" & IIf(Request.Form("cboAdminType") = "S", "Y", updArr(i)) & "', AllowDel = '" & IIf(Request.Form("cboAdminType") = "S", "Y", delArr(i)) & "', ModBy = " & Session("loginAdminId") & ", ModDt = getdate()"  & vbCrLf & _
					''		 " WHERE AdminId = " & Request.Form("hidRecId") & " and FunctionId = " & i
					sqlStr = "update AccessControl set AllowAdd = '" & addArr(i)  & "', " & vbCrLf & _
							 " AllowUpd = '" & updArr(i) & "', AllowDel = '" & delArr(i) & "', ModBy = " & Session("loginAdminId") & ", ModDt = getdate()"  & vbCrLf & _
							 " WHERE AdminId = " & Request.Form("hidRecId") & " and FunctionId = " & i
					response.Write(sqlStr&"<br>")
					objConn.Execute(sqlStr)		
				Next
			end if
			if objConn.state > 0 then
				if objConn.Errors.Count > 0 then
					objConn.RollbackTrans 
				else
					objConn.CommitTrans
					isSaved = true
				end if
			end if
		end if 'end if is valid data
	end if 'End if is save action
	'''''''''''''''''''''''''''''''''''''
	'
	' Save -
	'
	''''''''''''''''''''''''''''''''''''
		
	if isSaved then
		Response.Redirect "/admin/admin/mntadmin.asp"
	else
		if isEmpty(Request.Form("hidRecId")) or trim(Request.Form("hidRecId")) = "" then
			' New 
			sqlStr = "Select '' as AdminLoginName, '0' as AdminType, '0' as CompanyId, '0' as DistrictId, '' as FunctionId, '' as AllowAdd, '' as AllowUpd, '' as AllowDel, '' as ModBy, '' as ModDt " & vbCrLf & _
			         " union all " & vbCrLf & _
                     " Select '' as AdminLoginName, '0' as AdminType, '0' as CompanyId, '0' as DistrictId, '' as FunctionId, '' as AllowAdd, '' as AllowUpd, '' as AllowDel, '' as ModBy, '' as ModDt " & vbCrLf & _
					 " union all " & vbCrLf & _
                     " Select '' as AdminLoginName, '0' as AdminType, '0' as CompanyId, '0' as DistrictId, '' as FunctionId, '' as AllowAdd, '' as AllowUpd, '' as AllowDel, '' as ModBy, '' as ModDt " & vbCrLf & _
                     " union all " & vbCrLf & _
                     " Select '' as AdminLoginName, '0' as AdminType, '0' as CompanyId, '0' as DistrictId, '' as FunctionId, '' as AllowAdd, '' as AllowUpd, '' as AllowDel, '' as ModBy, '' as ModDt " & vbCrLf & _
                     " union all " & vbCrLf & _
                     " Select '' as AdminLoginName, '0' as AdminType, '0' as CompanyId, '0' as DistrictId, '' as FunctionId, '' as AllowAdd, '' as AllowUpd, '' as AllowDel, '' as ModBy, '' as ModDt " & vbCrLf & _
                     " union all " & vbCrLf & _
                     " Select '' as AdminLoginName, '0' as AdminType, '0' as CompanyId, '0' as DistrictId, '' as FunctionId, '' as AllowAdd, '' as AllowUpd, '' as AllowDel, '' as ModBy, '' as ModDt " & vbCrLf & _
					 " union all " & vbCrLf & _
                     " Select '' as AdminLoginName, '0' as AdminType, '0' as CompanyId, '0' as DistrictId, '' as FunctionId, '' as AllowAdd, '' as AllowUpd, '' as AllowDel, '' as ModBy, '' as ModDt " & vbCrLf & _
					 " union all " & vbCrLf & _
                     " Select '' as AdminLoginName, '0' as AdminType, '0' as CompanyId, '0' as DistrictId, '' as FunctionId, '' as AllowAdd, '' as AllowUpd, '' as AllowDel, '' as ModBy, '' as ModDt "	& vbCrLf & _
					 " union all " & vbCrLf & _
                     " Select '' as AdminLoginName, '0' as AdminType, '0' as CompanyId, '0' as DistrictId, '' as FunctionId, '' as AllowAdd, '' as AllowUpd, '' as AllowDel, '' as ModBy, '' as ModDt " & vbCrLf & _
					 " union all " & vbCrLf & _
                     " Select '' as AdminLoginName, '0' as AdminType, '0' as CompanyId, '0' as DistrictId, '' as FunctionId, '' as AllowAdd, '' as AllowUpd, '' as AllowDel, '' as ModBy, '' as ModDt "		 
					 
		else
			' Update
			sqlStr = "select " & vbCrLf & _
			" adm.AdminId, AdminLoginName, AdminType, CompanyId, DistrictId, FunctionId, AllowAdd, AllowUpd, AllowDel," & vbCrLf & _
			" CASE " & vbCrLf & _
			"      WHEN acl.ModBy IS NULL THEN (select AdminLoginName from AdminUser where AdminId  = adm.ModBy) " & vbCrLf & _
			"      ELSE (select AdminLoginName from AdminUser where AdminId  = acl.ModBy) " & vbCrLf & _
			"   END AS ModBy," & vbCrLf & _
			" CASE " & vbCrLf & _
			"      WHEN acl.ModDt > adm.ModDt THEN acl.ModDt" & vbCrLf & _
			"      WHEN acl.ModDt < adm.ModDt THEN adm.ModDt" & vbCrLf & _
			"      ELSE adm.ModDt" & vbCrLf & _
			"   END AS ModDt" & vbCrLf & _
			" FROM AdminUser adm LEFT OUTER JOIN AccessControl acl" & vbCrLf & _
			"   ON adm.AdminId = acl.AdminId" & vbCrLf & _
			"	where adm.AdminId = " & Request.Form("hidRecId")
		end if
	
		openRs objRst, objConn, sqlStr
%>		  
		  <table width="700" border="0" cellspacing="0" cellpadding="0">
		  <tr><td colspan="2" height="25" valign="top" class="contentNZC"><%showLocalErrMsg(errMsgId)%></td></tr>
              
              <form name="frmAction" method="post" action="/admin/admin/mntadmindtl.asp">
                <input type="hidden" name="hidRecId" value="<%=Request.Form("hidRecId")%>">
                <input type="hidden" name="hidCaller" value="">
                <input type="hidden" name="doSaveAction" value="Y">
                <%
	Dim recIdx
	recIdx = 0
	while NOT objRst.EOF
		recIdx = recIdx + 1
		if recIdx = 1 then
%>
                <tr> 
                  <td width="150" height="25" valign="top" class="contentNZC">管理员登入名称</td>
                  <td valign="top" class="contentNZC"><input type="text" name="txtAdminLoginName" class="textField1ZC" value="<%=getValue(objRst, "txtAdminLoginName", "AdminLoginName")%>">
                    (预设密码为: &quot;<%=application("gDefaultAdminPwd")%>&quot;)&nbsp;&nbsp; <input name="button" type="button" class="button3ZC" onClick="javacript:doAction('R', '<%=Request.Form("hidRecId")%>');" value="重设密码"></td>
                </tr>
				<tr> 
                  <td colspan="2" height="25" valign="top" class="contentNZC">(注: 更改管理员登入名称后, 必须重设密码方可登入)</td>
                </tr>
                <tr> 
                  <td height="10" colspan="2" valign="top" class="contentNZC"><img src="/images/color_30k.gif" width="100%" height="1"  valign="top"></td>
                </tr>
                <tr> 
                  <td height="25" valign="top" class="contentNZC">管理员类别</td>
                  <td valign="top"> <select name="cboAdminType" class="textField1ZC">
                      <option value="N" <%=IIf((getValue(objRst, "cboAdminType", "AdminType") = "N"), "selected", "") %>>普通管理员</option>
                      <option value="S" <%=IIf((getValue(objRst, "cboAdminType", "AdminType") = "S"), "selected", "") %>>主管理员</option>
                    </select> </td>
                </tr>
                <tr> 
                  <td height="10" colspan="2" valign="top" class="contentNZC"><img src="/images/color_30k.gif" width="100%" height="1"  valign="top"></td>
                </tr>
                <tr> 
                  <td height="25" valign="top" class="contentNZC">公司</td>
                  <td valign="top"><%writeCboCompany getValue(objRst, "cboCompany", "companyid") %></td>
                </tr>
                <tr> 
                  <td height="10" colspan="2" valign="top" class="contentNZC"><img src="/images/color_30k.gif" width="100%" height="1"  valign="top"></td>
                </tr>
                <tr> 
                  <td height="25" valign="top" class="contentNZC">地区</td>
                  <td valign="top"><%writeCboDistrict getValue(objRst, "cboDistrict", "DistrictId") %></td>
                </tr>
                <tr> 
                  <td height="10" colspan="2" valign="top" class="contentNZC"><img src="/images/color_30k.gif" width="100%" height="1"  valign="top"></td>
                </tr>
                <tr> 
                  <td height="25" valign="top" class="contentNZC">此位管理员允许管理的工作</td>
                  <td valign="top">&nbsp;</td>
                </tr>
                <tr> 
                  <td height="25" colspan="2" class="contentNZC"><table border="1" cellspacing="0" cellpadding="3">
                      <tr bordercolor="#999999"> 
                        <td height="25" class="contentNZC">&nbsp;</td>
                        <td width="100" class="contentNZC">新增</td>
                        <td width="100" class="contentNZC">更新</td>
                        <td width="100" class="contentNZC">删除</td>
                        <td class="contentNZC">其他</td>
                      </tr>
                      <tr bordercolor="#999999"> 
                        <td width="100" height="25" class="contentNZC">管理员资料</td>
                        <td class="contentNZC"><input type="checkbox" name="chkAdminAdd" value="Y" <%=getCheckBoxState(getValue(objRst, "chkAdminAdd", "AllowAdd"))%>></td>
                        <td class="contentNZC"><input type="checkbox" name="chkAdminUpd" value="Y" <%=getCheckBoxState(getValue(objRst, "chkAdminUpd", "AllowUpd"))%>></td>
                        <td class="contentNZC"><input type="checkbox" name="chkAdminDel" value="Y" <%=getCheckBoxState(getValue(objRst, "chkAdminDel", "AllowDel"))%>></td>
                        <td class="contentNZC">&nbsp;</td>
                      </tr>
<%               
	    end if 
		if recIdx = 2 then
%>
                      <tr bordercolor="#999999"> 
                        <td height="25" class="contentNZC">公司新闻</td>
                        <td class="contentNZC"><input type="checkbox" name="chkNewsAdd" value="Y" <%=getCheckBoxState(getValue(objRst, "chkNewsAdd", "AllowAdd"))%>></td>
                        <td class="contentNZC"><input type="checkbox" name="chkNewsUpd" value="Y" <%=getCheckBoxState(getValue(objRst, "chkNewsUpd", "AllowUpd"))%>></td>
                        <td class="contentNZC"><label class="contentNZC"> 
                          <input type="checkbox" name="chkNewsDel" value="Y" <%=getCheckBoxState(getValue(objRst, "chkNewsDel", "AllowDel"))%>>
                          </label></td>
                        <td class="contentNZC">&nbsp;</td>
                      </tr>
                      <%			    
		end if
		if recIdx = 3 then
%>
                      <tr bordercolor="#999999"> 
                        <td height="25" class="contentNZC">部门政策及程序</td>
                        <td class="contentNZC"><input type="checkbox" name="chkPolicyAdd" value="Y" <%=getCheckBoxState(getValue(objRst, "chkPolicyAdd", "AllowAdd"))%>></td>
                        <td class="contentNZC"><input type="checkbox" name="chkPolicyUpd" value="Y" <%=getCheckBoxState(getValue(objRst, "chkPolicyUpd", "AllowUpd"))%>></td>
                        <td class="contentNZC"><label class="contentNZC"> 
                          <input type="checkbox" name="chkPolicyDel" value="Y" <%=getCheckBoxState(getValue(objRst, "chkPolicyDel", "AllowDel"))%>>
                          </label></td>
                        <td class="contentNZC">&nbsp;</td>
                      </tr>
                      <%			    
		end if
		if recIdx = 4 then
%>
                      <tr bordercolor="#999999"> 
                        <td height="25" class="contentNZC">月厂事件及同事行程</td>
                        <td class="contentNZC"><input type="checkbox" name="chkEventAdd" value="Y" <%=getCheckBoxState(getValue(objRst, "chkEventAdd", "AllowAdd"))%>></td>
                        <td class="contentNZC"><input type="checkbox" name="chkEventUpd" value="Y" <%=getCheckBoxState(getValue(objRst, "chkEventUpd", "AllowUpd"))%>></td>
                        <td class="contentNZC"><input type="checkbox" name="chkEventDel" value="Y" <%=getCheckBoxState(getValue(objRst, "chkEventDel", "AllowDel"))%>></td>
                        <td class="contentNZC">&nbsp;</td>
                      </tr>
                      <%			    
		end if
		if recIdx = 5 then
%>
                      <tr bordercolor="#999999"> 
                        <td height="25" class="contentNZC">用户管理</td>
                        <td class="contentNZC"><input type="checkbox" name="chkStaffAdd" value="Y" <%=getCheckBoxState(getValue(objRst, "chkStaffAdd", "AllowAdd"))%>></td>
                        <td class="contentNZC"><input type="checkbox" name="chkStaffUpd" value="Y" <%=getCheckBoxState(getValue(objRst, "chkStaffUpd", "AllowUpd"))%>></td>
                        <td class="contentNZC"><input type="checkbox" name="chkStaffDel" value="Y" <%=getCheckBoxState(getValue(objRst, "chkStaffDel", "AllowDel"))%>></td>
                        <td class="contentNZC"><input type="checkbox" name="chkStaffUpd2" value="Y" <%=getCheckBoxState(getValue(objRst, "chkStaffUpd", "AllowUpd"))%>>
                        重设密码</td>
                      </tr>
                      <%			    
		end if
		if recIdx = 6 then
%>
                      <tr bordercolor="#999999"> 
                        <td height="25" class="contentNZC">常规事务联络</td>
                        <td class="contentNZC"><input type="checkbox" name="chkGfcAdd" value="Y" <%=getCheckBoxState(getValue(objRst, "chkGfcAdd", "AllowAdd"))%>></td>
                        <td class="contentNZC"><input type="checkbox" name="chkGfcUpd" value="Y" <%=getCheckBoxState(getValue(objRst, "chkGfcUpd", "AllowUpd"))%>></td>
                        <td class="contentNZC"><input type="checkbox" name="chkGfcDel" value="Y" <%=getCheckBoxState(getValue(objRst, "chkGfcDel", "AllowDel"))%>></td>
                        <td class="contentNZC">&nbsp;</td>
                      </tr>
<%					  
		end if
		if recIdx = 7 then
%>
                      <tr bordercolor="#999999"> 
                        <td height="25" class="contentNZC">表格</td>
                        <td class="contentNZC"><input type="checkbox" name="chkFormAdd" value="Y" <%=getCheckBoxState(getValue(objRst, "chkFormAdd", "AllowAdd"))%>></td>
                        <td class="contentNZC"><input type="checkbox" name="chkFormUpd" value="Y" <%=getCheckBoxState(getValue(objRst, "chkFormUpd", "AllowUpd"))%>></td>
                        <td class="contentNZC"><input type="checkbox" name="chkFormDel" value="Y" <%=getCheckBoxState(getValue(objRst, "chkFormDel", "AllowDel"))%>></td>
                        <td class="contentNZC">&nbsp;</td>
                      </tr>
<%			    
		end if
		if recIdx = 8 then
%>
                      <tr bordercolor="#999999"> 
                        <td height="25" class="contentNZC">图片库</td>
                        <td class="contentNZC"><input type="checkbox" name="chkEventPhotoAdd" value="Y" <%=getCheckBoxState(getValue(objRst, "chkEventPhotoAdd", "AllowAdd"))%>></td>
                        <td class="contentNZC"><input type="checkbox" name="chkEventPhotoUpd" value="Y" <%=getCheckBoxState(getValue(objRst, "chkEventPhotoUpd", "AllowUpd"))%>></td>
                        <td class="contentNZC"><input type="checkbox" name="chkEventPhotoDel" value="Y" <%=getCheckBoxState(getValue(objRst, "chkEventPhotoDel", "AllowDel"))%>></td>
                        <td class="contentNZC">&nbsp;</td>
                      </tr>
<%
		end if
		if recIdx = 9 then
%>
					  <tr bordercolor="#999999"> 
                        <td height="25" class="contentNZC">讨论区</td>
                        <td class="contentNZC"><input type="checkbox" name="chkForumAdd" value="Y" <%=getCheckBoxState(getValue(objRst, "chkForumAdd", "AllowAdd"))%>></td>
                        <td class="contentNZC"><input type="checkbox" name="chkForumUpd" value="Y" <%=getCheckBoxState(getValue(objRst, "chkForumUpd", "AllowUpd"))%>></td>
                        <td class="contentNZC"><input type="checkbox" name="chkForumDel" value="Y" <%=getCheckBoxState(getValue(objRst, "chkForumDel", "AllowDel"))%>></td>
                        <td class="contentNZC">&nbsp;</td>
                      </tr>
                    
<%		end if 
			
		if NOT objRst.EOF then
			objRst.moveNext
		end if
	Wend
%>
					</table>
                    <label class="contentNZC"> </label></td>
                </tr>
                <tr> 
                  <td height="25" class="contentNZC">&nbsp;</td>
                  <td><label class="contentNZC"> </label></td>
                </tr>
<%
			 'write the modified by and modified on info
			 if NOT objRst.EOF then
			  writeAuditDetailRow objRst, true
			 end if
		'end if
		
		
			'write the save, reset buttons
			writeDtlButtRow
			
			
	
%>
              </form>
            </table></td>
          <!-- InstanceEndEditable --></tr>
      </table></td>
  </tr>
  <tr> 
    <td height="20" bgcolor="#D7D7D7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="copyrightZC">&nbsp;&nbsp;&nbsp;版权&copy; 2004 信佳国际集团有限公司所有</td>
          <td align="right" nowrap class="copyrightZC">最佳使用环境 - IE 6.0&nbsp;&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
<div id="menu1" style="position:absolute; left:18px; top:43px; width:360px; height:25px; z-index:1; visibility: hidden;">
  <table height="25" border="1" cellpadding="0" cellspacing="0" bordercolor="#999999" bgcolor="#E6E6FF">
    <tr align="center" valign="bottom"> 
      <td width="120"><a href="#" class="link7ZC">新增管理员</a></td>
      <td width="120"><a href="#" class="link7ZC">取消管理员</a></td>
      <td width="120"><a href="#" class="link7ZC">更新管理员资料</a></td>
    </tr>
  </table>
</div>
<div id="menu2" style="position:absolute; left:80px; top:43px; width:360px; height:25px; z-index:1; visibility: hidden;"> 
  <table height="25" border="1" cellpadding="0" cellspacing="0" bordercolor="#999999" bgcolor="#E6E6FF">
    <tr align="center" valign="bottom"> 
      <td width="120"><a href="#" class="link7ZC">新增管理员</a></td>
      <td width="120"><a href="#" class="link7ZC">取消管理员</a></td>
      <td width="120"><a href="#" class="link7ZC">更新管理员资料</a></td>
    </tr>
  </table>
</div>
</body>
<!-- InstanceEnd --></html>
<%	
	end if 'End if is saved
Else
	Response.Redirect "/admin/main.asp"
end if 'End if is really has access right
%>
<!--#include virtual="/admin/commadmfooter.asp"-->	
