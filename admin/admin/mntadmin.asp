<!--#include virtual="/admin/commadmheader.asp"-->

<html><!-- InstanceBegin template="/Templates/admin.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" --> 
<title>Suga Intranet Administration System</title>

<script language="Javascript">
function doAction(actionType, recId){
	if(actionType == 'U'){
		document.frmAction.action = "/admin/admin/mntadmindtl.asp";
		document.frmAction.hidRecId.value = recId;	
		document.frmAction.submit();
	}else if(actionType == 'D'){
		if (confirm("<%=getDeleteAlertMsg()%>")) {
			document.frmAction.action = "/admin/delete.asp";	
			document.frmAction.hidRecId.value = recId;	
			document.frmAction.submit();
		}
	}
}
</script>

<!-- InstanceEndEditable --> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- InstanceBeginEditable name="head" --> <!-- InstanceEndEditable --> 
<link href="/style/link.css" rel="stylesheet" type="text/css">
<link href="/style/text.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_showHideLayers() { //v6.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}
//-->
</script>
<link href="/style/form.css" rel="stylesheet" type="text/css">
</head>

<body background="/images/admin/page_bg.gif" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="20" bgcolor="#000096"><table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="15">&nbsp;</td>
          <td><img src="/images/admin/logo.gif" width="47" height="13"></td>
          <td align="right"><img src="/images/admin/sys.gif" width="131" height="18"></td>
        </tr>
      </table>
      
    </td>
  </tr>
  <tr> 
    <td height="23" valign="bottom" bgcolor="#D7D7D7"><table border="0" cellspacing="0" cellpadding="0">
        <tr align="center">
		  <%writeAdminHeaderMenu%>

        </tr>
      </table></td>
  </tr>
  <tr> 
    <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="30" height="45">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td height="20">&nbsp;</td>
          <!-- InstanceBeginEditable name="heading" --> 
          <td valign="top" class="H1ZC">管理员资料管理</td>
          <!-- InstanceEndEditable --></tr>
        <tr> 
          <td height="20" colspan="2" valign="top"><img src="/images/color_30k.gif" width="100%" height="1"></td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
          <!-- InstanceBeginEditable name="content" -->
          <td>
<%
'check the access right first 
if Session("AdminAdminAdd") Or Session("AdminAdminUpd") Or Session("AdminAdminDel") then
	sqlStr = "select " & vbCrLf & _
			" adm.AdminId, AdminLoginName, AdminType, CompanyId, DistrictId, FunctionId, AllowAdd, AllowUpd, AllowDel," & vbCrLf & _
			" CASE " & vbCrLf & _
			"      WHEN acl.ModBy IS NULL THEN (select AdminLoginName from AdminUser where AdminId  = adm.ModBy) " & vbCrLf & _
			"      ELSE (select AdminLoginName from AdminUser where AdminId  = acl.ModBy) " & vbCrLf & _
			"   END AS ModBy," & vbCrLf & _
			" CASE " & vbCrLf & _
			"      WHEN acl.ModDt > adm.ModDt THEN acl.ModDt" & vbCrLf & _
			"      WHEN acl.ModDt < adm.ModDt THEN adm.ModDt" & vbCrLf & _
			"      ELSE adm.ModDt" & vbCrLf & _
			"   END AS ModDt" & vbCrLf & _
			" FROM AdminUser adm LEFT OUTER JOIN AccessControl acl" & vbCrLf & _
			"   ON adm.AdminId = acl.AdminId" & vbCrLf & _
			" order by adm.AdminLoginName, FunctionId"

	objRst.Open sqlStr, objConn, 3, 3
%>	

<form name="frmAction"  method="post" action="">
	<input type="hidden" name="hidRecId" value="">
	<input type="hidden" name="hidCaller" value="/admin/mntadmin.asp">
</form>

<%	
	if Session("AdminAdminAdd") then
%>
		<br><br><a href="/admin/admin/mntadmindtl.asp"><img src="/images/admin/btn_new.gif" width="63" height="22" border="0"></a>
<%
	end if
%>		  
		  <table width="700" border="1" cellpadding="3" cellspacing="0" bordercolor="#DDDDDD">
              <tr> 
                <td width="15" bgcolor="#EEEEEE" class="contentNZC">&nbsp;</td>
                <td height="25" bgcolor="#EEEEEE" class="contentNZC"><font color="#000096"><strong>管理员<br>
                  登入名称</strong></font></td>
                <td bgcolor="#EEEEEE" class="contentNZC"><font color="#000096"><strong>管理员类别</strong></font></td>
                <td bgcolor="#EEEEEE" class="contentNZC"><font color="#000096"><strong>公司</strong></font></td>
                <td bgcolor="#EEEEEE" class="contentNZC"><font color="#000096"><strong>地区</strong></font></td>
                <td bgcolor="#EEEEEE" class="contentNZC"><font color="#000096"><strong>管理工作</strong></font></td>
                <td bgcolor="#EEEEEE" class="contentNZC"><font color="#000096"><strong>新增</strong></font></td>
                <td bgcolor="#EEEEEE" class="contentNZC"><font color="#000096"><strong>更新</strong></font></td>
                <td bgcolor="#EEEEEE" class="contentNZC"><font color="#000096"><strong>删除</strong></font></td>
                <td nowrap bgcolor="#EEEEEE" class="contentNZC"><font color="#000096"><strong>上次更新<br>
                  的管理员</strong></font></td>
                <td bgcolor="#EEEEEE" class="contentNZC"><font color="#000096"><strong>上次更新<br>
                  日期</strong></font></td>
                <%	
	if Session("AdminAdminDel") then
%>
                <td bgcolor="#EEEEEE"><font color="#000096">&nbsp;</font></td>
                <%
	end if
%>
              </tr>
              <%
	Dim preAdminId
	preAdminId = -1
	while NOT objRst.EOF
		'set the access control with bit
		
%>
              <tr> 
                <td align="center" valign="top" class="contentNZC"><img src="/images/dot_orange_2.gif" width="6" height="12"></td>
                <td height="25" nowrap class="contentNZC"> <%
		if  Session("AdminAdminUpd") then
			if CInt(preAdminId) <> CInt(objRst("AdminId")) then
%><%=objRst("AdminLoginName")%><%			
			else
				Response.Write "&nbsp;"		
			end if
		else
			if CInt(preAdminId) <> CInt(objRst("AdminId")) then
				Response.Write objRst("AdminLoginName")
			else
				Response.Write "&nbsp;"
			end if
		end if
%> </td>
                <%			
			if CInt(preAdminId) <> CInt(objRst("AdminId")) then
%>
                <td nowrap class="contentNZC"><%=getAdminType(objRst("adminType"))%></td>
                <td nowrap class="contentNZC"><%=getCompanyName(objRst("CompanyId"))%></td>
                <td nowrap class="contentNZC"><%=getRegionName(objRst("DistrictId"))%></td>
                <%				
			else
%>
                <td nowrap>&nbsp;</td>
                <td nowrap>&nbsp;</td>
                <td nowrap>&nbsp;</td>
                <%			
			end if
%>
                <td nowrap class="contentNZC"><%=gArrFunctionName(objRst("functionId"))%></td>
                <td nowrap class="contentNZC"><%=IIf("Y" = objRst("AllowAdd"), "是", "否")%></td>
                <td nowrap class="contentNZC"><%=IIf("Y" = objRst("AllowUpd"), "是", "否")%></td>
                <td nowrap class="contentNZC"><%=IIf("Y" = objRst("AllowDel"), "是", "否")%></td>
                <td class="contentNZC"><%=IIf(not IsEmpty(objRst("ModBy")) and trim(objRst("ModBy")) <> "" , objRst("ModBy"), "&nbsp;" )%></td>
                <td class="contentNZC"><%=formatDateWithTime(objRst("ModDt"), "/", 0)%></td>
				 <td>
                <%	
	if Session("AdminAdminDel") then
		if Cint(preAdminId) <> Cint(objRst("AdminId")) then
%>
               <a href="javascript:doAction('D', <%=objRst("AdminId")%>)"><img src="/images/admin/btn_del.gif" width="45" height="22" border="0"></a>
             <%		
		end if
	end if
%>
                <%	
	if Session("AdminEventUpd") then
		if Cint(preAdminId) <> Cint(objRst("AdminId")) then
%>
               <a href="javascript:doAction('U', <%=objRst("AdminId")%>)"><img src="/images/btn_update.gif" width="45" height="22" border="0"></a>
             <%		
		end if
	end if
%>
</td>
              </tr>
              <%	
		if Cint(preAdminId) <> Cint(objRst("AdminId")) then
			preAdminId = objRst("AdminId")
		end if
		
		objRst.MoveNext
	Wend
%>
            </table></td>
          <!-- InstanceEndEditable --></tr>
      </table></td>
  </tr>
  <tr> 
    <td height="20" bgcolor="#D7D7D7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="copyrightZC">&nbsp;&nbsp;&nbsp;版权&copy; 2004 信佳国际集团有限公司所有</td>
          <td align="right" nowrap class="copyrightZC">最佳使用环境 - IE 6.0&nbsp;&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
<div id="menu1" style="position:absolute; left:18px; top:43px; width:360px; height:25px; z-index:1; visibility: hidden;">
  <table height="25" border="1" cellpadding="0" cellspacing="0" bordercolor="#999999" bgcolor="#E6E6FF">
    <tr align="center" valign="bottom"> 
      <td width="120"><a href="#" class="link7ZC">新增管理员</a></td>
      <td width="120"><a href="#" class="link7ZC">取消管理员</a></td>
      <td width="120"><a href="#" class="link7ZC">更新管理员资料</a></td>
    </tr>
  </table>
</div>
<div id="menu2" style="position:absolute; left:80px; top:43px; width:360px; height:25px; z-index:1; visibility: hidden;"> 
  <table height="25" border="1" cellpadding="0" cellspacing="0" bordercolor="#999999" bgcolor="#E6E6FF">
    <tr align="center" valign="bottom"> 
      <td width="120"><a href="#" class="link7ZC">新增管理员</a></td>
      <td width="120"><a href="#" class="link7ZC">取消管理员</a></td>
      <td width="120"><a href="#" class="link7ZC">更新管理员资料</a></td>
    </tr>
  </table>
</div>
</body>
<!-- InstanceEnd --></html>
<%	
Else
	Response.Redirect "/admin/main.asp"
end if 'End if is really has access right
%>
<!--#include virtual="/admin/commadmfooter.asp"-->	