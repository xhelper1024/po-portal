﻿<!--#include virtual="/b_util/data.asp"-->
<!--#include virtual="/b_util/error_msg.asp"-->

<%

function nl2br(str)
	nl2br=replace(str,VbCrLf, "<br >")
end function

Function IIf( expr, truepart, falsepart )
   IIf = falsepart
   If expr Then IIf = truepart
End Function

Function in_array(element,tableau)
  Dim i
 For i=0 To Ubound(tableau) 
  If trim(tableau(i)) = trim(element) Then 
   in_array = True
   Exit Function
  Else 
   in_array = False
  End if  
 Next
End Function

Private sub openRs(ByRef objRst, ByRef dbConn, ByVal sqlStr )
	if objRst.state > 0 then
		objRst.close
	end if
	objRst.Open sqlStr, dbConn, 3, 3
End sub


Private Function EncryptText(ByVal strEncryptionKey, ByVal strTextToEncrypt)
    ' Declare variables
    Dim outer, inner, Key, strTemp
    ' For each character in strEncryptionKey
    For outer = 1 To Len(strEncryptionKey)
        ' Get a character to use as our encryption 
        ' key in this iteration of the OUTER loop
        key = Asc(Mid(strEncryptionKey, outer, 1))
	
        ' For each character in strTextToEncrypt
        For inner = 1 To Len(strTextToEncrypt)
            ' Update our encrypted text
            strTemp = strTemp & Chr(Asc(Mid(strTextToEncrypt, inner, 1)) Xor key)
            ' Change our encryption key to mix things up in the INNER loop.
            key = (key + Len(strEncryptionKey)) Mod 256
        Next
        ' Update the strTextToEncrypt variable before 
        ' the next iteration of the OUTER loop
        strTextToEncrypt = strTemp
        ' Reset strTemp for the next iteration of the OUTER loop.
        strTemp = ""
    Next
    ' Assign the value of the encrypted text to the function name 
    ' so we can return the value to the caller
    EncryptText = strTextToEncrypt
End Function


Sub writeCboEventType(selectedEventTypeIdx)
	Dim i
	Response.Write "<select name=""cboEventType"" class=""textField1ZC"" id=""cboEventType"">"
    for i=0 to ubound(gArrEventType)
        if not isEmpty(selectedEventTypeIdx) and selectedEventTypeIdx <> "" then
        	if CInt(selectedEventTypeIdx) = i then
			    Response.Write "<option value=""" & i & """ selected>"
			    Response.Write gArrEventType(i) & "</option>"
			else
			    Response.Write "<option value=""" & i & """>"
			    Response.Write gArrEventType(i)& "</option>"    
			end if
    	else
		    Response.Write "<option value=""" & i & """>"
		    Response.Write gArrEventType(i)& "</option>"
		end if
	Next
    Response.Write "</select>"
End Sub

Sub writeCboEventPhotoType(selectedEventPhotoTypeIdx)
	Dim i
	Response.Write "<select name=""cboEventPhotoType"" class=""textField1ZC"" id=""cboEventPhotoType"">"
    for i=0 to ubound(gArrGalleryEventType) 
        if not isEmpty(selectedEventPhotoTypeIdx) and selectedEventPhotoTypeIdx <> "" then
        	if CInt(selectedEventPhotoTypeIdx) = i then
			    Response.Write "<option value=""" & i & """ selected>"
			    Response.Write gArrGalleryEventType(i) & "</option>"
			else
			    Response.Write "<option value=""" & i & """>"
			    Response.Write gArrGalleryEventType(i)& "</option>"    
			end if
    	else
		    Response.Write "<option value=""" & i & """>"
		    Response.Write gArrGalleryEventType(i)& "</option>"
		end if
	Next
    Response.Write "</select>"
End Sub

Sub writeCboCompany(selectedCompanyIdx)
	Dim i
	Response.Write "<select name=""cboCompany"" class=""textField1ZC"" id=""cboCompany"">"
    for i=0 to ubound(gArrCompanyName)
        if not isEmpty(selectedCompanyIdx) and selectedCompanyIdx <> "" then
        	if CInt(selectedCompanyIdx) = i then
			    Response.Write "<option value=""" & i & """ selected>"
			    Response.Write getCompanyName(i) & "</option>"
			else
			    Response.Write "<option value=""" & i & """>"
			    Response.Write getCompanyName(i)& "</option>"    
			end if
    	else
		    Response.Write "<option value=""" & i & """>"
		    Response.Write getCompanyName(i)& "</option>"
		end if
	Next
	Response.Write "<option value=""-1"">没有指定</option>"
    Response.Write "</select>"
End Sub

' For any page require more than one company for selection
Sub writeCboCompanyForMultipleSel(selectedCompanyIdx)
	Dim i, notSpecificSelected

	Response.Write "<select name=""cboCompany"" class=""textField1ZC"" id=""cboCompany"">"
	Response.Write "<option value=""-2"">请选择公司</option>"
    for i=0 to ubound(gArrCompanyName)
        if not isEmpty(selectedCompanyIdx) and selectedCompanyIdx <> "" then
        	if CInt(selectedCompanyIdx) = i then
			    Response.Write "<option value=""" & i & """ selected>"
			    Response.Write getCompanyName(i) & "</option>"
				notSpecificSelected = ""
			else
			    Response.Write "<option value=""" & i & """>"
			    Response.Write getCompanyName(i)& "</option>"    
			end if
			if CInt(selectedCompanyIdx) = -1 then
				notSpecificSelected = "selected"
			end if
    	else
		    Response.Write "<option value=""" & i & """>"
		    Response.Write getCompanyName(i)& "</option>"
		end if
	Next
	Response.Write "<option value=""-1""" & notSpecificSelected & ">没有指定</option>"
    Response.Write "</select>"
End Sub

Sub writeCboDistrict(selectedDistrictIdx)
	Dim i
	Response.Write "<select name=""cboDistrict"" class=""textField1ZC"" id=""cboDistrict"">"
    for i=0 to ubound(gArrRegion)
    	if not isEmpty(selectedDistrictIdx) and selectedDistrictIdx <> "" then
	        if CInt(selectedDistrictIdx) = i then 
			    Response.Write "<option value=""" & i & """ selected>"
			    Response.Write getRegionName(i) & "</option>"
	    	else
			    Response.Write "<option value=""" & i & """>"
			    Response.Write getRegionName(i)& "</option>"
			end if
		else
			Response.Write "<option value=""" & i & """>"
			Response.Write getRegionName(i)& "</option>"
		end if
	Next
	'Response.Write "<option value=""-1"">没有指定</option>"
    Response.Write "</select>"
End Sub

Sub writeCboDistrictMultiple(selectedDistrictIdx)
	Dim i
	Response.Write "<select name=""cbomDistrict"" class=""textField1ZC"" id=""cbomDistrict"" multiple size=""10"">"
    for i=0 to ubound(gArrRegion)
    	if not isEmpty(selectedDistrictIdx) and selectedDistrictIdx <> "" then
	        if CInt(selectedDistrictIdx) = i then 
			    Response.Write "<option value=""" & i & """ selected>"
			    Response.Write getRegionName(i) & "</option>"
	    	else
			    Response.Write "<option value=""" & i & """>"
			    Response.Write getRegionName(i)& "</option>"
			end if
		else
			Response.Write "<option value=""" & i & """>"
			Response.Write getRegionName(i)& "</option>"
		end if
	Next
	'Response.Write "<option value=""-1"">没有指定</option>"
    Response.Write "</select>"
End Sub

Sub writeCboDistrictMultiple2(selectedDistrictIdx1)
	Dim i, selectedDistrictIdx, iArraySize, printSelected
	if selectedDistrictIdx1 <> "" then
		selectedDistrictIdx =  Split(selectedDistrictIdx1, ",")
	else
		selectedDistrictIdx = ""
	end if
	Response.Write "<select name=""cbomDistrict"" class=""textField1ZC"" id=""cbomDistrict"" multiple size=""8"">"
	
    for i=0 to ubound(gArrRegion)
    	if not isEmpty(selectedDistrictIdx1) and selectedDistrictIdx1 <> "" then
	        if in_array(i,selectedDistrictIdx) then 
			    Response.Write "<option value=""" & i & """ selected>"
			    Response.Write getRegionName(i) & "</option>"
	    	else
			    Response.Write "<option value=""" & i & """>"
			    Response.Write getRegionName(i) & "</option>"
			end if
		else
			if i = 0 then printSelected = "selected" else printSelected = "" end if
			Response.Write "<option value=""" & i & """ "&printSelected&">"
			Response.Write getRegionName(i) & "</option>"
		end if
	Next
	'Response.Write "<option value=""-1"">没有指定</option>"
    Response.Write "</select>"
End Sub

' For any page require more than one district(region) for selection
Sub writeCboDistrictForMultipleSel(selectedDistrictIdx)
	Dim i, notSpecificSelected
	
	Response.Write "<select name=""cbomDistrict"" class=""textField1ZC"" id=""cbomDistrict"">"
	Response.Write "<option value=""-2"">请选择地区</option>"
    for i=0 to ubound(gArrRegion)
    	if not isEmpty(selectedDistrictIdx) and selectedDistrictIdx <> "" then
	        if CInt(selectedDistrictIdx) = i then 
			    Response.Write "<option value=""" & i & """ selected>"
			    Response.Write getRegionName(i) & "</option>"
				notSpecificSelected = ""
	    	else
			    Response.Write "<option value=""" & i & """>"
			    Response.Write getRegionName(i)& "</option>"
			end if
			if CInt(selectedDistrictIdx) = -1 then
				notSpecificSelected = "selected"
			end if
		else
			Response.Write "<option value=""" & i & """>"
			Response.Write getRegionName(i)& "</option>"
		end if
	Next
	'Response.Write "<option value=""-1""" & notSpecificSelected & ">没有指定</option>"
    Response.Write "</select>"
End Sub

' For any page require more than one department for selection
Sub writeCboDeptForMultipleSel(selectedDeptIdx)
	Dim i, notSpecificSelected
	
	Response.Write "<select name=""cboDept"" class=""textField1ZC"" id=""cboDept"">"
	Response.Write "<option value=""-2"">请选择部门</option>"
    for i=0 to ubound(gArrDeptName)
	if gArrDeptName(i)<>"" then
    	if not isEmpty(selectedDeptIdx) and selectedDeptIdx <> "" then
	        if CInt(selectedDeptIdx) = i then 
			    Response.Write "<option value=""" & i & """ selected>"
			    Response.Write getDeptName(i) & "</option>"
				notSpecificSelected = ""
	    	else
			    Response.Write "<option value=""" & i & """>"
			    Response.Write getDeptName(i)& "</option>"
			end if
			if CInt(selectedDeptIdx) = -1 then
				notSpecificSelected = "selected"
			end if
		else
		    Response.Write "<option value=""" & i & """>"
		    Response.Write getDeptName(i)& "</option>"
		end if	
	end if
	Next
	Response.Write "<option value=""-1"" " & notSpecificSelected & ">没有指定</option>"
    Response.Write "</select>"
End Sub

Sub writeCboDept(selectedDeptIdx)
	Dim i
	Response.Write "<select name=""cboDept"" class=""textField1ZC"" id=""cboDept"">"
    for i=0 to ubound(gArrDeptName)
	if gArrDeptName(i)<>"" then
    	if not isEmpty(selectedDeptIdx) and selectedDeptIdx <> "" then
	        if CInt(selectedDeptIdx) = i then 
			    Response.Write "<option value=""" & i & """ selected>"
			    Response.Write getDeptName(i) & "</option>"
	    	else
			    Response.Write "<option value=""" & i & """>"
			    Response.Write getDeptName(i)& "</option>"
			end if
		else
		    Response.Write "<option value=""" & i & """>"
		    Response.Write getDeptName(i)& "</option>"
		end if	
	end if
	Next
	Response.Write "<option value=""-1"">没有指定</option>"
    Response.Write "</select>"
End Sub

Function getAdminType(adminType)
	if "S" = adminType then
		getAdminType = "主管理员"
	else	
		getAdminType = "普通管理员"
	End If	
End Function

Function getMsgType(msgType)
	if "M" = msgType then
		getMsgType = "留言"
	else
		if "V" = msgType then
			getMsgType = "投票事项"
		else
			getMsgType = ""
		end if
	End If	
End Function


Function isLogIn
	if isEmpty(Session("loginAdminId")) then
		isLogIn = false
	elseif trim(Session("loginAdminId")) <> "" then
		isLogIn = true
	end if
End Function

Sub writeAdminHeaderMenu %>

<td>
	<table border="0" cellspacing="0" cellpadding="0" align="left">
		<tr>
			<td width="20"></td>
			<td>
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
<%
	'if Session("AdminAdminAdd") Or Session("AdminAdminUpd") Or Session("AdminAdminDel") then
		Response.Write "<td width=""100"" class=""contentNEN""><a href=""/admin/admin/mntadmin.asp"" class=""link7ZC"">管理员资料管理</a></td>"
	'end if
	'if Session("AdminNewsAdd") Or Session("AdminNewsUpd") Or Session("AdminNewsDel") then
		Response.Write "<td width=""60"" class=""contentNEN""><a href=""/admin/news/mntnews.asp"" class=""link7ZC"">新闻管理</a></td>"
	'end if
	'if Session("AdminPolicyAdd") Or Session("AdminPolicyUpd") Or Session("AdminPolicyDel") then
		Response.Write "<td width=""120"" class=""contentNEN""><a href=""/admin/policy/mntpolicy.asp"" class=""link7ZC"">部门政策/程序管理</a></td>"
	'end if
	'if Session("AdminFormAdd") Or Session("AdminFormUpd") Or Session("AdminFormDel") then
		Response.Write "<td width=""60"" class=""contentNEN""><a href=""/admin/form/mntform.asp"" class=""link7ZC"">表格管理</a></td>"
	'end if
	'if Session("AdminEventAdd") Or Session("AdminEventUpd") Or Session("AdminEventDel") then
		Response.Write "<td width=""60"" class=""contentNEN""><a href=""/admin/event/mntevent.asp"" class=""link7ZC"">月历管理</a></td>"
	'end if
		Response.Write "<td width=""150"" class=""contentNEN""><a href=""/admin/training/mnttraining.asp"" class=""link7ZC"">課程及培訓管理</a></td>"
	
%></tr></table>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<%
	
	'if Session("AdminStaffAdd") Or Session("AdminStaffUpd") Or Session("AdminStaffDel") then
		Response.Write "<td width=""60""><a href=""/admin/staff/mntstaff.asp"" class=""link7ZC"">用户管理</a></td>"
	'end if
	'if Session("AdminGfcAdd") Or Session("AdminGfcUpd") Or Session("AdminGfcDel") then
		'Response.Write "<a href=""/admin/msg/mntmsg.asp"">Maintain Message</a>&nbsp;&nbsp;"
		Response.Write "<td width=""80"" class=""contentNEN""><a href=""/admin/gfc/mntgfc.asp"" class=""link7ZC"">常规事务联络</a></td>"
	'end if
	'if Session("AdminEventPhotoAdd") Or Session("AdminEventPhotoUpd") Or Session("AdminEventPhotoDel") then
		Response.Write "<td width=""70"" class=""contentNEN""><a href=""/admin/eventphoto/mnteventphoto.asp"" class=""link7ZC"">图片库管理</a></td>"
	'end if
		Response.Write "<td width=""70"" class=""contentNEN""><a href=""/admin/forum/mntforum.asp"" class=""link7ZC"">讨论区管理</a></td>"
Response.Write "<td width=""120"" class=""contentNEN""><a href=""/admin/chgpwd.asp"" class=""link7ZC"">更改管理员密码</a></td>"	
Response.Write "<td width=""70"" class=""contentNEN"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=""/admin/logout.asp"" class=""link7ZC"">登出</a></td>"	

%>				</tr>
				</table>
			</td> 
		</tr>
	</table>
</td>
<%
End Sub

Sub setACL(adminType, functionId, allowAdd, allowUpd, allowDel)
	Session("AdminType") = adminType
	if adminType = "S" then
		Session("AdminAdminAdd") = true
		Session("AdminAdminUpd") = true
		Session("AdminAdminDel") = true
		Session("AdminNewsAdd") = true
		Session("AdminNewsUpd") = true
		Session("AdminNewsDel") = true
		Session("AdminPolicyAdd") = true
		Session("AdminPolicyUpd") = true
		Session("AdminPolicyDel") = true
		Session("AdminEventAdd") = true
		Session("AdminEventUpd") = true
		Session("AdminEventDel") = true
		Session("AdminStaffAdd") = true
		Session("AdminStaffUpd") = true
		Session("AdminStaffDel") = true
		Session("AdminGfcAdd") = true
		Session("AdminGfcUpd") = true
		Session("AdminGfcDel") = true
		Session("AdminFormAdd") = true
		Session("AdminFormUpd") = true
		Session("AdminFormDel") = true
		Session("AdminEventPhotoAdd") = true
		Session("AdminEventPhotoUpd") = true
		Session("AdminEventPhotoDel") = true
		Session("AdminForumAdd") = true
		Session("AdminForumUpd") = true
		Session("AdminForumDel") = true
	else
		select case CInt(functionId)
			case 0
				if "Y" = allowAdd then
					Session("AdminAdminAdd") = true
				end if
				
				if "Y" = allowUpd then
					Session("AdminAdminUpd") = true
				end if
				
				if "Y" = allowDel then
					Session("AdminAdminDel") = true
				end if
			case 1
				if "Y" = allowAdd then
					Session("AdminNewsAdd") = true
				end if
				
				if "Y" = allowUpd then
					Session("AdminNewsUpd") = true
				end if
				
				if "Y" = allowDel then
					Session("AdminNewsDel") = true
				end if
			case 2
				if "Y" = allowAdd then
					Session("AdminPolicyAdd") = true
				end if
				
				if "Y" = allowUpd then
					Session("AdminPolicyUpd") = true
				end if
				
				if "Y" = allowDel then
					Session("AdminPolicyDel") = true
				end if
			case 3
				if "Y" = allowAdd then
					Session("AdminEventAdd") = true
				end if
				
				if "Y" = allowUpd then
					Session("AdminEventUpd") = true
				end if
				
				if "Y" = allowDel then
					Session("AdminEventDel") = true
				end if
			case 4
				if "Y" = allowAdd then
					Session("AdminStaffAdd") = true
				end if
				
				if "Y" = allowUpd then
					Session("AdminStaffUpd") = true
				end if
				
				if "Y" = allowDel then
					Session("AdminStaffDel") = true
				end if			
			case 5
				if "Y" = allowAdd then
					Session("AdminGfcAdd") = true
				end if
				
				if "Y" = allowUpd then
					Session("AdminGfcUpd") = true
				end if
				
				if "Y" = allowDel then
					Session("AdminGfcDel") = true
				end if
			case 6
				if "Y" = allowAdd then
					Session("AdminFormAdd") = true
				end if
				
				if "Y" = allowUpd then
					Session("AdminFormUpd") = true
				end if
				
				if "Y" = allowDel then
					Session("AdminFormDel") = true
				end if
			case 7
				if "Y" = allowAdd then
					Session("AdminEventPhotoAdd") = true
				end if
				
				if "Y" = allowUpd then
					Session("AdminEventPhotoUpd") = true
				end if
				
				if "Y" = allowDel then
					Session("AdminEventPhotoDel") = true
				end if		
			case 8
				if "Y" = allowAdd then
					Session("AdminForumAdd") = true
				end if
				
				if "Y" = allowUpd then
					Session("AdminForumUpd") = true
				end if
				
				if "Y" = allowDel then
					Session("AdminForumDel") = true
				end if					
		end select 
	End if	
End Sub


Sub resetAllSession
	Session("AdminType") = empty
	Session("loginAdminId") = empty
	Session("actionErrId") = empty
	
	Session("AdminAdminAdd") = false
	Session("AdminAdminUpd") = false
	Session("AdminAdminDel") = false
	
	Session("AdminNewsAdd") = false
	Session("AdminNewsUpd") = false
	Session("AdminNewsDel") = false
	
	Session("AdminPolicyAdd") = false
	Session("AdminPolicyUpd") = false
	Session("AdminPolicyDel") = false
	
	Session("AdminEventAdd") = false
	Session("AdminEventUpd") = false
	Session("AdminEventDel") = false
	
	Session("AdminStaffAdd") = false
	Session("AdminStaffUpd") = false
	Session("AdminStaffDel") = false
	
	Session("AdminGfcAdd") = false
	Session("AdminGfcUpd") = false
	Session("AdminGfcDel") = false
	
	Session("AdminFormAdd") = false
	Session("AdminFormUpd") = false
	Session("AdminFormDel") = false
	
	Session("AdminEventPhotoAdd") = false
	Session("AdminEventPhotoUpd") = false
	Session("AdminEventPhotoDel") = false
	
	Session("AdminForumAdd") = false
	Session("AdminForumUpd") = false
	Session("AdminForumDel") = false
End Sub

Sub showErrMsg
	if not isEmpty(Session("actionErrIdArr")) then
		if IsArray(Session("actionErrIdArr")) then
			if ubound(Session("actionErrIdArr")) > 0 and Not isEmpty(Session("actionErrIdArr")(0)) and Not isEmpty(Session("actionErrIdArr")(1)) then
				Dim i
				Response.Write "<font size=""2"" color=""#FF0000"">错误：<ul>"
				for i = 0 to ubound(Session("actionErrIdArr")) - 1
					if errIdArr(i) <> "" then
						Response.Write "<li>" & commonErrMsg(Session("actionErrIdArr")(i))
					end if
				Next
				Response.Write "</ul></font>"
				Session("actionErrIdArr") = empty
			elseif Not isEmpty(Session("actionErrIdArr")(0)) then
				Response.Write "<font size=""2"" color=""#FF0000"">错误：" & commonErrMsg(Session("actionErrIdArr")(0)) & "</font>"
				Session("actionErrIdArr") = empty
			end if
		else
			if Session("actionErrIdArr") <> "" then
				Response.Write "<font size=""2"" color=""#FF0000"">错误：" & commonErrMsg(Session("actionErrIdArr")) & "</font>"
				Session("actionErrIdArr") = empty
			end if
		end if
	end if
End Sub

Sub showLocalErrMsg(errIdArr)
	if not isEmpty(errIdArr) then
		if IsArray(errIdArr) then
			if ubound(errIdArr) > 0 and Not isEmpty(errIdArr(0)) and Not isEmpty(errIdArr(1)) then
				Dim i
				Response.Write "<font size=""2"" color=""#FF0000"">错误：<ul>"
				for i = 0 to ubound(errIdArr) - 1
					if errIdArr(i) <> "" then 
						'Response.Write "<li>" & errIdArr(i) & " - "  & commonErrMsg(errIdArr(i))
						Response.Write "<li>" & commonErrMsg(errIdArr(i))
					end if
				Next
				Response.Write "</ul></font>"
			elseif Not isEmpty(errIdArr(0)) then
				Response.Write "<font size=""2"" color=""#FF0000"">错误：" & commonErrMsg(errIdArr(0)) & "</font>"
			end if
		else
			if errIdArr <> "" then
				Response.Write "<font size=""2"" color=""#FF0000"">错误：" & commonErrMsg(errIdArr) & "</font>"
			end if
		end if
		errIdArr = empty
	end if
End Sub

Function getValue(rsObj, formField, dbField)
	if isEmpty(Request.Form(formField)) then

		if not isEmpty(rsObj) and rsObj.state > 0 then

			getValue = rsObj(dbField)

		else
			getValue = ""
		end if
	else

		getValue = Request.Form(formField)
	end if
end function

Function getValueInArr(rsObj, formField, dbField, i)
	if Request.Form(formField) = "" then
		if not isEmpty(rsObj) and rsObj.state > 0   then
			if rsObj.recordCount  = 1 then
				if i = 0 then
					getValueInArr = getValue(rsObj, formField, dbField)
				else
					getValueInArr = ""
				end if
			elseif i+1 < rsObj.recordCount then
				rsObj.Move i+1, 1
				getValueInArr = rsObj(dbField)
				rsObj.moveFirst
			end if
		else
			getValueInArr = ""
		end if
	else
		Dim theFormFieldArray
	
		theFormFieldArray = split(Request.Form(formField), ", ")
		
		if arrayLength(theFormFieldArray) = 0 or i >= arrayLength(theFormFieldArray) then
			getValueInArr = ""
			exit function
		end if
		
		getValueInArr = theFormFieldArray(i)
	end if
end function

Function getCheckBoxState(value)
	if not isEmpty(value) then
		if trim(value) = "Y" Or trim(value) = "1" then
			getCheckBoxState = "checked"
		end if
	end if
end function

Function getDocLink(docLink)
	if not isEmpty(docLink) and  trim(docLink) <> "" then
		if "" <> docLink then
			Dim lastStrokeIdx
			lastStrokeIdx = InStrRev(docLink,"/")
			if lastStrokeIdx > 0 then
				getDocLink = "<a href=""" & docLink &  """ class=""link3ZC"" target=""_blank"">" & Mid(docLink,lastStrokeIdx + 1) & "</a>"
			else
				getDocLink = "<a href=""" & docLink &  """ class=""link3ZC"" target=""_blank"">" & docLink & "</a>" 
			end if
		End if
	else
		getDocLink = "&nbsp;"
	End if
End Function

Sub writeDtlButtRow
    Response.write "<tr>" 
    Response.write "<td height=""25"" class=""contentNZC"">&nbsp;</td>"
    Response.write "<td><p>"
	Response.write "<input type=""submit"" value=""储存"" class=""button3ZC"">&nbsp;&nbsp;&nbsp;&nbsp;<input type=""reset"" value=""重设"" class=""button3ZC"">"
    Response.write "<br>"
    Response.write "</p></td>"
    Response.write "</tr>"
End Sub

Sub writeAuditDetailRow (ByRef objRst, ByVal isMntAdmin)
	Response.Write("<tr>" )
	Response.Write("<td height=""10"" colspan=""2"" class=""contentNZC"" valign=""top""><img src=""/images/color_30k.gif"" width=""100%"" height=""1""></td>")
	Response.Write("</tr>")
    Response.write "<tr>"
    Response.write "<td height=""25"" class=""contentNZC"" valign=""top"">上次更新的管理员</td>"
    Response.write "<td valign=""top"" class=""contentNZC"">" & IIf(isMntAdmin, objRst("ModBy"), objRst("AdminLoginName")) & "</td>"
    Response.write "</tr>"
	Response.Write("<tr>" )
	Response.Write("<td height=""10"" colspan=""2"" class=""contentNZC"" valign=""top""><img src=""/images/color_30k.gif"" width=""100%"" height=""1""></td>")
	Response.Write("</tr>")	
    Response.write "<tr>"
    Response.write "<td height=""25"" class=""contentNZC"" valign=""top"">上次更新日期</td>"
    Response.write "<td valign=""top"" class=""contentNZC"">" & formatDateWithTime(objRst("ModDt"), "/", 0) & "</td>"
    Response.write "</tr>"
	Response.Write("<tr>" )
	Response.Write("<td height=""10"" colspan=""2"" class=""contentNZC"" valign=""top""><img src=""/images/color_30k.gif"" width=""100%"" height=""1""></td>")
	Response.Write("</tr>")
End Sub

' outputPattern
'	0 : DD/MM/YYYY HH:min:ss 		, where sep = /
'	1 : MM/DD/YYYY HH:min:ss		, where sep = /
'	2 : DD/MMM/YYYY	HH:min:ss	    , where sep = / and MMM is Month name such Jan, Feb
' 	3 : DD/MMMM/YYYY HH:min:ss	    , where sep = / and MMMM is Month name such January, February
' theDate has to been date, String will not be processed
' formatDate will return String
function formatDateWithTime(theDate, sep, outputPattern)
	if Not IsEmpty(theDate) And theDate <> "" then
		if varType(theDate) <> 7 then
			formatDate = ""
			exit function
		end if
	
		select case outputPattern
			case 0 : 
				formatDateWithTime = IIf(Len(Day(theDate)) < 2, "0" & Day(theDate), Day(theDate)) & sep & IIf(Len(month(theDate)) < 2, "0" & month(theDate), month(theDate)) & sep & Year(theDate) & " " & IIf(Len(Hour(theDate)) < 2, "0" & Hour(theDate) , Hour(theDate)) & ":" & IIf(Len(Minute(theDate)) < 2, "0" & Minute(theDate) , Minute(theDate) ) & ":" & IIf(Len(Second(theDate)) < 2, "0" & Second(theDate), Second(theDate))
			case 1 : 
				formatDateWithTime = IIf(Len(month(theDate)) < 2, "0" & month(theDate), month(theDate)) & sep & IIf(Len(Day(theDate)) < 2, "0" & Day(theDate), Day(theDate)) & sep & Year(theDate) & " " & IIf(Len(Hour(theDate)) < 2, "0" & Hour(theDate) , Hour(theDate)) & ":" & IIf(Len(Minute(theDate)) < 2, "0" & Minute(theDate) , Minute(theDate) ) & ":" & IIf(Len(Second(theDate)) < 2, "0" & Second(theDate), Second(theDate))
			case 2 :
				formatDateWithTime = IIf(Len(Day(theDate)) < 2, "0" & Day(theDate), Day(theDate)) & sep & monthname(IIf(Len(month(theDate)) < 2, "0" & month(theDate), month(theDate)), true) & sep & Year(theDate) & " " & IIf(Len(Hour(theDate)) < 2, "0" & Hour(theDate) , Hour(theDate)) & ":" & IIf(Len(Minute(theDate)) < 2, "0" & Minute(theDate) , Minute(theDate) ) & ":" & IIf(Len(Second(theDate)) < 2, "0" & Second(theDate), Second(theDate))
			case 3 :
				formatDateWithTime = IIf(Len(Day(theDate)) < 2, "0" & Day(theDate), Day(theDate)) & sep & monthname(IIf(Len(month(theDate)) < 2, "0" & month(theDate), month(theDate)), false) & sep & Year(theDate) & " " & IIf(Len(Hour(theDate)) < 2, "0" & Hour(theDate) , Hour(theDate)) & ":" & IIf(Len(Minute(theDate)) < 2, "0" & Minute(theDate) , Minute(theDate) ) & ":" & IIf(Len(Second(theDate)) < 2, "0" & Second(theDate), Second(theDate))
		end select
	end if
end function

' outputPattern : DD/MM/YYYY 		
' theDate has to been date, String will not be processed
' formatDate will return String
function formatAdminMntDate(theDate)
	if Not isEmpty(theDate) then
		if varType(theDate) <> 7 then
			formatAdminMntDate = theDate
		else
			formatAdminMntDate = IIf(Len(Day(theDate)) < 2, "0" & Day(theDate), Day(theDate)) & "/" & IIf(Len(month(theDate)) < 2, "0" & month(theDate), month(theDate)) & "/" & Year(theDate)
		end if
	else
		formatAdminMntDate = theDate
	end if
	
end function

function formatAdminMntTime(theTime)
	if Not isEmpty(theTime) then
		if varType(theTime) <> 7 then
			formatAdminMntTime = theTime
		else
			dim h, m, s
			
			h = IIf(Len(Hour(theTime)) < 2, "0" & Hour(theTime), Hour(theTime))
			m = IIf(Len(Minute(theTime)) < 2, "0" & Minute(theTime), Minute(theTime))
			s = IIf(Len(Second(theTime)) < 2, "0" & Second(theTime), Second(theTime))
			
			formatAdminMntTime = h & ":" & m & ":" & s
			
		end if
	else
		formatAdminMntTime = theTime
	end if

end function

'format should be dd/mm/yyyy
function isValidAdminInputDate(inDate)
	if not isEmpty(inDate) then
		if Len(inDate) = 10 then
		if Mid(inDate, 3, 1) = "/" And Mid(inDate, 6, 1) = "/" then
				if isDate(inDate) then
					isValidAdminInputDate = true
				else
					isValidAdminInputDate = false
				end if
			else
				isValidAdminInputDate = false
			end if
		else
			isValidAdminInputDate = false
		end if
	else
		isValidAdminInputDate = false
	end if
end function

'format should be hh:mm
function isValidAdminInputTime(inTimeStr)
	if inTimeStr = "" then 	
		isValidAdminInputTime = false
		exit function
	end if
	
	dim inTimeArray
	
	inTimeArray = split(inTimeStr, ":")
	
	if arrayLength(inTimeArray) <> 3 then
		isValidAdminInputTime = false
		exit function
	end if

	dim h, m, s
	h = inTimeArray(0)
	m = inTimeArray(1)
	s = inTimeArray(2)
	
	if not isnumeric(h) or not isnumeric(m) or not isnumeric(s) then
		isValidAdminInputTime = false
		exit function
	end if
	
	h = int(h)
	m = int(m)
	s = int(s)
	
	if h < 0 or h > 23 or m < 0 or m > 59 or s < 0 or s > 59 then
		isValidAdminInputTime = false
		exit function
	end if
	
	isValidAdminInputTime = true
end function

Function isValidMail(strEmail)
    'our variables
    Dim objRegExp , blnValid
    'create a new instance of the RegExp object , note we do not need Server.CreateObject("")
    Set objRegExp = New RegExp
    'this is our pattern we check .
    objRegExp.Pattern = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
    blnValid = objRegExp.Test(strEmail)
    If blnValid Then
        isValidMail = true
    Else
        isValidMail = false
    End If 
End Function
%>
