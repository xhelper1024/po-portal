<%@ CodePage="65001"%>
<% Option Explicit %>
<!--#include virtual="/b_util/user_common_include.asp"-->
<!--#include virtual="/b_ui/display.asp"-->
<!--#include virtual="/purchase/conf.asp"-->
<link   href="/style/form.css" type="text/css"></link >
<link   href="/style/style.css" type="text/css"></link >
<script type="text/javascript" src="/script/jquery-1.5.2.js"></script>
<script type="text/javascript" src="/script/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/script/update_line.js"></script>
<script type="text/javascript" src="/script/calendar.js"></script>
<script type="text/javascript" src="/script/qrcodejs-master/qrcode.js"></script>
<style rel="stylesheet">
#print-page {
	display: none;
}
</style>

<style media="print">
#print-page {
	display: block;
}

#print-page table {
	width: 100%;
}
</style>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%
Dim addPreloadImg
%>
<!--#include virtual="/framework/head.asp"-->
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2" ><!--#include virtual="/framework/top_nav.asp"--></td>
	</tr>
	<tr>
		<td colspan="2"><!--#include virtual="/framework/banner.asp"--></td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<!--#include virtual="/framework/left_nav.asp"-->
					<td valign="top">
						<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>  
								<td height="350" valign="top">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr> 
											<td height="22" background="/images/bg_heading.gif" id="contentID"><span class="H1ZC">Print Label</span></td>
										</tr>
										<tr> 
											<td valign="top" class="contentNZC">
												<form name="searchForm">
													<table id="myTab" width="100%" border="0" align="center" cellpadding="2" cellspacing="0" overflow:scroll>
														<tr>
															<th align="left">
																<label>物料编号：<input type="text" name="item" id="item" value="<%=request.querystring("item")%>" disabled="disabled"/></label>
															</th>
															<th align="left">
																<label>生产日期：<input type="input" name="create_date" id="create_date" onclick="new Calendar().show(this);"/></label>
															</th>
															<th align="left">
																<label>生产批次：<input type="text" name="create_no" id="create_no"/></label>
															</th>
															<th align="left">
																<label>剩余PO未交货数量：<input type="number" name="remain" id="remain" value="13000" disabled="disabled"/></label>
															</th>
															<th align="left">
																<label>最小包装数量：<input type="number" name="spq" id="spq" value="2000" disabled="disabled"/></label>
															</th>
														</tr>
														<tr><td colspan="5"><hr></td></tr>
														<tr>
															<td><b>序号</b></td>
															<td colspan="4"><b>标签数量</b></td>
														</tr>
														<!-- <tr class="label">
															<td>1</td>
															<td colspan="4">
																<input class="label-qty" type="text" value="" disabled="disabled"/><input type="button" onclick="updateLabel(this)" value="修改"/>
															</td>
														</tr> -->
													</table>
													<hr>
													<input type="button" onclick="newLabel(0)" value="新增标签"/>
													<input type="button" onclick="printLabel()" value="打印"/>
													<span id="tip"></span>
												</form>
												<div id="content"></div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<!--#include virtual="/framework/foot_nav.asp"-->
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<iframe id="print-page">
	<div style="width: 100; border: 1px solid #000; text-align: center; padding-bottom: 20px;">
		<h2>物料标识卡</h2>
		<table style="margin: auto;" border="1" cellspacing="0">
			<tr>
				<td colspan="2" rowspan="6">
					<div id="qrcode1">
						<img src="/sdf.png">
					</div>
				</td>
				<th>ID</th>
				<td>123456789012</td>
			</tr>
			<tr>
				<th>供应商</th>
				<td>深圳腾讯有限公司</td>
			</tr>
			<tr>
				<th>厂商批号</th>
				<td>AVW18C07XD</td>
			</tr>
			<tr>
				<th>生产日期</th>
				<td>AVW18C07XD</td>
			</tr>
			<tr>
				<th>失效日期</th>
				<td>AVW18C07XD</td>
			</tr>
			<tr>
				<th>存放区域</th>
				<td>AVW18C07XD</td>
			</tr>
			<tr>
				<th>订单</th>
				<td>AVW18C07XD</td>
				<th>数量</th>
				<td>AVW18C07XD</td>
			</tr>
			<tr>
				<th>物料</th>
				<td>AVW18C07XD</td>
				<th>类型</th>
				<td>AVW18C07XD</td>
			</tr>
			<tr>
				<th>描述</th>
				<td colspan="3">AVW18C07XD</td>
			</tr>
		</table>
	</div>
</iframe>
<!-- <script type="text/javascript" src="/script/search.js"></script> -->
<!--#include virtual="/framework/footing.asp"-->
</body>
<script>
	$(document).on("input", ".label-qty", function() {
		var totalQty = 0;
		var spqQty = parseInt($("#spq").val());
		// 统计总数
		var thisQty = parseInt($(this).val());
		$(".label-qty").each(function() {
			totalQty = totalQty + parseInt($(this).val());
		});
		// totalQty = totalQty - thisQty;
		remain = parseInt($("#remain").val());
		// 总数不能超过PO剩余交货数量
		if(totalQty > remain) {
			if (remain + thisQty - totalQty > spqQty)
				$(this).val(spqQty);
			else
				$(this).val(remain + thisQty - totalQty)
		}
		// 当前输入的值如果大于最小
		if(thisQty > spqQty) {
			if(remain + thisQty - totalQty > spqQty)
				$(this).val(spqQty)
			else
				$(this).val(remain + thisQty - totalQty)
		}
	});
	
	window.onload = function() {
		remain = parseInt($("#remain").val());
		spq = parseInt($("#spq").val());
		qty = Math.round(remain / spq);
		for(var i = 0; i < qty; i++) {
			if(remain > spq)
				newLabel(spq);
			else
				newLabel(remain);
			remain = remain - spq;
		}
	}
	
	function updateLabel(obj) {
		domQty = obj.parentNode.children[0];
		domQty.removeAttribute("disabled");
		domQty.focus();
		obj.setAttribute("value", "保存");
		obj.setAttribute("onclick", "saveLabel(this)");
	}
	
	function removeLabel(obj) {
		flag = confirm("确定移除？");
		if(flag) {
			domQty = obj.parentNode.parentNode;
			domLabel = domQty.nextSibling;
			while(domLabel) {
				if(isIe()) labelText = domLabel.childNodes[0];
				else labelText = domLabel.children[0];
				labelText.innerText = labelText.innerText - 1;
				domLabel = domLabel.nextSibling;
			}
			if (isIe())
				domQty.parentNode.removeChild(domQty);
			else
				domQty.remove();
		}
	}
	
	function saveLabel(obj) {
		domQty = obj.parentNode.children[0];
		domQty.setAttribute("disabled", "disabled");
		obj.setAttribute("value", "修改");
		obj.setAttribute("onclick", "updateLabel(this)");
	}
	
	function newLabel(val) {
		newRow = document.createElement("tr");
		newRow.setAttribute("class", "label");
		col1 = document.createElement("td");
		col2 = document.createElement("td");
		col2.setAttribute("colspan", "4");
		
		input1 = document.createElement("input");
		input1.setAttribute("class", "label-qty");
		input1.setAttribute("type", "number");
		input1.setAttribute("max", $("#spq").val());
		input1.setAttribute("min", 0);
		input1.setAttribute("value", val);
		input1.setAttribute("disabled", "disabled");
		input2 = document.createElement("input");
		input2.setAttribute("type", "button");
		input2.setAttribute("onclick", "updateLabel(this)");
		input2.setAttribute("value", "修改");
		input3 = document.createElement("input");
		input3.setAttribute("type", "button");
		input3.setAttribute("onclick", "removeLabel(this)");
		input3.setAttribute("value", "移除");
		
		col2.appendChild(input1);
		col2.appendChild(input2);
		col2.appendChild(input3);
		col1.appendChild(document.createTextNode(getLabelLength() + 1));
		newRow.appendChild(col1);
		newRow.appendChild(col2);
		
		myTab = document.getElementById("myTab");
		myTab.appendChild(newRow);
	}
	
	function emptyCheck() {
		if($("#create_date").val().trim() == "") {
			alert("生产日期不能为空！");
			$("#create_date").focus();
			return false;
		}
		if($("#create_no").val().trim() == "") {
			alert("生产批次不能为空！");
			$("#create_no").focus();
			return false;
		}
		return true;
	}
	
	function printLabel() {
		flag = confirm("确定打印？");
		if(flag && emptyCheck()) {
			// 非空检测;
			// 动态设置打印内容
			setPrintContent();
			// 向v中追加打印数据，可以将界面的元素追加进来
			// var h = window.open("打印窗口", "_blank");
			// h.document.write($("#print-page").prop("outerHTML"));
			// h.document.close();
			// h.print();
			// h.close();
			// $("#print-page").print();
			// var newstr = $("#print-page").html(); 
			// console.log($("#print-page")[0]);
			$("#print-page")[0].contentWindow.print();
			
		}
	}
	
	/*
	<div style="width: 100; border: 1px solid #000; text-align: center; padding-bottom: 20px;">
		<h2>物料标识卡</h2>
		<table style="margin: auto;" border="1" cellspacing="0">
			<tr>
				<td colspan="2" rowspan="6">
					<div id="qrcode1">
						<img src="/sdf.png">
					</div>
				</td>
				<th>ID</th>
				<td>123456789012</td>
			</tr>
			<tr>
				<th>供应商</th>
				<td>深圳腾讯有限公司</td>
			</tr>
			<tr>
				<th>厂商批号</th>
				<td>AVW18C07XD</td>
			</tr>
			<tr>
				<th>生产日期</th>
				<td>AVW18C07XD</td>
			</tr>
			<tr>
				<th>失效日期</th>
				<td>AVW18C07XD</td>
			</tr>
			<tr>
				<th>存放区域</th>
				<td>AVW18C07XD</td>
			</tr>
			<tr>
				<th>订单</th>
				<td>AVW18C07XD</td>
				<th>数量</th>
				<td>AVW18C07XD</td>
			</tr>
			<tr>
				<th>物料</th>
				<td>AVW18C07XD</td>
				<th>类型</th>
				<td>AVW18C07XD</td>
			</tr>
			<tr>
				<th>描述</th>
				<td colspan="3">AVW18C07XD</td>
			</tr>
		</table>
	</div>
	*/
	function setPrintContent() {
		var totalQty = 0;
		$(".label-qty").each(function() {
			totalQty = totalQty + parseInt($(this).val());
		});
		// 先清空内容
		$($("#print-page")[0].contentWindow.document.body).html("");
		totalLabel = getLabelLength();
		for(var i = 1; i <= totalLabel; i++) {
			$($("#print-page")[0].contentWindow.document.body).append(`
			<div style="width: 100%; border: 1px solid #000; text-align: center; padding-bottom: 20px; margin-top: 20px; page-break-before:always;">
				<h2>物料标识卡</h2>
				<table style="margin: auto; text-align: center; width: 90%;" border="1" cellspacing="0">
					<tr>
						<td colspan="2" rowspan="7">
							<div id="qrcode` + i + `" class="qrcode"></div>
						</td>
						<th>ID</th>
						<td><%=request.querystring("item")%></td>
					</tr>
					<tr>
						<th>供应商</th>
						<td>深圳腾讯有限公司</td>
					</tr>
					<tr>
						<th>厂商批号</th>
						<td>AVW18C07XD</td>
					</tr>
					<tr>
						<th>生产日期</th>
						<td>2019-09-20</td>
					</tr>
					<tr>
						<th>失效日期</th>
						<td>2023-09-20</td>
					</tr>
					<tr>
						<th>存放区域</th>
						<td>B2F-6C</td>
					</tr>
					<tr>
						<th>CTN NO</th>
						<td>` + i + `/` + totalLabel + `</td>
					</tr>
					<tr>
						<th>订单</th>
						<td>POSIO123129</td>
						<th>数量/总数</th>
						<td>` + $(".label-qty")[i - 1].value + `/` + totalQty + `</td>
					</tr>
					<tr>
						<th>物料</th>
						<td><%=request.querystring("item")%></td>
						<th>类型</th>
						<td>关键物料</td>
					</tr>
					<tr>
						<th>描述</th>
						<td colspan="3">物料描述</td>
					</tr>
				</table>
			</div>
			`);
			setQrCode("qrcode" + i, "<%=request.querystring("item")%>");
		}
		// console.log($("#print-page").html());
	}
	
	function getLabelLength() {
		domLabels = document.getElementsByClassName("label");
		return domLabels.length;
	}
	
	function insertAfter(newElement, targentElement) {
        var parent = targentElement.parentNode;
        if (parent.lastChild == targentElement) {
            parent.appendChild(newElement);
        } else {
            parent.insertBefore(newElement, targentElement.nextSibling)
        }
    }
	
	function isIe() {
		return !!window.ActiveXObject || "ActiveXObject" in window;
	}
	
	function setQrCode(id, text) {
		new QRCode($("#print-page")[0].contentWindow.document.getElementById(id), {
			// text: JSON.stringify(jsonTxt),
			text: text,
			width: 128,
			height: 128,
			colorDark : "#000000",
			colorLight : "#ffffff",
			correctLevel : QRCode.CorrectLevel.H
		});
	}
</script>
</html>
