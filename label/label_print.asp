<%@ CodePage = "65001" %>
<% Option Explicit %>
<!--#include virtual="/b_ui/display.asp"-->
<!--#include virtual="/purchase/conf.asp"-->
<!--#include virtual="/b_util/user_common_include.asp"-->
<script src="/script/check.js"></script>
<script type="text/javascript" src="/static/qrcodejs-master/qrcode.min.js"></script>
<style>
.form-horizontal .row {
	margin: 0 55px 0 -15px;
}
</style>
<!--#include virtual="/label/utils/label_print_controller.asp"-->
<div class="panel panel-default">
	<div class="panel-heading">
		<h1>Download Label</h1>
	</div>
	<div class="panel-body">
		<div id="myTabContent" class="tab-content">
			<div class="tab-pane fade in active" id="list">
				<div class="row form-group">
					<div class="col-sm-12">
						<ul class="breadcrumb">
							<li>订单号：<code id="po_no">xxx</code></li>
							<li>项目：<code id="line_no">xxx</code></li>
							<li>物料编号：<code id="item">xxx</code></li>
							<li>创建日期：<code id="po_date">xxx</code></li>
							<li>采购订单数量：<code id="po_qty">0</code></li>
							<li>未清数量：<code id="delivery_qty">800</code></li>
							<!--<li>计量单位：<span id="unit">xxx</span></li>-->
							<li class="hide">状态：<code id="status">xx</code></li>
							<li class="hide">物料类型：<code id="item_type">xxx</code></li>
							<li class="hide">物料描述：<code id="item_desc">xxx</code></li>
							<li class="hide">制造商：<code id="mfg">xx</code></li>
							<li class="hide">制造商名称：<code id="mfg_name">xxx</code></li>
							<li class="hide">MPN：<code id="mfg_no">xxx</code></li>
							<li class="hide">关键物料：<code id="normt">xxx</code></li>
							<li class="hide">ABC标识：<code id="maabc">xxx</code></li>
							<li class="hide">isNewRecord：<code id="is_new_record">xx</code></li>
							<li class="hide">工厂：<code id="werks">xxxx</code></li>
							<li class="hide">工厂：<code id="werks_name">xxxx</code></li>
							<li class="hide">存放区域：<code id="store_position">xx</code></li>
							<li class="hide">检查间隔：<code id="expire_date">xx</code></li>
						</ul>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-4">
						<div class="input-group">
							<span class="input-group-addon" title="已打印数量 * Printed Qty" data-trigger="hover" data-container="body"
								data-toggle="popover" data-placement="top" data-content="已经包装并生成条码的数量">
								已打印数量 * Printed Qty <i class="glyphicon glyphicon-question-sign"></i></span>
							<input type="number" class="form-control" min="0" id="already_qty" name="already_qty" disabled="disabled"/>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="input-group">
							<span class="input-group-addon" title="可打印数量 * Printable Qty" data-trigger="hover" data-container="body"
								data-toggle="popover" data-placement="top" data-content="可打印数量<=采购订单数量-已打印数量">
								可打印数量 * Printable Qty <i class="glyphicon glyphicon-question-sign"></i></span>
							<input type="number" class="form-control" min="0" id="remain_qty" name="remain_qty"/>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="input-group">
							<span class="input-group-addon" title="采购订单单位">采购订单单位 * Unit</span>
							<input type="text" class="form-control" readonly="readonly" id="unit"/>
						</div>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-4">
						<div class="input-group">
							<span class="input-group-addon" title="最小包装数量 * SPQ" data-trigger="hover" data-container="body"
								data-toggle="popover" data-placement="top" data-content="每箱可包装数量">
								最小包装数量 * SPQ <i class="glyphicon glyphicon-question-sign"></i></span>
							<input type="number" class="form-control" min="0" id="self_spq" name="self_spq" />
						</div>
					</div>
					<div class="col-sm-4">
						<div class="input-group">
							<span class="input-group-addon text-info" title="每箱可装SPQ * SPQ Qty/Ctn" data-trigger="hover" data-container="body"
								data-toggle="popover" data-placement="bottom" data-content="注1：【每箱可装SPQ】表示每个外箱可以包装多少个最小包装 
								PS:[SPQ Qty/Ctn] represents how many minimum packages packed per carton 
								注2：【每箱可装SPQ】不填则表示不需要外箱包装 
								PS:[SPQ Qty/Ctn] represents no need extra outer packing if there is not fill in ">
								每箱可装SPQ * SPQ Qty/Ctn <i class="glyphicon glyphicon-inbox"></i></span>
							<input type="number" class="form-control" min="0" id="box_spq" name="box_spq" />
						</div>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-4">
						<div class="input-group">
							<span class="input-group-addon" title="Tips" data-trigger="hover"
							data-container="body" data-toggle="popover" data-placement="top" 
							data-content="生产批次 * Lot No.">生产批次 * Lot No.<i class="glyphicon glyphicon-info-sign"></i></span>
							<input class="form-control" id="prod_no" name="prod_no" type="text"/>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="input-group">
							<span class="input-group-addon" title="Tips" data-trigger="hover"
							data-container="body" data-toggle="popover" data-placement="top" 
							data-content="生产日期 * Date Code">生产日期 * Date Code <i class="glyphicon glyphicon-calendar"></i></span>
							<input class="form-control datepicker" autocomplete="off" id="prod_date" name="prod_date" type="text"/>
						</div>
					</div>
					<div class="col-sm-4">
						<button class="btn btn-sm btn-info" title="Translate" data-trigger="hover"
							data-container="body" data-toggle="popover" data-placement="top" 
							data-content="Package" id="btn-package" onclick="generateLable()"><span class="glyphicon glyphicon-inbox"></span> 包装 </button>
						<button class="btn btn-sm btn-success" title="Translate" data-trigger="hover"
							data-container="body" data-toggle="popover" data-placement="top" 
							data-content="Generate lable and Save" id="btn-generate" onclick="saveLabel()" disabled><span class="glyphicon glyphicon-qrcode"></span> 生成并保存标签 </button>
						<button class="btn btn-sm btn-primary" title="Translate" data-trigger="hover"
							data-container="body" data-toggle="popover" data-placement="top" 
							data-content="Download" id="btn-download" onclick="downloadLabel()" disabled><span class="glyphicon glyphicon-download-alt"></span> 下载 </button>
						<button class="btn btn-sm btn-primary" title="Translate" data-trigger="hover"
							data-container="body" data-toggle="popover" data-placement="top" 
							data-content="Print" id="btn-print" onclick="printLabel()" disabled><span class="glyphicon glyphicon-print"></span> 打印 </button>
						<a class="btn btn-sm btn-default" title="Translate" data-trigger="hover"
							data-container="body" data-toggle="popover" data-placement="top" 
							data-content="Back" href="javascript: history.go(-1);;"><span class="glyphicon glyphicon-circle-arrow-left"></span> 返回 </a>
						<button class="btn btn-sm btn-link" title="Translate" data-trigger="hover"
							data-container="body" data-toggle="popover" data-placement="top" 
							data-content="Refresh" onclick="history.go(0)"><span class="glyphicon glyphicon-refresh"></span> 刷新 </button>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-12">
						<ul class="breadcrumb" style="margin: 0rem;">
							<li>外箱标签数量：<code id="box_qty">0</code></li>
							<li>最小包装标签数量：<code id="spq_qty">0</code></li>
							<li>包装总数量：<code id="total_qty">0</code></li>
							<li class="hide"><a class="text-info" id="btn-new-box-label" onclick="newBoxLabel(0, 0, 0); computeBoxQty(1)">新增外箱</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel-footer">
		<div class="row form-group hide" id="label">
			<div class="col-sm-6">
				<h3>标签1</h3>
				<table class="table table-striped table-bordered table-hover table-condensed" id="myTab0">
					<thead>
					<tr>
						<th>序号</th>
						<th>条码ID</th>
						<th>标签类型</th>
						<th>包装数量</th>
						<th>物料数量</th>
						<th>操作</th>
					</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<iframe id="print-page"></iframe>
<script>
var tempData = JSON.parse(sessionStorage.getItem("<%=request("po_no") & request("line_no")%>"));
$(function () {
	// 提示
	$("[data-toggle='popover']").popover();
	
	// 初始化datepicker
	$(".datepicker").datepicker({
		language: "zh-CN",
		autoclose: true, //选择后自动关闭
		clearBtn: true,//清除按钮
		format: "yyyy-mm-dd",
		todayHighlight : true,
		todayBtn: "linked",
		initialDate: new Date(), //也可以通过function函数获得值
	});
	// 如果不是Chrome浏览器，则不允许打印功能
	var ua = navigator.userAgent.toLocaleLowerCase();
	if(ua.indexOf("chrome") == -1 || ua.indexOf("edge") != -1) {
		$('#btn-print').remove();
	}
});

// 输入检测
$(document).on("input", "#myTab1 .package-qty", function() {
	var totalQty = 0;
	var spqQty = parseInt(parseInt($("#self_spq").val())!=0 && $("#self_spq").val().trim()!="" ? $("#self_spq").val(): $("#spq").text());
	// 统计总数
	var thisQty = parseInt($(this).val());
	$(".myTab .package-qty").each(function() {
		totalQty = totalQty + parseInt($(this).val().trim());
	});
	// totalQty = totalQty - thisQty;
	deliveryQty = parseInt($("#delivery_qty").text().trim());
	// 总数不能超过PO剩余交货数量
	if(totalQty > deliveryQty) {
		if (deliveryQty + thisQty - totalQty > spqQty)
			$(this).val(spqQty);
		else
			$(this).val(deliveryQty + thisQty - totalQty)
	}
	// 当前输入的值如果大于最小
	if(thisQty > spqQty) {
		if(deliveryQty + thisQty - totalQty > spqQty)
			$(this).val(spqQty)
		else
			$(this).val(deliveryQty + thisQty - totalQty)
	}
});

function labelInit() {
	$('#label').html(
	'<div class="col-sm-6">'
	+	'<h3>标签1</h3>'
	+	'<table class="table table-striped table-bordered table-hover table-condensed" id="myTab0">'
	+		'<thead>'
	+		'<tr>'
	+			'<th>序号</th>'
	+			'<th>条码ID</th>'
	+			'<th>标签类型</th>'
	+			'<th>物料数量</th>'
	+			'<th>操作</th>'
	+		'</tr>'
	+		'</thead>'
	+		'<tbody></tbody>'
	+	'</table>'
	+'</div>');
}

// 生成标签 By 谢辉 2020-04-10 16:51:48
function generateLable() {
	if(emptyCheck()) {
		var remainQty = parseFloat($('#remain_qty').val().trim());	// 可打印标签数量
		var spq = parseFloat($('#self_spq').val().trim());			// 最小包装数量
		var labelQty = Math.ceil(remainQty / spq);					// 要生成的标签数量
		$('#spq_qty').html(labelQty);								// 显示要生成的标签数量
		$('#total_qty').html(remainQty);							// 显示包装总数量
		var boxSpq = parseInt($('#box_spq').val().trim());			// 每箱可装SPQ
		var max_qty = 100
		if(boxSpq && boxSpq != 0) {
			$('#label').html("");
			$('#btn-new-box-label').parent().removeClass('hide');
			var boxLabelQty = Math.ceil(labelQty / boxSpq);
			$('#box_qty').html(boxLabelQty);						// 显示外箱的标签数量
			// 如果标签数量超过100张，则不允许一次性生成
			if (boxLabelQty + labelQty > max_qty) {
				parent.layer.alert('包装时，本次标签数量（= 外箱标签数量 + 最小包装标签数量）不允许超过' + max_qty + '张，请修改参数', {icon: 2});
				$("#remain_qty").focus();
				return false;
			}
			for(var i = 0; i < boxLabelQty; i++) {					// 生成标签，带外箱
				if(remainQty > spq * boxSpq) {
					newBoxLabel(spq, spq * boxSpq, boxSpq);
				} else {
					newBoxLabel(spq, remainQty, boxSpq);
				}
				remainQty = remainQty - (spq * boxSpq);
			}
		} else {
			labelInit(); 											// 初始化标签栏
			$('#btn-new-box-label').parent().addClass('hide');		// 
			$('#box_qty').html(0);									// 显示外箱的标签数量
			// 如果标签数量超过100张，则不允许一次性生成
			if (labelQty > max_qty) {
				parent.layer.alert('包装时，本次标签数量（= 外箱标签数量 + 最小包装标签数量）不允许超过' + max_qty + '张，请修改参数', {icon: 2});
				$("#remain_qty").focus();
				return false;
			}
			for(var i = 0; i < labelQty; i++) {						// 生成标签
				if(remainQty > spq) {
					newLabel("myTab0", spq);
				} else {
					newLabel("myTab0", remainQty);
				}
				remainQty = remainQty - spq;
			}
		}
		$('#label').removeClass('hide');							// 取消隐藏
		$("#btn-generate").removeAttr("disabled");					// 移除禁用属性
	}
}

// 新增标签
function newLabel(id, qty) {
	$("#" + id + " tbody").append(
	'<tr>'
	+	'<td>' + ($("#" + id + " tbody").find('.package-qty').length + 1) + '</td>'
	+	'<td></td>'
	+	'<td class="text-primary" data-type="02">最小包装条码</td>'
	+	'<td>'
	+		'<input class="form-control package-qty" type="number" min="0" maxlength="' + qty + '" value="' + qty + '"/>'
	+	'</td>'
	+	'<td class="text-center">'
	+		'<button class="btn btn-sm btn-danger btn-delete" onclick="removeLabel(this)" title="删除最小包装条码"><i class="glyphicon glyphicon-minus"></i></button>'
	+	'</td>'
	+'</tr>');
}

// 在标签后增加内箱标签
function newLabelAfter(obj) {
	// console.log($(obj).parent().parent().parent().parent().find('.package-qty'))
	spq = parseFloat($("#self_spq").val());
	$(obj).parent().parent().parent().parent().append(
	'<tr>'
	+	'<td>' + ($(obj).parent().parent().parent().find('.package-qty').length + 1) + '</td>'
	+	'<td></td>'
	+	'<td class="text-primary" data-type="02">最小包装条码</td>'
	+	'<td>'
	+		'<input class="form-control package-qty" type="number" min="0" maxlength="' + spq + '" value="' + spq + '"/>'
	+	'</td>'
	+	'<td class="text-center">'
	+		'<button class="btn btn-sm btn-danger btn-delete" onclick="removeLabel(this)" title="删除最小包装条码"><i class="glyphicon glyphicon-minus"></i></button>'
	+	'</td>'
	+'</tr>');
	// 修改最小包装标签数量显示
	var spq_qty = parseInt($('#spq_qty').html().trim());
	$('#spq_qty').html(spq_qty + 1);
	// Begin Update By XieHui At 2020-04-27 14:36:17
	var curBox = $(obj).parent().parent().find('td:eq(3) input');
	curBox.val(parseFloat(curBox.val()) + spq);
	// End Update By XieHui At 2020-04-27 14:36:17
	updateTotal();
}

// 新增外箱标签
/**
 * @param {Object} spq 最小包装数量
 * @param {Object} remainQty 可打印数量
 * @param {Object} boxSpq 外箱最小包装数量
 */
function newBoxLabel(spq, remainQty, boxSpq) {
	var thisBoxSpq = Math.ceil(remainQty / spq);
	var id = "myTab" + $('#label').find('table').length;
	$('#label').append(
	'<div class="col-sm-6">'
	+	'<h3>标签<span>' + ($('#label').find('table').length + 1) + '</span></h3>'
	+	'<table class="table table-striped table-bordered table-hover table-condensed" id="' + id + '">'
	+		'<thead>'
	+		'<tr>'
	+			'<th>序号</th>'
	+			'<th>条码ID</th>'
	+			'<th>标签类型</th>'
	+			'<th>物料数量</th>'
	+			'<th>操作</th>'
	+		'</tr>'
	+		'</thead>'
	+		'<tbody>'
	+			'<tr class="box">'
	+				'<td>0</td>'
	+				'<td></td>'
	+				'<td class="text-success" data-type="01">外箱条码</td>'
	+				'<td>'
	+					'<input class="form-control package-box-qty" readonly="readonly" type="number" min="0" value="' + remainQty + '"/>'
	+				'</td>'
	+				'<td class="text-center">'
	+					'<button class="btn btn-sm btn-info btn-delete" onclick="newLabelAfter(this)" title="新增最小包装条码">'
	+						'<i class="glyphicon glyphicon-plus"></i>'
	+					'</button>'
	+				'</td>'
	+			'</tr>'
	+		'</tbody>'
	+	'</table>'
	+'</div>');
	for(var i = 0; i < thisBoxSpq; i++) {
		if(remainQty >= spq) {
			newLabel(id, spq);
		} else {
			newLabel(id, remainQty);
		}
		remainQty = remainQty - spq;
	}
}

// 移除外箱标签
function removeAllLabel(obj) {
	//index = parent.layer.confirm("Are you sure to remove this label?", {
	//	btn: ["Sure", "No"]
	//}, function() {
	parent.layer.close(index);
	domQty = $(obj).parent().parent().parent().parent().parent();
	domNext = domQty.next('div');
	while(domNext.length != 0) {
		domNext.find('h3').html('标签<span>' + (domNext.find('h3 span').html() - 1) + '</span>');
		domNext = domNext.next('div');
	}
	domQty.remove();
	//});
}

// 移除标签
function removeLabel(obj) {
	index = parent.layer.confirm("Are you sure to remove this?", {
		btn: ["Sure", "No"]
	}, function() {
		parent.layer.close(index);
		domQty = $(obj).parent().parent();
		domNext = domQty.next('tr');
		while(domNext.length != 0) {
			$(domNext.children()[0]).html($(domNext.children()[0]).text() - 1);
			domNext = domNext.next('tr');
		}
		// 修改外箱数量
		var qty = parseFloat(domQty.find('.package-qty').val().trim()? domQty.find('.package-qty').val().trim(): 0);
		if($('#box_spq').val() && $('#box_qty').val() != '') {
			var originQty = parseFloat(domQty.parent().find('.package-box-qty').val());
			domQty.parent().find('.package-box-qty').val(originQty - qty);
		}
		// 修改最小包装标签数量显示
		var spq_qty = parseInt($('#spq_qty').html().trim());
		$('#spq_qty').html(spq_qty - 1);
		if($('#box_spq').val() && domQty.parent().find('tr').length == 2) {
			removeAllLabel(obj);
			computeBoxQty(-1);
		} else {
			domQty.remove();
		}
		updateTotal();
	});
}

// 非空检测;
function emptyCheck(option) {
	if($("#remain_qty").val().trim() == "0") {
		parent.layer.msg("已经全部打印，无法继续打印！");
		$("#remain_qty").focus();
		return false;
	}
	if($("#self_spq").val().trim() == "") {
		parent.layer.msg("包装数量不能为空！");
		$("#self_spq").focus();
		return false;
	}
	if($("#prod_no").val().trim() == "") {
		parent.layer.msg("生产批次不能为空！");
		$("#prod_no").focus();
		return false;
	}
	if($("#prod_date").val().trim() == "") {
		parent.layer.msg("生产日期不能为空！");
		$("#prod_date").focus();
		return false;
	}
	return true;
}

// 生成并保存标签
function saveLabel() {
	index = parent.layer.confirm("Are you sure to save it?", {
		btn: ["Sure", "No"]
	}, function() {
		parent.layer.close(index);
		if(checkTotal()) {
			// 先获取条码id
			getQrcodeId();
		} else {
			parent.layer.alert('包装数量不能超过可打印数量，请修改后并移除空行再保存');
		}
	});
}

function checkTotal() {
	var total = parseFloat($('#total_qty').html().trim());
	return total <= $('#remain_qty').val()
}

function getQrcodeId() {
	var box_qty = parseInt($('#box_qty').html().trim());
	var spq_qty = parseInt($('#spq_qty').html().trim());
	var data = [{
		"PACTY": "01",
		"Z_NUM": box_qty
	}, {
		"PACTY": "02",
		"Z_NUM": spq_qty
	}]
	$.ajax({
		// 请求方式
		type: "POST",
		// 请求的媒体类型
		contentType: "application/x-www-form-urlencoded",
		// 返回的数据类型
		dataType: "json",
		// 请求地址
		url: "/label/utils/get_label_qrcodes.asp",
		// 数据，json字符串
		data: {
			baseAuth: baseAuth,
			data: JSON.stringify(data)
		},
		success: function(res) {
			if(res.DATA) {
				if(res.DATA.length > 0) {
					for(var i = 0; i < box_qty; i++) {
						$('.package-box-qty').eq(i).parent().parent().find('td:eq(1)').html(res.DATA[i].HUIDENT.replace(/\b(0+)/gi,""))
					}
					for(var i = 0; i < spq_qty; i++) {
						$('.package-qty').eq(i).parent().parent().find('td:eq(1)').html(res.DATA[i + box_qty].HUIDENT.replace(/\b(0+)/gi,""))
					}
					check();
				} else {
					parent.layer.alert(res.MESSAGE.MSG_CONTENT);
				}
			} else {
				parent.layer.alert(res.msg);
			}
		},
	});
}

function save() {
	var qrcodes = [];
	$('#label').find('.table').each(function() {
		var child = [];
		$(this).find('tbody tr').each(function() {
			var dat = {};
			dat["no"] = $(this).find('td:eq(0)').html();
			dat["qrcode_id"] = $(this).find('td:eq(1)').html();
			dat["package_type"] = $(this).find('td:eq(2)').attr('data-type');
			dat["package_qty"] = $(this).find('td:eq(3)').children('input').val();
			dat["prod_no"] = $("#prod_no").val();
			dat["prod_date"] = $("#prod_date").val();
			dat["expire_date"] = addDate($('#prod_date').val(), parseInt($('#expire_date').text()));
			child.push(dat);
		});
		qrcodes.push(child);
	});
	// console.log(qrcodes);
	// console.log($('#myTab1').children('tbody tr').text());
	// var $boxLabels = $($("#print-page")[0].contentWindow.document).find('.box-label');
	// var $labels = $($("#print-page")[0].contentWindow.document).find('.label');
	var data = {
		// id: Math.uuid(12),						// ID
		po_no: tempData.EBELN,						// PO号
		line_no: tempData.EBELP,					// 行项目号
		material_no: tempData.MATNR,				// 物料编号
		material_type: tempData.MTART,				// 物料类型
		material_desc: tempData.MAKTX,				// 物料描述
		print_qty: $('#total_qty').text(),			// 本次要打印的总数量
		printed_qty: $('#already_qty').val(),		// 已打印的数量
		po_qty: tempData.MENGE,						// 采购数量
		po_unit: tempData.MEINS,					// 采购单位
		po_date: tempData.AEDAT,					// 凭证日期
		mfg: tempData.MFRNR,						// 制造商
		mfg_no: tempData.MFRPN,						// 制造商编号
		mfg_name: tempData.NAME1,					// 制造商名称
		prod_no: $('#prod_no').val(),				// 生产批次
		prod_date: $('#prod_date').val(),			// 生产日期
		status: tempData.LOEKZ,						// 状态
		werks: tempData.WERKS,						// 工厂代码
		werks_name: tempData.WERKS_NAME1,			// 工厂名称
		lgort: tempData.LGORT,						// 仓储位置
		expire_date: addDate($('#prod_date').val(), parseInt($('#expire_date').text())),			// 过期日期
		store_position: tempData.ZE_LOCATIONCODE,	// 仓储位置
		spq_qty: $('#spq_qty').text(),				// 最小包装数量
		box_qty: $('#box_qty').text(),				// 每箱可装spq
		normt: tempData.NORMT,						// 关键物料，行业标准描述
		maabc: tempData.MAABC,						// ABC标识
		pstyp: tempData.PSTYP,						// 项目类型
		create_by: "<%=Session("LoginName")%>",		// 创建人
		is_new_record: $('#is_new_record').html(),	// 是否新记录
		vendor_code: "<%=Session("Username")%>",	// 供应商代码
		baseAuth: baseAuth,							// 验证
		qrcodes: JSON.stringify(qrcodes)
	}
	// console.log(data);
	sessionStorage.setItem("print_data", JSON.stringify(data))
	$.ajax({
		url: "/label/utils/save_label_records.asp",			// 请求地址
		type: "POST",										// 请求方式
		data: data,											// 数据，json字符串
		dataType: "json",									// 返回的数据类型
		contentType: "application/x-www-form-urlencoded",	// 请求的媒体类型
		success: function(res) {
			// console.log(res);
			if(res.success) {
				// 禁止修改
				$('#btn-package').remove();
				$('#btn-generate').remove();
				$('.btn-delete').remove();					// 
				$('#btn-new-box-label').remove();			// 
				parent.layer.alert("条码保存成功");
				$("#btn-download").removeAttr("disabled");	// 移除禁用属性
				$("#btn-print").removeAttr("disabled");		// 移除禁用属性
				$("input").attr("disabled", "disabled");	// 不允许修改
			} else {
				parent.layer.alert(res.msg);
			}
		}, error: function(e){
			parent.layer.alert("保存失败，接口异常");
		}
	});
}

/**
 * 检查是否超出交货数量了
 */
function check() {
	// getPrintedQty();
	var totalQty = parseFloat($('#total_qty').text());
	var printedQty = parseFloat($('#already_qty').val());
	$.ajax({
		url: "/label/utils/get_po_list_test.asp",
		type: "POST",
		data: {
			baseAuth: baseAuth,						// 校验
			po_no: tempData.EBELN,					// PO号
			line_no: tempData.EBELP					// 行项目号
		},
		dataType: "json",									// 返回的数据类型
		contentType: "application/x-www-form-urlencoded",	// 请求的媒体类型
		success: function(res) {
			if(totalQty > res.DATA[0].MENGE - printedQty) {
				parent.layer.alert("保存失败，SAP已经修改了订单数量");
			} else {
				save();
			}
		}, error: function(e){
			parent.layer.alert("检查失败，接口异常");
		}
	});
}

/**
 * 修改外箱标签数量显示
 * @param {Object} val 正负数入参，+1，-1
 */
function computeBoxQty(val) {
	var box_qty = parseInt($('#box_qty').html().trim());
	$('#box_qty').html(box_qty + val);
}

function downloadLabel() {
	res = JSON.parse(sessionStorage.getItem("print_data"));
	// console.log(res);
	var poInfo = res
	var labelInfo = []
	var qrcodes = JSON.parse(res.qrcodes)
	for(var i in qrcodes) {
		var label = qrcodes[i]
		var parent_qrcode_id = ""
		for(var j in label) {
			if(label[j].no == "0") {
				parent_qrcode_id = label[j].qrcode_id;
			}
			label[j].parent_qrcode_id = parent_qrcode_id;
			label[j].po_no = $('#po_no').text();
			label[j].line_no = $('#line_no').text();
			labelInfo.push(label[j]);
		}
	}
	// console.log(labelInfo);
	download(poInfo, labelInfo);
}

function printLabel() {
	res = JSON.parse(sessionStorage.getItem("print_data"));
	// console.log(res);
	var poInfo = res
	var labelInfo = []
	var qrcodes = JSON.parse(res.qrcodes)
	for(var i in qrcodes) {
		var label = qrcodes[i]
		var parent_qrcode_id = ""
		for(var j in label) {
			if(label[j].no == "0") {
				parent_qrcode_id = label[j].qrcode_id;
			}
			label[j].parent_qrcode_id = parent_qrcode_id;
			label[j].po_no = $('#po_no').text();
			label[j].line_no = $('#line_no').text();
			labelInfo.push(label[j]);
		}
	}
	// console.log(labelInfo);
	print(poInfo, labelInfo);
}

// 下载条码
function download(poInfo, labelInfo) {
	index = parent.layer.confirm('Are you sure download？', {
		btn: ['Sure','No']
	}, function() {
		parent.layer.close(index);
		// 动态设置打印内容
		setPrintContent(poInfo, labelInfo);
		// $("#print-page")[0].contentWindow.print();
		savePDF(poInfo.po_no + "-" + poInfo.line_no);
	});
}

// 打印
function print(poInfo, labelInfo) {
	index = parent.layer.confirm('Are you sure print？', {
		btn: ['Sure','No']
	}, function() {
		parent.layer.close(index);
		// 动态设置打印内容
		setPrintContent(poInfo, labelInfo);
		$("#print-page")[0].contentWindow.print();
		// savePDF();
	});
}
</script>
<script src="/label/common/utils/print.js"></script>
</html>