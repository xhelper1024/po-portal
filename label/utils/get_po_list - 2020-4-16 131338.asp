<%@ CodePage = "65001" %>
<% Option Explicit %>
<!--#include virtual="/label/common/utils/dbutils.asp"-->

<%
Response.charset = "UTF-8"
Dim sort: sort = request("sort")
Dim page: page = request("page")
Dim dateTo: dateTo = request("dateTo")
Dim line_id: line_id = request("line_id")
Dim dateFrom: dateFrom = request("dateFrom")
Dim pageSize: pageSize = request("pageSize")
Dim sortOrder: sortOrder = request("sortOrder")
Dim vendorCode: vendorCode = request("vendorCode")

Dim connStr: connStr = "Provider=sqloledb;Data Source=" & Application("gsHostName") & _
	";Initial Catalog=" & Application("gsDBName") & ";User Id=" & Application("gsUsrTypeAdmin") & _
	";Password=" & Application("gsAdminPwd")
Dim dbConn: Set dbConn = Server.CreateObject("ADODB.Connection")
dbConn.Open connStr
Dim rs: Set rs = Server.CreateObject("ADODB.Recordset")

Dim result: result = "{""pageSize"": """ & pageSize & """, ""page"": """ & page & """, ""rows"": ["
Dim sqlStr: sqlStr = "SELECT TOP " & pageSize & " * FROM suga_t_maintenance_po"
Dim innerSqlStr: innerSqlStr = "SELECT TOP " & pageSize * (page - 1) & " LINE_ID FROM suga_t_maintenance_po "
Dim condition: condition = " WHERE 1=1 "
' ID查询
Dim count: count = 0
IF line_id <> "" Then
	condition = condition & " AND LINE_ID = '" & line_id & "' "
	' suga_t_label_records
	Dim extraSqlStr: extraSqlStr = "SELECT * FROM suga_t_label_records WHERE LINE_ID = '" & line_id & "'"
	rs.Open extraSqlStr, dbConn, 3
	count = rs.RecordCount
	rs.NextRecordSet
END IF
' 判断LINE_ID在Record表中是否已经存在，如果存在则返回提示已存在
' IF count > 0 THEN
' 	result = result & "], ""total"": " & count & ", ""success"": false, ""msg"": ""此记录已经生成过条码了""}"
' 	response.write result
' ELSE
	' 日期查询条件
	IF dateFrom <> "" THEN
		condition = condition & " AND PO_ISSU_DATE > '" & dateFrom & "' "
	END IF
	IF dateTo <> "" THEN
		condition = condition & " AND PO_ISSU_DATE < '" & dateTo & "' "
	END IF
	IF dateFrom <> "" AND dateTo <> "" THEN
		condition = condition & " AND PO_ISSU_DATE between '" & dateFrom & "' AND '" & dateTo & "'"
	END IF

	' 供应商查询
	IF vendorCode <> "" Then
		condition = condition & " AND VENDOR = '" & vendorCode & "' "
	END IF

	condition = condition & " AND REPLY_FLAG = 'Y' "

	Dim orderBy: orderBy = " ORDER BY PO, LINE "
	innerSqlStr = innerSqlStr & condition & orderBy

	sqlStr = sqlStr & condition & " AND LINE_ID NOT IN (" & innerSqlStr & ") " & orderBy
	rs.open sqlStr, dbConn, 3
	response.cookies("sql") = sqlStr

	DO UNTIL rs.EOF
		result = result & "{"
		Dim i:i = 0
		Dim x
		FOR EACH x IN rs.Fields
			Dim name, value
			IF x.name <> "" THEN
				name = replace(x.name, """", "'")
			END IF
			IF x.value <> "" THEN
				value = replace(x.value, """", "'")
			END IF
			IF i = 0 THEN
				result = result & """" & name & """: """ & value & """"
			ELSE
				result = result & ", """ & name & """: """ & value & """"
			END IF
			i = 1
		NEXT
		rs.MoveNext
		IF rs.EOF THEN
			result = result & "}"
		ELSE
			result = result & "},"
		END IF
	LOOP

	rs.NextRecordSet

	sqlStr = "select count(*) as total from suga_t_maintenance_po" & condition

	rs.Open sqlStr, dbConn, 3
	Dim total: total = rs.Fields("total")
	result = result & "], ""total"": " & total & ", ""success"": true}"
	response.write result
' END IF
%>