<%@ CodePage = "65001" %>
<% Option Explicit %>
<!--#include virtual="/label/common/utils/dbutils.asp"-->
<!--#include virtual="/label/common/utils/json.asp"-->
<script language="jscript" runat="server">
	Array.prototype.get = function(x) { return this[x]; };
	function parseJSON(strJSON) { return eval("(" + strJSON + ")"); }
</script>
<%
Response.charset = "GBK"
Dim record_id: record_id = request("id")
Dim po_no: po_no = request("po_no")
Dim line_no: line_no = request("line_no")
Dim material_no: material_no = request("material_no")
Dim material_type: material_type = request("material_type")
Dim material_desc: material_desc = request("material_desc")
Dim print_qty: print_qty = cdbl(request("print_qty"))
Dim printed_qty: printed_qty = cdbl(request("printed_qty"))
' 计算此次打印的数量
printed_qty = printed_qty + print_qty
Dim po_qty: po_qty = request("po_qty")
Dim is_new_record: is_new_record = request("is_new_record")
Dim po_unit: po_unit = request("po_unit")
Dim po_date: po_date = request("po_date")
Dim mfg: mfg = request("mfg")
Dim mfg_no: mfg_no = request("mfg_no")
Dim mfg_name: mfg_name = request("mfg_name")
Dim prod_no: prod_no = request("prod_no")
Dim prod_date: prod_date = request("prod_date")
Dim status: status = request("status")
Dim expire_date: expire_date = request("expire_date")
Dim store_position: store_position = request("store_position")
Dim spq_qty: spq_qty = request("spq_qty")
Dim box_qty: box_qty = request("box_qty")
Dim normt: normt = request("normt")
Dim maabc: maabc = request("maabc")
Dim create_by: create_by = request("create_by")
Dim vendor_code: vendor_code = request("vendor_code")
Dim werks: werks = request("werks")
Dim werks_name: werks_name = request("werks_name")
Dim lgort: lgort = request("lgort")
Dim pstyp: pstyp = request("pstyp")
Dim qrcodes: qrcodes = request("qrcodes")
Dim baseAuth: baseAuth = request("baseAuth")

Dim connStr: connStr = "Provider=sqloledb;Data Source=" & Application("gsHostName") & _
	";Initial Catalog=" & Application("gsDBName") & ";User Id=" & Application("gsUsrTypeAdmin") & _
	";Password=" & Application("gsAdminPwd")
Dim dbConn: Set dbConn = Server.CreateObject("ADODB.Connection")
dbConn.Open connStr
Dim rs: Set rs = Server.CreateObject("ADODB.Recordset")

Dim sqlStr
IF is_new_record = "true" THEN
	sqlStr = "INSERT INTO [dbo].[suga_t_label_records] " &_
		"([po_no], [line_no], [material_no], [material_desc], [po_date], " &_
		"[po_qty], [printed_qty], [po_unit], [mfg], [mfg_no], [mfg_name], [werks], [werks_name], [lgort], " &_
		"[material_type], [normt], [maabc], [spq_qty], [vendor_code], " &_
		"[box_qty], [create_time], [store_position], [status], [create_by], [pstyp]) " &_
		"VALUES " &_
		"('" & po_no & "', '" & line_no & "', '" & material_no & "', N'" & material_desc & "', '" & po_date & "', " &_
		"'" & po_qty & "', '" & printed_qty & "', '" & po_unit & "', '" & mfg & "', '" & mfg_no & "', N'" & mfg_name & "', " &_
		"N'" & werks & "',N'" & werks_name & "',N'" & lgort & "', " &_
		"'" & material_type & "', '" & normt & "', '" & maabc & "', '" & spq_qty & "', '" & vendor_code & "', " &_
		"'" & box_qty & "', GETDATE(), '" & store_position & "', '" & status & "', '" & create_by & "', '" & pstyp & "');"
ELSE
	sqlStr = "UPDATE [dbo].[suga_t_label_records] SET printed_qty=printed_qty+" & print_qty &_
		", update_time=GETDATE(), " &_
		"werks='" & werks & "', lgort='" & lgort & "', store_position='" & store_position & "', " &_
		"status='" & status & "', pstyp='" & pstyp & "', mfg='" & mfg & "', mfg_no='" & mfg_no & "', mfg_name=N'" & mfg_name & "'" &_
		" WHERE po_qty>=printed_qty+" & print_qty & " AND po_no='" & po_no & "' AND line_no='" & line_no & "'"
END IF
' response.write(is_new_record)
' response.write(sqlStr)

Dim postData: Set postData = jsArray()
Dim obj: Set obj = parseJSON(qrcodes)
Dim insertStr, i, j
For i = 0 To obj.length - 1
	Dim qrcodeArr: Set qrcodeArr = obj.get(i)
	Dim parent_qrcode_id
	For j = 0 To qrcodeArr.length - 1
		Dim originArr: Set originArr = qrcodeArr.get(j)
		IF j = 0 AND box_qty <> 0 THEN
			parent_qrcode_id = originArr.qrcode_id
		ELSE
			Set postData(Null) = jsObject()
			postData(Null)("LGNUM") = ""
			postData(Null)("HUIDENT") = originArr.qrcode_id
			postData(Null)("EBELN") = po_no
			postData(Null)("EBELP") = line_no
			Dim tmpY: tmpY = Year(Now)
			Dim tmpM: tmpM = Month(Now)
			Dim tmpD: tmpD = Day(Now)
			If Len(tmpM) = 1 Then
				tmpM = "0" & tmpM
			End If
			If Len(tmpD) = 1 Then
				tmpD = "0" & tmpD
			End If
			postData(Null)("ERDAT") = tmpY & tmpM & tmpD
			Dim tmpH: tmpH = Hour(TIME)
			If Len(tmpH) = 1 Then
				tmpH = "0" & tmpH
			End If
			Dim tmpMin: tmpMin = Minute(TIME)
			Dim tmpS: tmpS = Second(TIME)
			postData(Null)("ERTIM") = tmpH & tmpMin & tmpS
			postData(Null)("ERNAM") = Session("Username")
			postData(Null)("HU_TYP") = originArr.package_type
			postData(Null)("BZ_MENGE") = originArr.package_qty
			postData(Null)("HU_PAR") = parent_qrcode_id
			postData(Null)("PAR_TYP") = "01"
			postData(Null)("MATNR") = material_no
			postData(Null)("HUDEL") = ""
			
			postData(Null)("BZ_MENGE_MIN") = spq_qty
			postData(Null)("MECAP") = box_qty
			postData(Null)("MEINS") = po_unit
			postData(Null)("AEDAT") = po_date
			postData(Null)("MENGE") = po_qty
			postData(Null)("MFRPN") = mfg_no
			postData(Null)("MFRNR") = mfg
			postData(Null)("MAABC") = maabc
			postData(Null)("MTART") = material_type
			postData(Null)("NORMT") = normt
			postData(Null)("PROD_NO") = prod_no
			postData(Null)("HSDAT") = prod_date
			postData(Null)("EXPIRE_DATE") = expire_date
			postData(Null)("ZE_LOCATIONCODE") = store_position
			postData(Null)("LIFNR") = vendor_code
			postData(Null)("WERKS") = werks
			postData(Null)("LGORT") = lgort
			postData(Null)("STATUS") = ""
			postData(Null)("PSTYP") = pstyp
		END IF
		insertStr = insertStr & "INSERT INTO [dbo].[suga_t_label_qrcodes]" &_
		"([qrcode_id], [parent_qrcode_id], [package_qty], [package_type], [po_no], [line_no]," &_
		" [create_time], [material_no], [prod_no], [prod_date], [expire_date]) VALUES "&_
		"(N'" & originArr.qrcode_id & "', N'" &_
				parent_qrcode_id & "', '" &_
				originArr.package_qty & "', N'" &_
				originArr.package_type & "', '" &_
				po_no & "', '" &_
				line_no & "', " &_
				"GETDATE(), '" &_
				material_no & "', '" &_
				prod_no & "', '" &_
				prod_date & "', '" &_
				expire_date & "');"
	Next
Next
' response.write(insertStr)

ON ERROR RESUME NEXT
Dim result: Set result = jsObject()
dbConn.BeginTrans
' 判断是否处于登录状态
IF Session("loginStaffId") <> "" Then
	' 开始事务
	' rs.open sqlStr, dbConn, 3, 1
	Dim num
	dbConn.Execute sqlStr, num
	' 判断是否更新成功
	IF ISNUMERIC(num) AND CINT(num) > 0 THEN
		dbConn.Execute insertStr
		IF ERR.NUMBER = 0 THEN
			' Begin Update By XieHui At 2020-04-27 09:15:40
			' 保存到SAP,先封装一下格式
			Dim sUrl: sUrl = Application("API_URL") & "?ACTION=SAVE_HU_INFORMATION"
			Dim xmlhttp: Set xmlhttp = Server.CreateObject("MSXML2.ServerXMLHTTP")
			xmlhttp.open "POST", sURL, False
			xmlhttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
			xmlhttp.setRequestHeader "Authorization", baseAuth
			xmlhttp.send(postData.jsString)
			Dim res: res = xmlhttp.responseText
			Dim ret: Set ret = parseJSON(res)
			result("sapreturn") = ret.MSG_TYPE
			result("postData") = postData.jsString
			Set xmlhttp = Nothing
			' End Update By XieHui At 2020-04-27 09:15:40
			IF result("sapreturn") <> "S" THEN
				dbConn.RollbackTrans
				result("success") = false
				result("msg") = ret.MSG_CONTENT
			ELSE
				dbConn.CommitTrans
				result("success") = true
				result("msg") = "保存成功"
			END IF
		ELSE
			dbConn.RollbackTrans
			result("success") = false
			result("msg") = "条码保存失败: " & ERR.DESCRIPTION
			result("errorCode") = ERR.NUMBER
		END IF
	ELSE
		dbConn.RollbackTrans
		result("success") = false
		result("msg") = "条码保存失败, 超出可打印数量" & ERR.DESCRIPTION
		result("errorCode") = num
		' result("sqlStr") = sqlStr
	END IF
	response.write(result.jsString)
ELSE
	result("success") = false
	result("msg") = "条码保存失败, 请重新登录"
	response.write(result.jsString)
END IF
%>