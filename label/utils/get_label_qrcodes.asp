<%@ CodePage = "65001" %>
<% Option Explicit %>
<!--#include virtual="/label/common/utils/dbutils.asp"-->
<!--#include virtual="/label/common/utils/json.asp"-->
<script language="JavaScript" runat="server" src="/label/common/utils/json.js"></script>

<%
Response.charset = "UTF-8"
Dim data: data = request("data")
Dim baseAuth: baseAuth = request("baseAuth")

Dim result: Set result = jsObject()
' 判断是否处于登录状态
IF Session("loginStaffId") <> "" Then
	' 调用接口
	Dim sUrl: sUrl = Application("API_URL") & "?ACTION=GET_HU"
	Dim xmlhttp: Set xmlhttp = Server.CreateObject("MSXML2.ServerXMLHTTP")
	xmlhttp.open "POST", sURL, False
	xmlhttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
	xmlhttp.setRequestHeader "Authorization", baseAuth
	xmlhttp.send(data)

	Dim res: res = xmlhttp.responseText
	' 返回值
	response.write(res)
	Set xmlhttp = Nothing
ELSE
	result("success") = false
	result("msg") = "条码获取失败, 请重新登录"
	response.write(result.jsString)
END IF
%>