<%@ CodePage="65001" %>
<% Option Explicit %>
<!--#include virtual="/label/common/utils/json.asp"-->
<script language="jscript" runat="server">
	Array.prototype.get = function(x) { return this[x]; };
	function parseJSON(strJSON) { return eval("(" + strJSON + ")"); }
</script>

<%
' response.charset="GBK"
Dim baseAuth: baseAuth = request("baseAuth")
Dim result: Set result = jsArray()
' 获取条码数据
Dim connStr: connStr = "Provider=sqloledb;Data Source=" & Application("gsHostName") & _
	";Initial Catalog=" & Application("gsDBName") & ";User Id=" & Application("gsUsrTypeAdmin") & _
	";Password=" & Application("gsAdminPwd")
Dim conn: Set conn = Server.CreateObject("ADODB.Connection")
Dim sqlStr: sqlStr = "SELECT * FROM SUGA_T_LABEL_RECORDS AS R LEFT JOIN SUGA_T_LABEL_QRCODES AS Q ON R.PO_NO = Q.PO_NO AND R.LINE_NO = Q.LINE_NO AND Q.PACKAGE_TYPE = '02'"
Dim rs: Set rs = Server.CreateObject("ADODB.Recordset")
conn.Open connStr
rs.Open sqlStr, conn, 3
DO UNTIL rs.EOF
	Set result(Null) = jsObject()
	Dim x
	FOR EACH x IN rs.Fields
		' result(Null)(x.name) = x.value
		SELECT CASE x.name
		CASE "po_no":
			result(Null)("EBELN") = x.value
		CASE "line_no":
			result(Null)("EBELP") = x.value
		CASE "material_no":
			result(Null)("MATNR") = x.value
		CASE "material_desc":
			result(Null)("MKTXT") = x.value
		CASE "po_date":
			result(Null)("AEDAT") = x.value
		CASE "po_qty":
			result(Null)("MENGE") = x.value
		CASE "po_unit":
			result(Null)("MEINS") = x.value
		CASE "mfg_no":
			result(Null)("MFRPN") = x.value
		CASE "material_type":
			result(Null)("MTART") = x.value
		CASE "normt":
			result(Null)("NORMT") = x.value
		CASE "maabc":
			result(Null)("MAABC") = x.value
		CASE "prod_no":
			result(Null)("PROD_NO") = x.value
		CASE "prod_date":
			result(Null)("HSDAT") = x.value
		CASE "spq_qty":
			result(Null)("BZ_MENGE_MIN") = x.value
		CASE "box_qty":
			result(Null)("MECAP") = x.value
		CASE "expire_date":
			result(Null)("EXPIRE_DATE") = x.value
		CASE "store_position":
			result(Null)("ZE_LOCATIONCODE") = x.value
		CASE "status":
			result(Null)("STATUS") = x.value
		CASE "create_by":
			result(Null)("ERNAM") = x.value
		CASE "vendor_code":
			result(Null)("LIFNR") = x.value
		CASE "werks":
			result(Null)("WERKS") = x.value
		CASE "mfg":
			result(Null)("MFRNR") = x.value
		CASE "qrcode_id":
			result(Null)("HUIDENT") = x.value
		CASE "parent_qrcode_id":
			result(Null)("HU_PAR") = x.value
		CASE "package_qty":
			result(Null)("BZ_MENGE") = x.value
		CASE "package_type":
			result(Null)("HU_TYP") = x.value
		CASE "is_delete":
			IF x.value = "Y" THEN
				result(Null)("HUDEL") = "X"
			ELSE
				result(Null)("HUDEL") = ""
			END IF
			result(Null)("PAR_TYP") = "01"
			Dim tmpY: tmpY = Year(Now)
			Dim tmpM: tmpM = Month(Now)
			Dim tmpD: tmpD = Day(Now)
			If Len(tmpM) = 1 Then
				tmpM = "0" & tmpM
			End If
			If Len(tmpD) = 1 Then
				tmpD = "0" & tmpD
			End If
			result(Null)("ERDAT") = tmpY & tmpM & tmpD
			Dim tmpH: tmpH = Hour(TIME)
			If Len(tmpH) = 1 Then
				tmpH = "0" & tmpH
			End If
			Dim tmpMin: tmpMin = Minute(TIME)
			Dim tmpS: tmpS = Second(TIME)
			result(Null)("ERTIM") = tmpH & tmpMin & tmpS
		CASE ELSE:
			result(Null)(x.name) = x.value
		END SELECT
	NEXT
	rs.MoveNext
LOOP
' 更新到SAP系统去

Dim sUrl: sUrl = Application("API_URL") & "?ACTION=SAVE_HU_INFORMATION"
Dim xmlhttp: Set xmlhttp = Server.CreateObject("MSXML2.ServerXMLHTTP")
xmlhttp.open "POST", sURL, False
xmlhttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
xmlhttp.setRequestHeader "Authorization", baseAuth
xmlhttp.send(result.jsString)
response.write(xmlhttp.responseText)

%>