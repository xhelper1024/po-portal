<%@ CodePage = "65001" %>
<% Option Explicit %>
<!--#include virtual="/label/common/utils/dbutils.asp"-->
<!--#include virtual="/label/common/utils/json.asp"-->
<script language="jscript" runat="server">
	Array.prototype.get = function(x) { return this[x]; };
	function parseJSON(strJSON) { return eval("(" + strJSON + ")"); }
</script>

<%
Response.charset = "GBK"
Dim baseAuth: baseAuth = request("baseAuth")
Dim po_no: po_no = request("po_no")
Dim line_no: line_no = request("line_no")
Dim create_time: create_time = request("create_time")
Dim package_qty: package_qty = request("package_qty")

Dim connStr: connStr = "Provider=sqloledb;Data Source=" & Application("gsHostName") & _
	";Initial Catalog=" & Application("gsDBName") & ";User Id=" & Application("gsUsrTypeAdmin") & _
	";Password=" & Application("gsAdminPwd")
Dim dbConn: Set dbConn = Server.CreateObject("ADODB.Connection")
dbConn.Open connStr

' 先获取已打印的条码信息
Dim sqlStr: sqlStr = "SELECT * FROM SUGA_T_LABEL_RECORDS AS R LEFT JOIN SUGA_T_LABEL_QRCODES AS Q ON R.PO_NO = Q.PO_NO AND R.LINE_NO = Q.LINE_NO WHERE 1 = 1 "
Dim condition: condition = " AND Q.po_no='" & po_no & "' AND Q.line_no='" & line_no & "'"
condition = condition & " AND Q.create_time='" & create_time & "'"
Dim rs: Set rs = Server.CreateObject("ADODB.Recordset")
Dim result: Set result = jsObject()
Dim postData: Set postData = jsArray()
sqlStr = sqlStr & condition
' response.write sqlStr
rs.Open sqlStr, dbConn, 3

' 清空sqlStr, condition
sqlStr = ""
condition = ""

DO UNTIL rs.EOF
	Set postData(Null) = jsObject()
	postData(Null)("LGNUM") = ""
	postData(Null)("HUIDENT") = rs.Fields("qrcode_id")
	postData(Null)("EBELN") = rs.Fields("po_no")
	postData(Null)("EBELP") = rs.Fields("line_no")
	Dim tmpY: tmpY = Year(Now)
	Dim tmpM: tmpM = Month(Now)
	Dim tmpD: tmpD = Day(Now)
	If Len(tmpM) = 1 Then
		tmpM = "0" & tmpM
	End If
	If Len(tmpD) = 1 Then
		tmpD = "0" & tmpD
	End If
	postData(Null)("ERDAT") = tmpY & tmpM & tmpD
	Dim tmpH: tmpH = Hour(TIME)
	If Len(tmpH) = 1 Then
		tmpH = "0" & tmpH
	End If
	Dim tmpMin: tmpMin = Minute(TIME)
	Dim tmpS: tmpS = Second(TIME)
	postData(Null)("ERTIM") = tmpH & tmpMin & tmpS
	postData(Null)("ERNAM") = Session("Username")
	postData(Null)("HU_TYP") = rs.Fields("package_type")
	postData(Null)("BZ_MENGE") = rs.Fields("package_qty")
	postData(Null)("HU_PAR") = rs.Fields("parent_qrcode_id")
	postData(Null)("PAR_TYP") = "01"
	postData(Null)("MATNR") = rs.Fields("material_no")
	postData(Null)("HUDEL") = "X"
	
	postData(Null)("BZ_MENGE_MIN") = rs.Fields("spq_qty")
	postData(Null)("MECAP") = rs.Fields("box_qty")
	postData(Null)("MEINS") = rs.Fields("po_unit")
	postData(Null)("AEDAT") = rs.Fields("po_date")
	postData(Null)("MENGE") = rs.Fields("po_qty")
	postData(Null)("MFRPN") = rs.Fields("mfg_no")
	postData(Null)("MFRNR") = rs.Fields("mfg")
	postData(Null)("MAABC") = rs.Fields("maabc")
	postData(Null)("MTART") = rs.Fields("material_type")
	postData(Null)("NORMT") = rs.Fields("normt")
	postData(Null)("PROD_NO") = rs.Fields("prod_no")
	postData(Null)("HSDAT") = rs.Fields("prod_date")
	postData(Null)("EXPIRE_DATE") = rs.Fields("expire_date")
	postData(Null)("ZE_LOCATIONCODE") = rs.Fields("store_position")
	postData(Null)("LIFNR") = rs.Fields("vendor_code")
	postData(Null)("WERKS") = rs.Fields("werks")
	postData(Null)("STATUS") = ""
	postData(Null)("PSTYP") = rs.Fields("pstyp")
	sqlStr = sqlStr & "UPDATE suga_t_label_qrcodes SET is_delete='Y' WHERE qrcode_id='" & rs.Fields("qrcode_id") & "';"
	rs.MoveNext
LOOP

result("sql") = sqlStr
condition = " AND po_no='" & po_no & "' AND line_no='" & line_no & "'"
Dim updateSql: updateSql = "UPDATE suga_t_label_records SET printed_qty=printed_qty-" & package_qty & " WHERE 1=1" & condition

' response.write sqlStr
rs.close

ON ERROR RESUME NEXT
' 开始事务
dbConn.BeginTrans
rs.Open sqlStr, dbConn, 3
IF ERR.NUMBER = 0 THEN
	rs.Open updateSql, dbConn, 3
	IF ERR.NUMBER = 0 THEN
		' 保存到SAP, 先封装一下格式
		Dim sUrl: sUrl = Application("API_URL") & "?ACTION=SAVE_HU_INFORMATION"
		Dim xmlhttp: Set xmlhttp = Server.CreateObject("MSXML2.ServerXMLHTTP")
		xmlhttp.open "POST", sURL, False
		xmlhttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
		xmlhttp.setRequestHeader "Authorization", baseAuth
		xmlhttp.send(postData.jsString)
		Dim res: res = xmlhttp.responseText
		Dim ret: Set ret = parseJSON(res)
		result("sapreturn") = ret.MSG_TYPE
		result("postdata") = postData.jsString
		
		IF result("sapreturn") <> "S" THEN
			dbConn.RollbackTrans
			result("success") = false
			result("msg") = ret.MSG_CONTENT
		ELSE
			dbConn.CommitTrans
			result("success") = true
			result("msg") = "删除成功"
		END IF
	ELSE
		dbConn.RollbackTrans
		result("success") = false
		result("msg") = ERR.DESCRIPTION
	END IF
ELSE
	dbConn.RollbackTrans
	result("success") = false
	result("msg") = ERR.DESCRIPTION
END IF

response.write(result.jsString)
%>