<script>
$(function() {
	getPrintedQty();
	var po_data = JSON.parse(sessionStorage.getItem("<%=request("po_no") + request("line_no")%>"));
	// console.log(po_data);
	if(po_data && po_data != null) {
		var printed_qty = parseFloat($('#already_qty').val().trim());
		if(printed_qty == 0) {
			printed_qty = parseFloat(po_data.MENGE) - parseFloat(po_data.W_MENGE);
			$('#already_qty').val(printed_qty);
		}
		var remain_qty = po_data.MENGE - printed_qty > po_data.W_MENGE ? po_data.W_MENGE : po_data.MENGE - printed_qty;
		// console.log(po_data)
		// $('#spq').html(po_data.SPQ)
		$('#unit').val(po_data.MEINS)
		$('#po_no').html(po_data.EBELN)
		$('#item').html(po_data.MATNR)
		$('#line_no').html(po_data.EBELP)
		// $('#line_id').html(po_data.LINE_ID)
		$('#item_type').html(po_data.MTART)
		$('#item_desc').html(po_data.MAKTX)
		$('#po_qty').html(po_data.MENGE)
		$('#delivery_qty').html(po_data.W_MENGE)
		$('#remain_qty').val(remain_qty)
		$('#remain_qty').attr("maxlength", remain_qty)
		$('#mfg_name').html(po_data.NAME1)
		$('#mfg_no').html(po_data.MFRPN)
		$('#mfg').html(po_data.MFRNR)
		$('#po_date').html(po_data.AEDAT)
		$('#normt').html(po_data.NORMT)
		$('#maabc').html(po_data.MAABC)
		$('#werks').html(po_data.WERKS)
		$('#werks_name').html(po_data.WERKS_NAME1)
		$('#store_position').html(po_data.ZE_LOCATIONCODE)
		$('#expire_date').html(po_data.PRFRQ)
		// console.log(po_data.DELIVERY_DATE);
	} else {
		parent.layer.alert(res.msg);
		window.location.href = "/label/label_record.asp"
	}
});

// 数量输入检测
$(document).on('input', '#remain_qty', function() {
	var thisQty = parseFloat($(this).val());
	$(this).val(thisQty);
	var poQty = parseFloat($('#po_qty').html());
	var alreadyQty = parseFloat($('#already_qty').val());
	// 不能大于采购订单数量-已包装数量
	if(poQty - alreadyQty < thisQty || thisQty < 0) {
		$(this).val(poQty - alreadyQty);
	}
}).on('input', '#self_spq', function() {
	var thisQty = parseFloat($(this).val());
	$(this).val(thisQty);
}).on('input', '#box_spq', function() {
	var thisQty = parseInt($(this).val());
	$(this).val(thisQty);
}).on('input', '.package-qty', function() {
	var thisQty = parseFloat($(this).val());
	$(this).val(thisQty);
	var max = parseFloat($(this).attr('maxlength'));
	if(max && max != 0 && thisQty > max) {
		$(this).val(max);
	}
	// 修改外箱包装数量
	var total = 0;
	$(this).parent().parent().parent().find('.package-qty').each(function() {
		total = total + parseFloat($(this).val().trim()? $(this).val().trim(): 0);
	});	
	$(this).parent().parent().parent().find('.package-box-qty').val(total);
	updateTotal();
})/*.on('input', 'input', function() {
	var max = parseFloat($(this).attr('maxlength'));
	var thisQty = parseFloat($(this).val());
	if(max && max != 0 && thisQty > max) {
		$(this).val(max);
	}
})*/;

// 修改包装总数量
function updateTotal() {
	var totalQty = 0;
	if($('#box_spq').val() && $('#box_qty').val() != '') {
		$('.package-box-qty').each(function() {
			totalQty = totalQty + parseFloat($(this).val().trim()? $(this).val().trim(): 0);
		});
	} else {
		$('.package-qty').each(function() {
			totalQty = totalQty + parseFloat($(this).val().trim()? $(this).val().trim(): 0);
		});
	}
	$('#total_qty').html(totalQty);
}

function getPrintedQty() {
	$.ajax({
		url: "/label/utils/get_printed_qty.asp",
		type: "POST",
		contentType: "application/x-www-form-urlencoded",
		dataType: "json",
		async: false,
		data: {
			po_no: "<%=request.querystring("po_no")%>",
			line_no: "<%=request.querystring("line_no")%>"
		},
		success: function(res) {
			if(res.success) {
				$('#already_qty').val(parseFloat(res.printed_qty));
				$('#is_new_record').html(res.is_new_record);
			} else {
				// do Nothing
			}
		},
		error: function(e){
			console.log(e.status);
			console.log(e.responseText);
		}
	});
}
</script>