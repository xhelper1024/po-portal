<%@ CodePage="65001" %>
<% Option Explicit %>

<%
Dim id: id = request("id")
Dim sort: sort = request("sort")
Dim page: page = request("page")
Dim dateTo: dateTo = request("dateTo")
Dim dateFrom: dateFrom = request("dateFrom")
Dim pageSize: pageSize = request("pageSize")
Dim sortOrder: sortOrder = request("sortOrder")
Dim hasLabel: hasLabel = request("hasLabel")

Dim result: result = "{""pageSize"": " & pageSize & ", ""page"": " & page & ", ""sortOrder"": """ & sortOrder & """, ""rows"": ["
Dim connStr: connStr = "Provider=sqloledb;Data Source=" & Application("gsHostName") & _
	";Initial Catalog=" & Application("gsDBName") & ";User Id=" & Application("gsUsrTypeAdmin") & _
	";Password=" & Application("gsAdminPwd")
Dim dbConn: Set dbConn = Server.CreateObject("ADODB.Connection")
dbConn.Open connStr

Dim sqlStr: sqlStr = "SELECT TOP " & pageSize & " * FROM suga_t_label_records"
Dim innerSqlStr: innerSqlStr = "SELECT TOP " & (pageSize * (page - 1)) & " id FROM suga_t_label_records"
Dim isDelete: isDelete = request("isDelete")
IF isDelete = "" THEN
	isDelete = "N"
END IF
Dim condition: condition = " where is_delete='" & isDelete & "' "
' 日期查询条件
IF dateFrom <> "" THEN
	condition = condition & " AND create_date > '" & dateFrom & "'"
END IF
IF dateTo <> "" THEN
	condition = condition & " AND create_date < '" & dateTo & "'"
END IF
IF dateFrom <> "" AND dateTo <> "" THEN
	condition = condition & " AND create_date between '" & dateFrom & "' AND '" & dateTo & "'"
END IF

' 分页
IF sort = "" THEN
	sort = "po_no"
END IF
Dim orderBy: orderBy = " ORDER BY ID DESC, " & sort & " " & sortOrder
IF page <> "" AND pageSize <> "" THEN
	innerSqlStr = innerSqlStr & condition
	innerSqlStr = " AND id not in (" & innerSqlStr & orderBy & ") "
END IF

' 根据id查询
IF id <> "" THEN
	condition = condition & " AND id = '" & id & "'"
END IF

sqlStr = sqlStr & condition & innerSqlStr & orderBy
' response.cookies("sql") = sqlStr

Dim dbRs: Set dbRs = Server.CreateObject("ADODB.Recordset")
dbRs.Open sqlStr, dbConn, 3
Dim total: total = 0
Dim record_id

DO UNTIL dbRs.EOF
	result = result & "{"
	Dim i: i = 0
	Dim x
    FOR EACH x IN dbRs.Fields
		IF i = 0 THEN
			result = result & """" & x.name & """: """ & x.value & """"
		ELSE
			result = result & ", """ & x.name & """: """ & x.value & """"
		END IF
		i = 1
    NEXT
	record_id = dbRs(0)
	' response.write(record_id)
    dbRs.MoveNext
	IF dbRs.EOF THEN
		result = result & "}"
	ELSE
		result = result & "},"
	END IF
LOOP

dbRs.NextRecordSet
sqlStr = "select count(*) as total from suga_t_label_records" & condition
dbRs.Open sqlStr, dbConn, 3
total = dbRs.Fields("total")

' Create on 2020年4月3日14:51:35
' 如果需要条码信息
Dim labels: labels = """labels"": ["
IF hasLabel THEN
	Dim rs: Set rs = Server.CreateObject("ADODB.Recordset")
	rs.Open "select * from suga_t_label_qrcodes where record_id = '" & record_id & "' ORDER BY package_type DESC", dbConn, 3
	DO UNTIL rs.EOF
		Dim j: j = 0
		labels = labels & "{" 
		FOR EACH x IN rs.Fields
			IF j = 0 THEN
				labels = labels & """" & x.name & """: """ & x.value & """"
			ELSE
				labels = labels & ", """ & x.name & """: """ & x.value & """"
			END IF
			j = j + 1
		NEXT
		rs.MoveNext
		IF  rs.EOF THEN
			labels = labels & "}"
		ELSE
			labels = labels & "}, "
		END IF
	LOOP
END IF
labels = labels & "]"

result = result & "], " & labels & ", ""total"": " & total & "}"

// 写出数据
response.write result
%>