<%@ CodePage="65001" %>
<% Option Explicit %>
<!--#include virtual="/label/common/utils/json.asp"-->

<%
response.charset="GBK"
Dim item: item = request("item")
Dim po_no: po_no = request("po_no")
Dim line_no: line_no = request("line_no")
Dim dateTo: dateTo = request("dateTo")
Dim dateFrom: dateFrom = request("dateFrom")
Dim vendorCode: vendorCode = request("vendorCode")
Dim createBy: createBy = request("createBy")
' 分页条件
Dim pageSize: pageSize = request("pageSize")
Dim page: page = request("page")
Dim sort: sort = request("sort")
Dim sortOrder: sortOrder = request("sortOrder")

Dim result: Set result = jsObject()
Set result("rows") = jsArray()

Dim connStr: connStr = "Provider=sqloledb;Data Source=" & Application("gsHostName") & _
	";Initial Catalog=" & Application("gsDBName") & ";User Id=" & Application("gsUsrTypeAdmin") & _
	";Password=" & Application("gsAdminPwd")
Dim dbConn: Set dbConn = Server.CreateObject("ADODB.Connection")
dbConn.Open connStr

Dim sqlStr: sqlStr = "SELECT TOP " & pageSize & " * FROM suga_t_label_records WHERE 1=1 "

Dim condition
Dim orderBy: orderBy = " Order By create_time DESC"
IF sort <> "" Then
	orderBy = " Order By " & sort & " " & sortOrder
END IF

' 物料号查询条件
IF item <> "" THEN
	condition = condition & " AND material_no LIKE '%" & item & "%'"
END IF

' 只有角色为供应商时,才按创建人查找
IF Session("stafflevel") = "3" THEN
	condition = condition & " AND create_by='" & createBy & "'"
END IF

' 日期查询条件
IF dateFrom <> "" THEN
	condition = condition & " AND create_time > '" & dateFrom & "'"
END IF
IF dateTo <> "" THEN
	condition = condition & " AND create_time < '" & dateTo & "'"
END IF
IF dateFrom <> "" AND dateTo <> "" THEN
	condition = condition & " AND create_time between '" & dateFrom & "' AND '" & dateTo & "'"
END IF

' 供应商代码
IF vendorCode <> "" THEN
	condition = condition & " AND vendor_code='" & vendorCode & "' "
END IF

' PO行项目查询
IF po_no <> "" AND line_no <> "" THEN
	condition = condition & " AND po_no='" & po_no & "' AND line_no='" & line_no & "' "

' Begin Update By XieHui At 2020-04-27 14:36:44
ELSEIF po_no <> "" THEN
	condition = condition & " AND po_no='" & po_no & "' "
ELSEIF line_no <> "" THEN
	condition = condition & " AND line_no='" & line_no & "' "
END IF
' End Update By XieHui At 2020-04-27 14:36:44

' 分页条件
Dim pageCond: pageCond = " AND po_no+line_no NOT IN (SELECT TOP " & (pageSize * (page - 1)) &_
						 " po_no+line_no FROM suga_t_label_records WHERE 1=1 " & condition & orderBy & ") "

sqlStr = sqlStr & condition & pageCond & orderBy
' response.cookies("sql") = sqlStr
' response.write(sqlStr)

Dim dbRs: Set dbRs = Server.CreateObject("ADODB.Recordset")
dbRs.Open sqlStr, dbConn, 3
' response.write(dbRs.RecordCount)

' Dim total: total = 0
DO UNTIL dbRs.EOF
	Set result("rows")(Null) = jsObject()
	Dim x
	FOR EACH x IN dbRs.Fields
		result("rows")(Null)(x.name) = x.value
	NEXT
	' total = total + 1
    dbRs.MoveNext
LOOP

' Begin Update By XieHui At 2020-04-20 09:38:52
' response.write(result("rows").length & "----")
dbRs.NextRecordSet
sqlStr = "select count(*) as total from suga_t_label_records WHERE 1=1 " & condition
dbRs.Open sqlStr, dbConn, 3
Dim total: total = dbRs.Fields("total")
' End Update By XieHui At 2020-04-20 09:38:52
result("total") = total

' Create on 2020年4月3日14:51:35
' 是否查询具体PO行项目的标签数据
IF po_no <> "" AND line_no <> "" THEN
	Set result("labels") = jsArray()
	Dim cond: cond = " AND po_no='" & po_no & "' AND line_no='" & line_no & "' "
	Dim rs: Set rs = Server.CreateObject("ADODB.Recordset")
	rs.Open "SELECT * FROM suga_t_label_qrcodes WHERE 1=1" & cond, dbConn, 3
	DO UNTIL rs.EOF
		Set result("labels")(Null) = jsObject()
		FOR EACH x IN rs.Fields
			result("labels")(Null)(x.name) = x.value
		NEXT
		rs.MoveNext
	LOOP
END IF

// 写出数据
response.write(result.jsString)
%>