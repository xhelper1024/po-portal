<%@ CodePage = "65001" %>
<% Option Explicit %>
<!--#include virtual="/label/common/utils/dbutils.asp"-->
<!--#include virtual="/label/common/utils/json.asp"-->

<%
' response.charset = "GBK"
Dim baseAuth: baseAuth = request("baseAuth")
Dim item: item = request("item")
Dim po_no: po_no = request("po_no")
Dim line_no: line_no = request("line_no")
Dim dateTo: dateTo = replace(request("dateTo"), "-", "")
Dim dateFrom: dateFrom = replace(request("dateFrom"), "-", "")
Dim vendorCode: vendorCode = request("vendorCode")
' Dim vendorCodeHigh: vendorCodeHigh = request("vendorCodeHigh")

' 生成json数据
Dim formData: Set formData = jsArray()

' 供应商代码
Set formData(Null) = jsObject()
formData(Null)("SHLPFIELD") = "LIFNR"
formData(Null)("SIGN") = "I"
If vendorCode <> "" Then
	formData(Null)("OPTION") = "CP"
	formData(Null)("LOW") = vendorCode
ELSE
	formData(Null)("OPTION") = "CP"
	formData(Null)("LOW") = "*"
END If
' formData(Null)("HIGH") = vendorCodeHigh

' 凭证日期
If dateFrom <> "" AND dateTo <> "" Then
	Set formData(Null) = jsObject()
	formData(Null)("SHLPFIELD") = "AEDAT"
	formData(Null)("SIGN") = "I"
	formData(Null)("OPTION") = "BT"
	formData(Null)("LOW") = dateFrom
	formData(Null)("HIGH") = dateTo
ELSEIF dateFrom <> "" Then
	Set formData(Null) = jsObject()
	formData(Null)("SHLPFIELD") = "AEDAT"
	formData(Null)("SIGN") = "I"
	formData(Null)("OPTION") = "EQ"
	formData(Null)("LOW") = dateFrom
ELSEIF dateTo <> "" Then
	Set formData(Null) = jsObject()
	formData(Null)("SHLPFIELD") = "AEDAT"
	formData(Null)("SIGN") = "I"
	formData(Null)("OPTION") = "LE"
	formData(Null)("LOW") = dateTo
END If

' 物料号
If item <> "" Then
	Set formData(Null) = jsObject()
	formData(Null)("SHLPFIELD") = "MATNR"
	formData(Null)("SIGN") = "I"
	formData(Null)("OPTION") = "CP"
	formData(Null)("LOW") = item
END If

' 采购订单号
If po_no <> "" Then
	Set formData(Null) = jsObject()
	formData(Null)("SHLPFIELD") = "EBELN"
	formData(Null)("SIGN") = "I"
	formData(Null)("OPTION") = "CP"
	formData(Null)("LOW") = po_no
END If

' 行项目号
IF line_no <> "" Then
	Set formData(Null) = jsObject()
	formData(Null)("SHLPFIELD") = "EBELP"
	formData(Null)("SIGN") = "I"
	formData(Null)("OPTION") = "EQ"
	formData(Null)("LOW") = line_no
END IF

' response.write(formData.jsString)
' 判断是否处于登录状态
IF Session("loginStaffId") <> "" Then
	' 请求SAP数据
	Dim sUrl: sUrl = Application("API_URL") & "?ACTION=PO_QUERY"
	Dim xmlhttp: Set xmlhttp = Server.CreateObject("MSXML2.ServerXMLHTTP")
	xmlhttp.open "POST", sURL, False
	xmlhttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
	xmlhttp.setRequestHeader "Authorization", baseAuth
	xmlhttp.send(formData.jsString)
	Dim res: res = xmlhttp.responseText
	response.write(res)
	Set xmlhttp = Nothing
ELSE
	result("success") = false
	result("msg") = "记录获取失败, 请重新登录"
	response.write(result.jsString)
END IF
%>