<nav class="headingbar navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/">SUGA PO Portal</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav list-inline">
				<li><a href="/">首页</a></li>
				<li><a data-url="/change_pwd.asp">更改密码</a></li>
				<li><a href="javascript: if (window.confirm('你是否限要登出PO Portal?')) window.location='/clientLogout.asp'">登出</a></li>
				<li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" href="#">账号：<%=Session("LoginName")%></a></li>
				<li><a href="javascript:;" id="welcome">欢迎你：<%=Session("Username")%></a></li>
			</ul>
		</div>
	</div>
</nav>
<script>
sayWord($('#welcome'), 300, true);
</script>