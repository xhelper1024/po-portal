function submitToSAP() {
	index = parent.layer.confirm('确定同步数据库所有数据到SAP？', {
		btn: ['Sure', 'No']
	}, function() {
		parent.layer.close(index);
		var i;
		$.ajax({
			url: "/label/utils/submitToSAP.asp",
			type: "POST",
			contentType: "application/x-www-form-urlencoded",
			dataType: "json",
			data: {
				baseAuth: baseAuth
			},
			beforeSend: function() {
				i = showLoad();
			},
			success: function(res) {
				closeLoad(i);
				parent.layer.msg(res.MSG_CONTENT);
			}
		})
	});
}

function showLoad() {
	return parent.layer.msg('拼命执行中...', {
		icon: 16,
		shade: [0.5, '#7d7d7d'],
		scrollbar: false,
		offset: 'auto',
		time: 100000,
	});
}

function closeLoad(index) {
	parent.layer.close(index);
}