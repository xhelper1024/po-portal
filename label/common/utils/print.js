// 生成二维码
function setQrCode(id, text, colorDark, colorLight) {
	new QRCode($("#print-page")[0].contentWindow.document.getElementById(id), {
		// text: JSON.stringify(jsonTxt),
		text: text,
		width: 150,
		height: 150,
		colorDark: colorDark ? colorDark : "#000",
		colorLight: colorLight ? colorLight : "#fff",
		correctLevel: QRCode.CorrectLevel.H
	});
}

// 设置打印内容
function setPrintContent(poInfo, labelInfo) {
	// 先清空内容
	$print_page = $($("#print-page")[0].contentWindow.document.body);
	$print_page.html("");
	for (var i = 0; i < labelInfo.length; i++) {
		if (labelInfo[i].package_type == "01") {
			$print_page.append(
				'<div class="label" style="background: #fff; margin: 0rem; padding: 0rem; text-align: center; vertical-align: middle; page-break-after: always;">' +
				'<table style="background: #fff; width: 35rem; height: 17.5rem; text-align: center; padding: 0.3125rem; margin: auto;" cellspacing="0" border="1">' +
				'<tr rowspan="2">' +
				'<th colspan="4" style="font-size: 1.5rem;">外箱标识卡</th>' +
				'</tr>' +
				'<tr>' +
				'<td colspan="2" rowspan="6">' +
				'<div id="box_qrcode' + i + '" class="qrcode"></div>' +
				'</td>' +
				'<td style="width: 4.875rem;">' + (poInfo.werks == '0161' ? '越南ID' : 'ID') + '</td>' +
				'<td>' + labelInfo[i].qrcode_id.replace(/\b(0+)/gi, "") + '</td>' +
				'</tr>' +
				'<tr>' +
				'<td>供应商</td>' +
				'<td><div style="margin: auto; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; width: 15.875rem;">' +
				poInfo.mfg_name + '</div></td>' +
				'</tr>' +
				'<tr>' +
				'<td>厂商批号</td>' +
				'<td>' + labelInfo[i].prod_no + '</td>' +
				'</tr>' +
				'<tr>' +
				'<td>包装数</td>' +
				'<td>' + labelInfo[i].package_qty + '</td>' +
				'</tr>' +
				'<tr>' +
				'<td>总容量</td>' +
				'<td>' + labelInfo[i].package_qty + ' / 箱</td>' +
				'</tr>' +
				'<tr>' +
				'<td>存放区域</td>' +
				'<td>' + poInfo.store_position + '</td>' +
				'</tr>' +
				'<tr>' +
				'<td>订单</td>' +
				'<td>' + poInfo.po_no + '-' + poInfo.line_no + '</td>' +
				'<td>物料</td>' +
				'<td>' + poInfo.material_no + '</td>' +
				'</tr>' +
				'<tr>' +
				'<td>描述</td>' +
				'<td colspan="3">' +
				'<div style="margin: auto; nowrap; overflow: hidden; text-overflow: ellipsis; width: 30.125rem;">' + poInfo.material_desc +
				'</div>' +
				'</td>' +
				'</tr>' +
				'</table></div>');
			setQrCode("box_qrcode" + i, labelInfo[i].qrcode_id);
		} else {
			var vendor_code = "";
			if (labelInfo[i].parent_qrcode_id != "") {
				vendor_code = ' / ' + labelInfo[i].parent_qrcode_id.replace(/\b(0+)/gi, "");
			}
			$print_page.append(
				'<div class="label" style="background: #fff; margin: 0rem; padding: 0rem; text-align: center; vertical-align: middle; page-break-after: always;">' +
				'<table style="background: #fff; width: 35rem; height: 17.5rem; text-align: center; padding: 0.3125rem; margin: auto;" cellspacing="0" border="1">' +
				'<tr>' +
				'<td colspan="2" rowspan="6">' +
				'<div id="qrcode' + i + '" class="qrcode"></div>' +
				'</td>' +
				'<td style="width: 4.875rem;">' + (poInfo.werks == '0161' ? '越南ID' : 'ID') + '</td>' +
				'<td>' + labelInfo[i].qrcode_id.replace(/\b(0+)/gi, "") + vendor_code + '</td>' +
				'</tr>' +
				'<tr>' +
				'<td>供应商</td>' +
				'<td>' +
				'<div style="margin: auto; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; width: 15.875rem;">' +
				poInfo.mfg_name +
				'</div>' +
				'</td>' +
				'</tr>' +
				'<tr>' +
				'<td>厂商批号</td>' +
				'<td>' + labelInfo[i].prod_no + '</td>' +
				'</tr>' +
				'<tr>' +
				'<td>生产日期</td>' +
				'<td>' + labelInfo[i].prod_date + '</td>' +
				'</tr>' +
				'<tr>' +
				'<td>失效日期</td>' +
				'<td>' + labelInfo[i].expire_date + '</td>' +
				'</tr>' +
				'<tr>' +
				'<td>存放区域</td>' +
				'<td>' + poInfo.store_position + '</td>' +
				'</tr>' +
				'<tr>' +
				'<td>订单</td>' +
				'<td>' + poInfo.po_no + '-' + poInfo.line_no + '</td>' +
				'<td>数量</td>' +
				'<td>' + labelInfo[i].package_qty + poInfo.po_unit + '</td>' +
				'</tr>' +
				'<tr>' +
				'<td>类型</td>' +
				'<td>' + poInfo.maabc + (poInfo.normt ? '●' : "") + '</td>' +
				'<td>物料</td>' +
				'<td>' +
				'<div>' + poInfo.material_no +
				'</div>' +
				'</td>' +
				'</tr>' +
				'<tr>' +
				'<td>MPN</td>' +
				'<td colspan="3">' +
				'<div style="margin: auto; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; width: 30.125rem;">' +
				poInfo.mfg + "/" + poInfo.mfg_no +
				'</div>' +
				'</td>' +
				'</tr>' +
				'<tr>' +
				'<td>描述</td>' +
				'<td colspan="3">' +
				'<div style="margin: auto; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; width: 30.125rem;">' +
				poInfo.material_desc +
				'</div>' +
				'</td>' +
				'</tr>' +
				'</table></div>');
			setQrCode("qrcode" + i, labelInfo[i].qrcode_id);
		}
		$print_page.css('margin', '0px');
		$print_page.css('padding', '0px');
		$print_page.find('.label').css('padding', '10px 0px 0px');
		$print_page.find('table').css('margin', '0px;');
		$print_page.find('table').css('border', 'none');
		$print_page.find('td').css('font-size', '16px');
		$print_page.find('td').css('font-family', 'Microsoft Yahei, Yuanti SC, Arial');
		$print_page.find('td').css('font-weight', 'bold');
		$print_page.find('td').css('white-space', 'nowrap');
	}
}

function savePDF1() {
	var target = $("#print-page")[0].contentWindow.document.body;
	var pageWidth = $(target).find('table').css('width').replace('px', '');
	var pageHeight = pageWidth / 2;
	// 判断是横版还是竖版
	var direction = pageWidth > pageHeight ? "l" : "p";
	var pdf = new jsPDF(direction, 'pt', [pageWidth, pageHeight]);
	var options = {
		pagesplit: true
	};

	$(target).find('table').each(function() {
		var table = $(this)[0];
		pdf.addHTML(table, options, function() {
			// console.log($(this));

		});
		pdf.save("label.pdf");
	});
}

// 保存为PDF
function savePDF(file_name) {
	var target = $("#print-page")[0].contentWindow.document.body;
	$(target).find('.label').css('padding', '0px');
	$(target).find('table').css('padding', '10px');
	// var DPI = getDPI();
	// 直接输出成文件
	html2canvas(target, {
		onrendered: function(canvas) {
			// target.appendChild(canvas);
			// console.log(DPI[0], DPI[1]);
			var pageData = canvas.toDataURL('IMAGE/JPEG', 1.0);
			// 将canvas转成Image对象，并且缩放到指定宽度
			// var imageData = new Image();
			// imageData.src = pageData;
			// imageData.width = 80 * DPI[0] / 25.4
			// imageData.height = imageData.width / canvas.width * canvas.height;
			// 再将Image对象转成Base64格式字符串
			// getBase64FromImage(imageData, function(base64) {

			// });
			var pageWidth = canvas.width;
			var pageHeight = pageWidth / 2;

			var leftHeight = canvas.height;
			var offsetHeight = 0;

			// 判断是横版还是竖版
			var direction = pageWidth > pageHeight ? "l" : "p";
			var pdf = new jsPDF(direction, 'pt', [pageWidth, pageHeight]);
			// 如果内容未超过一页高度，则无需分页
			if (canvas.height <= pageHeight) {
				pdf.addImage(pageData, 'jpeg', 0, 0, canvas.width, canvas.height);
			} else {
				while (leftHeight > 0) {
					pdf.addImage(pageData, 'jpeg', 0, offsetHeight, canvas.width, canvas.height);
					leftHeight -= pageHeight;
					offsetHeight -= pageHeight;
					if (leftHeight > 0) {
						pdf.addPage();
					}
				}
			}
			pdf.save(file_name + "-" + getCurTime() + ".pdf");
		}
	})
}

function getCurTime() {
	var now = new Date();
	var month = now.getMonth() + 1; //月
	var day = now.getDate(); //日
	var hh = now.getHours(); //时
	var mm = now.getMinutes(); //分

	var clock = "";
	if (month < 10)
		clock += "0";
	clock += month;
	if (day < 10)
		clock += "0";
	clock += day;
	if (hh < 10)
		clock += "0";
	clock += hh;
	if (mm < 10) clock += '0';
	clock += mm;
	return clock;
}

function getDPI() {
	var arrDPI = new Array();
	if (window.screen.deviceXDPI != undefined) {
		arrDPI[0] = window.screen.deviceXDPI;
		arrDPI[1] = window.screen.deviceYDPI;
	} else {
		var tmpNode = document.createElement("DIV");
		tmpNode.style.cssText = "width:1in; height:1in; position:absolute; left:0px; top:0px; z-index:99; visibility:hidden";
		document.body.appendChild(tmpNode);
		arrDPI[0] = parseInt(tmpNode.offsetWidth);
		arrDPI[1] = parseInt(tmpNode.offsetHeight);
		tmpNode.parentNode.removeChild(tmpNode);
	}
	return arrDPI;
}

// 实现将项目的图片转化成base64
function getBase64FromImage(_img, callback) {
	//处理缩放，转格式
	_img.onload = function() {
		var _canvas = document.createElement("canvas");
		var w = this.width;
		var h = this.height;
		_canvas.setAttribute("width", w);
		_canvas.setAttribute("height", h);
		var ctx = _canvas.getContext("2d");
		ctx.drawImage(this, 0, 0, w, h);
		// ctx.mozImageSmoothingEnabled = false;
		// ctx.webkitImageSmoothingEnabled = false;
		// ctx.msImageSmoothingEnabled = false;
		// ctx.imageSmoothingEnabled = false;
		// ctx.oImageSmoothingEnabled = false;
		var base64 = _canvas.toDataURL("image/jpeg");
		// $('.panel-footer').append("<img src='"+base64+"' />")
		callback(base64);
	}
}
