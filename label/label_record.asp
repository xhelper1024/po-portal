<%@ CodePage = "65001" %>
<% Option Explicit %>
<!--#include virtual="/b_ui/display.asp"-->
<!--#include virtual="/purchase/conf.asp"-->
<!--#include virtual="/b_util/user_common_include.asp"-->
<script src="/script/check.js"></script>
<div class="panel panel-default">
	<div class="panel-heading">
		<ul class="nav nav-tabs">
			<li><a href="/label/label_list.asp">PO List</a></li>
			<li class="active"><a href="#record" data-toggle="tab">Label Record</a></li>
		</ul>
	</div>
	<div class="panel-body">
		<div id="toolbar">
			<form class="searchForm" method="POST">
				<div class="row form-group">
					<div class="col-sm-4">
						<div class="input-group">
							<div class="input-group-addon">供应商</div>
							<% IF session("stafflevel") = 3 Then %>
							<input type="text" class="form-control" name="vendorCode" value="<%=Session("Username")%>" readonly="readonly"/>
							<% ELSE %>
							<input type="text" class="form-control" name="vendorCode" value="<%=Request.form("vendorCode")%>" placeholder="请输入供应商"/>
							<% END IF %>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="input-group">
							<div class="input-group-addon">PO号</div>
							<input type="text" class="form-control" name="po_no" value="<%=Request.form("po_no")%>" placeholder="请输入PO号"/>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="input-group">
							<div class="input-group-addon">行项目</div>
							<input type="text" class="form-control" name="line_no" value="<%=Request.form("line_no")%>" placeholder="请输入行项目号"/>
						</div>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-4">
						<div class="input-group">
							<div class="input-group-addon">物料号</div>
							<input type="text" class="form-control" name="item" value="<%=Request.form("item")%>" placeholder="请输入物料号"/>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="sr-only" for="dateFrom">Print Date</label>
						<div class="input-group">
							<div class="input-group-addon">Print Date</div>
							<input type="text" class="form-control datepicker" autocomplete="off" name="dateFrom" value="<%=request.form("dateFrom")%>" placeholder="From">
							<div class="input-group-addon">~</div>
							<input type="text" class="form-control datepicker" autocomplete="off" name="dateTo" value="<%=request.form("dateTo")%>" placeholder="TO" />
						</div>
					</div>
					<div class="col-sm-4">
						<button id="btn_search" type="button" class="btn btn-primary">
							<i class="layui-icon layui-icon-search"></i> Search
						</button>
					</div>
				</div>
			</form>
		</div>
		<table id="record-table" class="data-table table-striped" style="font: 0.75rem;"></table>
	</div>
</div>
<iframe id="print-page"></iframe>
<script>
$table = $("#record-table")
$(function(){
	// 将打印页面的body元素的margin值设为0，很重要
	$('#print-page').load(function () {
		var x = $($("#print-page")[0].contentWindow.document.body);
		x.css('margin', '0px');
	});
	$table.bootstrapTable("resetView", {
		height: $(window).height() * 0.8
	});
	$(".datepicker").datepicker({
		language: "zh-CN",
		autoclose: true, //选择后自动关闭
		clearBtn: true,//清除按钮
		format: "yyyy/mm/dd",
		todayHighlight : true,
		todayBtn: "linked",
		initialDate: new Date(), //也可以通过function函数获得值
	});
});
$table.bootstrapTable({
	url: '/label/utils/get_record_list.asp',
	method: 'POST',										//请求方式（*）
	contentType: "application/x-www-form-urlencoded",	// 以formdata方式传递数据
	dataType: "json",
	maintainSelected: true,
	toolbar: '#toolbar',					// 工具按钮用哪个容器
	toolbarAlign: "center",
	cache: false,							// 是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
	striped: true,
	pagination: true,						// 是否显示分页（*）
	//sortable: false,						// 是否启用排序
	//sortOrder: "asc",						// 排序方式
	sidePagination: "server",				// 分页方式：client客户端分页，server服务端分页（*）
	pageNumber: 1,							// 初始化加载第一页，默认第一页,并记录
	pageSize: 15,							// 每页的记录行数（*）
	pageList: [15, 30, 50],					// 可供选择的每页的行数（*）
	search: false,							// 是否显示表格搜索
	strictSearch: true,
	showExport: true,						// 导出报表
	//showColumns: true,					// 是否显示所有的列（选择显示的列）
	//showRefresh: true,					// 是否显示刷新按钮
	minimumCountColumns: 2,					// 最少允许的列数
	clickToSelect: false,					// 是否启用点击选中行
	uniqueId: "po_no, line_no",				// 每一行的唯一标识，一般为主键列
	//showToggle: true,						// 是否显示详细视图和列表视图的切换按钮
	cardView: false,						// 是否显示详细视图
	detailView: false,						// 是否显示父子表
	idField: 'po_no, line_no',				// 指定主键
	singleSelect: true,						// 开启单选,想要获取被选中的行数据必须要有该参数
	//得到查询的参数
	queryParams : function (params) {
		// console.log(params)
		//这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
		var temp = {
			pageSize: params.limit,                     	// 页面大小
			page: (params.offset / params.limit) + 1,   	// 页码
			sort: params.sort,								// 排序顺序
			sortOrder: params.order,						// 排序参数
			<!-- Begin Update By XieHui At 2020-04-27 14:37:26 -->
			po_no: $('input[name="po_no"]').val(),				// po_no
			line_no: $('input[name="line_no"]').val(),			// 行项目号
			item: $('input[name="item"]').val(),				// 物料号
			<!-- End Update By XieHui At 2020-04-27 14:37:26 -->
			dateTo: $('input[name="dateTo"]').val(),			// Print Date上限
			dateFrom: $('input[name="dateFrom"]').val(),		// Print Date下限
			vendorCode: $('input[name="vendorCode"]').val(),	// 供应商代码
			createBy: "<%=Session("LoginName")%>",				// 创建人账号
		};
		return temp;
	}, responseHandler: function(res) {
		// res = $.parseJSON(res);
		// console.log(res);
		return {
			"total": res.total,
			"rows": res.rows
		};
	},columns: [
		{field: '', title: 'No.', align: "center", valign: "middle", formatter: function(value, row, index) {
			return index + 1;
		}},
		{field: 'po_no', title: 'PO', sortable: true, align: "center", valign: "middle", formatter: function(value, row, index) {
			sessionStorage.setItem(row['po_no'] + row['line_no'], JSON.stringify(row));
			return '<a title="Detail" href="/label/label_detail.asp?po_no=' + row['po_no'] + '&line_no=' + row['line_no']
				+'" class="btn-print" style="color: #337ab7;">' + value + '</a>';
		},},
		{field: 'line_no', title: 'LineNo', sortable: true, align: "center", valign: "middle"},
		{field: 'material_no', title: 'Item', sortable: true, align: "center", valign: "middle"},
		{field: 'material_type', title: 'Type', sortable: false, align: "center", valign: "middle"},
		{field: 'po_qty', title: 'PoQty', sortable: true, align: "center", valign: "middle"},
		{field: 'printed_qty', title: 'PrintedQty', sortable: true, align: "center", valign: "middle", formatter: function(value, row, index) {
			var str = ''
			if(row['po_qty'] == value) {
				str = '<span class="text-success">' + value + '</span>'
			} else {
				str = '<span class="text-danger">' + value + '</span>'
			}
			return str
		}},
		{field: 'po_unit', title: 'UOM', sortable: false, align: "center", valign: "middle"},
		// {field: 'mfg', title: 'MFG', sortable: false, align: "center", valign: "middle"},
		// {field: 'prod_no', title: 'ProduceNo', sortable: true, align: "center", valign: "middle"},
		// {field: 'prod_date', title: 'ProduceDate', sortable: true, align: "center", valign: "middle"},
		// {field: 'status', title: 'Status', sortable: true, align: "center", valign: "middle"},
		{field: 'create_time', title: 'CreateTime', sortable: true, align: "center", valign: "middle"},
		// {field: '', title: 'Option', align: "center", valign: "middle"}
	], onLoadSuccess: function () {
		
	}, onLoadError: function () {
		parent.layer.msg("数据加载失败！");
	}, onDblClickRow: function (row, $element) {
		console.log(row);
	}/* formatNoMatches: function () {
		//没有匹配的结果
		return "Sorry, there's no record!";
	}*/
});

$("#btn_search").on('click', function() {
	$table.bootstrapTable('refresh')
});
</script>
</html>
