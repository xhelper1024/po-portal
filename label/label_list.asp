<%@ CodePage=65001%>
<% Option Explicit %>
<!--#include virtual="/b_ui/display.asp"-->
<!--#include virtual="/purchase/conf.asp"-->
<!--#include virtual="/b_util/user_common_include.asp"-->
<script src="/script/check.js"></script>
<%
	if request.cookies(Application("gDefaultCookiesName")) <> "" AND (Session("loginStaffId") = empty or Session("loginStaffId") = "") then
		response.Redirect "/cookiesLogin.asp"
	elseif Session("loginStaffId") = empty or Session("loginStaffId") = "" then 
		response.Redirect("/index.asp") 
	end if
%>
<div class="panel panel-default">
	<div class="panel-heading">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#list" data-toggle="tab">PO List</a></li>
			<li><a href="/label/label_record.asp">Label Record</a></li>
		</ul>
	</div>
	<div class="panel-body">
		<div id="toolbar_1">
			<form class="searchForm" method="POST">
				<div class="row form-group">
					<div class="col-sm-4">
						<div class="input-group">
							<div class="input-group-addon">供应商</div>
							<% IF session("stafflevel") = 3 Then %>
							<input type="text" class="form-control" name="vendorCode" value="<%=Session("Username")%>" readonly="readonly"/>
							<% ELSE %>
							<input type="text" class="form-control" name="vendorCode" value="<%=Request.form("vendorCode")%>" placeholder="请输入供应商"/>
							<% END IF %>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="input-group">
							<div class="input-group-addon">PO号</div>
							<input type="text" class="form-control" name="po_no" value="<%=Request.form("po_no")%>" placeholder="请输入PO号"/>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="input-group">
							<div class="input-group-addon">行项目</div>
							<input type="text" class="form-control" name="line_no" value="<%=Request.form("line_no")%>" placeholder="请输入行项目号"/>
						</div>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-4">
						<div class="input-group">
							<div class="input-group-addon">物料号</div>
							<input type="text" class="form-control" name="item" value="<%=Request.form("item")%>" placeholder="请输入物料号"/>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="input-group">
							<div class="input-group-addon">创建日期</div>
							<input class="form-control datepicker" autocomplete="off" type="text" name="dateFrom" value="<%=request.form("dateFrom")%>" placeholder="FROM"/>
							<div class="input-group-addon">~</div>
							<input class="form-control datepicker" autocomplete="off" type="text" name="dateTo" value="<%=request.form("dateTo")%>" placeholder="TO" />
						</div>
					</div>
					<div class="col-sm-4">
						<button id="btn_search" type="button" class="btn btn-primary">
							<i class="layui-icon layui-icon-search"></i> Search
						</button>
					</div>
				</div>
			</form>
		</div>
		<table id="data-table" class="data-table table-striped" style="font: 0.75rem;"></table>
	</div>
</div>
<script>
$table = $("#data-table");
$(function(){
	$table.bootstrapTable("resetView", {
		height: $(window).height() * 0.8
	});
	$(".datepicker").datepicker({
		language: "zh-CN",
		autoclose: true, //选择后自动关闭
		clearBtn: true,//清除按钮
		format: "yyyy-mm-dd",
		todayHighlight : true,
		todayBtn: "linked",
		initialDate: new Date(), //也可以通过function函数获得值
	});
});
$table.bootstrapTable({
	url: '/label/utils/get_po_list_test.asp',
	// url: '/label/utils/get_po_list.asp',
	method: 'POST',                      //请求方式（*）
	contentType: "application/x-www-form-urlencoded",
	dataType: "json",
	maintainSelected: true,
	// showFullscreen : true,
	toolbar: '#toolbar_1',					// 工具按钮用哪个容器
	toolbarAlign: "center",
	cache: false,							// 是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
	striped: true,
	pagination: true,						// 是否显示分页（*）
	//sortable: false,						// 是否启用排序
	//sortOrder: "asc",						// 排序方式
	sidePagination: "client",				// 分页方式：client客户端分页，server服务端分页（*）
	pageNumber: 1,							// 初始化加载第一页，默认第一页,并记录
	pageSize: 15,							// 每页的记录行数（*）
	pageList: [15, 30, 50],					// 可供选择的每页的行数（*）
	search: false,							// 是否显示表格搜索
	strictSearch: true,
	showExport: true,						// 导出报表
	//showColumns: true,					// 是否显示所有的列（选择显示的列）
	//showRefresh: true,					// 是否显示刷新按钮
	minimumCountColumns: 2,					// 最少允许的列数
	clickToSelect: false,					// 是否启用点击选中行
	uniqueId: "id",							// 每一行的唯一标识，一般为主键列
	//showToggle: true,						// 是否显示详细视图和列表视图的切换按钮
	cardView: false,						// 是否显示详细视图
	detailView: false,						// 是否显示父子表
	idField: 'po_no, line_no',				// 指定主键
	singleSelect: true,						// 开启单选,想要获取被选中的行数据必须要有该参数
	//得到查询的参数
	queryParams : function (params) {
		// console.log(params)
		// 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
		var temp = {
			// pageSize: params.limit,                     	// 页面大小
			// page: (params.offset / params.limit) + 1,   	// 页码
			// sort: params.sort,							// 排序列名  
			// sortOrder: params.order,						// 排位命令（desc，asc）
			baseAuth: baseAuth,								// 身份验证
			vendorCode: $('input[name="vendorCode"]').val(),// 供应商代码
			po_no: $('input[name="po_no"]').val(),			// PO号
			line_no: $('input[name="line_no"]').val(),		// 行项目号
			item: $('input[name="item"]').val(),			// 物料号
			// vendorCode: $('input[name="vendorCode"]').val(),
			dateFrom: $('input[name="dateFrom"]').val(),
			dateTo: $('input[name="dateTo"]').val()
		};
		return temp;
	}, responseHandler: function(res) {
		// res = $.parseJSON(res);
		if(res.DATA) {
			return {
				"total": res.DATA.length,
				"rows": res.DATA
			};
		}
	},columns: [
		// {field :"checked", title: '编号', checkbox:true},
		{field: '', title: 'No.', align: "center", valign: "middle", formatter: function(value, row, index) {
			return index + 1;
		}},
		{field: 'EBELN', title: 'PO', sortable: true, align: "center", valign: "middle", formatter: function(value, row, index) {
			// return '<a href="/label/label_print.asp?po_no='+ row['EBELN'] +'&line_no='+ row['EBELP'] 
			// +'" class="btn-print" style="color: #337ab7;">' + value + '</i></a>';
			sessionStorage.setItem(row['EBELN'] + row['EBELP'], JSON.stringify(row));
			return '<a href="/label/label_print.asp?po_no='+ row['EBELN'] +'&line_no='+ row['EBELP']
				+'" class="btn-print" style="color: #337ab7;">' + value + '</i></a>';
		},},
		{field: 'EBELP', title: 'Line', sortable: true, align: "center", valign: "middle"},
		{field: 'MATNR', title: 'Item', sortable: true, align: "center", valign: "middle"},
		<% IF session("stafflevel") <> 3 Then %>
		{field: 'MFRNR', title: 'MFG', sortable: false, align: "center", valign: "middle"},
		{field: 'MFRPN', title: 'MFG P/N', sortable: true, align: "center", valign: "middle"},
		<% END IF %>
		// {field: 'MAKTX', title: 'Item Dsec', sortable: false, align: "center", valign: "middle"},
		{field: 'AEDAT', title: 'Issue Date', sortable: true, align: "center", valign: "middle"},
		{field: 'MENGE', title: 'PO Qty', sortable: true, align: "center", valign: "middle"},
		{field: 'W_MENGE', title: 'OPO Qty', sortable: true, align: "center", valign: "middle"},
		{field: 'MEINS', title: 'UOM', sortable: false, align: "center", valign: "middle"},
		{field: 'PSTYP', title: 'Project Type', sortable: true, align: "center", valign: "middle"},
		{field: 'PRFRQ', title: 'Inspection interval', sortable: true, align: "center", valign: "middle"},
		// {field: 'MTART', title: 'Item Type', sortable: true, align: "center", valign: "middle"},
		// {field: 'LOEKZ', title: 'LOEKZ', sortable: true, align: "center", valign: "middle"},
	], onLoadSuccess: function (res) {
		if(res.total > 0) {
			// parent.layer.msg("加载成功！");
		} else {
			parent.layer.msg("查询无数据！");
		}
	}, onLoadError: function () {
		parent.layer.msg("数据加载失败！");
	}, onDblClickRow: function (row, $element) {
		// console.log(row);
	}
});

$("#btn_search").on('click', function() {
	$table.bootstrapTable('refresh')
});
</script>