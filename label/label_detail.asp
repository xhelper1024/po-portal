<%@ CodePage = "65001" %>
<% Option Explicit %>
<!--#include virtual="/b_ui/display.asp"-->
<!--#include virtual="/purchase/conf.asp"-->
<!--#include virtual="/b_util/user_common_include.asp"-->
<script src="/script/check.js"></script>
<script type="text/javascript" src="/static/qrcodejs-master/qrcode.min.js"></script>
<div class="panel panel-default">
	<div class="panel-heading">
		<h1>Label Detail</h1>
	</div>
	<div class="panel-body">
		<div id="myTabContent" class="tab-content">
			<div class="tab-pane fade in active" id="list">
				<div class="row form-group">
					<div class="col-sm-12">
						<ul class="breadcrumb">
							<li>订单号：<code id="po_no">xxx</code></li>
							<li>项目：<code id="line_no">xxx</code></li>
							<li>物料编号：<code id="item">xxx</code></li>
							<li>创建日期：<code id="po_date">xxx</code></li>
							<li>采购订单数量：<code id="po_qty">0</code></li>
						</ul>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-3">
						<div class="input-group">
							<span class="input-group-addon" title="已打印数量 * Printed Qty" data-trigger="hover" data-container="body"
								data-toggle="popover" data-placement="top" data-content="已经包装并生成条码的数量">
								已打印数量 * Printed Qty <i class="glyphicon glyphicon-question-sign"></i></span>
							<input type="number" class="form-control" min="0" id="printed_qty" name="printed_qty" disabled="disabled"/>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="input-group">
							<span class="input-group-addon" title="可打印数量 * Printable Qty" data-trigger="hover" data-container="body"
								data-toggle="popover" data-placement="top" data-content="可打印数量<=采购订单数量-已打印数量">
								可打印数量 * Printable Qty <i class="glyphicon glyphicon-question-sign"></i></span>
							<input type="number" class="form-control" min="0" id="remain_qty" name="remain_qty" disabled="disabled"/>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="input-group">
							<span class="input-group-addon" title="采购订单单位">采购订单单位 * Unit</span>
							<input type="text" class="form-control" readonly="readonly" id="po_unit"/>
						</div>
					</div>
					<div class="col-sm-3">
						<a class="btn btn-sm btn-default" title="Translate" data-trigger="hover"
							data-container="body" data-toggle="popover" data-placement="top" 
							data-content="Back" href="javascript: history.go(-1);;"><span class="glyphicon glyphicon-circle-arrow-left"></span> 返回 </a>
						<button class="btn btn-sm btn-link" title="Translate" data-trigger="hover"
							data-container="body" data-toggle="popover" data-placement="top" 
							data-content="Refresh" onclick="history.go(0)"><span class="glyphicon glyphicon-refresh"></span> 刷新 </button>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-12">
						<ul class="breadcrumb" style="margin: 0rem;">
							<li>外箱标签数量：<code id="box_qty">0</code></li>
							<li>最小包装标签数量：<code id="spq_qty">0</code></li>
							<li>包装总数量：<code id="total_qty">0</code></li>
							<li class="hide"><a class="text-info" id="btn-new-box-label" onclick="newBoxLabel(0, 0, 0); computeBoxQty(1)">新增外箱</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel-footer">
		<div class="row">
			<div class="col-sm-12">
				<fieldset>
					<legend>打印记录</legend>
					<table class="table table-hover table-bordered table-condensed" id="records">
						<thead>
							<tr>
								<th>序号</th>
								<th>生产批次</th>
								<th>生产日期</th>
								<th>打印日期</th>
								<th>过期日期</th>
								<th>包装数量</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</fieldset>
			</div>
		</div>
		<% IF Session("LoginName") = "hui.xie@suga.com.cn" THEN %>
		<div class="row">
			<div class="col-sm-6">
				<button onclick="submitToSAP()" class="btn btn-sm btn-danger">同步整个数据库至SAP</button>
			</div>
		</div>
		<% END IF %>
	</div>
</div>
<iframe id="print-page"></iframe>
<script src="/label/common/utils/submitToSAP.js"></script>
<script>
var rows = JSON.parse(sessionStorage.getItem("<%=request("po_no") + request("line_no")%>"));
$(function () {
	// 设置页面内容
	$('#po_no').html(rows['po_no']);
	$('#line_no').html(rows['line_no']);
	$('#item').html(rows['material_no']);
	$('#po_date').html(rows['po_date']);
	$('#po_qty').html(rows['po_qty']);
	$('#printed_qty').val(rows['printed_qty']);
	$('#remain_qty').val(rows['po_qty'] - rows['printed_qty']);
	$('#po_unit').val(rows['po_unit']);
	// 提示
	$("[data-toggle='popover']").popover();
	// 寻找打印记录
	getPrintedRecords();
});

// 重新打印
function download(poInfo, labelInfo) {
	index = parent.layer.confirm('Are you sure download？', {
		btn: ['Sure','No']
	}, function() {
		parent.layer.close(index);
		// 动态设置打印内容
		setPrintContent(poInfo, labelInfo);
		// $("#print-page")[0].contentWindow.print();
		savePDF(poInfo.po_no + "-" + poInfo.line_no);
	});
}

function print(poInfo, labelInfo) {
	index = parent.layer.confirm('Are you sure print？', {
		btn: ['Sure','No']
	}, function() {
		parent.layer.close(index);
		// 动态设置打印内容
		setPrintContent(poInfo, labelInfo);
		$("#print-page")[0].contentWindow.print();
		// savePDF();
	});
}

// 找出此PO行项目的所有打印记录
function getPrintedRecords() {
	var $table = $('#records tbody');
	$.ajax({
		url: '/label/utils/get_printed_records.asp',
		type: "POST",
		contentType: "application/x-www-form-urlencoded",
		dataType: "json",
		data: {
			po_no: "<%=request.querystring("po_no")%>",
			line_no: "<%=request.querystring("line_no")%>"
		},
		success: function(res) {
			if(res.success) {
				var str = ''
				<% IF session("stafflevel") = 1 OR session("stafflevel") = 2 Then %>
				str = '<a class="btn btn-sm btn-link" style="color: red;" title="delete" id="btn-delete"><i class="layui-icon layui-icon-delete"></i> 删除</a>';
				<% END IF %>
				str = str + '<a class="btn btn-sm btn-link" title="download" id="btn-download"><i class="layui-icon layui-icon-download-circle"></i> 下载</a>';
				// 如果不是Chrome浏览器，则不允许打印功能
				var ua = navigator.userAgent.toLocaleLowerCase();
				if(ua.indexOf("chrome") != -1 && ua.indexOf("edge") == -1) {
					str = str + '<a class="btn btn-sm btn-link" title="print" id="btn-print"><i class="layui-icon layui-icon-print"></i> 打印</a>';
				}
				$table.html("");
				for(var i = 0; i < res.data.length; i++) {
					$table.append(
					'<tr>'
					+	'<td>' + (i + 1) + '</td>'
					+	'<td>' + res.data[i].prod_no + '</td>'
					+	'<td>' + res.data[i].prod_date + '</td>'
					+	'<td>' + res.data[i].create_time + '</td>'
					+	'<td>' + res.data[i].expire_date + '</td>'
					+	'<td>' + res.data[i].package_qty + '</td>'
					+	'<td>' + str + '</td>'
					+'</tr>');
				}
			}
		},
		error: function(e){
			console.log(e.status);
			console.log(e.responseText);
		}
	})
}

$(document).on('click', '#btn-delete', function() {
	var obj = $(this);
	index = parent.layer.confirm('Are you sure delete？', {
		btn: ['Sure','No']
	}, function() {
		parent.layer.close(index);
		$.ajax({
			url: '/label/utils/delete_printed_records.asp',
			type: "POST",
			contentType: "application/x-www-form-urlencoded",
			dataType: "json",
			data: {
				baseAuth: baseAuth,
				po_no: "<%=request.querystring("po_no")%>",
				line_no: "<%=request.querystring("line_no")%>",
				create_time: obj.parent().parent().find('td:eq(3)').html(),
				package_qty: obj.parent().parent().find('td:eq(5)').html()
			},
			success: function(res) {
				parent.layer.msg(res.msg);
				if(res.success) {
					history.go(0);
				}
			},
			error: function(e){
				console.log(e.status);
				console.log(e.responseText);
			}
		});
	});
}).on('click', '#btn-download', function() {
	// 获取此刻的标签记录
	$.ajax({
		url: '/label/utils/get_printed_records.asp',
		type: "POST",
		contentType: "application/x-www-form-urlencoded",
		dataType: "json",
		data: {
			po_no: "<%=request.querystring("po_no")%>",
			line_no: "<%=request.querystring("line_no")%>",
			create_time: $(this).parent().parent().find('td:eq(3)').html()
		},
		success: function(res) {
			if(res.success) {
				download(rows, res.data);
			} else {
				parent.layer.msg(res.msg);
			}
		},
		error: function(e){
			console.log(e.status);
			console.log(e.responseText);
		}
	});
}).on('click', '#btn-print', function() {
	// 获取此刻的标签记录
	$.ajax({
		url: '/label/utils/get_printed_records.asp',
		type: "POST",
		contentType: "application/x-www-form-urlencoded",
		dataType: "json",
		data: {
			po_no: "<%=request.querystring("po_no")%>",
			line_no: "<%=request.querystring("line_no")%>",
			create_time: $(this).parent().parent().find('td:eq(3)').html()
		},
		success: function(res) {
			if(res.success) {
				print(rows, res.data);
			} else {
				parent.layer.msg(res.msg);
			}
		},
		error: function(e){
			console.log(e.status);
			console.log(e.responseText);
		}
	});
});

</script>
<script src="/label/common/utils/print.js"></script>
</html>